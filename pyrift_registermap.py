# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_registermap.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

# import support for word, excel file generation
# this a non default module in typical python install
# So handle error case if missing, just warn but continue
# (If user don't generate word file, no reason to stop on error)
# if import sucessed, then special feature will be included in pyrift
try:
    from docx import Document
    from docx.shared import Cm # unused import here, just to check for error in register import
    docx_import_error = "" # False if python-docx is installed and available
except ImportError:
    docx_import_error = \
        "Failed importing docx support to generate word file\n"\
        "word docx generation is not available"
try:
    from xlsxwriter import Workbook
    xlsxwriter_import_error = "" # False if xlsxwriter is installed and available
    import xlsxwriter
except ImportError:
    xlsxwriter_import_error = \
        "Failed importing xlsxwriter support to generate excl file\n"\
        "xlsxwriter generation is not available"

from pyrift_field import PyriftBase, Field, Access, Cdc, FieldEncode
from pyrift_register import Register
from pyrift_maptableentry import MapTableEntry, MapTableType
from pyrift_signal import Signal
from pyrift_param import Param
from pyrift_host import Pyrift_Host
# from pyrift_field import Pad_Field

import sys # for sys.hexversion
import platform # to report python version platform.sys.version
import re
import os
import math
from operator import attrgetter
import itertools
# library to support xml generation
import xml.etree.ElementTree as ET
from inspect import cleandoc

# ----------------------------------------------------------------------------


class RegisterMap(PyriftBase):
    """
        RegisterMap is collection of Register with notion of address location
        Each register is stored in reg_list in increasing address order
        base_address is the start address of collection or register (for registermap consider standalone)
    """

    def __init__(self,
        name,
        interface_name = "IF_RIF",
        module_name = "rif",
        desc = "",
        base_address = 0,
        align=0,
        regmap_length_align=0,
        pre_pad=0,
        post_pad=0,
        array=None):

        super(RegisterMap, self).__init__()

        self.name = name
        self.array = array
        self.desc = desc

        # Main goal of register is to collect list of field
        self.reg_list = []
        # module generated may use extra signal (like user reset signal, ...)
        # extra_signal_list collect all these extra signals
        self.extra_signal_list = []
        self._validated_signal_list = [] # extra signal list after validation
                                         # may be corrected around default reset signal
        # module generated may use constant parameter that propagate to verilog package
        # param_list collect all these defined parameter
        self.param_list = []

        # module generated may use enum definition that propagate to cheader/verilog package
        # enum_list collect all these defined enum (usefull only if not used as encode for any field)
        self.enum_list = []

        # list of enum used in any register or extended by enum_list (set during validate())
        self.encode_list = []

        # define default reset signal for field that do not overload
        # It is significant only if registermap is used as root level
        # (not included in other registermap)
        # On validate_check_premap, it is added to _validated_signal_list if no other default signal is defined
        # and at least one field use reset
        self.default_reset_signal = Signal("rst_n", activelow=True, sync=False)

        # define default hwce : clock enable for hw interfacing
        # To be used to slow down the host RIF interfacing (ready pause on host bus)
        # to allow hw to detect short pulse like swmod, wr pulse for external register, ...
        # Each field will check after validate_check_premap if it is defined
        # (can be overloaded directly inside Field definition)
        # if defined, must be active high and synchronous
        self.default_hwce_signal = None
        # ... = Signal("hwce", activelow=False, sync=True)

        # define extra signal to freeze host interface
        # To be used to prevent host bus to complete another cycle (Not ready pause on host bus)
        # On APB this signal active will force PREADY to 0
        self.freeze_hostbus_signal = None

        # base address is initially 0, and unlikely to change
        # this is the base for local offset computation
        # Top level rif can use it to define the base address of rif decoding
        self.base_address = base_address

        # offset_to_parent only if RegisterMap have a parent,
        # then the base_address should be parent.base_address+self.offset_to_parent
        self.offset_to_parent = 0

        # Select the host interface : default to APB
        self.rif_host_bus = "APB"
        self.pyrift_host = None # class handling code generation encapsulation for each bus, set on validate

        # In case of APB, addr[31:16] is decoded to PSEL
        # addr[15:0] is decoded inside rif decoder
        self.rif_host_bus_msb = 15
        # rif_addr_msb : set to have enough bit to address decode every register
        # usually set as ceil(log2( size of regmap))-1
        # for 4 register 32bit : 16 byte address size : [3:0] are significant addressing bit,
        #                        log2(4) ==> 4, rif_addr_msb ==> 3
        self.rif_addr_msb = 0
        # dw > 8 may leave unsignificant lsb in address decoding
        # Rif_addr LSB used is dw dependant : 32b=>2, 16b=>1, 8b=>0
        self.rif_addr_lsb = 0

        # map attribute : pad and align
        # Alignement is done before padding
        # - alignment padding
        # - pre_pad
        # - actual registermap
        # - post_pad
        self.pre_pad = pre_pad
        self.post_pad = post_pad
        if align==0:
            # use 0 as default parameter value for align
            # adjust to DW that may have been set differently between def of function and now
            # (DW as default param may have taken old DW before user change it)
            self.align = PyriftBase.dw//8 # default for current dw bits architecture
        else:
            self.align = align

        # length align put constraint on regmap length (apply both for array of regmap, or single regmap)
        # larger value can infer REGMAP_LENGTH_ALIGN_PAD at the end of regmap item
        # final regmap length is padded to match a multiple of array_length_align

        if align==0:
            # use 0 as default parameter value for align
            # adjust to DW that may have been set differently between def of function and now
            # (DW as default param may have taken old DW before user change it)
            self.regmap_length_align = PyriftBase.dw//8 # default for 8,16,32 bits architecture
        else:
            self.regmap_length_align = regmap_length_align

        self.interface_name = interface_name
        self.module_name = module_name
        self.uvm_regmodel_classname = None # default, otherwise overload the uvm regmodel class name

        # parent is used when register is included in a parent container
        # allow check for single parent inclusion
        self.parent = None

        self.pyrift_dirname = os.path.dirname(os.path.abspath(__file__))
        # print(f"pyrift running from {self.pyrift_dirname}")
        self.user_notice = self.pyrift_dirname+"/lib/default_user_notice.txt"

        self.add_default_nettype_wire_at_eof = False

        # Following member are created on validate in root regmap
        # self.reg_with_aggregate_entry_set : set of register entry in maptable that involve aggregate fields
        # self.aggregate_name_set : set of all name of aggregate field


    def add(self, register, align=0, pad=0):
        """
            Add either Register or registerMap
            Assume registerMap added have already all register
        """
        if (not isinstance(register, Register)):
            if (not isinstance(register, RegisterMap)):
                raise RuntimeError("Unexpected register added : not a Register or RegisterMap")

        if(register.parent is not None):
            # register is already owned by another parent
            # register sharing is not supported
            raise RuntimeError("Adding register which already have a parent : Sharing is not supported")
        # print("adding reg %s to %s, parent = " % (register.name, self.name), register.parent)

        register.parent = self
        # pad and align are recorded inside register
        if(pad != 0):
            register.pre_pad = pad
            # post-pad is to be handled directly when register is created
            # print ("Adding pad to register as pre-padding")
        if(align != 0):
            register.align = align
        self.reg_list.append(register)


    def add_signal(self, signal):
        """
            Add signal and keep track in extra_signal_list
            Assume registerMap added have already all register
        """
        if (not isinstance(signal, Signal)):
            raise RuntimeError("Unexpected signal added : not a Signal instance")
        self.extra_signal_list.append(signal)

    def add_param(self, param):
        """
            Add param and keep track in param_list
        """
        if (not isinstance(param, Param)):
            raise RuntimeError("Unexpected param added : not a Param instance")
        self.param_list.append(param)

    def add_enum(self, added_enum):
        """
            Add added_enum and keep track in enum_list (candidate for output in cheader and verilog package)
        """
        if (not issubclass(added_enum, FieldEncode)):
            print (added_enum)
            raise RuntimeError("Unexpected enum added : not a FieldEncode subclass")
        self.enum_list.append(added_enum)

    def get_signal_by_name(self, signal_name):
        for signal in self._validated_signal_list:
            if signal.name == signal_name:
                return signal
        msg = (f"Unable to locate signal '{signal_name}' into regmap {self.name}",
               f"Know list is {[signal.name for signal in self._validated_signal_list]}")
        raise  RuntimeError(msg)

    def get_all_register_or_regmap(self):
        """
            get register list (can be a true register or a regmap as well)
        """
        return [r for r in self.reg_list if r.ispresent]

    def get_all_register_in_hierarchy(self):
        """
            get register list (only true register, regmap are expanded)
        """
        register_list = []
        for register in self.get_all_register_or_regmap():
            if (isinstance(register, Register)):
                register_list.append(register)
            elif (isinstance(register, RegisterMap)):
                # sub register map, add the registerMap hierarchy
                register_list.extend(register.get_all_register_in_hierarchy())
            else:
                raise RuntimeError("Unexpected register class : not a Register or RegisterMap")

        return register_list

    def get_min_align_from_below(self, top):
        """
            get from hierarchy below current regmap what is the minimum alignment required
            This is used for array of regmap, to adjust the REGMAP_LENGTH_ALIGN_PAD
            If not an array, the align from hierarchy below is ignored (can align regmap as needed)
            Should be Least common multiple instead of min (OK if handling only power of 2)

            if top=False, we are already down the hierarchy,
            report any alignement constraint whatever the array at current level

            self.regmap_length_align can be used to force re-align the regmap length
        """
        min_align = PyriftBase.dw//8
        # print(f"==>{self.name}.default min_align={min_align} regmap_length_align={self.regmap_length_align}")
        if self.array or not top:
            for register in self.get_all_register_or_regmap():
                if (isinstance(register, Register)):
                    if register.align > min_align:
                        min_align = register.align
                elif (isinstance(register, RegisterMap)):
                    # sub register map, add the contribution from sub-regmap itself
                    if register.align > min_align:
                        min_align = register.align
                    # or from the hierarchy below this regmap
                    regmap_min_align = register.get_min_align_from_below(top=False)
                    if regmap_min_align > min_align:
                        min_align = regmap_min_align
                else:
                    raise RuntimeError("Unexpected register class : not a Register or RegisterMap")

            # considered warning user about alignment propagation, but may be too verbose ...
            # if min_align > self.regmap_length_align:
            #     # align spec as been adjusted from below hierarchy
            #     # if regmap_length_align do not cover that, then issue warning to user
            #     print(f"Warning: Align constraint in regmap:{self.name} infer regmap length padding to {min_align}")
            #     print(f"         You can manually set {self.name}.regmap_length_align = 0x{min_align:02x} to suppress this warning")

        # else: not array and top => don't care about alignement of below tree
        #       (single instance so no risk of inconsistent array part alignment)

        if self.regmap_length_align>min_align:
            min_align = self.regmap_length_align

        # print(f"         ==>{self.name}.default min_align={min_align}")
        return min_align

    def get_root_parent(self):
        parent = self
        while parent.parent:
            # progress through parent tree to root parent (parent is None)
            parent = parent.parent
        return parent

    def validate(self):
        """
            validate is 2 step:
              - step 1: validate_check_premap : hierarchy check processing in nested regmap and register
              - step 2: generate map table at root level through hierarchy
              - step 3: validate_field_collision
              - step 4: generate instance database
              - step 5: validate_check_postmap : register and field option checking
                        (alias can be resolved at this point)
        """
        #print(f"Validating regmap '{self.name}' now ...")

        # call Pyrift_Host class method, hopefully hiding subclass constructor
        # to get proper Pyrift_Host subclass to handle bus code generation
        self.pyrift_host = Pyrift_Host.get_pyrift_host(self.rif_host_bus)

        # user get a warning for using experimental host bus support
        self.pyrift_host.check_rif_host_bus()

        self.validate_check_premap()

        # create a root entry sentinel in map table
        regmap_root_entry = MapTableEntry()
        regmap_root_entry.type = MapTableType.ROOT
        regmap_root_entry.parent = regmap_root_entry # this is the root entry (self referenced)
        regmap_root_entry.register = self # this is the root entry
        regmap_root_entry.address = self.base_address
        regmap_root_entry.layer_range = ()
        regmap_root_entry.layer_index = ()

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # construct the map table, starting at root reg_map base address
        self.map_table, current_address = self.get_map_table(
                parent=regmap_root_entry,
                current_address=self.base_address)
        self.reg_map_size = current_address - self.base_address
        #print(self.map_table)
        #print(f"Regmap size = {self.reg_map_size}")

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        regmap_root_entry.size = self.reg_map_size
        self.map_table.insert(0, regmap_root_entry)

        # update the addressing size required to fit the rif
        nb_address_bit = math.ceil(math.log2(self.reg_map_size))
        self.rif_addr_msb = nb_address_bit - 1 # N bit is range [N-1:0]
        # dw > 8 may leave unsignificant lsb in address decoding
        # Rif_addr LSB used is dw dependant : 32b=>2, 16b=>1, 8b=>0
        self.rif_addr_lsb = self.dw.bit_length() - 4

        # Flag completion of interface generation
        # (not yet done, expected after to_verilog_interface() called)
        self.interface_generation_complete_flag = False

        if self.rif_host_bus_msb < self.rif_addr_msb:
            raise  RuntimeError(f"'rif_host_bus_msb' not large enough to support current amount of register\n"
                                f"at least [{self.rif_addr_msb}:0] required, where [{self.rif_host_bus_msb}:0] provided !\n"
                                f"Please increase {self.rif_host_bus_msb}\n")

        # print("RegMap addr range set to %d\n" % self.rif_addr_msb)
        #print(f"Validating regmap '{self.name}' Done")

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        # Check for field name collision on interface
        self.validate_field_collision()

        # generate instance database
        self.validate_gen_idb()

        # now map table is available, run post map validation
        # at field level, need mpatable to check field aliases
        self.validate_check_postmap()

        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    def validate_check_premap(self):
        """
            Register expected to be fully defined now
            Run validation check before allowing any translation (html doc, rtl verilog)
            Add pad fields around actual useful fields
        """

        # print(f"Checking regmap '{self.name}' now ...")

        # normalize array : integer are converted to tuple
        # 3 =>(3,)
        if isinstance(self.array, int):
            self.array = (self.array,)

        # finetune the extra_signal_list around reset definition
        # This is required to be done at regmap level before
        # the validate_check_premap can be call on register (check signal availability for reset or ...)
        self._validated_signal_list = []
        self._validated_signal_list.extend(self.extra_signal_list)
        # search for a user defined default reset
        list_user_default_reset = [s for s in self._validated_signal_list if s.field_reset]
        if len(list_user_default_reset)>1:
            print("Offending multiple User_default_reset are ",[s.name for s in list_user_default_reset])
            raise RuntimeError("Only single user default reset signal supported")
        elif  len(list_user_default_reset)==1:
            # exactly one reset defined from user extra signal
            # This overload the defaut reset signal (likely to replace rst_n default)
            self.default_reset_signal = list_user_default_reset[0]
        else:
            # no user defined reset, get sure to have the reset signal in signal list
            # for port creation
            # but only if the default is not set as None (no reset of flop by default)
            if self.default_reset_signal:
                self._validated_signal_list.append(self.default_reset_signal)

        # collect all top level register name (for duplicate checks)
        reg_dict_by_name = {}

        # Now can validate_check_premap sub ressource (register, registermap)
        for reg in self.get_all_register_or_regmap():
            # sub ressource may need validate and mapping propagation (if registerMap)
            reg.validate_check_premap()

            # check for duplicate case
            if reg.name in reg_dict_by_name:
                raise RuntimeError(f"Detected duplicate field:'{reg.name}' in regmap:{self.name}")

            # No duplicate found, store reg in dict to allow further duplicate detection
            reg_dict_by_name[reg.name] = reg

        # check on all true register
        for reg in self.get_all_register_in_hierarchy():
            # check register have at least one field
            if len(reg.get_all_actual_field()) == 0:
                raise  RuntimeError(f"Register {reg.name} seems to have 0 field !?\nValid register needs at least one field.")

        # update list of enum used (directly set self.encode_list)
        self.encode_list = self.get_encode_list()

        # check for deprecated apb msb definition
        if hasattr(self, 'rif_apb_bus_psel_msb'):
            raise  RuntimeError("rif_apb_bus_psel_msb attribute replace by rif_host_bus_msb,\n"
                                "Please update your script accordingly. Sorry for inconvenience ...\n"
                                "Host interface planned to be supported to other bus than APB")
        # print(f"Checking regmap '{self.name}' Done")

    def validate_check_postmap(self):
        """
            Register expected to be fully defined now
            Maptable is now generated, so field can find
            its alias to evaluate acces mode, storage requirement, ...
            Run final validation check of options before allowing any translation (html doc, rtl verilog)
        """
        # Now can validate_check register ressource (register)
        # (regmap have already be expanded in individual register in maptable)
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                entry.register.validate_check_postmap(entry)

    def validate_field_collision(self):
        """
            Each interface is a collection of field coming from several register
            interface target can be selected with interface_name attribute
            So at the end, several field with same name can be collected in same interface
            (this is not possible in verilog interface)
            This method is called on validate to detect such issue
            If collision is found, it may be fixed automatically with warning
            (using regmap hierarchy as field prefix to remove name collision)
            (it is recommended to use if_name option to explicitly setup with no collision)
        """
        #print("validate_field_collision")
        for if_name in self.get_verilog_interface_list():
            #print(f"Found if:{if_name}")
            field_dict = {}
            # Find all register that have field as member of if_name (register only on other IF are ignored)
            for entry in self.get_reg_entry_list_from_if_name(if_name):
                #print(entry.type, entry.register.name)
                if entry.is_first_of_array(): # other entry of array map to same if as array
                    for f in entry.register.get_all_actual_field():
                        if f.get_interface_name() == if_name:
                            # print(f"    Field={f.name} in reg={entry.register.name}")
                            field_info = {
                                'f_name' : f.name,
                                'f_name_in_if' : f.get_name_in_if(),
                                'reg_name' : entry.register.name,
                                'field' : f,
                                'reg_entry' : entry,
                            }
                            field_dict[f.get_name_in_if()] = field_dict.get(f.get_name_in_if(), []) + [field_info]
            # print (field_dict)
            for k_field, field_tab in field_dict.items():
                if len(field_tab)>1:
                    print(f"Duplicate field detected in if=:{if_name}")
                    print("##########################")
                    for f in field_tab:
                        print(f["f_name_in_if"], f["reg_name"])
                    print("##########################")
                    raise RuntimeError(f"Duplicate field name collision in interface {if_name}, field={f['f_name']}")

    def validate_gen_idb(self):
        """
            generate instance data base
            Pre-compute information once, such that can be re-used while processing regmap
            Create instance database, each reg entry in maptable have field_idb
            field_idb["fieldname"] will contain dict information about field instance
            (one field can have multiple instance if instantiated in register array)
            This includes info about aggregate field
            Check and populate alias links :
            field_idb['field_name']["alias_src"] = [{"entry":entry, "field":f}]
        """
        # print("validate_gen_idb")

        # create empty idb entry for each field in each register maptable entry
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                entry.field_idb = {} # empty dict, to be index by field name
                for f in entry.register.get_all_actual_field():
                    entry.field_idb[f.name] = {} # empty dict, to be index with "aggregate_info", ...
                    entry.field_idb[f.name]["alias_src"] = [] # empty list as no alias yet

        # check and populate alias links
        # search all field that have alias links, resolve link to actual entry map (store entry in idb)
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                for f in entry.register.get_all_actual_field():
                    if f.alias:
                        # print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                        # print(f"Detected alias field: source reg={entry.register.name}:{f.name}, alias path={f.alias}")
                        target_entry, target_field_name = entry.resolve_field_path(f.alias)
                        # print(f"Target alias found: reg={target_entry.register.name}, target_field_name={target_field_name}")
                        target_entry.field_idb[target_field_name]["alias_src"].append({"entry":entry, "field":f})

        # find all registers with aggregate field to be processed
        self.reg_with_aggregate_entry_set = set([]) # collect only register entry involved in any aggregation
        self.aggregate_name_set = set([]) # collect all existing aggregate name
        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                for f in entry.register.get_all_actual_field():
                    if f.is_agregate_field():
                        self.reg_with_aggregate_entry_set.add(entry)
                        aggregate_info = f.get_aggregate_info()
                        # print(aggregate_info)
                        self.aggregate_name_set.add(aggregate_info[0])

        # print("reg_with_aggregate_entry_set",[entry.register.name for entry in self.reg_with_aggregate_entry_set])
        # print("aggregate_name_set",self.aggregate_name_set)

        # Collect information on all field that are part of aggregate
        # /!\ could get one aggegate per couple : (aggregate,if_name)
        # store result : aggregate_info in a dictionary : self.aggregate_info_dict
        #                dictionnary key is f"{aggregate_name}.{if_name}"
        self.aggregate_info_dict = {}
        for aggregate_name in self.aggregate_name_set:
            for if_name in self.get_verilog_interface_list():
                aggregate_info_list = []
                # search in pre-detected register containing aggregation
                for entry in self.reg_with_aggregate_entry_set:
                    # entry is already check as first of array
                    # now need to search and filter field as aggregate and match current interface
                    for f in entry.register.get_all_actual_field():
                        # drop field if not interesting
                        if not f.is_agregate_field(with_name=aggregate_name) or f.get_interface_name() != if_name:
                            continue
                        # print("Found field:", f.name, "in reg:", entry.register.name, "@#:", f.get_aggregate_info()[1])
                        aggregate_info_list += [{
                            'aggregate_name' : aggregate_name,
                            'field' : f,
                            'bit_width' : f.bit_width,
                            'reg_entry' : entry,
                            'reg_name' : entry.register.name,
                            'lin_array_index' : entry.get_regmap_linear_index(include_leaf_reg=True),
                            'ag_index' : f.get_aggregate_info()[1],
                        }]
                # aggregate_info_list is sorted by index, then by linear index for reg in array
                aggregate_info_list.sort(key=lambda x: (x['ag_index'], x['reg_entry'].get_regmap_linear_index(include_leaf_reg=True)))
                #print (f"ag-info:{aggregate_name}.{if_name}:", aggregate_info_list)
                self.aggregate_info_dict[f"{aggregate_name}.{if_name}"] = aggregate_info_list
                # This gives the list of field that are constituant of aggregate on the interface
                # /!\ this can be empty if aggregate is on another interface

        # check consistency of aggregates, on all options in all constituant fields
        # - encode : not supported yet (so should be False in all fields)
        # - if_reshape : only present in first aggregate field (or present and equal in all other)
        # - hw, sw: equal in all fields
        # - hwclr, hwset: equal in all fields
        # - has_storage(...): equal in all fields: note has_storage needs to explore aliased fields
        # - swmod, delayed_swmod: equal in all fields
        # - swacc, early_swacc: equal in all fields
        # - swwe, swwel: equal in all fields
        # - rclr, rset: equal in all fields
        # print("Keys:", self.aggregate_info_dict.keys())
        for aggregate_key,aggregate_info_list in self.aggregate_info_dict.items():
            #print(f"Agg-check: Processing list for {aggregate_key}")
            # Each iteration of aggregate_info_list contain the list of field for a couple (f"{aggregate_name}.{if_name}")
            # /!\ this can be empty if aggregate is on another interface
            if len(aggregate_info_list)==0:
                # print(f"Agg-check: Empty list for {aggregate_key}")
                continue
            aggregate_name = aggregate_info_list[0]['aggregate_name']
            if any([info['field'].encode for info in aggregate_info_list]):
                raise RuntimeError(f"Enum encode is not supported for Field aggregation : @{aggregate_name}")
            if aggregate_info_list[0]['field'].if_reshape == None:
                # No if_reshape on first item, must be None everywhere
                if any([info['field'].if_reshape != None for info in aggregate_info_list]):
                    raise RuntimeError(f"If_reshape inconsistent (None, on first item, must be None "\
                                       f"on all other constituants of Field aggregation : @{aggregate_name}")
            else:
                if any( [ (info['field'].if_reshape != None) and (info['field'].if_reshape != aggregate_info_list[0]['field'].if_reshape)\
                          for info in aggregate_info_list[1:]]):
                    raise RuntimeError(f"if_reshape option must all be identical to first or None for second and all other "\
                                       f"constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].hw != aggregate_info_list[0]['field'].hw  for info in aggregate_info_list]):
                raise RuntimeError(f"hw option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].sw != aggregate_info_list[0]['field'].sw  for info in aggregate_info_list]):
                raise RuntimeError(f"sw option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].hwclr != aggregate_info_list[0]['field'].hwclr  for info in aggregate_info_list]):
                raise RuntimeError(f"hwclr option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].hwset != aggregate_info_list[0]['field'].hwset  for info in aggregate_info_list]):
                raise RuntimeError(f"hwset option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].swwr2hwif != aggregate_info_list[0]['field'].swwr2hwif  for info in aggregate_info_list]):
                raise RuntimeError(f"swwr2hwif option must be identical for all constituants of Field aggregation : @{aggregate_name}")

            # Now map table is just build:
            # Get sure to call has_storage to check aliased version is matching in aggregation
            if any([ info['field'].has_storage(aliased=True, map_table_entry=info['reg_entry']) != \
                     aggregate_info_list[0]['field'].has_storage(aliased=True, map_table_entry=aggregate_info_list[0]['reg_entry']) \
                     for info in aggregate_info_list]):
                raise RuntimeError(f"has_storage() option must be identical for all constituants of Field aggregation : @{aggregate_name}")

            if any([info['field'].swmod != aggregate_info_list[0]['field'].swmod  for info in aggregate_info_list]):
                raise RuntimeError(f"swmod option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].delayed_swmod != aggregate_info_list[0]['field'].delayed_swmod  for info in aggregate_info_list]):
                raise RuntimeError(f"delayed_swmod option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].swacc != aggregate_info_list[0]['field'].swacc  for info in aggregate_info_list]):
                raise RuntimeError(f"swacc option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].early_swacc != aggregate_info_list[0]['field'].early_swacc  for info in aggregate_info_list]):
                raise RuntimeError(f"early_swacc option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].swwe != aggregate_info_list[0]['field'].swwe  for info in aggregate_info_list]):
                raise RuntimeError(f"swwe option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].swwel != aggregate_info_list[0]['field'].swwel  for info in aggregate_info_list]):
                raise RuntimeError(f"swwel option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].rclr != aggregate_info_list[0]['field'].rclr  for info in aggregate_info_list]):
                raise RuntimeError(f"rclr option must be identical for all constituants of Field aggregation : @{aggregate_name}")
            if any([info['field'].rset != aggregate_info_list[0]['field'].rset  for info in aggregate_info_list]):
                raise RuntimeError(f"rset option must be identical for all constituants of Field aggregation : @{aggregate_name}")

        # now remap direct pointer to aggregate info to have quick access in each field
        # store in reg matable entry in field instance database : entry.field_idb["field_name"]
        for entry in self.reg_with_aggregate_entry_set:
            for f in entry.register.get_all_actual_field():
                if f.is_agregate_field():
                    aggregate_name = f.get_aggregate_info()[0]
                    if_name = f.get_interface_name()
                    entry.field_idb[f.name]["aggregate_info"] = self.aggregate_info_dict[f"{aggregate_name}.{if_name}"]


    def get_map_table(self, parent, current_address):
        """
            Build map_table list
            This collects all item in increasing address order (keeping herarchy information)
            item entry can be of different flavor:
             - register (one entry for each array index)
             - reg_map (and matching end entry) (pre-pad and post-pad are included)
             - reg_map_array (and matching end entry)
             - sub reg_map (or lower level)
            Each entry collect the info:
             - adress
             - layer_range : tuple with size for each dimension of current regmap or reg
               (2,3) for reg_map.array=(2,3), or register.array=(2,3)
             - layer_index : index in layer_range
               (0,0),(0,1),(0,2),(1,0),(1,1),(1,2) to cover of entry of (2,3) for layer_range
            parameter is current_address:
            return - the table map extension (as list) and
                   - the next address (current address + size of entry table)
        """
        map_table = []

        # ALIGN (processed before the REG_MAP structure, either array or non array)
        regmap_align_entry = None # possibly not needed
        if(self.align != 0):
            #check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.align % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"registerMap is aligned with non multiple of byte width:{PyriftBase.dw//8} (align={self.align})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            # get sure the current address is aligned
            if(current_address % self.align != 0):
                # mis-aligned, first re-align before current_address
                aligned_address = current_address - current_address % self.align
                # then add one align step to re-align properly
                aligned_address += self.align
                # re-align from current_address to aligned_address
                regmap_align_entry = MapTableEntry()
                regmap_align_entry.type = MapTableType.ALIGN_PAD
                regmap_align_entry.parent = parent
                regmap_align_entry.related_entry = None # TBD after end of regmap creation
                regmap_align_entry.address = current_address
                regmap_align_entry.register = self # relative regmap for pad (not actually a register)
                regmap_align_entry.size = aligned_address - current_address
                regmap_align_entry.layer = parent.layer+1
                regmap_align_entry.layer_range = () # no array on align or padding
                regmap_align_entry.layer_index = () # no array on align or padding
                map_table.append(regmap_align_entry)
                current_address += regmap_align_entry.size

        # Pre-PAD (processed before the REG_MAP structure, either array or non array)
        regmap_pre_pad_entry = None # possibly not needed
        if(self.pre_pad != 0):
            # check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.pre_pad % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"registermap with pad non multiple byte width:{PyriftBase.dw//8} (pad={self.pre_pad})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            regmap_pre_pad_entry = MapTableEntry()
            regmap_pre_pad_entry.type = MapTableType.PRE_PAD
            regmap_pre_pad_entry.parent = parent
            regmap_pre_pad_entry.related_entry = None # TBD after end of regmap creation
            regmap_pre_pad_entry.address = current_address
            regmap_pre_pad_entry.register = self # relative regmap for pad (not actually a register)
            regmap_pre_pad_entry.size = self.pre_pad
            regmap_pre_pad_entry.layer = parent.layer+1
            regmap_pre_pad_entry.layer_range = () # no array on align or padding
            regmap_pre_pad_entry.layer_index = () # no array on align or padding
            map_table.append(regmap_pre_pad_entry)
            # print(f"Adding pad regmap_pre_pad_entry @={current_address} size={regmap_pre_pad_entry.size}")
            current_address += regmap_pre_pad_entry.size


        # REGISTER MAP entry can be single entry or array ...
        if self.array:
            regmap_array_entry = MapTableEntry()
            regmap_array_entry.type = MapTableType.REG_MAP_ARRAY
            regmap_array_entry.register = self
            regmap_array_entry.parent = parent
            parent.child_list.update( {f"{self.name}[]" : regmap_array_entry} )
            regmap_array_entry.address = current_address
            # regmap_array_entry.size = TBD after end of array map
            regmap_array_entry.layer = parent.layer+1
            # REG_MAP_ARRAY entry have the index and range for current layer of array
            regmap_array_entry.layer_range = self.array
            regmap_array_entry.layer_index = (0,)*len(self.array)

            # if array is specified as integer, it is converted to a one dimension tuple in validate()
            if not type(self.array) is tuple:
                raise RuntimeError(f"array {self.array} expected to be of type tuple (not {type(self.array)}) (or converted from int for user)")
            if any([s < 2 for s in self.array]):
                raise RuntimeError(f"any array dimension is required larger than 2 : not OK for {self.array} of {self.name}")

            map_table.append(regmap_array_entry)

            array_range_list = [range(i) for i in self.array]
            layer_range = self.array

            for array_index in itertools.product(*array_range_list):
                # itertools.product provide an iterable
                # print(f"REGISTER-MAP array_index={array_index} of {self.name}")
                table_extension, current_address = self.get_map_table_single_array_item(
                    parent=parent, # as requested by caller of get_map_table
                    current_address=current_address,
                    layer_range=tuple(layer_range),
                    layer_index=tuple(array_index))
                map_table.extend(table_extension)

            # fix regmap_array_entry size (already inside map_table)
            regmap_array_entry.size = current_address - regmap_array_entry.address
            regmap_array_end_entry = MapTableEntry()
            regmap_array_end_entry.type = MapTableType.REG_MAP_ARRAY_END
            regmap_array_end_entry.parent = parent
            regmap_array_end_entry.register = self
            regmap_array_end_entry.address = current_address
            regmap_array_end_entry.size = regmap_array_entry.size
            regmap_array_end_entry.layer = parent.layer+1
            regmap_array_end_entry.layer_range = regmap_array_entry.layer_range
            regmap_array_end_entry.layer_index = regmap_array_entry.layer_index
            map_table.append(regmap_array_end_entry)
            just_created_regmap_entry = regmap_array_entry #either array or not
        #
        else:
            # no array on this regmap
            table_extension, current_address = self.get_map_table_single_array_item(
                parent=parent, # as requested by caller of get_map_table
                current_address=current_address,
                # no array, so layer array information is empty
                layer_range=(),
                layer_index=())
            map_table.extend(table_extension)
            just_created_regmap_entry = table_extension[0] # either array or not
                                                      # regmap is always the first entry of table extension

        # Post-PAD (processed after the REG_MAP structure, either array or non array)
        if(self.post_pad != 0):
            #check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.post_pad % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"registermap with pad non multiple of byte width:{PyriftBase.dw//8} (pad={self.post_pad})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            entry = MapTableEntry()
            entry.type = MapTableType.POST_PAD
            entry.parent = parent
            entry.related_entry = just_created_regmap_entry
            entry.address = current_address
            entry.register = self # relative regmap for pad (not actually a register)
            entry.size = self.post_pad
            entry.layer = parent.layer+1
            entry.layer_range = () # no array on align or padding
            entry.layer_index = () # no array on align or padding
            map_table.append(entry)
            current_address += entry.size


        # fill TBD field in created entry, now the regmap entry is completed
        if regmap_align_entry:
            regmap_align_entry.related_entry = just_created_regmap_entry # fill align TBD field with related
        if regmap_pre_pad_entry:
            regmap_pre_pad_entry.related_entry = just_created_regmap_entry # fill align TBD field with related

        return map_table, current_address

    def get_map_table_single_array_item(self, parent, current_address, layer_range, layer_index):
        map_table = []

        # main regmap_entry is created early, so it can be pointed as related_entry
        regmap_entry = MapTableEntry()

        # main regmap_entry is created earlier, now initializing class member
        regmap_entry.type = MapTableType.REG_MAP
        regmap_entry.parent = parent
        if(layer_range == ()):
            parent.child_list.update( {f"{self.name}" : regmap_entry} )
        else:
            # print("Li:", layer_index, str(layer_index), ','.join([str(i) for i in layer_index]))
            # [str(i) for i in layer_index]
            parent.child_list.update( {f"{self.name}[{','.join([str(i) for i in layer_index])}]" : regmap_entry} )
        regmap_entry.address = current_address
        regmap_entry.register = self # regmap (not actually a register)
        regmap_entry.size = 0 # to be fill when regmap table is completed
        regmap_entry.layer = parent.layer+1
        regmap_entry.layer_range = layer_range
        regmap_entry.layer_index = layer_index
        map_table.append(regmap_entry)


        for register in self.get_all_register_or_regmap():
            # print(f"Adding ressource to map_table {register.name}")
            if not (isinstance(register, Register) or isinstance(register, RegisterMap)):
                raise RuntimeError("Unexpected register class : not a Register or RegisterMap")
            # One register/registermap can create one or more (if array) entry for map_table
            # print(f"Adding register/sub regmap to map_table {register.name}")
            table_extension, current_address = register.get_map_table(
                    parent=regmap_entry, # self is parent for sub map_table
                    current_address=current_address)
            map_table.extend(table_extension)
            # print(f"Sub reg map add reach @={current_address}")

        length_align_pad = self.get_min_align_from_below(top=True)
        # print(f"{self.name}: Min align from below {length_align_pad}")
        # check for align constraint impact on regmap size
        # check the current regmap size versus the align constraint
        if (current_address - regmap_entry.address) % length_align_pad != 0:
            # size not matching the align constraint, add padding to fix
            entry = MapTableEntry()
            entry.type = MapTableType.REGMAP_LENGTH_ALIGN_PAD
            entry.parent = parent
            entry.related_entry = regmap_entry
            entry.address = current_address
            entry.register = self # relative regmap for pad (not actually a register)
            entry.size = length_align_pad - ((current_address - regmap_entry.address) % length_align_pad)
            entry.layer = parent.layer+1
            entry.layer_range = layer_range
            entry.layer_index = layer_index
            map_table.append(entry)
            current_address += entry.size

        # fix reg map entry size (already inside map_table)
        regmap_entry.size = current_address - regmap_entry.address

        # Add END entry to match REG_MAP entry as a marker to help map_table processing
        regmap_end_entry = MapTableEntry()
        regmap_end_entry.type = MapTableType.REG_MAP_END
        regmap_end_entry.parent = parent
        regmap_end_entry.address = current_address
        regmap_end_entry.register = self # regmap (not actually a register)
        regmap_end_entry.size = regmap_entry.size
        regmap_end_entry.layer = parent.layer+1
        regmap_end_entry.layer_range = layer_range
        regmap_end_entry.layer_index = layer_index
        map_table.append(regmap_end_entry)

        return map_table, current_address

    def get_encode_list(self):
        """
            Build the list of enum encode used in register field
        """
        encode_list = []
        for register in self.get_all_register_or_regmap():
            encode_list.extend(register.get_encode_list())

        # may add enum provided as add_enum, even if not used in any register field
        encode_list.extend(self.enum_list)

        # convert list to set (unique)
        # set([]) # use set to keep unique (enum expect only once)
        # get sorted by class __name__ so the generated output file are reproductible
        encode_list = sorted(set(encode_list), key=attrgetter('__name__'))
        return encode_list

    def to_map_table(self):
        map_table = ""
        for idx,entry in enumerate(self.map_table):
            header =  f"{'    '*entry.layer}"\
                      f"[{idx}]@0x{entry.address:08x}:{entry.get_summary()}"
            entry_detail = ""
            if entry.type in [ MapTableType.REG_MAP_ARRAY, MapTableType.REG_MAP, \
                               MapTableType.REG_MAP_ARRAY_END, MapTableType.REG_MAP_END]:
                entry_detail += f":{entry.get_regmap_hierarchy(max_range=False)}\n"
            elif entry.type in [ MapTableType.REG_ARRAY, MapTableType.REG_ARRAY_END]:
                entry_detail += f":{entry.get_layer_bracket(range=True)}\n"
            elif entry.type in [ MapTableType.REG]:
                entry_detail += f":{entry.get_layer_bracket(range=False)}/{entry.get_layer_bracket(range=True)}\n"
            else:
                # padding, align
                entry_detail += f":\n"
            entry_detail += f":LIndex={entry.layer_index}/LRange={entry.layer_range}\n"
            entry_detail += f":Parent={entry.parent.get_regmap_hierarchy(max_range=False)}\n"
            # child_str = [child+':>'+reg.register.name+'#'+reg.type.name+str(reg.layer_index) for child, reg in entry.child_list.items()]
            child_str = [child for child, reg in entry.child_list.items()]
            entry_detail += f":Child={child_str}\n"
            entry_detail += f":Size={entry.size}\n"

            # get hierachy info
            entry_hier_info = entry.get_hierarchy_info()
            hier_index =\
                ''.join([''.join([f"[{id}]" for id in tup]) for tup in entry_hier_info['regmap_index_list']])+\
                ''.join([f"[{id}]" for id in entry_hier_info['register_index']])
            hier_range =\
                ''.join([''.join([f"[{id}]" for id in tup]) for tup in entry_hier_info['regmap_range_list']])+\
                ''.join([f"[{id}]" for id in entry_hier_info['register_range']])
            entry_detail += f":Hier={entry.get_regmap_hierarchy(max_range=False)}::Idx={hier_index}/{hier_range}\n"
            entry_detail += f":Rel={entry.related_entry.get_regmap_hierarchy(max_range=False) if entry.related_entry else ''}\n"
            entry_detail += f":Li={entry.linear_index}/{entry.get_array_size_in_hierarchy()}\n"
            if entry.type in [ MapTableType.REG]:
                entry_detail += f":Rg-map range={entry.get_regmap_array(range=True, include_leaf_reg=False)}\n"
            if hasattr(entry, 'field_idb'):
                entry_detail += f":idb={entry.field_idb.keys()}\n"

            if entry.type in [ MapTableType.REG]:
                entry_detail += f":Volatile={entry.register.is_volatile(entry)}\n"
            # add extra debug info on alias perception by aliased fields
            if entry.type in [ MapTableType.REG]:
                for field in entry.register.get_all_actual_field():
                    sto = field.has_storage(aliased=True, map_table_entry=entry)
                    entry_detail += f":Alias:R={entry.register.name},F={field.name},storage={sto}\n"
            # get_src_alias for field
            if entry.type in [ MapTableType.REG]:
                for field in entry.register.get_all_actual_field():
                    alias_list = ':'.join([d['field'].name for d in field.get_src_alias(entry)])
                    entry_detail += f":get_src_alias({field.name})=>{alias_list}\n"
            # get_all_alias for field
            if entry.type in [ MapTableType.REG]:
                for field in entry.register.get_all_actual_field():
                    alias_list = ':'.join([d['field'].name for d in field.get_all_alias(entry)])
                    entry_detail += f":get_all_alias({field.name})=>{alias_list}\n"

            # Add detail to header (with identation)
            entry_detail = ('\n'+" "*len(header)).join(entry_detail.strip().split("\n"))+"\n"
            map_table += header + entry_detail
        return map_table

    def to_html(self):
        html = ""
        for idx,entry in enumerate(self.map_table):
            # Register map
            if entry.type == MapTableType.REG_MAP_ARRAY:
                html += f"<h2><p>Regmap Array : {entry.get_regmap_hierarchy(max_range=False)}</p></h2>\n"
            if entry.type == MapTableType.REG_MAP_ARRAY_END:
                html += f"<h4><p>Regmap Array end: {entry.get_regmap_hierarchy(max_range=False)}</p></h4>\n"
            if entry.type == MapTableType.REG_MAP:
                html += f"<h1><caption>RegisterMap: {entry.get_regmap_hierarchy(max_range=False)}</caption></h1>\n"
                html += f"<p>Addr = 0x{entry.address:04x}:0x{entry.address+entry.size:04x}<br>\n"
                html += f"Reg map size = {entry.size}</p>\n"
            if entry.type == MapTableType.REG_MAP_END:
                html += f"<h4><caption>RegisterMap End: {entry.get_regmap_hierarchy(max_range=False)}</caption></h4>\n"
                html += f"<p>Addr = 0x{entry.address:04x}<br>\n"
                html += f"Reg map size = {entry.size}</p>\n"
            # Register
            if entry.type == MapTableType.REG_ARRAY:
                html += f"<h2><p>Reg Array : {entry.register.name}{entry.get_layer_range_definition()}</p></h2>\n"
            if entry.type == MapTableType.REG:
                if entry.is_first_of_array():
                    html += f"<hr>"
                    html += f"<h2><p>0x{entry.address:04x}: {entry.register.name}"
                    html += f"{entry.get_layer_bracket(range=False)}</p></h2>\n"
                    html += entry.register.to_html(entry)
                else:
                    html += f"<hr><p>0x{entry.address:04x}: {entry.register.name}"
                    html += f"{entry.get_layer_bracket(range=False)}</p>\n"
            # Padding or alignement
            if entry.type == MapTableType.PRE_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Pre-Pad:size={entry.size}"\
                        f" on {entry.related_entry.get_regmap_hierarchy(max_range=False)}</p></h2>\n"
            if entry.type == MapTableType.POST_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Post-Pad:size={entry.size}"\
                        f" on {entry.related_entry.get_regmap_hierarchy(max_range=False)}</p></h2>\n"
            if entry.type == MapTableType.ALIGN_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Align Pad:size={entry.size}"\
                        f" on {entry.related_entry.get_regmap_hierarchy(max_range=False)}</p></h2>\n"
            if entry.type == MapTableType.REGMAP_LENGTH_ALIGN_PAD:
                html += f"<hr>"
                html += f"<h2><p> 0x{entry.address:04x}:0x{entry.address+entry.size:04x}: Regmap Length Align Pad:size={entry.size}"\
                        f" on {entry.related_entry.get_regmap_hierarchy(max_range=False)}</p></h2>\n"

        html += "<hr>"
        timestamp_and_version = self.pyrift_generation_message(self.user_notice,comment_header='').replace('\n', '<br>\n')
        html += f"<p>{timestamp_and_version}</p>\n"
        return html

    @staticmethod
    def md_escape(str):
        # array bracket are mixed up with link def in markdown, escape '[' to prevent that
        return str.replace('[', '\\[').replace(']', '\\]')

    def svd_fill_cluster(self, root_regmap, regmap_name, regmap_cluster_et, base_address):
        # regmap to convert ot SVD is either the root or a sub_regmap

        # Check if regmap exist as array
        have_array = any(entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_ARRAY
                         for entry in root_regmap.map_table)
        if have_array:
            regmap_array_entry = next( entry for entry in root_regmap.map_table
                                       if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_ARRAY)
            # print (f"     regmap {regmap_name} IS array : {regmap_array_entry.register.name}"
            #        f"{regmap_array_entry.get_array_range_definition()}")
        # find first REG_MAP item in map_table (error StopIteration raised if not found)
        regmap_start_idx = next( n for n,entry in enumerate(root_regmap.map_table)
                                 if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP)
        # find first REG_MAP_END item in map_table (error StopIteration raised if not found)
        regmap_end_idx = next( n for n,entry in enumerate(root_regmap.map_table)
                                 if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_END)
        # print (f"Struct regmap:{regmap_start_idx}:{root_regmap.map_table[regmap_start_idx].register.name}"\
        #                      f"{root_regmap.map_table[regmap_start_idx].get_array_index_bracket()}")
        # print (f"Struct regmap end:{regmap_end_idx}:{root_regmap.map_table[regmap_start_idx].register.name}"\
        #                      f"{root_regmap.map_table[regmap_start_idx].get_array_index_bracket()}")

        cluster_et = ET.SubElement(regmap_cluster_et, 'cluster')
        cluster_base_address = root_regmap.map_table[regmap_start_idx].address # reference for register offset computation
        ET.SubElement(cluster_et, 'name').text =\
            root_regmap.map_table[regmap_start_idx].register.name +\
            ('[%s]' if have_array else '')

        ET.SubElement(cluster_et, 'addressOffset').text = hex(cluster_base_address - base_address)
        cluster_et.append(ET.Comment(f"addressAbsolute={hex(cluster_base_address)}"))
        cluster_description = root_regmap.map_table[regmap_start_idx].register.desc
        if not cluster_description:
            cluster_description='-'
        ET.SubElement(cluster_et, 'description').text = \
            ''.join([ f"{line}\n" for line in cleandoc(cluster_description).splitlines()]).strip()


        # Iterate on regmap (REG_MAP, REG_MAP_END excluded), first item only is regmap array
        entry_iter = iter(root_regmap.map_table[regmap_start_idx+1:regmap_end_idx])
        for entry in entry_iter:
            if entry.type == MapTableType.REG_MAP_ARRAY:
                array_entry = entry
                # print(f"{regmap_name}:// Sub RegisterMap Array: {array_entry.register.name}{entry.layer_range}\n")
                entry.register.svd_fill_cluster(
                    root_regmap=root_regmap,
                    regmap_name=entry.register.name,
                    regmap_cluster_et=cluster_et,
                    base_address=cluster_base_address)
                while(entry.type != MapTableType.REG_MAP_ARRAY_END or entry.register.name != array_entry.register.name):
                    entry = next(entry_iter)

            if entry.type == MapTableType.REG_MAP:
                regmap_entry_name = entry.register.name
                entry.register.svd_fill_cluster(
                    root_regmap=root_regmap,
                    regmap_name=entry.register.name,
                    regmap_cluster_et=cluster_et,
                    base_address=cluster_base_address)
                # print(f"{regmap_name}:// Sub RegisterMap (non array): {entry.register.name}\n")
                while(entry.type != MapTableType.REG_MAP_END or entry.register.name != regmap_entry_name):
                    entry = next(entry_iter)

            # process either REG_ARRAY or REG exclusively
            if entry.type == MapTableType.REG_ARRAY:
                array_entry = entry
                # array_name = entry.register.name
                # array_range = entry.layer_range
                # print(f"{regmap_name}:Register ARRAY is : {array_name}, addr={entry.address}=0x{entry.address:04x}, "\
                #       f"Array{entry.layer_range} {entry.get_noarray_hw_name()}\n")

                # all align and pad and done previous or after to REG_ARRAY
                # so REG_ARRAY is always followed by REG entry (first reg of array)
                entry = next(entry_iter)
                first_reg_entry = entry # found first REG just after REG_ARRAY
                # skip till we reach the last entry of array
                last_entry_index = tuple(i-1 for i in entry.layer_range)
                # print ("Search for last index : {last_entry_index}")
                while (entry.layer_index != last_entry_index):
                    # print (entry.layer_index)
                    entry = next(entry_iter)
                # print("End...", entry.type, entry.layer_index)

                # process first register as if single reg
                register_et = ET.SubElement(cluster_et, 'register')
                entry.register.svd_fill_register(
                    map_table_entry=first_reg_entry,
                    register_et=register_et,
                    cluster_base_address=cluster_base_address)
                # then add array information
                # convert multi-dimensional array in layer to linear array
                ET.SubElement(register_et, 'dim').text = str(array_entry.get_array_layer_size())
                ET.SubElement(register_et, 'dimIncrement').text = str(first_reg_entry.size) # in byte

            # process either REG_ARRAY or REG exclusively
            elif entry.type == MapTableType.REG:
                # if reached here we are sure this is not an array
                # (as REG_ARRAY have consume all REG entry above (in a while loop with next()))
                # print(f"{regmap_name}:Register is : {entry.register.name}, addr={entry.address}=0x{entry.address:04x}, "\
                #      f"Array{entry.layer_range} TU_{entry.get_noarray_hw_name()}\n")
                register_et = ET.SubElement(cluster_et, 'register')
                entry.register.svd_fill_register(
                    map_table_entry=entry,
                    register_et=register_et,
                    cluster_base_address=cluster_base_address)

        # Regmap fully scan, so now add array information
        if have_array:
            # convert multi-dimensional array in layer to linear array
            ET.SubElement(cluster_et, 'dim').text = str(regmap_array_entry.get_array_layer_size())
            # regmap individual size is not available directly, so divide the array size by dim
            ET.SubElement(cluster_et, 'dimIncrement').text = \
                str(regmap_array_entry.size // regmap_array_entry.get_array_layer_size())

    def to_svd_tree(self):
        # try to generate debug XML format in-line with http://www.keil.com/pack/doc/CMSIS/SVD/html/index.html
        # xml tree have a root, use to insert subelement
        # trying to match found exemple like:
        # <device schemaVersion="1.3"
        #         xmlns:xs="http://www.w3.org/2001/XMLSchema-instance"
        #         xs:noNamespaceSchemaLocation="CMSIS-SVD.xsd">
        root = ET.Element('device',
                schemaVersion="1.3.6",
                # 'xmlns:xs'="http://www.w3.org/2001/XMLSchema-instance",
                #'xs:noNamespaceSchemaLocation'="CMSIS-SVD.xsd"
            )
        tree = ET.ElementTree(root)

        ET.SubElement(root, 'name').text = self.module_name # need to match peripherals name ?
        ET.SubElement(root, 'description').text = 'Dummy device description generated by pyrift'
        ET.SubElement(root, 'addressUnitBits').text = '8' # byte addressable memory
        ET.SubElement(root, 'width').text = '32' # byte addressable memory


        cpu = ET.SubElement(root, 'cpu') # no usefull info from pyrift registers, to be provided by template ?
        ET.SubElement(cpu, 'name').text = 'other'
        ET.SubElement(cpu, 'revision').text = 'r0p0'
        ET.SubElement(cpu, 'endian').text = 'little'
        ET.SubElement(cpu, 'fpuPresent').text = 'false'
        ET.SubElement(cpu, 'fpuDP').text = 'false'
        ET.SubElement(cpu, 'nvicPrioBits').text = '4' # required range 2..8, default 4
        ET.SubElement(cpu, 'deviceNumInterrupts').text = '2' #
        ET.SubElement(cpu, 'vendorSystickConfig').text = 'false'


        peripherals = ET.SubElement(root, 'peripherals')
        peripheral = ET.SubElement(peripherals, 'peripheral')
        ET.SubElement(peripheral, 'name').text = self.module_name
        ET.SubElement(peripheral, 'version').text = '0.0'
        regmap_desc = self.desc
        if not regmap_desc:
            regmap_desc = '-'
        ET.SubElement(peripheral, 'description').text =\
            ''.join([ f"{line}\n" for line in cleandoc(regmap_desc).splitlines()]).strip()
        ET.SubElement(peripheral, 'baseAddress').text = f"0x{self.base_address:08x}"

        # add interrupt entry to remove warning in SVDConv
        # like <interrupt>
        #           <name>ABCDE_INT</name>
        #           <value>12</value>
        #      </interrupt>
        interrupt_et = ET.SubElement(peripheral, 'interrupt')
        ET.SubElement(interrupt_et, 'name').text = 'dummy'
        ET.SubElement(interrupt_et, 'description').text = 'No description for dummy'
        ET.SubElement(interrupt_et, 'value').text = '0'

        # address block describe the memory mapping envelop of full peripheral from baseAddress
        # <addressBlock>
        #   <offset>0x0</offset>
        #   <size>0x400</size>
        #   <usage>registers</usage>
        #   <protection>s</protection>
        # </addressBlock>
        address_block = ET.SubElement(peripheral, 'addressBlock')
        ET.SubElement(address_block, 'offset').text = "0x0"
        ET.SubElement(address_block, 'size').text = f"0x{1<<(self.rif_host_bus_msb+1):08x}"
        ET.SubElement(address_block, 'usage').text = f"registers"

        registers = ET.SubElement(peripheral, 'registers')
        # SVD.registers is the top cluster : mapped to the top RegisterMap of Rif
        # SVD.registers => set of {SVD.register, SVD.cluster}
        # SVD.cluster => set of {SVD.register, SVD.cluster}
        #
        # sub RegisterMap are then mapped to SVD.cluster
        # pyrift registers are mapped to SVD.register

        # The top cluster is filled with current top regmap
        self.svd_fill_cluster(
            root_regmap=self,
            regmap_name=self.name,
            regmap_cluster_et=registers,
            base_address=self.base_address)

        # Append a comment for pyrift information (separator is not -- as not friendly to xml comment)
        root.append(ET.Comment(self.pyrift_generation_message(self.user_notice,comment_header='',separator='=')))

        return tree

    def to_markdown(self, variant='gitlab'):
        markdown = ""
        if PyriftBase.md_option['header_address_map']: # address map is optional in documentation
                        # header level in markdown
            markdown = '#'*PyriftBase.md_option['header_start_level'] + ' ' + \
                       PyriftBase.md_option['header_address_map'] % {'name': self.name, 'module_name': self.module_name}
            markdown += "\n"
            reg_header_needed = True # reset on each regmap md addition, before restarting register list
            for idx,entry in enumerate(self.map_table):
                ## Register map ARRAY
                if entry.type == MapTableType.REG_MAP_ARRAY and PyriftBase.md_option['with_regmap_array_header']:
                    rm_hier = self.md_escape(entry.get_regmap_hierarchy(max_range=False).replace('/', PyriftBase.md_option['regmap_separator']))
                    markdown += f"## {rm_hier}  \n"
                    # if entry.register.desc:
                    #     # print("desc=", reg_description)
                    #     markdown += f"Description {entry.register.name}:  \n"
                    #     regmap_description = entry.register.desc.replace("\r\n", "\n").replace("\n","  \n")
                    #     markdown += f"{regmap_description}  \n"
                    #markdown += f"# {rm_hier}, size=0x{entry.size:04X}\n"
                #if entry.type == MapTableType.REG_MAP_ARRAY_END:
                ## Register map
                if entry.type == MapTableType.REG_MAP:
                    if PyriftBase.md_option['with_regmap_header']:
                        rm_hier = self.md_escape(entry.get_regmap_hierarchy(max_range=False).replace('/', PyriftBase.md_option['regmap_separator']))
                        markdown += '#'*(PyriftBase.md_option['header_start_level']+1) + ' ' + \
                                    f"{rm_hier}\n"
                        if entry.register.desc and entry.is_first_of_array():
                            # print("desc=", reg_description)
                            markdown += f"Description {entry.register.name}:  \n"
                            regmap_description = entry.register.desc.replace("\r\n", "\n").replace("\n","  \n")
                            markdown += f"{regmap_description}  \n"
                    reg_header_needed = True

                    # markdown += f"  0x{entry.address:04x}:0x{entry.address+entry.size:04x}  \n"
                    # markdown += f"  Reg map size = {entry.size}  \n"
                if entry.type == MapTableType.REG_MAP_ARRAY_END:
                    if PyriftBase.md_option['with_regmap_footer']:
                        rm_hier = self.md_escape(entry.get_regmap_hierarchy(max_range=False).replace('/', PyriftBase.md_option['regmap_separator']))
                        markdown += '#'*(PyriftBase.md_option['header_start_level']+1) + ' ' + \
                                    f"End of {rm_hier}\n"
                    reg_header_needed = True

                ## Register
                #if entry.type == MapTableType.REG_ARRAY:
                if entry.type == MapTableType.REG:
                    if reg_header_needed:
                        markdown += f"  \n"
                        markdown += f"| Address | Register "
                        if PyriftBase.md_option['regmap_column_oneline_desc']:
                            markdown += f"| Desc summary "
                        # TODO: add support for multiline description (need +---+---+ formatting of table)
                        # if PyriftBase.md_option['regmap_column_desc']:
                        #     markdown += f"| Description "
                        markdown += "|  \n"
                        markdown += f"|---------|----------"
                        if PyriftBase.md_option['regmap_column_oneline_desc']:
                            markdown += f"|--------------"
                        # if PyriftBase.md_option['regmap_column_desc']:
                        #     markdown += f"|-------------"
                        markdown += "|  \n"
                        reg_header_needed = False
                    # register in address table, just a link to actual register details
                    # gitlab markdown requires linkto be lower case
                    rm_hier = self.md_escape(entry.get_regmap_hierarchy(max_range=False, include_root_regmap=False))
                    if PyriftBase.md_option['link_to_reg']:
                        markdown += f"|[{entry.register.get_md_formatted_address(entry)}](#{entry.register.name.lower()})|"
                        markdown += f"[{entry.register.name.lower()}{entry.get_layer_bracket()}](#{entry.register.name.lower()})"
                    else:
                        markdown += f"|{entry.register.get_md_formatted_address(entry)}|"
                        markdown += f"{entry.register.name.lower()}{entry.get_layer_bracket()}"
                    if PyriftBase.md_option['regmap_column_oneline_desc']:
                        markdown += f"|{entry.register.get_oneline_desc()}"
                    # if PyriftBase.md_option['regmap_column_desc']:
                    #     reg_description = entry.register.desc.replace("\r\n", "\n").replace("\n","  \n")
                    #     markdown += f"|{reg_description}"
                    markdown += "|  \n"
                ## Padding or alignement
                #if entry.type == MapTableType.PRE_PAD:
                #if entry.type == MapTableType.POST_PAD:
                #if entry.type == MapTableType.ALIGN_PAD:
                #if entry.type == MapTableType.REGMAP_LENGTH_ALIGN_PAD:

            # markdown = markdown.replace('[', '\\[').replace(']', '\\]')

        if PyriftBase.md_option['header_register_detail']:  # register detail section is optional in documentation
            markdown += "  \n------------------------------------  \n\n"
                         # header level in markdown
            markdown += '#'*PyriftBase.md_option['header_start_level'] + ' ' + \
                        PyriftBase.md_option['header_register_detail']
            register_entry_list = [entry for entry in self.map_table if entry.type == MapTableType.REG and entry.is_first_of_array()]
            for entry in register_entry_list:
                markdown += '#'*(PyriftBase.md_option['header_start_level']+1) + ' ' + \
                            f"{entry.register.name}\n"
                markdown += entry.register.to_markdown(entry, variant)

        if PyriftBase.md_option['header_version_info']:  # version section is optional in documentation
            markdown += "\n---------------------------------  \n\n"
                         # header level in markdown
            markdown += '#'*PyriftBase.md_option['header_start_level'] + ' ' + \
                        PyriftBase.md_option['header_version_info']
            timestamp_and_version = self.pyrift_generation_message(self.user_notice,comment_header='').strip().replace('\n', '  \n')
            # --- as line separator require an extra new line before to render correctly
            timestamp_and_version = timestamp_and_version.replace("\n---", "\n\n---")
            markdown += f"{timestamp_and_version}\n"
        return markdown

    def to_verilog_register(self):
        """
            Generate the verilog register implementation for all register
        """
        verilog_declaration = ""
        verilog_declaration += "// ----------------------------------------------\n"
        verilog_declaration += "// Declare signals used in all registers of remap\n"
        verilog_declaration += "// ----------------------------------------------\n"
        verilog = ""
        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog instanciate register : {entry.register.name}")
                reg_verilog_declaration, reg_verilog = entry.register.to_verilog(entry)
                verilog_declaration += reg_verilog_declaration
                verilog += reg_verilog
        verilog_declaration += "// ----------------------------------------------\n"

        return verilog_declaration+verilog

    def to_verilog_decoder(self):
        """
            Generate the verilog decoding from generic host interface (APB or ...)
            Generate the verilog address decoding logic for each register
            generate @ decoding to register in the current register map
            mux _swrdata from each register to rif_rdata
            use the generic "host_access_wr" signal to gate the write decoding
        """
        verilog = ""
        verilog += "// host interface bus cycle decoder\n"
        verilog += "logic [31:0] rif_base_address;\n" # 32 bit address whatever the data width used
        verilog += "assign rif_base_address = 32'h%08X;\n" % self.base_address
        verilog += "logic rif_access_swwr;\n"
        verilog += "logic rif_access_swrd;\n"
        verilog += "logic rif_access_swrd_early;\n"
        verilog += "logic rif_access_ready;\n"
        verilog += "assign host_access_ready = rif_access_ready;\n"
        if self.rif_host_bus_msb > self.rif_addr_msb:
            # extra msb from host address range to actual rif size range : top level decoding of rif needed
            verilog += "assign rif_access_swwr = host_access_wr  &&\n"
            verilog += "                         (host_addr[%d:%d] == rif_base_address[%d:%d]);\n" %\
                ((self.rif_host_bus_msb, self.rif_addr_msb+1) * 2)
            verilog += "assign rif_access_swrd = host_access_rd  &&\n"
            verilog += "                         (host_addr[%d:%d] == rif_base_address[%d:%d]);\n" %\
                ((self.rif_host_bus_msb, self.rif_addr_msb+1) * 2)
            verilog += "assign rif_access_swrd_early = host_access_rd_early  &&\n"
            verilog += "                               (host_addr[%d:%d] == rif_base_address[%d:%d]);\n" %\
                ((self.rif_host_bus_msb, self.rif_addr_msb+1) * 2)
        else:
            # self.rif_host_bus_msb == self.rif_addr_msb
            # no extra address bit of host for top level decoding of rif
            verilog += "assign rif_access_swwr = host_access_wr;\n"
            verilog += "assign rif_access_swrd = host_access_rd;\n"
            verilog += "assign rif_access_swrd_early = host_access_rd_early;\n"
        verilog += "\n"

        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog decode register : {entry.register.name}")
                reg_name = entry.get_full_hw_name()
                verilog += f"// Decoding register {entry.register.name} {reg_name}:@=0x{entry.address:0x}\n"

                # generate address decode verilog for each _swwr/_swrd
                verilog += f"logic {reg_name}_r_swwr;\n"
                verilog += f"logic {reg_name}_r_swrd;\n"
                verilog += f"logic {reg_name}_r_swrd_early;\n"
                verilog += f"logic {reg_name}_r_ready;"\
                               f"// ready only for external register (or unused)\n"
                verilog += f"logic [{PyriftBase.dw-1}:0] {reg_name}_swrdata;\n"

                verilog += f"assign {reg_name}_r_swwr = rif_access_swwr"
                if self.rif_addr_msb >= 2:
                    verilog += f"\n   && (host_addr[{self.rif_addr_msb}:{self.rif_addr_lsb}] == "\
                                   f"{self.rif_addr_msb-self.rif_addr_lsb+1}'('h{entry.address:0x}/{self.dw//8}));\n"
                else:
                    # Handle single register case ith 0 bit address decoding size
                    verilog += f";\n"
                verilog += f"assign {reg_name}_r_swrd = rif_access_swrd"
                if self.rif_addr_msb >= 2:
                    verilog += f"\n   && (host_addr[{self.rif_addr_msb}:{self.rif_addr_lsb}] == "\
                                   f"{self.rif_addr_msb-self.rif_addr_lsb+1}'('h{entry.address:0x}/{self.dw//8}));\n"
                else:
                    # Handle single register case ith 0 bit address decoding size
                    verilog += f";\n"
                verilog += f"assign {reg_name}_r_swrd_early = rif_access_swrd_early"
                if self.rif_addr_msb >= 2:
                    verilog += f"\n   && (host_addr[{self.rif_addr_msb}:{self.rif_addr_lsb}] == "\
                                   f"{self.rif_addr_msb-self.rif_addr_lsb+1}'('h{entry.address:0x}/{self.dw//8}));\n"
                else:
                    # Handle single register case ith 0 bit address decoding size
                    verilog += f";\n"

                #####################" verilog += entry.register.to_verilog(entry)
        verilog += f"// -------------------------------\n"

        verilog += f"// generated data muxing based on *_r_swrd for all register\n"

        # iterate over all register inside address map:
        # generate data muxing _r_swrd
        verilog += f"always_comb begin : proc_host_rdata_for_register\n"
        verilog += f"   rif_rdata = 0; // rdata is 0 unless actual read reg occurs\n"
        # each register is checked for active read (exclusive by nature of decoder)
        # So "OR" every active reg is equivalent to muxing
        # (cascade muxing seems trick to optimize by synthesis for large number of register :
        # more than 1000 regs may segfault compiler/synth tools !,
        # "or" based logic seems to already pre-opt for verilog compiler)
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"Verilog data muxing rfor egister : {entry.register.name}")
                reg_name = entry.get_full_hw_name()
                verilog += f"   if( {reg_name}_r_swrd) begin\n"
                verilog += f"      rif_rdata = rif_rdata | {reg_name}_swrdata;\n"
                verilog += f"   end\n"
        verilog += f"end\n"

        verilog += f"// -------------------------------\n"
        verilog += f"// generated ready muxing for all register\n"
        # rif_access_ready = 1 unless the accessed register is not giving ready
        verilog += f"always_comb begin : proc_host_ready\n"
        rif_default_rdy = f"!{self.freeze_hostbus_signal.name}" if self.freeze_hostbus_signal else "1"
        verilog += f"   rif_access_ready = {rif_default_rdy};\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                #print (f"Verilog ready muxing for register : {entry.register.name}")
                reg_name = entry.get_full_hw_name()
                verilog += f"   // generate ready selection from register ready, either read or write\n"
                verilog += f"   if( ({reg_name}_r_swrd || {reg_name}_r_swwr) &&\n"
                verilog += f"       !{reg_name}_r_ready) begin\n"
                verilog += f"      rif_access_ready = 0; // force to 0 due to accessed register not ready\n"
                verilog += f"   end\n"
        verilog += f"end\n"
        verilog += f"// -------------------------------\n"
        return verilog


    def get_verilog_interface_list(self):
        """
            explore all register/field to list all interface involved
        """
        interface_list = set([])
        # print(f"############ ------Searching for all interfaces")

        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"register : {entry.register.name}, if={entry.register.get_interface_name()}")
                for field in entry.register.get_all_actual_field():
                    if_name = field.get_interface_name()
                    # print(f"Found interface {if_name}")
                    interface_list.add(if_name)
        return sorted(interface_list)

    def get_reg_entry_list_from_if_name(self, if_name):
        result_list = []
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                if len([f for f in entry.register.get_all_actual_field() if (f.get_interface_name() == if_name)])>0:
                    # Register that is part of if_name now confirmed, so add to return list
                    result_list.append(entry)
        return result_list

    def get_root_entry(self):
        """
            From regmap, find map_table, extract root entry (should first entry in list)
        """
        if self.map_table[0].type != MapTableType.ROOT:
            raise RuntimeError(f"Unexpected condition: First entry of Map table should be a ROOT entry !?")
        return self.map_table[0]


    def to_verilog_interface_internal_aggregate(self, if_name, aggregate_name):
        """
            generate verilog interface declaration for aggregate
            provided parameter are:
            if_name: name of target interface
            aggregate_name: name of all target field : @aggregate_name.##
            uses regmap pre-computed pre-filtered list of register including aggegate fields
            constituant fields are search inside all register of reg_with_aggregate_set
        """
        verilog = ""
        aggregate_info_list = self.aggregate_info_dict[f"{aggregate_name}.{if_name}"]
        # aggregate_info_list report the list of field that are part of the aggregate
        # /!\ this can be empty if aggregate is on another interface
        # if list is empty, nothing to generate in verilog, just return empty result
        if len(aggregate_info_list)==0:
            return verilog


        # TODO: update obsolete comment for aggregate implementation ...
        #        """\
        #            Generate field signal in verilog hw interface
        #            following signal are handled as single bit or vector[] root link shown
        #              ---------------------------------------------------------
        #              FIELD rtl level   # multi IF level (root)   # multi IF for slave
        #              ---------------------------------------------------------
        #            - field[:0]         |       root[:N]          |   Nothing
        #            - field_next[:0]    |       root_next[:N]     |   Nothing
        #            - field_hwset[:0]   |       root_hwset[:N]    |   Nothing
        #            - field_hwclr[:0]   |       root_hwclr[:N]    |   Nothing
        #            - field_hwwe        |       root_hwwe         |   Nothing
        #            - field_swmod       |       field_swmod       |   field_swmod
        #            - field_swacc       |       field_swacc       |   field_swacc
        #            if signal can be reshaped: .if_reshape = (2,) get [n-1:0][1:0]if_signal
        #            (from a 2*n packed field)
        #        """

        # generate the verilog body of interface
        # use aggregate_name for actual name inside the verilog interface
        # Do not collect any array structure from regmap hierarchy:
        # All regmap hierarchy is flatten, concatenate for all constituant, possibly reshaped

        bit_width = sum([info['field'].bit_width for info in aggregate_info_list])

        extra_dim = ""
        if_reshape = aggregate_info_list[0]['field'].if_reshape
        if if_reshape:
            # so far support only single extra dimension
            reshape_size = if_reshape[0]
            if bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.get_name()}")
            bit_width //= reshape_size
            extra_dim = f"[{reshape_size-1}:0]"
        verilog += f"   // field aggregation expansion for: {aggregate_name}, bw={bit_width} x dim={extra_dim}\n"

        if aggregate_info_list[0]['field'].hw.is_readable():
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name};\n"
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })
        if aggregate_info_list[0]['field'].hw.is_writable():
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name}_next;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_next",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })

        if (aggregate_info_list[0]['field'].hwset):
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name}_hwset;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_hwset",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })
        if (aggregate_info_list[0]['field'].hwclr):
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name}_hwclr;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_hwclr",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })

        # Note has_storage() is checking the aliased version of field
        if (aggregate_info_list[0]['field'].hw.is_writable() and \
            aggregate_info_list[0]['field'].has_storage(aliased=True, map_table_entry=aggregate_info_list[0]['reg_entry'])):
            verilog += f"      logic {aggregate_name}_hwwe;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_hwwe",
                "if_name" : if_name,
                "type" : f"logic"
            })

        # swmod and swacc are not altered by aggregation
        # 2 signal provided : _f_sw* : one bit per field, _sw* main signal active if any field is accessed
        if (aggregate_info_list[0]['field'].swmod or aggregate_info_list[0]['field'].delayed_swmod):
            verilog += f"      logic [{len(aggregate_info_list)-1}:0] {aggregate_name}_f_swmod; // one signal for each aggregated field\n"
            verilog += f"      logic {aggregate_name}_swmod; // any field of aggregated is accessed\n"
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_f_swmod",
                "if_name" : if_name,
                "type" : f"logic [{len(aggregate_info_list)-1}:0]"
            })
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_swmod",
                "if_name" : if_name,
                "type" : f"logic"
            })
        if (aggregate_info_list[0]['field'].swacc or aggregate_info_list[0]['field'].early_swacc):
            verilog += f"      logic [{len(aggregate_info_list)-1}:0]  {aggregate_name}_f_swacc; // one signal for each aggregated field\n"
            verilog += f"      logic {aggregate_name}_swacc; // any field of aggregated is accessed\n"
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_f_swacc",
                "if_name" : if_name,
                "type" : f"logic [{len(aggregate_info_list)-1}:0]"
            })
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_swacc",
                "if_name" : if_name,
                "type" : f"logic"
            })
        if aggregate_info_list[0]['field'].swwe or aggregate_info_list[0]['field'].swwel: # hardware enabling field modification
            if not(aggregate_info_list[0]['field'].sw.is_writable() or aggregate_info_list[0]['field'].rclr or aggregate_info_list[0]['field'].rset):
                raise RuntimeError(f"found swwe option for field  {aggregate_name} that is"\
                                   f" not sw writable, nor rclr , nor rset ?")
        if aggregate_info_list[0]['field'].swwe: # hardware enabling field modification (active high)
            verilog += f"      logic  {aggregate_name}_swwe;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_swwe",
                "if_name" : if_name,
                "type" : f"logic"
            })
        if aggregate_info_list[0]['field'].swwel: # hardware enabling field modification (active low)
            verilog += f"      logic  {aggregate_name}_swwel;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{aggregate_name}_swwel",
                "if_name" : if_name,
                "type" : f"logic"
            })
        if (aggregate_info_list[0]['field'].swwr2hwif):
            verilog += f"      logic [{len(aggregate_info_list)-1}:0]  {aggregate_name}_f_ifsw_wr; // one signal for each aggregated field\n"
            verilog += f"      logic {aggregate_name}_ifsw_wr; // any field of aggregated is sw written\n"
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name}_ifsw_wr_data;\n"
            verilog += f"      logic [{bit_width - 1}:0]{extra_dim} {aggregate_name}_ifsw_wr_bmask;\n"
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_f_ifsw_wr",
                "if_name" : if_name,
                "type" : f"logic [{len(aggregate_info_list)-1}:0]"
            })
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_ifsw_wr",
                "if_name" : if_name,
                "type" : f"logic"
            })
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_ifsw_wr_data",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })
            self.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{aggregate_name}_ifsw_wr_bmask",
                "if_name" : if_name,
                "type" : f"logic [{bit_width - 1}:0]{extra_dim}"
            })

        verilog += f"   // ----------------------------------\n"
        return verilog


    def to_verilog_interface_by_name(self, if_name):
        """
            Generate the verilog interface filtering by interface name
            (to be called once for each interface name used)
        """

        verilog = ""
        verilog += f"interface {if_name};\n"
        # Find all register that have field as member of if_name (register only on other IF are ignored)
        # Exclude aggregate fields to generation

        for entry in self.get_reg_entry_list_from_if_name(if_name):
            # only first register of array generate a signal in this interface
            # all entries processed here are MapTableType.REG
            reg_short_name =  entry.get_short_hw_name()
            if entry.is_first_of_array():
                # print (f"register inIF: {reg_short_name}, if={if_name} range={entry.layer_range}")
                verilog += f"   //  [{entry.address:08X}]:{reg_short_name}{entry.get_layer_bracket(range=False)}"
                verilog +=        f" in regmap: {entry.parent.get_regmap_hierarchy(max_range=False)}\n"
                # for verilog interface, map table entry passed with map_table_entry
                # Exclude aggregate fields to generation
                verilog += entry.register.to_verilog_interface_by_name(if_name=if_name, map_table_entry=entry)

                verilog += f"   //  [{entry.address:08X}]:{reg_short_name}\n"
                verilog += f"   // ----------------------------------\n"
            else:
                verilog += f"   //  [{entry.address:08X}]:{reg_short_name}{entry.get_layer_bracket(range=False)}"
                verilog +=        f" in regmap: {entry.parent.get_regmap_hierarchy(max_range=False)}\n"
                verilog += f"   // ----------------------------------\n"

        # Iterate on each aggregate name in turn (use sorted aggregate names collected during validate)
        # (search for all aggregate fields to be collected,
        # during verilog generation it is filtered by if_name)
        for aggregate_name in sorted(self.aggregate_name_set):
            # print(f"Processing aggregate : {aggregate_name}")
            verilog += self.to_verilog_interface_internal_aggregate(if_name=if_name, aggregate_name=aggregate_name)

        # here input and output list should be ready for current interface,
        # possibly other interface can be collected as well
        # so generate the modport
        current_if_input_list = [io for io in self.interface_io_list  if io['rif_out_dir']==False and io['if_name']==if_name]
        current_if_output_list = [io for io in self.interface_io_list  if io['rif_out_dir']==True and io['if_name']==if_name]
        # print("if io =", [modport_io for modport_io in self.interface_io_list] )
        # print("if input =", [modport_in for modport_in in current_if_input_list] )
        # print("if output =", [modport_out for modport_out in current_if_output_list] )
        verilog += f"\n"
        verilog += f"   // ----------------------------------\n"
        verilog += f"\n"
        if len(current_if_input_list)+len(current_if_output_list)>0:
            # non empty interface, create the modport
            verilog += f"   modport rif(\n"
            verilog += ",\n".join(["      input  "+ port['name'] for port in current_if_input_list] +
                                  ["      output "+ port['name'] for port in current_if_output_list]) + "\n"
            verilog += f"   );\n"
            verilog += f"\n"
            verilog += f"   // ----------------------------------\n"
            verilog += f"\n"
            verilog += f"   modport hw(\n"
            verilog += ",\n".join(["      output "+ port['name'] for port in current_if_input_list] +
                                  ["      input  "+ port['name'] for port in current_if_output_list]) + "\n"
            verilog += f"   );\n"
        else:
            verilog += f"   // Empty interface, skip modport generation\n"
        verilog += f"\n"
        verilog += f"   // ----------------------------------\n"
        verilog += f"\n"

        verilog += f"endinterface // {if_name}\n"

        return verilog

    def to_verilog_unwrap_interface_declare_by_name(self, if_name):
        """
            Generate the verilog interface signal declaration for port of rif_unwrap
            (to be called once for each interface name used)
            This reuse the interface definition collected during module interface generation
            (during to_verilog_interface() ---> to_verilog_interface_by_name() )
        """
        verilog = ""
        verilog += f"   // declare ports for interface : {if_name}.rif {if_name.lower()}\n"
        # while generating interface, all type and name of signal of interface have been
        # collected in self.interface_io_list (done for all interface, so need to filter by if_name)
        # Then need to dump all entry
        for io in self.interface_io_list:
            if io['if_name']==if_name:
                verilog += f"   {'output' if io['rif_out_dir'] else 'input '} var {io['type']} "\
                              f"{io['if_name'].lower()}_{io['name']},\n"
        verilog += f"\n"

        return verilog


    def to_verilog_unwrap_interface_connect_by_name(self, if_name):
        """
            Generate the verilog interface signal declaration for port of rif_unwrap
            (to be called once for each interface name used)
            This reuse the interface definition collected during module interface generation
            (during to_verilog_interface() ---> to_verilog_interface_by_name() )
        """
        verilog = ""
        verilog += f"// connect ports for interface : {if_name}.hw {if_name.lower()}.hw\n"
        # while generating interface, all type and name of signal of interface have been
        # collected in self.interface_io_list (done for all interface, so need to filter by if_name)
        # Then need to dump all entry
        for io in self.interface_io_list:
            if io['if_name']==if_name:
                if io['rif_out_dir']:
                    # output
                    verilog += f"assign {io['if_name'].lower()}_{io['name']} = {if_name.lower()}.{io['name']};\n"
                else:
                    # input
                    verilog += f"assign {if_name.lower()}.{io['name']} = {io['if_name'].lower()}_{io['name']};\n"
        verilog += f"\n"

        return verilog


    def to_verilog_interface(self):
        """
            Generate the verilog interface of rif to rest of hw design
        """
        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"

        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of package without enum // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"

        # while generating the interface the input and output list are collected at regmap level
        # field.to_verilog_interface_by_name will add directly while running
        # interface_io_list element expected to be dict as : {
        #   'rif_out_dir':True(output to hw)/False(input from hw) #
        #   'name': ... #
        #   'if_name': ... #
        #   'type': at interface level (include array, enum, ...) #
        #}
        self.interface_io_list = []

        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_interface_by_name(if_name)

        # Flag completion of interface generation
        self.interface_generation_complete_flag = True
        return verilog

    def to_verilog_connect_interface(self):
        """
            Generate the verilog connection of internal register to interface
        """
        verilog = ""
        verilog += "// Connect register to interface %s\n" % self.interface_name
        for map_table_entry in self.map_table:
            if map_table_entry.type == MapTableType.REG:
                # print (f"CI register : {map_table_entry.register.name}, if={map_table_entry.register.get_interface_name()}")
                reg_name = map_table_entry.get_full_hw_name()

                verilog += f"   //  [{map_table_entry.address:08x}]:{reg_name}\n"
                # for verilog interface, parent register map passed with map_table_entry
                verilog += map_table_entry.register.to_verilog_connect_interface(map_table_entry)
                verilog += f"   //  [{map_table_entry.address:08x}]:{reg_name}\n"
                verilog += f"   // ----------------------------------\n"

        return verilog

    def to_verilog_clk_and_reset_port(self):
        """
            Generate the verilog module clock and reset port
            Use for both module and module_unwrap
        """

        verilog = ""
        verilog += f"   // Clock and Reset\n"
        verilog += f"   input  var logic clk,\n"
        # rst_n will be generated as part of extra signal list
        if len(self._validated_signal_list)>0:
            verilog += f"   // extra user input signal ...\n"
            for signal in self._validated_signal_list:
                signal_width = ""
                signal_width = f"[{signal.width-1}:0]" if signal.width>1 else ""
                verilog += f"   input  var logic {signal_width} {signal.name}, // "
                verilog += f"Synch " if signal.sync else "async "
                verilog += f"activelow " if signal.activelow else "activehigh "
                verilog += f"+ Default field RESET " if signal.field_reset else ""
                verilog += f"//\n"
        return verilog

# ----------------------------------------------------------------------------

    #     ##     ##  #######  ########  ##     ## ##       ########
    #     ###   ### ##     ## ##     ## ##     ## ##       ##
    #     #### #### ##     ## ##     ## ##     ## ##       ##
    #     ## ### ## ##     ## ##     ## ##     ## ##       ######
    #     ##     ## ##     ## ##     ## ##     ## ##       ##
    #     ##     ## ##     ## ##     ## ##     ## ##       ##
    #     ##     ##  #######  ########   #######  ######## ########
    def to_verilog_module(self):
        """
            Generate the verilog module implementation of rif
        """

        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"
        verilog += "// RIF module verilog implementation\n"

        verilog += "\n"
        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of package without enum // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"
        verilog += "// enforce check for net type (var or wire is mandatory)\n"
        verilog += "`default_nettype none\n"
        verilog += "\n"
        verilog += f"module {self.module_name} (\n"
        verilog += self.to_verilog_clk_and_reset_port()
        verilog += "\n"

        verilog += f"   // RIF to user HW interface port\n"
        if len(self.interface_io_list)>0:
            # connect interface port to module but only if non empty
            for if_name in self.get_verilog_interface_list():
                verilog += f"   {if_name}.rif {if_name.lower()},\n"
        else:
                verilog += f"   // interface is empty : skipped connection\n"

        verilog += "\n"

        # declare host bus port inside module declaration
        # for APB this includes : if_apb
        # for APB_CDC this includes : apb_cdc_clk, if_apb_cdc
        verilog += self.pyrift_host.host_port_declare()

        verilog += ");\n"
        verilog += "\n"


        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "\n"
        verilog += "// host_port_cdc_layer()\n"
        # include host CDC if needed
        verilog += self.pyrift_host.host_port_cdc_layer(regmap=self)

        # convert APB or ... to generic signals
        # convert host bus to generic signals
        verilog += "// -------------------------\n"
        verilog += "// host_bus_to_generic_bus()\n"
        verilog += "// -------------------------\n"
        verilog += self.pyrift_host.host_bus_to_generic_bus(regmap=self)
        verilog += "//\n"

        verilog += "// --------------------\n"
        verilog += "// to_verilog_decoder()\n"
        verilog += "// --------------------\n"
        verilog += self.to_verilog_decoder()
        verilog += "//\n"
        verilog += "// ----------------------------\n"
        verilog += "// host_bus_read_from_generic()\n"
        verilog += "// ----------------------------\n"
        verilog += self.pyrift_host.host_bus_read_from_generic()  # convert host bus to generic signals
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "//\n"
        verilog += "// to_verilog_register()\n"
        verilog += "//\n"
        verilog += self.to_verilog_register()
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "//\n"
        verilog += "// to_verilog_connect_interface()\n"
        verilog += "//\n"
        verilog += self.to_verilog_connect_interface()
        verilog += "//\n"
        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "endmodule\n"
        if self.add_default_nettype_wire_at_eof:
            verilog += "// get back default_nettype from none to wire\n"
            verilog += "// (expect multifile compile with following files with undeclared identifiers ? ...)\n"
            verilog += "`default_nettype wire\n"
        return verilog

# ----------------------------------------------------------------------------
#     ##     ## ##    ## ##      ## ########     ###    ########
#     ##     ## ###   ## ##  ##  ## ##     ##   ## ##   ##     ##
#     ##     ## ####  ## ##  ##  ## ##     ##  ##   ##  ##     ##
#     ##     ## ## ## ## ##  ##  ## ########  ##     ## ########
#     ##     ## ##  #### ##  ##  ## ##   ##   ######### ##
#     ##     ## ##   ### ##  ##  ## ##    ##  ##     ## ##
#      #######  ##    ##  ###  ###  ##     ## ##     ## ##

    def to_verilog_unwrap(self):
        """
            Generate the verilog module wrapper around rif
            This unwrap the interface (to APB and RIF) so no interface are used externaly
            This should allow instantiation of RIF where interface are not supported
            (could be in mixed mode vhdl/verilog, where verilog instantiation is severely limted to basic port)
            (Other use case is for verification in verilator,
            which has no support for interface at top level at the moment)
            You get a module with lots of ports (instead of the couple of interface + few signal)
            but you can co-exist with basic environment
        """
        verilog = ""
        verilog += self.pyrift_generation_message(self.user_notice)
        verilog += "// Autogeneration may overwrite this file :\n"
        verilog += "// You should probably avoid modifying this file directly\n"
        verilog += "// ------------------------------------------------------\n"
        verilog += "// RIF module verilog unwrapper implementation\n"
        verilog += "// Port should have only basic type : logic, enum, ... (no interface, struct, ...)\n"
        verilog += "\n"
        if len(self.encode_list):
            verilog += f"import {self.module_name}_pkg::*; // get enum definitions\n"
        else:
            verilog += f"// no import of package without enum // import {self.module_name}_pkg::*; // get enum definitions\n"
        verilog += "\n"
        verilog += "// enforce check for net type (var or wire is mandatory)\n"
        verilog += "`default_nettype none\n"
        verilog += "\n"
        verilog += f"module {self.module_name}_unwrap (\n"
        verilog += self.to_verilog_clk_and_reset_port()
        verilog += "\n"

        #
        if not self.interface_generation_complete_flag:
            raise  RuntimeError("Unwrap module generation require the interface to have been processed already\n"\
                                "Please call write_verilog_interface() before write_verilog_unwrap()");
        verilog += f"   // RIF to user HW interface port\n"
        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_unwrap_interface_declare_by_name(if_name)

        # Host bus interface declaration
        verilog += self.pyrift_host.host_bus_unwrap_port()

        verilog += ");\n"
        verilog += "\n"

        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "\n"

        verilog += f"// Interface declaration for RIF instanciation\n"
        for if_name in self.get_verilog_interface_list():
            verilog += f"{if_name} {if_name.lower()}();\n"
        verilog += "\n"

        verilog += self.pyrift_host.host_bus_unwrap_if_declare(regmap=self)

        verilog += f"// RIF instanciation\n"
        verilog += f"{self.module_name} i_{self.module_name} (\n"
        verilog += f"   .clk(clk),\n"
        # rst_n will be generated as part of extra signal list
        if len(self._validated_signal_list)>0:
            verilog += f"   // reset, extra user input signal ...\n"
            for signal in self._validated_signal_list:
                verilog += f"   .{signal.name} ({signal.name}), // "
                verilog += f"Synch " if signal.sync else "async "
                verilog += f"activelow " if signal.activelow else "activehigh "
                verilog += f"+ Default field RESET " if signal.field_reset else ""
                verilog += f"//\n"
        verilog += f"\n"

        if len(self.interface_io_list)>0:
            # connect interface port to module but only if non empty
            for if_name in self.get_verilog_interface_list():
                verilog += f"   .{if_name.lower()}({if_name.lower()}.rif),\n"

        verilog += self.pyrift_host.host_bus_unwrap_instance_port_connect()

        verilog += f");\n"

        # Host bus interface connection
        verilog += self.pyrift_host.host_bus_unwrap_connect()

        verilog += f"// interface connection (modport .hw) to unwrap module port\n"
        for if_name in self.get_verilog_interface_list():
            verilog += self.to_verilog_unwrap_interface_connect_by_name(if_name)

        verilog += "\n"

        verilog += f"endmodule // {self.module_name}_unwrap\n"
        return verilog

# ----------------------------------------------------------------------------

    def to_verilog_package(self):
        """
            Generate the verilog package file (containing enum definition)
        """
        pkg = ""
        pkg += self.pyrift_generation_message(self.user_notice)
        pkg += "// Autogeneration may overwrite this file :\n"
        pkg += "// You should probably avoid modifying this file directly\n"

        pkg += f"// ---------------------------------------------------------------------------\n"
        pkg += f"// package definition for {self.module_name} and {self.interface_name}\n"
        pkg += f"// ---------------------------------------------------------------------------\n"
        pkg += f"package {self.module_name}_pkg;\n"
        pkg += f"\n"
        # generate the typedef for all enum used
        for e in self.encode_list:
            # one typedef for each enum
            # first evaluate the number of bit needed
            if not issubclass(e, FieldEncode):
                raise RuntimeError(f"Field.encode is required to use FieldEncode (IntEnum not supported anymore)"\
                                   f" encode={e.__name__}:{e.__bases__}")
            enum_bit_length = e.get_bit_length()
            if e.get_desc(): # description is added if non empty
                pkg += ''.join([f"// Enum Description: {line}\n" for line in cleandoc(e.get_desc()).splitlines() if line])
            for val in e:
                if e.get_value_desc(val.value):
                    pkg += ''.join([ f"// Enum Description: {val.value}> {line}\n"\
                                     for line in cleandoc(e.get_value_desc(val.value)).splitlines()
                                     if line ])
            pkg += f"typedef enum logic [{enum_bit_length-1}:0] {{\n"
            pkg += ",\n".join([f"    {val.name} = {enum_bit_length}'d{val.value}" for val in e])
            pkg += f"\n"
            pkg += f"}} Tenum_{e.__name__};\n"
            pkg += f"\n"

        pkg += f"// --------------------------------------------------------------\n"
        for param in self.param_list:
            if param.desc != None:
                pkg += ''.join([f"// Param Description: {line}\n" for line in cleandoc(param.desc).splitlines() if line])
            pkg += f"localparam {param.name} = 'h{param.value:0x};\n"
            pkg += f"\n"

        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"// Address mapping information for regmap:\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG_MAP:
                pkg += f"// RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}\n"
                pkg += f"{'// 'if PyriftBase.svpkg_option['no_constant_for_base_address'] else ''}"
                pkg += f"localparam {entry.get_full_hw_name().upper()}__BASE_ADDRESS = 32'h{entry.address:08X};\n"
                pkg += f"localparam {entry.get_full_hw_name().upper()}__SIZE = 32'h{entry.size:08X};\n"
        pkg += f"\n"
        # decoding address range for top level : rif_host_bus_msb
        pkg += f"localparam {self.name.upper()}__HOST_BUS_DECODE_MSB = {self.rif_host_bus_msb};\n"
        # decoding address mask for top level : [rif_host_bus_msb:rif_addr_lsb]
        pkg += f"localparam {self.name.upper()}__HOST_BUS_DECODE_MASK = "\
               f"32'h{(1<<(self.rif_host_bus_msb+1))-(1<<self.rif_addr_lsb):0x};\n"
        pkg += f"\n"
        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"// Extra constant information on registers:\n"
        # iterate over all register inside map_table
        # (only first register of array is consider (other of same array have identical type)):
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # pkg details are provided only once for first item of array
                if entry.is_first_of_array():
                    pkg += f"// Register is : {entry.register.name}, hierarchy={entry.get_regmap_hierarchy(max_range=True)}\n"
                    pkg += entry.register.to_verilog_package(entry)
                    pkg += f"// - - - - -\n"
                if  entry.get_array_size_in_hierarchy() != 1:
                    # the register is part of array: add an extra constant for address of each instance
                    # (user request for bypassing reg_model and direct handling of reg addressing)
                    pkg += f"// Register is array: {entry.register.name}, hierarchy={entry.get_regmap_hierarchy(max_range=False)}\n"
                    pkg += entry.register.to_verilog_package_array_constant(entry)
                    pkg += f"// - - - - -\n"

        pkg += f"\n"
        pkg += f"// --------------------------------------------------------------\n"
        pkg += f"\n"
        pkg += f"endpackage\n"
        return pkg

# ----------------------------------------------------------------------------
#      ######  ##     ## ########    ###    ########
#     ##    ## ##     ## ##         ## ##   ##     ##
#     ##       ##     ## ##        ##   ##  ##     ##
#     ##       ######### ######   ##     ## ##     ##
#     ##       ##     ## ##       ######### ##     ##
#     ##    ## ##     ## ##       ##     ## ##     ##
#      ######  ##     ## ######## ##     ## ########

    def to_chead(self, wrap_define=True, with_define_constant=False):
        """
            Generate the C header file
        """

        # print(f"Chead generation for regmap={self.name}")
        # generate the structure mapping to ease C code writing using the RIF
        chead = ""
        chead += self.pyrift_generation_message(self.user_notice)
        chead += "// Autogeneration may overwrite this file :\n"
        chead += "// You should probably avoid modifying this file directly\n"
        chead += "// ------------------------------------------------------\n"
        if wrap_define:
            chead += f"#ifndef __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"#define __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"\n"
        chead += f"// C header definition for register map : {self.name}\n"
        chead += f"// module name :  {self.module_name}\n"
        chead += f"\n"
        chead += f"#include <stdint.h>\n"
        chead += f"\n"
        chead += f"// typedef enum for all enum field used in any register\n"

        # generate the typedef for all enum used
        for e in self.encode_list:
            chead += "// ---------------------------------------------\n"
            if e.get_desc(): # description is added if non empty
                chead += ''.join([f"// Enum Description: {line}\n" for line in cleandoc(e.get_desc()).splitlines() if line])
            for val in e:
                if e.get_value_desc(val.value):
                    chead += ''.join([ f"// Enum Description: {val.value}> {line}\n"\
                                       for line in cleandoc(e.get_value_desc(val.value)).splitlines() if line])
            if PyriftBase.chead_option["wrap_enum_with_ifdef"]:
                chead += f"#ifndef __CHEAD_ENUM_DEF_{e.__name__}\n"
                chead += f"#define __CHEAD_ENUM_DEF_{e.__name__}\n"
            chead += f"typedef enum te_{e.__name__} {{\n"
            chead += ",\n".join([f"    {val.name} = {val.value}" for val in e])
            chead += f"\n"
            chead += f"}} Tenum_{e.__name__};\n"
            if PyriftBase.chead_option["wrap_enum_with_ifdef"]:
                chead += f"#endif // __CHEAD_ENUM_DEF_{e.__name__}\n"


        # generate C header typedef for each register
        # iterate over all register inside map_table
        # (only first register of array is consider (other of same array have identical type)):
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # cheader details are provided only once for first item of array
                if entry.is_first_of_array():
                    chead += f"\n"
                    chead += f"// ##############################################################\n"
                    chead += f"// Register is : {entry.register.name}, hierarchy={entry.get_regmap_hierarchy(max_range=True)}\n"
                    chead += f"// ##############################################################\n"
                    chead += f"\n"
                    chead += entry.register.to_chead(
                        entry,
                        with_define_constant=with_define_constant,
                        define_prefix=self.module_name.upper()+"__")
                # for each item or array give a chance to add detail if special reset table require it
                if entry.get_array_size_in_hierarchy() > 1 and with_define_constant:
                    chead += entry.register.to_chead_reset_detail(
                            entry, define_prefix=self.module_name.upper()+"__")

        # generate C header typedef for each regmap
        # iterate over all regmap inside map_table
        # (only first regmap of array is consider (other of same array have identical type)):
        # list all regmap type needed in hierarchy
        chead += f"// ##############################################################\n"
        chead += f"// REGMAP Structure definition for hierarchy\n"
        chead += f"// ##############################################################\n"
        chead += f"\n"
        sub_regmap_list = [entry for entry in self.map_table if entry.type==MapTableType.REG_MAP]
        # print(f"Found regmap[{len(sub_regmap_list)}]")
        # define all struct in reverse ordered
        # (to have deeper in hierarchy first, and help c compile dependencies)
        for entry in reversed(sub_regmap_list):
            if entry.is_first_of_array():
                #print(f"TS list: {entry.register.name}")
                chead += self.to_chead_struct_by_regmap_name(entry.register.name)

        # iterate over all register/pad entry inside map_table
        # (only first register of array is consider (remaining of array are duplicated by array)):
        # Pad entry are expanded
        # chead += self.to_chead_struct_by_regmap_name(self.name)
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"// Parameter also put in verilog package are dump here\n"
        for param in self.param_list:
            if param.desc != None:
                chead += ''.join([f"// Param Description: {line}\n" for line in cleandoc(param.desc).splitlines() if line])
            chead += f"#define {param.name}  0x{param.value:0x}\n"
            chead += f"\n"

        chead += f"\n"
        if with_define_constant:
            chead += f"// --------------------------------------------------------------\n"
            chead += f"\n"
            chead += f"// Address mapping information for regmap:\n"
            for entry in self.map_table:
                if entry.type == MapTableType.REG_MAP:
                    chead += f"// RegisterMap: {entry.register.name}{entry.get_array_index_bracket()}\n"
                    chead += f"{'// 'if PyriftBase.chead_option['no_constant_for_base_address'] else ''}"
                    chead +=   f"#define {entry.get_full_hw_name().upper()}__BASE_ADDRESS 0x{entry.address:08X}\n"
                    chead += f"#define {entry.get_full_hw_name().upper().upper()}__SIZE 0x{entry.size:08X}\n"
            chead += f"\n"
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        if wrap_define:
            chead += f"#endif // __CHEAD_DEF_{self.module_name.upper()}\n"
            chead += f"\n"

        return chead

    def to_chead_struct_by_regmap_name(self, regmap_name):
        chead = ""
        # Check if regmap exist as array
        have_array = any(entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_ARRAY
                         for entry in self.map_table)
        # find first REG_MAP item in map_table (error StopIteration raised if not found)
        regmap_start_idx = next( n for n,entry in enumerate(self.map_table)
                                 if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP)
        # find first REG_MAP_END item in map_table (error StopIteration raised if not found)
        regmap_end_idx = next( n for n,entry in enumerate(self.map_table)
                                 if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_END)
        # print (f"Struct regmap:{regmap_start_idx}:{self.map_table[regmap_start_idx].register.name}"\
        #                      f"{self.map_table[regmap_start_idx].get_array_index_bracket()}")
        # print (f"Struct regmap end:{regmap_end_idx}:{self.map_table[regmap_start_idx].register.name}"\
        #                      f"{self.map_table[regmap_start_idx].get_array_index_bracket()}")

        chead += f"// map_table [{regmap_start_idx+1}:{regmap_end_idx}]\n"
        chead += f"typedef struct\n"
        chead += f"{{\n"
        entry_iter = iter(self.map_table[regmap_start_idx+1:regmap_end_idx])
        for entry in entry_iter:
            if entry.type == MapTableType.REG_MAP_ARRAY:
                array_range = entry.get_regmap_array(range=True, include_leaf_reg=False)
                array_name = entry.register.name
                array_entry = entry
                chead += f"    // ---------------------------------------------\n"
                chead += f"    // Sub RegisterMap Array: {array_name}{entry.layer_range}\n"
                chead += f"    // Addr = {entry.address}=0x{entry.address:04x}:0x{entry.address+entry.size:04x}, Size=+{entry.size}Bytes\n"
                while(entry.type != MapTableType.REG_MAP_ARRAY_END or entry.register.name != array_name):
                    entry = next(entry_iter)
                chead += f"    TS_{array_name} {array_name}{array_entry.get_layer_bracket(range=True)};\n"

            if entry.type == MapTableType.REG_MAP:
                regmap_entry = entry
                regmap_entry_name = entry.register.name
                chead += f"    // ---------------------------------------------\n"
                chead += f"    // Sub RegisterMap (non array): {regmap_entry.register.name}\n"
                chead += f"    // Addr = 0x{regmap_entry.address:04x}:0x{regmap_entry.address+regmap_entry.size:04x}, Size=+{entry.size}Bytes\n"
                while(entry.type != MapTableType.REG_MAP_END or entry.register.name != regmap_entry_name):
                    entry = next(entry_iter)
                chead += f"    TS_{regmap_entry.register.name} {regmap_entry.register.name};\n"
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                chead += f"    // Register is : {entry.register.name}, addr={entry.address}=0x{entry.address:04x}, "\
                                                f"Array{entry.layer_range}\n"
                chead += f"    TU_{entry.get_noarray_hw_name()} "
                # volatile option is now handled inside "register.to_chead()" call
                chead += f"{entry.register.name}"
                chead += f"{entry.get_layer_bracket(range=True)};\n"
            if entry.type == MapTableType.PRE_PAD:
                chead += f"    // Pre-Pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}Bytes => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % (PyriftBase.dw//8) != 0:
                    raise RuntimeError(f"Non {PyriftBase.dw}bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    uint{PyriftBase.dw}_t pad_{entry.address:04x}[{entry.size//(PyriftBase.dw//8)}];\n"
            if entry.type == MapTableType.POST_PAD:
                chead += f"    // Post-Pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}Bytes => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % (PyriftBase.dw//8) != 0:
                    raise RuntimeError(f"Non {PyriftBase.dw}bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    uint{PyriftBase.dw}_t pad_{entry.address:04x}[{entry.size//(PyriftBase.dw//8)}];\n"
            if entry.type == MapTableType.ALIGN_PAD:
                chead += f"    // -------------------------------------------------- \n"
                chead += f"    // Align pad for {entry.register.name}: addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}Bytes => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % (PyriftBase.dw//8) != 0:
                    raise RuntimeError(f"Non {PyriftBase.dw}bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    uint{PyriftBase.dw}_t pad_{entry.address:04x}[{entry.size//(PyriftBase.dw//8)}];\n"
            if entry.type == MapTableType.REGMAP_LENGTH_ALIGN_PAD:
                chead += f"    // Regmap Length Align Pad : addr={entry.address}=0x{entry.address:04x}, Size=+{entry.size}Bytes => "\
                         f"new addr=0x{entry.address+entry.size:0x}\n"
                if entry.size % (PyriftBase.dw//8) != 0:
                    raise RuntimeError(f"Non {PyriftBase.dw}bit pad @{entry.address:04x} : size={entry.size} bytes")
                chead += f"    uint{PyriftBase.dw}_t pad_{entry.address:04x}[{entry.size//(PyriftBase.dw//8)}];\n"

        # can add {self.map_table.get_root_entry().register.module_name}_ into struct name ?
        # Normally no name conflict accross different top reg map
        if have_array:
            regmap_array_entry = next( entry for entry in self.map_table
                                       if entry.register.name==regmap_name and entry.type==MapTableType.REG_MAP_ARRAY)
            #print (f"     regmap {regmap_name} IS array")
            chead += f"}} TS_{regmap_name};\n"
            chead += f"typedef TS_{regmap_name} TS_Array_{regmap_name}{regmap_array_entry.get_layer_bracket(range=True)};\n"
        else:
            #print (f"     regmap {regmap_name} IS NO array")
            chead += f"}} TS_{regmap_name};\n"
        chead += f"\n"

        return chead
# ----------------------------------------------------------------------------
#     ##     ## ##     ## ##     ##
#     ##     ## ##     ## ###   ###
#     ##     ## ##     ## #### ####
#     ##     ## ##     ## ## ### ##
#     ##     ##  ##   ##  ##     ##
#     ##     ##   ## ##   ##     ##
#      #######     ###    ##     ##

    def to_regmodel_uvm(self):
        """
            Generate the register model in uvm (for rif verification)
        """
        regmodel = ""
        regmodel += self.pyrift_generation_message(self.user_notice)
        regmodel += "// Autogeneration may overwrite this file :\n"
        regmodel += "// You should probably avoid modifying this file directly\n"
        regmodel += "// ------------------------------------------------------\n"
        regmodel += (
                "// uvm_reg_field:\n"
                "//\n"
                "//function void configure(      uvm_reg     parent,\n"
                "//  int     unsigned    size,\n"
                "//  int     unsigned    lsb_pos,\n"
                "//      string      access,\n"
                "//      bit     volatile,\n"
                "//      uvm_reg_data_t      reset,\n"
                "//      bit     has_reset,\n"
                "//      bit     is_rand,\n"
                "//      bit     individually_accessible     )\n"
                "\n"
                "//set_access\n"
                "//'RO'      W: no effect, R: no effect\n"
                "//'RW'      W: as-is, R: no effect\n"
                "//'RC'      W: no effect, R: clears all bits\n"
                "//'RS'      W: no effect, R: sets all bits\n"
                "//'WRC'     W: as-is, R: clears all bits\n"
                "//'WRS'     W: as-is, R: sets all bits\n"
                "//'WC'      W: clears all bits, R: no effect\n"
                "//'WS'      W: sets all bits, R: no effect\n"
                "//'WSRC'    W: sets all bits, R: clears all bits\n"
                "//'WCRS'    W: clears all bits, R: sets all bits\n"
                "//'W1C'     W: 1/0 clears/no effect on matching bit, R: no effect\n"
                "//'W1S'     W: 1/0 sets/no effect on matching bit, R: no effect\n"
                "//'W1T'     W: 1/0 toggles/no effect on matching bit, R: no effect\n"
                "//'W0C'     W: 1/0 no effect on/clears matching bit, R: no effect\n"
                "//'W0S'     W: 1/0 no effect on/sets matching bit, R: no effect\n"
                "//'W0T'     W: 1/0 no effect on/toggles matching bit, R: no effect\n"
                "//'W1SRC'   W: 1/0 sets/no effect on matching bit, R: clears all bits\n"
                "//'W1CRS'   W: 1/0 clears/no effect on matching bit, R: sets all bits\n"
                "//'W0SRC'   W: 1/0 no effect on/sets matching bit, R: clears all bits\n"
                "//'W0CRS'   W: 1/0 no effect on/clears matching bit, R: sets all bits\n"
                "//'WO'      W: as-is, R: error\n"
                "//'WOC'     W: clears all bits, R: error\n"
                "//'WOS'     W: sets all bits, R: error\n"
                "//'W1'      W: first one after HARD reset is as-is, other W have no effects, R: no effect\n"
                "//'WO1'     W: first one after HARD reset is as-is, other W have no effects, R: error\n"
                "\n"
            )

        classname = self.uvm_regmodel_classname if self.uvm_regmodel_classname else f"{self.name}_regmodel"

        # iterate over all register inside map table:
        # generate uvmreg model class for each register (only first of array is used)
        for entry in self.map_table:
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                regmodel += f"\n"
                regmodel += f"// ##############################################################\n"
                regmodel += f"// Register is : {entry.register.name}, hierarchy={entry.get_noarray_hw_name()}\n"
                regmodel += f"// ##############################################################\n"
                regmodel += f"\n"
                regmodel += entry.register.to_regmodel_uvm(entry)

        regmodel += f"// --------------------------------------------------------------\n"
        regmodel += f"//                  REGISTER MODEL\n"
        regmodel += f"// --------------------------------------------------------------\n"
        regmodel += f"\n"
        regmodel += f"class {classname} extends uvm_reg_block;\n"
        regmodel += f"\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG and entry.is_first_of_array():
                # TODO: array should expand here
                regmodel += f"   rand {entry.get_noarray_hw_name()}_reg "\
                            f"{entry.get_short_hw_name()} "\
                            f"{entry.get_array_range_definition()};\n"
        regmodel += f"\n"
        regmodel += f"   `uvm_object_utils({classname})\n"
        regmodel += f"\n"
        regmodel += f"   function new(string name = \"{classname}\");\n"
        regmodel += f"      super.new(name, UVM_NO_COVERAGE);\n"
        regmodel += f"   endfunction: new\n"
        regmodel += f"\n"
        regmodel += f"   virtual function void build();\n"
        regmodel += f"\n"
        # base address provided at 0 for create map, could be 'h{self.base_address:0x} ?
        # each reg have offset from 0
        regmodel += f"      default_map = create_map(\"reg_model_map\", 0, {PyriftBase.dw//8}, UVM_LITTLE_ENDIAN);\n"
        regmodel += f"\n"
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                short_regname= entry.get_short_hw_name()
                regname= entry.get_noarray_hw_name()
                regmodel += f"      {short_regname}{entry.get_array_index_bracket()} = {regname}_reg::type_id::create(\"{short_regname}\",,get_full_name());\n"
                regmodel += f"      {short_regname}{entry.get_array_index_bracket()}.configure(this, null, \"\");\n"
                # TODO: will have to call *.set_reset to define the reset value of register is changing through array of register
                # if entry.get_array_size_in_hierarchy() > 1 and f.reset and not isinstance(f.reset, int):
                #     regmodel += f"      {regname}{entry.get_array_index_bracket()}.set_reset({entry.register.get_reset_value(map_table_entry):08X}, "HARD");\n"

                # explicit set the hdl path of instance (to support array case)
                regmodel += f"      {short_regname}{entry.get_array_index_bracket()}.set_reg_hdl_path(\"{entry.get_full_hw_name()}\");\n"

                # explicit set the field reset value of instance (to support array case)
                for field in entry.register.get_all_actual_field():
                    field_name = field.get_name()
                    regmodel += f"      {short_regname}{entry.get_array_index_bracket()}."\
                                f"set_{field_name}_field_reset_value('h{field.get_reset_value(entry):0x});\n"

                regmodel += f"      {short_regname}{entry.get_array_index_bracket()}.build();\n"
                regmodel += f"      {short_regname}{entry.get_array_index_bracket()}.reset();\n"
                regmodel += f"      default_map.add_reg({short_regname}{entry.get_array_index_bracket()},`UVM_REG_ADDR_WIDTH'h{entry.address:0x},\"RW\");\n"
                regmodel += f"\n"

        regmodel += f"   endfunction: build\n"
        regmodel += f"endclass : {classname}\n"

        return regmodel;

# ----------------------------------------------------------------------------
# Helper functions for user to write output file with auto creation of directory

    def write_chead(self, filename, wrap_define=True, with_define_constant=False):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_chead(
                wrap_define=wrap_define, with_define_constant=with_define_constant))

    def write_html(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_html())

    def write_svd(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        svd_tree = self.to_svd_tree()
        # Check version of python for SVD indentation : only available in version 3.9+
        if sys.hexversion >= 0x3090000:
            print(f"Note: SVD indentation using ElementTree indent function used")
            print(f"      Supported since version 3.9 alpha of python :")
            print(f"      Now using {platform.sys.version}")
            ET.indent(svd_tree)
        else:
            print(f"Note: To get pretty SVD indentation please use python version > 3.9a5")
        svd_tree.write(filename, xml_declaration=True)

    def write_markdown(self, filename, variant='gitlab'):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_markdown(variant))

    def write_map_table(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_map_table())

    def write_verilog_unwrap(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_unwrap())

    def write_verilog_module(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_module())

    def write_verilog_package(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_package())

    def write_verilog_interface(self, filename):
        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_verilog_interface())

    def write_regmodel_uvm(self, filename):
        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.to_regmodel_uvm())

    def write_version(self, filename):
        # create directory for file target if it does not exist
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, "w") as text_file:
            text_file.write(self.pyrift_generation_message(self.user_notice, force_version_and_gen_info=True))

    def report_excel_import_error(self):
        if(xlsxwriter_import_error):
            print("#####")
            print("write_excel() call requires xlsxwriter library to be installed")
            print("You can try : 'pip install XlsxWriter', possibly with '--user' or check pyrift wiki ...")
            print("#####")
            raise RuntimeError('XlsxWriter_import_error')

    def write_excel(self, filename):
        # Create an excel register summary with one column per bit, filled with each register field
        # Check for import error
        # at startup, if xlsxwriter is missing, error is catch and
        # pyrift silently continue (import not needed if no excel generated)
        # Now if trying to generate excel document we are in trouble
        # So we report error now
        self.report_excel_import_error()

        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        workbook = xlsxwriter.Workbook(filename)
        worksheet = workbook.add_worksheet()
        worksheet.set_landscape()
        worksheet.set_paper(9) # set to A4
        worksheet.set_margins(left=0.4, right=0.4, top=0.45, bottom=0.4) # unit as inch
        worksheet.repeat_rows(1) # bit numbering rule repeat on next page
        worksheet.fit_to_pages(1, 0)  # 1 page wide and as long as necessary.
        bold_header_left_format = workbook.add_format({'bold': True})
        bold_header_left_format.set_align('left')
        bold_header_right_format = workbook.add_format({'bold': True})
        bold_header_right_format.set_align('right')
        bold_header_center_format = workbook.add_format({'bold': True})
        bold_header_center_format.set_align('center')
        title_format = workbook.add_format({'bold': True, 'font_size': 24})

        # first row are regmap generic information + header common to all registers
        # Row 0 is Title
        worksheet.write_string(0,0, self.module_name,title_format)
        # Row 1 is Header line
        worksheet.write_string(1,0, "Register Name", bold_header_right_format)
        worksheet.set_column(0, 0, PyriftBase.xls_option["column_regname_width"])  # Width of column set.

        worksheet.write_string(1,1, "Address")
        worksheet.set_column(1, 1, 10)  # Width of column set.
        worksheet.set_row(1, None,workbook.add_format({'bold': True}))
        # prepare one column per bit of DW (start with MSB)
        worksheet.set_column(2, 2+PyriftBase.dw-1, PyriftBase.xls_option["column_bit_width"])  # Width of columns set.

        for column_offset,bit_index in enumerate(range(PyriftBase.dw-1, -1, -1)):
            worksheet.write_number(1,2+column_offset, bit_index, bold_header_center_format)

        # Header for R/w column
        worksheet.write_string(1,2+PyriftBase.dw, "r/w", bold_header_center_format)
        worksheet.set_column(2+PyriftBase.dw, 2+PyriftBase.dw, 5)  # Width of column set.
        # Header for Description column
        worksheet.write_string(1,2+PyriftBase.dw+1, "Reset", bold_header_left_format)
        # Width of reset column is set (12 for 32 bits, smaller for 16/8 bit).
        worksheet.set_column(2+PyriftBase.dw+1, 2+PyriftBase.dw+1, 12 if PyriftBase.dw>=32 else 8)

        worksheet.write_string(1,2+PyriftBase.dw+2, "Description", bold_header_left_format)
        worksheet.set_column(2+PyriftBase.dw+2, 2+PyriftBase.dw+2, PyriftBase.xls_option["column_desc_width"])  # Width of column set.

        # fill excel document here ...
        # iterate over all register inside map_table:
        current_row = 2
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"generate excel row for register : {entry.register.name}")
                row_count = entry.register.fill_excel_row(
                    workbook=workbook,
                    worksheet=worksheet,
                    row=current_row,
                    column_b0=2+PyriftBase.dw-1,
                    entry=entry)
                current_row = current_row + row_count # next register will be in next row

        workbook.close()


    def write_excel_ref(self, filename):
        # Create an excel register reference with one line per field of register
        # Check for import error
        # at startup, if xlsxwriter is missing, error is catch and
        # pyrift silently continue (import not needed if no excel generated)
        # Now if trying to generate excel document we are in trouble
        # So we report error now
        self.report_excel_import_error()

        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        workbook = xlsxwriter.Workbook(filename)
        worksheet = workbook.add_worksheet()
        worksheet.set_landscape()
        worksheet.set_paper(9) # set to A4
        worksheet.set_margins(left=0.4, right=0.4, top=0.45, bottom=0.4) # unit as inch
        worksheet.repeat_rows(1) # bit numbering rule repeat on next page
        worksheet.fit_to_pages(1, 0)  # 1 page wide and as long as necessary.
        bold_header_left_format = workbook.add_format({'bold': True})
        bold_header_left_format.set_align('left')
        bold_header_right_format = workbook.add_format({'bold': True})
        bold_header_right_format.set_align('right')
        bold_header_center_format = workbook.add_format({'bold': True})
        bold_header_center_format.set_align('center')
        title_format = workbook.add_format({'bold': True, 'font_size': 24})

        # first row are regmap generic information + header common to all registers
        # Row 0 is Title
        worksheet.write_string(0,0, self.module_name,title_format)
        # Row 1 is Header line
        worksheet.set_row(1, None,workbook.add_format({'bold': True}))

        worksheet.write_string(1,0, "Address")
        worksheet.set_column(1, 0, 10)  # Width of column set.
        worksheet.write_string(1,1, "Register Name", bold_header_right_format)
        worksheet.set_column(1, 1, PyriftBase.xls_option["column_regname_width"])  # Width of column set.
        worksheet.write_string(1,2, "Field Name")
        worksheet.set_column(2, 2, 30)  # Width of column set.
        # Header for R/w column
        worksheet.write_string(1,3, "r/w", bold_header_center_format)
        worksheet.set_column(3, 3, 5)  # Width of column set.
        worksheet.write_string(1,4, "BW")
        worksheet.set_column(4, 4, 4)  # Width of column set.
        worksheet.write_string(1,5, "Msb")
        worksheet.set_column(5, 5, 4)  # Width of column set.
        worksheet.write_string(1,6, "Lsb")
        worksheet.set_column(6, 6, 4)  # Width of column set.
        # Header for Description column
        worksheet.write_string(1,7, "Reset", bold_header_left_format)
        # Width of reset column is set (12 for 32 bits, smaller for 16/8 bit).
        worksheet.set_column(7, 7, 12 if PyriftBase.dw>=32 else 8)

        worksheet.write_string(1,8, "Description", bold_header_left_format)
        worksheet.set_column(8, 8, PyriftBase.xls_option["column_desc_width"])  # Width of column set.

        # fill excel document here ...
        # iterate over all register inside map_table:
        current_row = 2
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                for field in entry.register.get_all_actual_field():
                    # print (f"generate excel row for register/field : {entry.register.name}/{field.name}")
                    entry.register.fill_excel_ref_row(
                        workbook=workbook,
                        worksheet=worksheet,
                        row=current_row,
                        entry=entry,
                        register=entry.register,
                        field=field)
                    current_row = current_row + 1 # next register/field will be in next row

        workbook.close()


    def write_word(self, filename, template=""):
        # Check for import error
        # at startup, if python-docx is missing, error is catch and
        # pyrift silently continue (import not needed if no word generated)
        # Now if trying to generate word document we are in trouble
        # So we report error now
        if(docx_import_error):
            print("#####")
            print("write_word() call requires python-docx library to be installed")
            print("You can try : 'pip install python-docx' or check pyrift wiki ...")
            print("#####")
            raise RuntimeError(docx_import_error)

        # create directory for file target if it does not exist
        if len(os.path.dirname(filename))>0:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

        # default template is available from pyrift delivery
        default_word_template = self.pyrift_dirname+"/doc_template/pyrift_template.docx"

        if(not template):
            # no user template provided, used pyrift default
            template = default_word_template
        print(f"Generating word documentation from template :{template}")

        document = Document(template)

        # fill word document here ...
        # iterate over all register inside map_table:
        for entry in self.map_table:
            if entry.type == MapTableType.REG:
                # print (f"word document register : {entry.register.name}")
                entry.register.document_to_word(document, entry)


        document.save(filename)

# ----------------------------------------------------------------------------

# basic test code for module if not imported
if __name__ == "__main__":
    import os
    os.makedirs("output", exist_ok=True)

    # Normal 1 bit, RW
    frw = Field(name="Frw", bit_range='0', desc="1 bit, RW")
    frw2 = Field(name="Frw_2", bit_range='3:2', desc="2 bit, RW")
    fw1c = Field(name="FW1C", bit_range='4')
    fw1c.desc = "1 bit, W1C"
    fw1c.woclr = True
    fw1c2 = Field(name="FW1C_2", bit_range='7:6')
    fw1c2.desc = "2 bit, W1C"
    fw1c2.woclr = True

    fw1s = Field(name="FW1S", bit_range='12')
    fw1s.desc = "1 bit, W1S"
    fw1s.woset = True
    fw1s2 = Field(name="FW1S_2", bit_range='15:14')
    fw1s2.desc = "2 bit, W1S"
    fw1s2.woset = True

    # Test register, mix of different bit flavour
    reg = Register("Rcheck")
    reg.add(frw)
    reg.add(frw2)
    reg.add(fw1c)
    reg.add(fw1c2)
    reg.add(fw1s)
    reg.add(fw1s2)

    pad_reg = Register("Empty_Pad_Register")

    # register will not validate_check if not inserted inside registermap
    # reg.validate_check()

    # reg2 is not validate_check before adding to reg map
    reg_tab = [None] * 10
    for i in range(1, 10):
        reg_tab[i] = Register("R%d" % i)
        reg_tab[i].add(Field(name=f"F{i}rw", bit_range='0', desc="1 bit, RW"))
        reg_tab[i].add(Field(name=f"F{i}rw_2", bit_range='3:2', desc="2 bit, RW"))
    reg_tab[3].shortname = "ShrtR3"

    reg3 = Register("R3")
    reg3.add(Field(name="Frw", bit_range='0', desc="1 bit, RW"))

    reg_map2_1 = RegisterMap("register_map_test_2_1")
    reg_map2_1.add(reg_tab[7])
    reg_map2_1.add(reg_tab[8])

    reg_map2 = RegisterMap("register_map_test_2")
    reg_map2.add(reg_tab[5])
    reg_map2.add(reg_tab[6])
    reg_map2.add(reg_map2_1)

    reg_map = RegisterMap("register_map_test")
    reg_map.add(reg_tab[1])
    reg_map.add(reg_tab[2], align=8)
    reg_map.add(reg_tab[3])
    reg_map.add(reg_map2)
    reg_map.validate()
    try:
        # check adding twice a register is illegal
        reg_map.add(reg_tab[5])
    except RuntimeError:
        # print("successfully detected expected exception")
        pass
    else:
        raise RuntimeError("Missing exception on multiple register added")

    reg_map.write_html("output/reg_map.html")

    # update reg map, then can iterate over the list of all registers
    reg_map.base_address = 0x100
    # with open("output/regmap.sv", "w") as text_file:
    #     text_file.write(reg_map.to_verilog_decoder())
    # with open("output/all_reg.sv", "w") as text_file:
    #     text_file.write(reg_map.to_verilog_register())
    #     text_file.write(reg_map.to_verilog_connect_interface())

    reg_map.write_verilog_interface("output/if_rif.sv")
    reg_map.write_verilog_module("output/rif.sv")
    reg_map.write_chead("output/rif.h")

# ----------------------------------------------------------------------------
