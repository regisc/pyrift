# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_signal.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase
# ----------------------------------------------------------------------------


class Signal(PyriftBase):
    """
        Signal serve as declaration of extra input/output signal (like user reset signal)
    """

    def __init__(self, name,
        # bring all field to constructor parameter with default value
        # should help user for concise Field creation
        desc=None,
        width=1,
        sync=True,
        activelow=False,
        field_reset=False,
        ):
        super().__init__()
        self.name = name

        # desc is provided as constructor "desc" param
        # else carry default setting from base class
        if desc:
            self.desc = desc
        self.width = width

        #
        self.sync = sync # synchronous to clock
        self.activelow = activelow # active signal when low
        self.field_reset = field_reset # only one single signal can have field_reset set to True
                                       # If field_reset is True, then this signal is used as defaut
                                       # reset signal for all field not overloading this

    # when signal is used to generate reset, hide polarity and sync flavour behind method
    # typical flop is  ...
    # always_ff @(posedge clk or posedge {self.name}) begin // activehigh async reset signal
    # always_ff @(posedge clk or negedge {self.name}) begin // activelow  async reset signal
    # always_ff @(posedge clk) begin                        // synchronous reset signal (no edge sensitivity on reset)
    #
    #    // if condition used only if flop have reset
    #    if({self.name}) begin // Active High reset
    #    if(~{self.name}) begin // Active Low reset
    #
    #       flop_reg <= flop reset value...;
    #    end else
    #
    #    begin // code below with or without reset
    #       flop_reg <= ...;

    def reset_edge(self):
        """ report the reset condition to add in flop definition using this signal as reset
             "or posedge {self.name}" : activehigh async reset signal
             "or negedge {self.name}" : activelow  async reset signal
             ""                       : synchronous reset signal (no edge sensitivity on reset)
        """
        if self.activelow:
            result = f" or negedge {self.name}"
        else:
            result = f" or posedge {self.name}"
        if self.sync:
            result = "" # sync reset have no edge condition
                                 # (only react to clock edge)
        return result

    def high_name(self):
        """ either active high or low active reset, return signal name, posibly inverted to get as if active high
        """
        if self.activelow:
            result = f"~{self.name}"
        else:
            result = f"{self.name}"
        return result

    def low_name(self):
        """ either active high or low active reset, return signal name, posibly inverted to get as if active high
        """
        if self.activelow:
            result = f"{self.name}"
        else:
            result = f"~{self.name}"
        return result

# ----------------------------------------------------------------------------
