# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_register.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase, Field, Pad_Field, Cdc, Access
from pyrift_maptableentry import MapTableEntry, MapTableType
import itertools
import re
from functools import reduce
import sys

from inspect import cleandoc

# library to support xml generation
import xml.etree.ElementTree as ET

# if available docx import already in registermap
# possible error is ModuleNotFoundError: if not already imported
try:
    from docx.shared import Cm
except ModuleNotFoundError:
    pass # if no docx import done already in registermap, not needed
         # (no word generation will take place)

# ----------------------------------------------------------------------------

class Register(PyriftBase):
    """
        Register is collection of Field
        To create register : constructor, then add Fields, finish by validate_check_premap()
        field_list is the collection of usefull added fields
        pad_list is filled with all padding needed around field_list.
        pad_list is populated on validate_check
        (before validate_check_premap, pad_list is clear, in case validate_check_premap is called again)
    """

    def __init__(self,
        name=None,
        shortname=None,
        desc=None,
        array=None,
        external=False,  # no internal rtl implementation if external
        align=0,
        pre_pad=0,
        post_pad=0,
        sw_signed=False, # unsigned view from cheader is default
        sw_type=None, # default to [u]int(32|16|8)_t
        ispresent=True):
        super(Register, self).__init__()
        if (isinstance(name, Field)):
            # name is provided as field
            # This is a shortcut for a register with single field,
            # using same name for register as the provided field
            # shortname of register is name of Field
            if desc==None:
                desc=name.desc # using field for quick reg constructor, reuse desc as well
            self.__init__(
                name=None,
                shortname=name.name,
                desc=desc,
                array=array,
                external=external,
                align=align,
                pre_pad=pre_pad,
                post_pad=post_pad,
                sw_signed=sw_signed,
                sw_type=sw_type,
                ispresent=ispresent)
            self.add(name) # add field provided as name to the constructing register
        else:
            # normal constructor
            if shortname:
                # shortname provided in __init__, make default name
                self.name = name if name else shortname
                self.shortname = shortname
            else:
                if name:
                    self.name = name
                else:
                    raise RuntimeError("Register constructor makes name or shortname mandatory !")
                self.shortname = None # shortcut for verilog naming (interface and rtl)
                                      # /!\warning, if used this limit to single instance of register
                                      # with this shortname in all regmap
                                      # (otherwise you get name collision)
            # if shortname is defined, this is a short alias for full register hierarchy
            # This is allowed only if no collision with other register instance can occur

            # description is to be provided either in constructor or later
            if desc:
                # print("setting description to ", desc)
                self.desc = desc
            else:
                # not desc from constructor, just keep empty string
                self.desc = ""
            self.array = array
            self.sw_signed = sw_signed
            self.sw_type = sw_type
            self.ispresent = ispresent
            self.external = external

            # Main goal of register is to collect list of field
            self.field_list = []
            self.pad_list = []

            # map attribute : pad and align
            # Alignement is done before padding
            # - alignment padding
            # - pre_pad
            # - actual register
            # - post_pad
            self.pre_pad = pre_pad
            self.post_pad = post_pad
            if align==0:
                # use 0 as default parameter value for align
                # adjust to DW that may have been set differently between def of function and now
                # (DW as default param may have taken old DW before user change it)
                self.align = PyriftBase.dw//8 # default for current dw bits architecture
            else:
                self.align = align

            # current data width is supported (8,16,32)
            if(not PyriftBase.dw in set([8,16,32])):
                raise RuntimeError(
                    f"Data bus width is mandatory to be 8, 16, or 32, found unexpected dw={PyriftBase.dw}")

            # parent is used when register is included in a parent container
            # allow check for single parent inclusion
            self.parent = None

    def add(self, field):
        if(field.parent is not None):
            # field is already owned by another parent register
            # field sharing by register is not supported
            raise RuntimeError("Adding field which already have a parent register: Sharing is not supported")
        # print(f"adding field {field.name} to {self.name} parent register.")
        field.parent = self

        if (not isinstance(field, Field)):
            raise RuntimeError("Unexpected field added : not a Field")
        self.field_list.append(field)

    def get_all_field(self):
        all_field_list = self.get_all_actual_field() + self.pad_list
        all_field_list.sort(key=lambda x: x.lsb_bit)
        return all_field_list

    def get_all_actual_field(self, sort=True):
        all_field_list = [f for f in self.field_list if f.ispresent] # exclude self.pad_list and not present field
        if sort:
            all_field_list.sort(key=lambda x: x.lsb_bit)
        return all_field_list

    def get_field_by_name(self, name):
        for f in self.field_list: # exhaustive list, include not ispresent
            if name == f.name:
                return f
        return None

    def get_encode_list(self):
        return [field.encode for field in self.get_all_actual_field() if field.encode]

    def get_oneline_desc(self):
        reg_oneline_description = self.desc.replace("\r\n", "\n").splitlines()
        if len(reg_oneline_description)>=1:
            reg_oneline_description = reg_oneline_description[0].strip()
        else:
            reg_oneline_description = ""
        return reg_oneline_description

    def is_volatile(self, map_table_entry):
        # register is considered volatile if any field is volatile
        # print(f"reg {self.name} is volatile : {[f.is_volatile() for f in self.get_all_field()]}")
        return any([f.is_volatile(map_table_entry) for f in self.get_all_field()])

    def is_sw_read_only(self):
        # register is considered volatile if no field is writable
        # read only field is self.sw = Access.R (access.NA is not writable either (not legal ?))
        return all([not f.sw.is_writable() for f in self.get_all_field()])

    def get_sw_access(self):
        # register access is "or" operator applied on all field sw access
        return reduce(Access.__or__, [f.sw for f in self.get_all_field()])

    def get_ctype(self):
        int_type_option = "int" if self.sw_signed else "uint"
        reg_type = self.sw_type if self.sw_type else f"{int_type_option}{PyriftBase.dw}_t"
        return reg_type


    def get_interface_name(self):
        """
            register is part of a register hierarchy,
            Search first ancestor that have an interface_name
            (to be used for verilog interface connection)
            root parent may have several interface, so find out where it belong
        """
        parent = self # initial search point for interface_name is current register
        # print("IF search starting: for field",self.name, "p->", parent.name )
        while not getattr(parent, 'interface_name', None):
            if parent.parent:
                parent = parent.parent
            else:
                # still no interface name, and no more parent (reach root !!?)
                raise RuntimeError(f"unable to find interface name for register {self.name}")
            # print("IF search: ",parent.name)
        # print(f"Found interface {parent.interface_name}")
        return parent.interface_name

    def get_root_parent(self):
        parent = self
        while parent.parent:
            # progress through parent tree to root parent (parent is None)
            parent = parent.parent
        return parent

    def get_map_table(self, parent, current_address):
        # map table is a list of register mapped at correct address for this register
        # list can contain multiple entry in case of array attribute on register, or pad or align
        map_table = []

        # main regmap_entry is created early, so it can be pointed as related_entry
        main_entry = MapTableEntry()

        # ALIGN
        if(self.align != 0):
            #check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.align % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"register is aligned with non multiple of byte width:{PyriftBase.dw//8} (align={self.align})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            # get sure the current address is aligned
            if(current_address % self.align != 0):
                # mis-aligned, first re-align before
                aligned_address = current_address - current_address % self.align
                # then add one align step to re-align properly
                aligned_address += self.align
                # re-align from current_address to aligned_address
                entry = MapTableEntry()
                entry.type = MapTableType.ALIGN_PAD
                entry.parent = parent
                entry.related_entry = main_entry
                entry.address = current_address
                entry.register = self # relative register for pad
                entry.size = aligned_address - current_address
                entry.layer = parent.layer+1
                entry.layer_range = () # no array on align or padding
                entry.layer_index = () # no array on align or padding
                map_table.append(entry)
                current_address += entry.size
        # Pre-PAD
        if(self.pre_pad != 0):
            #check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.pre_pad % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"register with pad non multiple of byte width:{PyriftBase.dw//8} (pad={self.pre_pad})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            entry = MapTableEntry()
            entry.type = MapTableType.PRE_PAD
            entry.parent = parent
            entry.related_entry = main_entry
            entry.address = current_address
            entry.register = self # relative register for pad
            entry.size = self.pre_pad
            entry.layer = parent.layer+1
            entry.layer_range = () # no array on align or padding
            entry.layer_index = () # no array on align or padding
            map_table.append(entry)
            # print(f"Adding pad entry @={current_address} size={entry.size}")
            current_address += entry.size
        # REGISTER entry
        if self.array:
            array_entry = main_entry
            array_entry.type = MapTableType.REG_ARRAY
            array_entry.parent = parent
            parent.child_list.update( {f"{self.name}[]" : array_entry} )
            array_entry.register = self
            array_entry.address = current_address
            # array_entry.size = TBD after end of array map
            array_entry.layer = parent.layer+1
            array_entry.layer_range = self.array
            array_entry.layer_index = (0,)*len(self.array)

            # if array is specified as integer, it is converted to a one dimension tuple in validate()
            if not type(self.array) is tuple:
                raise RuntimeError(f"array {self.array} expected to be of type tuple (not {type(self.array)}) (or converted from int for user)")
            if any([s < 2 for s in self.array]):
                raise RuntimeError(f"any array dimension is required larger than 2 : not OK for {self.array} of {self.name}")

            # prepare iteration (but only for current level of register (ignore upper layer regmap))
            reg_array_range_list = [range(i) for i in self.array]

            map_table.append(array_entry)

            # build linear index in 1d while browsing the multidim array
            # find start linear index in regmap[]xReg[]
            # base for linear depend on position in regmap hierarchy if within an array of regmap
            linear_index = array_entry.get_array_layer_size() * parent.get_regmap_linear_index(include_leaf_reg=False)
            for array_index in itertools.product(*reg_array_range_list):
                # itertools.product provide an iterable
                # print("REGISTER array_index=", array_index)
                entry = MapTableEntry()
                entry.type = MapTableType.REG
                entry.parent = parent
                parent.child_list.update( {f"{self.name}[{','.join([str(i) for i in array_index])}]" : entry} )

                entry.address = current_address
                entry.register = self
                entry.size = PyriftBase.dw//8
                entry.layer = parent.layer+1
                entry.layer_range = self.array
                entry.layer_index = tuple(array_index)
                map_table.append(entry)
                current_address += entry.size
                entry.linear_index = linear_index
                linear_index += 1
            # fix array_entry size (already inside map_table)
            array_entry.size = current_address - array_entry.address

            # TODO: add REG_ARRAY_END entry ...
        else:
            entry = main_entry
            entry.type = MapTableType.REG
            entry.parent = parent
            parent.child_list.update( {f"{self.name}" : entry} )
            entry.address = current_address
            entry.register = self
            entry.size = PyriftBase.dw//8
            entry.layer = parent.layer+1
            entry.layer_range = ()
            entry.layer_index = ()
            map_table.append(entry)
            current_address += entry.size
        # Post-PAD
        if(self.post_pad != 0):
            #check align is multiple of dw (8/16/32 bits architecture are supported)
            if self.post_pad % (PyriftBase.dw//8) != 0:
                raise RuntimeError(
                    f"register with pad non multiple of byte width:{PyriftBase.dw//8} (pad={self.post_pad})\n"
                    f"Only 8/16/32 bit architecture are supported !")
            entry = MapTableEntry()
            entry.type = MapTableType.POST_PAD
            entry.parent = parent
            entry.related_entry = main_entry
            entry.address = current_address
            entry.register = self # relative register for pad
            entry.size = self.post_pad
            entry.layer = parent.layer+1
            entry.layer_range = () # no array on align or padding
            entry.layer_index = () # no array on align or padding
            map_table.append(entry)
            current_address += entry.size
        #
        # print(f"(Reg.add : {self.name} {map_table})")
        return map_table, current_address

    def to_html(self, map_table_entry):
        data_format = '%%0%dX' % (PyriftBase.dw/4)
        html = ""
        html +=  "<style>table, th, td {border: 1px solid black;}</style>\n"
        html += f"<caption>Register name: {self.name}</caption><br>\n"
        html += f"<caption>Register fullname: {map_table_entry.get_regmap_hierarchy()}</caption><br>\n"
        html += f"<caption>Register reset value: 0x{data_format % self.get_reset_value(map_table_entry)}</caption><br>\n"
        html += f"<caption>Reset valid mask: 0x{data_format % self.get_reset_valid_mask(map_table_entry)}</caption><br>\n"
        # check for array with special non constant reset
        if map_table_entry.get_array_size_in_hierarchy() > 1 and\
                any([f.reset and not isinstance(f.reset, int) for f in self.get_all_field()]):
            reset_table = [ f"0x{map_entry.register.get_reset_value(map_entry):x}" \
                            for map_entry in map_table_entry.get_all_register_array_entry()]
            html += f"<caption>Register array of reset value is : "
            html += ', '.join(reset_table)
            html += f"</caption><br>\n"

        reg_description = self.desc.replace("\r\n", "\n").replace("\n","<br>")
        if self.desc:
            # print("desc=", reg_description)
            html += f"Register description: {reg_description}\n"
        html +=  "<table>\n"
        html +=  "  <tr>\n"
        html +=  "    <th>Bit</th>\n"
        html +=  "    <th>Name</th>\n"
        html +=  "    <th>SW</th>\n"
        html +=  "    <th>HW</th>\n"
        html +=  "    <th>Reset</th>\n"
        html +=  "    <th>Store</th>\n"
        html +=  "    <th>Cdc</th>\n"
        html +=  "    <th>If</th>\n"
        html +=  "    <th>Desc</th>\n"
        html +=  "  </tr>\n"
        for f in self.get_all_actual_field(): # skip padding field
            html +=  "  <tr>\n"
            html += f"    <td>{f.bit_range_str()}</td>\n" # bit column in table
            field_name = f"{f.name}"
            if f.name_in_if:
                field_name += f",<br>name_in_if:<br>{f.name_in_if}"
            html += f"    <td>{field_name}</td>\n" # Name column in table
            sw_option = ""
            if f.woclr: sw_option+=", woclr"
            if f.woset: sw_option+=", woset"
            if f.wot: sw_option+=", wot"
            if f.wzc: sw_option+=", wzc"
            if f.wzs: sw_option+=", wzs"
            if f.wzt: sw_option+=", wzt"
            if f.wclr: sw_option+=", wclr"
            if f.wset: sw_option+=", wset"
            if f.rclr: sw_option+=", rclr"
            if f.rset: sw_option+=", rset"
            html += f"    <td>{f.sw.name+sw_option}</td>\n" # SW column in table
            html += f"    <td>{f.hw.name}</td>\n" # HW column in table
            # Reset column in table
            if f.is_valid_reset_value(map_table_entry):
                html += f"    <td>0x{f.get_reset_value(map_table_entry):0x}</td>\n"
            else:
                html += f"    <td>X</td>\n"
            html += f"    <td>{f.get_storage_type_str(map_table_entry)}</td>\n" # Storage type for field
            cdc_option = []
            if f.cdc_in!=Cdc.NA: cdc_option.append(f"IN={f.cdc_in.name}")
            if f.cdc_out!=Cdc.NA: cdc_option.append(f"OUT={f.cdc_out.name}")
            cdc_option = ",<br>".join(cdc_option)

            html += f"    <td>{cdc_option}</td>\n" # HW column in table
            html += f"    <td>{f.get_interface_name()}</td>\n" # IF column in table
            description = f.desc.replace("\r\n", "\n").replace("\n","<br>")
            if f.encode:
                enum_description = ""
                enum_description += f"{f.name} is using enum encoding: {f.encode.__name__}\n"
                if f.encode.get_desc(): # description is added if non empty
                    enum_description += ''.join([ f":{line}\n" \
                                                  for line in cleandoc(f.encode.get_desc()).splitlines()])
                for val in f.encode:
                    if f.encode.get_value_desc(val.value):
                        enum_description += ''.join([ f":{val.value}>{line}\n"\
                                                      for line in cleandoc(f.encode.get_value_desc(val.value)).splitlines() ])
                description += "<br>"
                description += enum_description.replace("\r\n", "\n").replace("\n","<br>")
            if map_table_entry.get_array_size_in_hierarchy() > 1 and f.reset and not isinstance(f.reset, int):
                description += "<br>"
                description += "Array have reset value table :<br>"
                rst_str = ', '.join([f"0x{rst:0x}" for rst in f.reset])
                description += f"Reset : {rst_str}<br>"

            html += f"    <td>{description}</td>\n" # description column in table
            html += f"  </tr>\n"
        html += "</table>\n"
        return html

    def svd_fill_register(self, map_table_entry, register_et, cluster_base_address):

        # name    String to identify the register.
        #         Register names are required to be unique within the scope of a peripheral.
        #         You can use the placeholder %s, which is replaced by the dimIndex substring.
        #         Use the placeholder [%s] only at the end of the identifier to generate arrays in the header file.
        #         The placeholder [%s] cannot be used together with dimIndex.
        ET.SubElement(register_et, 'name').text = map_table_entry.register.name +\
            ('[%s]' if self.array else '')

        # <size>, <access>, <resetValue>, and <resetMask> are mandatory on register level. (if not defined above)
        # size is in bit unit of register
        ET.SubElement(register_et, 'size').text = str(PyriftBase.dw)
        # register is basically always readable (pad are read as 0, not explicit read protection exist in pyrift)
        if self.is_sw_read_only():
            ET.SubElement(register_et, 'access').text = 'read-only'
        else :
            ET.SubElement(register_et, 'access').text = 'read-write'
        ET.SubElement(register_et, 'resetValue').text = hex(self.get_reset_value(map_table_entry))
        ET.SubElement(register_et, 'resetMask').text = hex(self.get_reset_valid_mask(map_table_entry))

        #
        # displayName When specified, then this string can be used by a graphical frontend to visualize the register.
        #             Otherwise the name element is displayed.
        #             displayName may contain special characters and white spaces.
        #             You can use the placeholder %s, which is replaced by the dimIndex substring.
        #             Use the placeholder [%s] only at the end of the identifier.
        #             The placeholder [%s] cannot be used together with dimIndex.

        # description String describing the details of the register.
        register_desc = self.desc
        if not register_desc: # description is kind of mandatory to remove verbose info in SVDConv
            register_desc = "-"
        ET.SubElement(register_et, 'description').text =\
            ''.join([ f"{line}\n" for line in cleandoc(register_desc).splitlines()]).strip()

        # alternateGroup  Specifies a group name associated with all alternate register that have the same name.
        #                 At the same time, it indicates that there is a register definition allocating the same
        #                 absolute address in the address space.
        # alternateRegister   This tag can reference a register that has been defined above to current location
        #                     in the description and that describes the memory location already.
        #                     This tells the SVDConv's address checker that the redefinition of this particular register is intentional.
        #                     The register name needs to be unique within the scope of the current peripheral.
        #                     A register description is defined either for a unique address location or
        #                     could be a redefinition of an already described address.
        #                     In the latter case, the register can be either marked alternateRegister and
        #                     needs to have a unique name, or it can have the same register name
        #                     but is assigned to a register subgroup through the tag alternateGroup (specified in version 1.0).
        # addressOffset   Define the address offset relative to the enclosing element.
        ET.SubElement(register_et, 'addressOffset').text = hex(map_table_entry.address - cluster_base_address)
        register_et.append(ET.Comment(f"addressAbsolute={hex(map_table_entry.address)}"))
        ET.SubElement(register_et, 'dataType').text = self.get_ctype()

        # add fields to register (fields tag must not be empty)
        if len(self.get_all_actual_field()):
            fields_et = ET.SubElement(register_et, 'fields')
            for f in self.get_all_actual_field():
                f.svd_fill_field(map_table_entry=map_table_entry, fields_et=fields_et)

    @staticmethod
    def md_strip(str):
        # array bracket and slash are stripped for link def in markdown (should match header def in markdown)
        return str.replace('[', '').replace(']', '').replace('/', '')

    @staticmethod
    def md_escape(non_markdown_str, variant):
        # inject \ before each special markdown char
        # This may corrupt a bit the column size in multiline cell for pandoc markdown,
        # but this is better than corrupting the description trying to interpret unintended special char
        # there is a special case for space at start of line to keep indent in pandoc variant of markdown
        markdown_special_char = "\\`*_{}[]()+-.!"
        for c in markdown_special_char:
            if c in non_markdown_str:
                non_markdown_str = non_markdown_str.replace(c, "\\" + c)
        # space at start of line is going to be ignore by markdown unless escaped
        if  variant == "pandoc":
            start_of_line_space_match = re.match(r"^( +)", non_markdown_str)
            if start_of_line_space_match:
                # space at start of line detected, now do substitution of space with backslash+space
                non_markdown_str = re.sub("^( +)",start_of_line_space_match.group(1).replace(' ','\\ '), non_markdown_str)
        return non_markdown_str

    def get_md_formatted_address(self, map_table_entry):
        return eval(f"f'{PyriftBase.md_option['register_address_formatting']}'", {"address":map_table_entry.address})

    def get_xls_formatted_address(self, map_table_entry):
        return eval(f"f'{PyriftBase.xls_option['register_address_formatting']}'", {"address":map_table_entry.address})

    def to_markdown(self, map_table_entry, variant):
        """
            to_markdown convert a register to its description as a markdown string
            variant parameter allow finetune the markdown format. Supported values are:
            'gitlab': default format so the markdown is rendered correctly within gitlab web browsing
            'pandoc': Have special formatting for multi-line cell to be parse correctly with pandoc
                      (pandoc have custom markdown with no common markdown syntax to render bth gitlab and pandoc correctly)
        """
        data_format = '%%0%dX' % (PyriftBase.dw/4)
        markdown = ""
        reg_description = self.desc.replace("\r\n", "\n").replace("\n","  \n")
        if self.desc:
            # print("desc=", reg_description)
            markdown += f"Register description:  \n"
            for line in cleandoc(reg_description).splitlines():
                if PyriftBase.md_option["desc_escape_markdown_special"]:
                    line = self.md_escape(line, variant)
                markdown += f"{line}  \n"

        if PyriftBase.md_option['with_register_reset_value']:
            markdown += f"Register reset value: 0x{data_format % self.get_reset_value(map_table_entry)}  \n"
        if PyriftBase.md_option['with_reset_valid_mask']:
            markdown += f"Reset valid mask: 0x{data_format % self.get_reset_valid_mask(map_table_entry)}  \n"
        # check for array with special non constant reset
        if map_table_entry.get_array_size_in_hierarchy() > 1 and\
                any([f.reset and not isinstance(f.reset, int) for f in self.get_all_field()]):
            reset_table = [ f"0x{map_entry.register.get_reset_value(map_entry):x}" \
                            for map_entry in map_table_entry.get_all_register_array_entry()]
            markdown += f"Register array of reset value is : "
            markdown += ', '.join(reset_table)
            markdown += f"  \n"

        if PyriftBase.md_option['with_reset_bit_table']:
            # reset value formatted as 32 bit bitfields
            # Never need multi-line cell, so no special format for pandoc variant
            markdown += f"\n"
            #
            markdown += f"| bit # |"
            for i in range(PyriftBase.dw):
                markdown += f" {str(PyriftBase.dw-1-i):2} |"
            markdown += f"  \n"
            #
            markdown += f"|-------|"
            for i in range(PyriftBase.dw):
                markdown += f"----|"
            markdown += f"  \n"
            #
            markdown += f"| Reset |"
            for i in range(PyriftBase.dw):
                markdown += f" {self.get_bit_reset_value(PyriftBase.dw-1-i, map_table_entry):2} |"
            markdown += f"  \n"
            #
            markdown += f"\n"
            markdown += f"\n"
            # table = document.add_table(rows=2, cols=PyriftBase.dw+1)
            # for i in range(PyriftBase.dw+1):
            #     table.rows[0].cells[i].width=Cm(0.5)
            #     table.rows[1].cells[i].width=Cm(0.5)
            # table.rows[0].cells[0].text="bit#"
            # table.rows[1].cells[0].text="Reset"
            # for i in range(PyriftBase.dw):
            #     table.rows[0].cells[1+i].text=str(PyriftBase.dw-1-i)
            #     table.rows[1].cells[1+i].text=self.get_bit_reset_value(PyriftBase.dw-1-i, map_table_entry)
            #     # cell margin can be customized here if needed with following (comment below)
            #     # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.left_indent=Cm(-0.15)
            #     # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.right_indent=Cm(-0.15)
            # # print("par-fmt=",dir(table.rows[0].cells[0])) # .paragraphs[0].paragraph_format.left_indent))
            # table.style = document.styles['Pyrift_Bitfield']


        # Remind register instance address
        # Register can be part of array instance of either regmap or register (find all relevant address)
        # Each address listed have hyperlink to main address table
        if PyriftBase.md_option['link_to_addr_map']:
            addr_list = ', '.join([
                f"[{self.get_md_formatted_address(entry)}]"\
                f"(#{self.md_strip(entry.parent.get_regmap_hierarchy(max_range=False).replace('/', PyriftBase.md_option['regmap_separator']))})"\
                for entry in map_table_entry.get_all_register_array_entry()
            ])
        else:
            addr_list = ', '.join([
                self.get_md_formatted_address(entry) for entry in map_table_entry.get_all_register_array_entry()
            ])
        markdown += f"Address: {addr_list}  \n"

        # Header text is defined early (used both for column width and header text render)
        header_bit        = PyriftBase.md_option['Header_Bit']
        header_field_name = PyriftBase.md_option['Header_Name']
        header_sw         = PyriftBase.md_option['Header_SW']
        header_hw         = PyriftBase.md_option['Header_HW']
        header_reset      = PyriftBase.md_option['Header_Reset']
        header_storage    = PyriftBase.md_option['Header_Store']
        header_cdc        = PyriftBase.md_option['Header_Cdc']
        header_if         = PyriftBase.md_option['Header_If']
        header_desc       = PyriftBase.md_option['Header_Desc']

        # setup default width for column to 4 (used for all empty cells)
        column_bit_width        = len(header_bit)
        column_field_name_width = len(header_field_name)
        column_sw_width         = len(header_sw)
        column_hw_width         = len(header_hw)
        column_reset_width      = len(header_reset)
        column_storage_width    = len(header_storage)
        column_cdc_width        = len(header_cdc)
        column_if_width         = len(header_if)
        column_desc_width       = len(header_desc)

        # now construct the table content, store in one dictionary per column (key index is field)
        cell_bit_list={}
        cell_field_name_list={}
        cell_sw_list={}
        cell_hw_list={}
        cell_reset_list={}
        cell_storage_list={}
        cell_cdc_list={}
        cell_if_list={}
        cell_desc_list={}
        for f in self.get_all_actual_field(): # skip padding field
            # Each field is describe with 1 line of cells
            # In 'pandoc' variant of markdown, a multiline cell is interleaved with the line of other cells
            # create each cell as multine (list of single line string), then interleave all cell after that
            cell_bit_list[f] = [f"{f.bit_range_str()}"] # bit column in table (oneline)

            cell_field_name_list[f] = [f"{f.name}"]
            # detail about hw interface is skipped if user not interested in interface relative info
            if f.name_in_if and PyriftBase.md_option['Column_If']:
                cell_field_name_list[f][0] += "," # add separator at end of first line
                cell_field_name_list[f].append(f"name_in_if:")
                cell_field_name_list[f].append(f"{f.name_in_if}")

            cell_sw_list[f] = [f"{f.sw.name}"] # SW column in table
            if f.woclr:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" woclr")
            if f.woset:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" woset")
            if f.wot:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wot")
            if f.wzc:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wzc")
            if f.wzs:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wzs")
            if f.wzt:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wzt")
            if f.wclr:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wclr")
            if f.wset:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" wset")
            if f.rclr:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" rclr")
            if f.rset:
                cell_sw_list[f][-1] += ","
                cell_sw_list[f].append(" rset")

            cell_hw_list[f] = [f"{f.hw.name}"] # HW column in table

            # Reset column in table
            if f.is_valid_reset_value(map_table_entry):
                reset_value = f.get_reset_value(map_table_entry)
                # single bit or small value like 0 or 1 are not 0x prefixed
                reset_value_string = f"0x{reset_value:0x}" if reset_value >=10 else f"{reset_value}"
                cell_reset_list[f] = [reset_value_string]
            else:
                cell_reset_list[f] = ["X"]

            # Storage type for field
            cell_storage_list[f] = [f"{f.get_storage_type_str(map_table_entry)}"]

            cell_cdc_list[f] = [] # empty list to collect CDC options
            if f.cdc_in!=Cdc.NA:
                cell_cdc_list[f].append(f"IN={f.cdc_in.name}")
            if f.cdc_out!=Cdc.NA:
                cell_cdc_list[f].append(f"OUT={f.cdc_out.name}")
            if not cell_cdc_list[f]:
                # get at list one empty line
                cell_cdc_list[f] = [""] # empty list cast to empty line

            # IF column in table
            cell_if_list[f] = [f"{f.get_interface_name()}"]

            # description column
            cell_desc_list[f] = []
            for line in cleandoc(f.desc).strip().splitlines():
                if PyriftBase.md_option["desc_escape_markdown_special"]:
                    line = self.md_escape(line, variant)
                cell_desc_list[f].append(line)
            # cell_desc_list[f] = cleandoc(f.desc).splitlines()
            if f.encode:
                cell_desc_list[f].append(f"{f.name} is using enum encoding: {f.encode.__name__}")
                if f.encode.get_desc(): # encode description is added if non empty
                    for line in cleandoc(f.encode.get_desc()).splitlines():
                        if PyriftBase.md_option["desc_escape_markdown_special"]:
                            line = self.md_escape(f"  {line}", variant)
                        cell_desc_list[f].append(line)

                for val in f.encode:
                    if f.encode.get_value_desc(val.value):
                        for line in cleandoc(f.encode.get_value_desc(val.value)).splitlines():
                            if PyriftBase.md_option["desc_escape_markdown_special"]:
                                line = self.md_escape(line, variant)
                            cell_desc_list[f].append(f"{val.value}: {line}")

            # add special multi value reset array for field
            if map_table_entry.get_array_size_in_hierarchy() > 1 and f.reset and not isinstance(f.reset, int):
                cell_desc_list[f].append("Array have reset value table :")
                rst_str = ', '.join([f"0x{rst:0x}" for rst in f.reset])
                cell_desc_list[f].append(f"Reset : {rst_str}")

            # cell list are all ready for current line
            # find column width required (needed for pandoc header)
            # Extracting length of longest line in cell_list
            # default keep track of previous line and default (empty cell still require to present correctly)
            # use header size as default for case of empty
            column_bit_width        = max(column_bit_width,        len(max(cell_bit_list[f],        key = len, default=header_bit)))
            column_field_name_width = max(column_field_name_width, len(max(cell_field_name_list[f], key = len, default=header_field_name)))
            column_sw_width         = max(column_sw_width,         len(max(cell_sw_list[f],         key = len, default=header_sw)))
            column_hw_width         = max(column_hw_width,         len(max(cell_hw_list[f],         key = len, default=header_hw)))
            column_reset_width      = max(column_reset_width,      len(max(cell_reset_list[f],      key = len, default=header_reset)))
            column_storage_width    = max(column_storage_width,    len(max(cell_storage_list[f],    key = len, default=header_storage)))
            column_cdc_width        = max(column_cdc_width,        len(max(cell_cdc_list[f],        key = len, default=header_cdc)))
            column_if_width         = max(column_if_width,         len(max(cell_if_list[f],         key = len, default=header_if)))
            column_desc_width       = max(column_desc_width,       len(max(cell_desc_list[f],       key = len, default=header_desc)))

        # Now all cell table are build, render the header of table, then each line to markdown

        # Header for register field table (one line per field after table header)
        # register field table : 2 flavour: 'gitlab' or 'pandoc'
        # multiline table cell use different syntax for each flavour of markdown
        # All cells content have been generated as list of lines (possibly empty)
        # in cell_*_list
        if variant == "gitlab":
            markdown +=  "  \n"
            # using variable formatting f"{a:_<{w}}" : string in a, right pad with space to width w
            column_head    = f"| {header_bit: <{column_bit_width}} | {header_field_name: <{column_field_name_width}} |"
            head_separator = f"|-{'-'*column_bit_width}-|-{'-'*column_field_name_width}-|"
            if(PyriftBase.md_option['Column_SW']):
                column_head    += f" {header_sw: <{column_sw_width}} |"
                head_separator += f"-{'-'*column_sw_width}-|"
            if(PyriftBase.md_option['Column_HW']):
                column_head    += f" {header_hw: <{column_hw_width}} |"
                head_separator += f"-{'-'*column_hw_width}-|"
            column_head    += f" {header_reset: <{column_reset_width}} |"
            head_separator += f"-{'-'*column_reset_width}-|"
            if(PyriftBase.md_option['Column_Store']):
                column_head    += f" {header_storage: <{column_storage_width}} |"
                head_separator += f"-{'-'*column_storage_width}-|"
            if(PyriftBase.md_option['Column_Cdc']):
                column_head    += f" {header_cdc: <{column_cdc_width}} |"
                head_separator += f"-{'-'*column_cdc_width}-|"
            if(PyriftBase.md_option['Column_If']):
                column_head    += f" {header_if: <{column_if_width}} |"
                head_separator += f"-{'-'*column_if_width}-|"
            column_head    += f" {header_desc: <{column_desc_width}} |"
            head_separator += f"-{'-'*column_desc_width}-|"

            markdown +=  f"{column_head}  \n"
            markdown +=  f"{head_separator}  \n"
        elif  variant == "pandoc":
            markdown +=  "  \n"
            # using variable formatting f"{a:_<{w}}" : string in a, right pad with space to width w
            head_separator1 = f"+-{'-'*column_bit_width}-+-{'-'*column_field_name_width}-+"
            column_head     = f"| {header_bit: <{column_bit_width}} | {header_field_name: <{column_field_name_width}} |"
            head_separator2 = f"+={'='*column_bit_width}=+={'='*column_field_name_width}=+"

            if(PyriftBase.md_option['Column_SW']):
                head_separator1 += f"-{'-'*column_sw_width}-+"
                column_head    += f" {header_sw: <{column_sw_width}} |"
                head_separator2 += f"={'='*column_sw_width}=+"
            if(PyriftBase.md_option['Column_HW']):
                head_separator1 += f"-{'-'*column_hw_width}-+"
                column_head    += f" {header_hw: <{column_hw_width}} |"
                head_separator2 += f"={'='*column_hw_width}=+"
            head_separator1 += f"-{'-'*column_reset_width}-+"
            column_head    += f" {header_reset: <{column_reset_width}} |"
            head_separator2 += f"={'='*column_reset_width}=+"
            if(PyriftBase.md_option['Column_Store']):
                head_separator1 += f"-{'-'*column_storage_width}-+"
                column_head    += f" {header_storage: <{column_storage_width}} |"
                head_separator2 += f"={'='*column_storage_width}=+"
            if(PyriftBase.md_option['Column_Cdc']):
                head_separator1 += f"-{'-'*column_cdc_width}-+"
                column_head    += f" {header_cdc: <{column_cdc_width}} |"
                head_separator2 += f"={'='*column_cdc_width}=+"
            if(PyriftBase.md_option['Column_If']):
                head_separator1 += f"-{'-'*column_if_width}-+"
                column_head    += f" {header_if: <{column_if_width}} |"
                head_separator2 += f"={'='*column_if_width}=+"
            head_separator1 += f"-{'-'*column_desc_width}-+"
            column_head    += f" {header_desc: <{column_desc_width}} |"
            head_separator2 += f"={'='*column_desc_width}=+"

            markdown +=  f"{head_separator1}  \n"
            markdown +=  f"{column_head}  \n"
            markdown +=  f"{head_separator2}  \n"
        else:
            raise  RuntimeError(f"Markdown variant unexpected: '{variant}'. Only 'gitlab' or 'pandoc' is supported")

        for f in self.get_all_actual_field(): # skip padding field
            # render one line of table
            if variant == "gitlab":
                # cell space padding is not used in markdown rendering, but more readable markdown in single line
                markdown +=  "|"
                markdown += '<br>'.join([f' {line: <{column_bit_width}} ' for line in cell_bit_list[f]])+"|" # bit column in table
                markdown += '<br>'.join([f' {line: <{column_field_name_width}} ' for line in cell_field_name_list[f]])+"|" # field name column in table
                if(PyriftBase.md_option['Column_SW']):
                    markdown += '<br>'.join([f' {line: <{column_sw_width}} ' for line in cell_sw_list[f]])+"|" # SW column in table
                if(PyriftBase.md_option['Column_HW']):
                    markdown += '<br>'.join([f' {line: <{column_hw_width}} ' for line in cell_hw_list[f]])+"|" # HW column in table
                markdown += '<br>'.join([f' {line: <{column_reset_width}} ' for line in cell_reset_list[f]])+"|" # Reset column in table

                if(PyriftBase.md_option['Column_Store']):
                    markdown += '<br>'.join([f' {line: <{column_storage_width}} ' for line in cell_storage_list[f]])+"|" # Storage type column in table
                if(PyriftBase.md_option['Column_Cdc']):
                    markdown += '<br>'.join([f' {line: <{column_cdc_width}} ' for line in cell_cdc_list[f]])+"|" # CDC column in table

                if(PyriftBase.md_option['Column_If']):
                    markdown += '<br>'.join([f' {line: <{column_if_width}} ' for line in cell_if_list[f]])+"|" # IF column in table

                markdown += '<br>'.join([f' {line: <{column_desc_width}} ' for line in cell_desc_list[f]])+"|" # Description column in table
                markdown += "  \n"

            elif  variant == "pandoc":
                # first find out how many lines for this row (multi-line cells are possible)
                height = max([len(cell_list) for cell_list in [
                    cell_bit_list[f],
                    cell_field_name_list[f],
                    cell_sw_list[f],
                    cell_hw_list[f],
                    cell_reset_list[f],
                    cell_storage_list[f],
                    cell_cdc_list[f],
                    cell_if_list[f],
                    cell_desc_list[f]
                ]])
                for line in range(height):
                    markdown +=  "|"
                    markdown +=     f' {(cell_bit_list[f][line]        if len(cell_bit_list[f])>line        else ""): <{column_bit_width}}'        +('\\|' if line<len(cell_bit_list[f])-1        else " |") # bit column in table
                    markdown +=     f' {(cell_field_name_list[f][line] if len(cell_field_name_list[f])>line else ""): <{column_field_name_width}}' +('\\|' if line<len(cell_field_name_list[f])-1 else " |") # field name column in table
                    if(PyriftBase.md_option['Column_SW']):
                        markdown += f' {(cell_sw_list[f][line]         if len(cell_sw_list[f])>line         else ""): <{column_sw_width}}'         +('\\|' if line<len(cell_sw_list[f])-1         else " |") # SW column in table
                    if(PyriftBase.md_option['Column_HW']):
                        markdown += f' {(cell_hw_list[f][line]         if len(cell_hw_list[f])>line         else ""): <{column_hw_width}}'         +('\\|' if line<len(cell_hw_list[f])-1         else " |") # HW column in table
                    markdown +=     f' {(cell_reset_list[f][line]      if len(cell_reset_list[f])>line      else ""): <{column_reset_width}}'      +('\\|' if line<len(cell_reset_list[f])-1      else " |") # Reset column in table

                    if(PyriftBase.md_option['Column_Store']):
                        markdown += f' {(cell_storage_list[f][line]    if len(cell_storage_list[f])>line    else ""): <{column_storage_width}}'    +('\\|' if line<len(cell_storage_list[f])-1    else " |") # Storage type column in table
                    if(PyriftBase.md_option['Column_Cdc']):
                        markdown += f' {(cell_cdc_list[f][line]        if len(cell_cdc_list[f])>line        else ""): <{column_cdc_width}}'        +('\\|' if line<len(cell_cdc_list[f])-1        else " |") # CDC column in table

                    if(PyriftBase.md_option['Column_If']):
                        markdown += f' {(cell_if_list[f][line]         if len(cell_if_list[f])>line         else ""): <{column_if_width}}'         +('\\|' if line<len(cell_if_list[f])-1         else " |") # IF column in table

                    markdown +=     f' {(cell_desc_list[f][line]       if len(cell_desc_list[f])>line       else ""): <{column_desc_width}}'       +('\\|' if line<len(cell_desc_list[f])-1       else " |") # Description column in table
                    markdown += "  \n"
                markdown +=  f"{head_separator1}  \n"

        markdown +=  "  \n" # separate table with next markdown text
        return markdown

    def to_verilog(self, map_table_entry):
        """
            convert a register to it verilog implementation string
            Method of Register, it is provided a MapTableEntry to provide address, array
            (the extra information about current instanciation)
        """
        reg_name = map_table_entry.get_full_hw_name()
        verilog = ""
        verilog_declaration = ""

        verilog += f"\n"
        verilog += f"// ##############################################################\n"
        verilog += f"// Register is : {self.name}, fullname={reg_name}\n"
        verilog += f"//    map@:{map_table_entry.address},\n"
        verilog += f"//    regmap: {map_table_entry.parent.get_regmap_hierarchy(max_range=False)}\n"
        verilog += f"//    regmap-array={map_table_entry.get_regmap_array(range=False, include_leaf_reg=False)}/"
        verilog +=                    f"{map_table_entry.get_regmap_array(range=True, include_leaf_reg=False)}"
        verilog +=       f"+reg-array={map_table_entry.get_layer_bracket(range=False)}/"
        verilog +=                    f"{map_table_entry.get_layer_bracket(range=True)}\n"
        verilog += f"// ##############################################################\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} control signals are:\n"
        verilog += f"//     logic {reg_name}_r_swwr;\n"
        verilog += f"//     logic {reg_name}_r_swrd;\n"
        verilog += f"//     logic {reg_name}_r_swrd_early;\n"
        verilog += f"//     logic [{PyriftBase.dw-1}:0] {reg_name}_swrdata;\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} field implementation\n"
        for f in self.get_all_field():
            (dec,ver) = f.to_verilog(map_table_entry)
            verilog_declaration += dec
            verilog += ver
        for f in self.get_all_actual_field():
            if not self.external:
                # no verilog_declaration (some signal are declared/used localy inside cdc_layer)
                verilog += f.to_verilog_internal_cdc_layer(map_table_entry)
        verilog += f"\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} field aggregation for sw rdata\n"
        verilog += f"assign {reg_name}_swrdata = {{\n"
        field_name_list = [
            # get name from field (!! short_name can squeeze the reg name)
            # if non readable field, substitute a ZERO constant in place
            f"{f.get_full_hw_name(map_table_entry)}[{f.bit_width - 1}:0]" if f.sw.is_readable() else f"{f.bit_width}'h0"
            for f in self.get_all_field()] # sorted LSB first
        # aggregate starting MSB
        verilog += f"    " + ',\n    '.join(reversed(field_name_list)) + "\n"
        verilog += f"}};\n"
        verilog += f"\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        verilog += f"// Register {reg_name} ready aggregation for all field\n"
        verilog += f"assign {reg_name}_r_ready ="

        # register ready depend on field ready from each field
        # A field without used ready will have the signal created and tie to 1 in the field
        # So the register ready is just the logic and between all field
        # external field already have _extready demuxed for either read or write
        # A slow hw with hwce will have the hwce input already integrated in field ready signal
        field_rdy_list = [
            f"{f.get_full_hw_name(map_table_entry)}_ready"
            for f in self.get_all_actual_field()
        ]
        if len(field_rdy_list)==0:
            # normally at least one field per register, so report error
            raise  RuntimeError(f"Register {self.name} seems to have 0 field !?")
        verilog += f"\n    " + ' &\n    '.join(field_rdy_list) + ";\n"
        verilog += f"// --------------------------------------------------------------\n"
        verilog += f"\n"
        return (verilog_declaration, verilog)

    def validate_check_premap(self):
        """
            Register expected to be fully defined now
            Run validation check before allowing any translation (html doc, rtl verilog)
            Add pad fields around actual useful fields
        """

        # normalize array : integer are converted to tuple
        # 3 =>(3,)
        if isinstance(self.array, int):
            self.array = (self.array,)

        # isascii check is silently skipped in version less than 3.7 (where isascii has been added)
        if sys.hexversion >= 0x3070000 and not self.desc.isascii():
            print (f"Warning: Non ascii char detected in desc of register {self.name}")

        # First check that no overlapping occurs between different fields
        # Possibly auto allocate field with AUTO bit range
        # This is the case if field.lsb_bit == 'TBV'
        covered_bit = set([PyriftBase.dw]) # fill bit above reg msb as blocker for auto allocation
        # get_all_actual_field cannot sort field as lsb are not yet auto allocated
        for field in self.get_all_actual_field(sort=False):
            # First check validaty of parameter setup at field level
            field.validate_check_premap()

            if field.lsb_bit == 'TBV':
                # try to autofit the field in the register
                # print("Autofitting field in register")
                try:
                    lsb = next(lsb for lsb in range(PyriftBase.dw)
                               if not set(range(lsb, lsb + field.bit_width)) & covered_bit
                    )
                except StopIteration:
                    # (error StopIteration raised if not found)
                    raise RuntimeError(f"Autofitting failed for field={field.name},bit_width={field.bit_width} in register"\
                                       f" with bit used={covered_bit}"  )

                # print(f"Found fitting field in register : lsb={lsb}")
                field.lsb_bit = lsb


            field_bit = set(range(field.lsb_bit, field.lsb_bit + field.bit_width))
            # print("Field range is :", field_bit)
            # check non overlapping of this field
            if field_bit & covered_bit:
                # print(field_bit, covered_bit, field_bit & covered_bit)
                raise RuntimeError(
                    "Overlapping bit_range at field checking" +
                    str(field_bit) + "<>" + str(covered_bit) + " in reg: " + self.name)
            covered_bit = covered_bit | field_bit
        # print("Cover=", covered_bit)
        full_bit = set(range(0, PyriftBase.dw))
        pad_bit = full_bit - covered_bit
        following_pad_bit = set({i + 1 for i in pad_bit})
        # print("Pad=", pad_bit)
        # print("FollowingPad=", following_pad_bit)
        # A first pad bit of multi-bit padding is : a pad bit, after a non pad bit
        start_pad = sorted(pad_bit - following_pad_bit)
        # print("Pad start=", start_pad)
        # A last pad bit of multi-bit padding is : a pad bit, before a non pad bit
        end_pad = sorted({i - 1 for i in (following_pad_bit - pad_bit)})
        # print("Pad end=", end_pad)

        # cleanup existing pad_list (refresh by new call of validate_check_premap)
        self.pad_list = []

        for pad_s, pad_e in zip(start_pad, end_pad):
            # print(f"PAD={pad_e}:{pad_s}")
            pad = Pad_Field(
                name=f"pad_{pad_e}_{pad_s}",
                bit_range=f"{pad_e}:{pad_s}"
            )
            pad.desc = "Pad bit (reserved, Read 0, write ignored)"
            self.pad_list.append(pad)

        # propagate the default hwce signal to each field if top regmap define it and
        # if not overloaded by field itself
        for field in self.get_all_actual_field():
            if field.hwce_signal_name != None:
                # this field have hwce clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.hwce_signal = self.parent.get_root_parent().get_signal_by_name(field.hwce_signal_name)
            else:
                # no explicit hwce signal for this field, use default reset
                field.hwce_signal = self.parent.get_root_parent().default_hwce_signal
            if field.cdc_out_signal_name != None:
                # this field have cdc clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.cdc_out_signal = self.parent.get_root_parent().get_signal_by_name(field.cdc_out_signal_name)
            if field.cdc_in_signal_name != None:
                # this field have cdc clock enabling feature
                # locate the signal in root regmap to get signal attribute
                field.cdc_in_signal = self.parent.get_root_parent().get_signal_by_name(field.cdc_in_signal_name)

            # check that all field in register have distinct name (required for chead structure)
            field_name_list = [field.name for field in self.get_all_actual_field()]
            # convert list to dict (all duplicate alias to same key)
            field_name_list_no_duplicate = list( dict.fromkeys(field_name_list) )
            if len(field_name_list) > len(field_name_list_no_duplicate):
                raise RuntimeError(f"Find duplicate field name in register {self.name}:", field_name_list)


    def validate_check_postmap(self, map_table_entry):
        """
            Register expected to be fully defined now
            Run validation check before allowing any translation (html doc, rtl verilog)
            Add pad fields around actual useful fields
        """
        for field in self.get_all_actual_field(sort=False):
            # First check validaty of parameter setup at field level
            field.validate_check_postmap(map_table_entry)


    def to_verilog_interface_by_name(self, if_name, map_table_entry):
        """
            Generate the verilog interface of rif for the field of current register to rest of hw design
            Filter by interface name : if_name (check if at least one field is member of if_name)
            aggregate field are filter from generation
        """
        verilog = ""
        for f in self.get_all_actual_field():
            if f.get_interface_name() == if_name and not f.is_agregate_field():
                verilog += f"      //  {f.name}\n"
                verilog += f.to_verilog_interface(if_name=if_name, map_table_entry=map_table_entry)

        return verilog

    def to_verilog_connect_interface(self, map_table_entry):
        """
            Generate the verilog connection to interface of all field of current register
            parent register map info passed with map_table_entry
        """
        verilog = ""
        for f in self.get_all_actual_field():
            verilog += f"      //  {f.name}\n"
            verilog += f.to_verilog_connect_interface(map_table_entry)

        return verilog

# ----------------------------------------------------------------------------

    def to_verilog_package(self, map_table_entry):
        """
            put constant definition about the register in verilog package
            (provide similar information as put inside cheader)
        """
        pkg = ""
        pkg += ''.join([f"// Register Description: {line}\n" for line in cleandoc(self.desc).splitlines()])
        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            pkg += ''.join([f"// Field Description {f.get_name()}[{f.bit_range_str()}]: {line}\n" \
                            for line in cleandoc(f.desc).splitlines()])
        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__LSB = {f.lsb_bit};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__MSB = {f.lsb_bit+f.bit_width-1};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__BITWIDTH = {f.bit_width};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__BITMASK = "\
                     f"{PyriftBase.dw}'h{((1<<f.bit_width)-1)<<f.lsb_bit:x};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__LSBBITMASK = "\
                     f"{PyriftBase.dw}'h{1<<f.lsb_bit:x};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__RESET = "\
                     f"{f.bit_width}'h{f.get_reset_value(map_table_entry):x};\n"
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__"\
                     f"{f.get_name().upper()}__RESET_VALUE_IS_VALID = "\
                     f"{1 if f.is_valid_reset_value(map_table_entry) else 0};\n"

        pkg += f"// Register reset value ...\n"
        pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__RESET = "\
                f"{PyriftBase.dw}'h{self.get_reset_value(map_table_entry):x};\n"
        pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__RESET_VALID_MASK = "\
                f"{PyriftBase.dw}'h{self.get_reset_valid_mask(map_table_entry):x};\n"
        pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__ADDRESS = "\
                f"32'h{map_table_entry.address:08X};\n"
        pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}__ADDRESS_OFFSET = "\
                f"32'h{map_table_entry.address-self.get_root_parent().base_address:08X};\n"

        return pkg

    def to_verilog_package_array_constant(self, map_table_entry):
        """
            Special constant are added in package for array case
            (found user with verilog verification code bypassing reg_model,
            so extra reg address for each reg instance of array seems useful)
            put constant definition about the register in verilog package
            (provide similar information as put inside cheader)
            (adress, reset value per register instance is provided)
        """
        pkg = ""
        pkg += f"localparam {map_table_entry.get_full_hw_name().upper()}__ADDRESS = "\
                f"32'h{map_table_entry.address:08X};\n"
        pkg += f"localparam {map_table_entry.get_full_hw_name().upper()}__RESET = "\
                f"{PyriftBase.dw}'h{self.get_reset_value(map_table_entry):x};\n"
        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            pkg += f"localparam {map_table_entry.get_noarray_hw_name().upper()}"\
                     f"{map_table_entry.get_suffix_array_name().upper()}__{f.get_name().upper()}__RESET = "\
                     f"{f.bit_width}'h{f.get_reset_value(map_table_entry):x};\n"
        return pkg


# ----------------------------------------------------------------------------

    def to_chead(self, map_table_entry, with_define_constant=False, define_prefix=""):
        chead = ""
        chead += f"// --------------------------------------------------------------\n"
        chead += f"\n"
        chead += f"// register is volatile : {self.is_volatile(map_table_entry)}\n"
        chead += f"// register is const : {self.is_sw_read_only()}\n"
        chead += ''.join([f"// Register Description: {line}\n" for line in cleandoc(self.desc).splitlines()])
        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            chead += ''.join([f"// Field Description {f.get_name()}[{f.bit_range_str()}]: {line}\n" \
                              for line in cleandoc(f.desc).splitlines()])

        chead += f"typedef struct\n"
        chead += f"{{\n"
        for f in self.get_all_field(): # sorted LSB first
            chead += f"    {'signed' if f.sw_signed else 'unsigned'} int {f.get_name()}:{f.bit_width};\n"
        chead += f"}} TS_{map_table_entry.get_noarray_hw_name()};\n"
        chead += f"\n"
        chead += f"typedef union\n"
        chead += f"{{\n"

        chead_volatile_option = "volatile " if self.is_volatile(map_table_entry) else ""
        chead_const_option = "const " if self.is_sw_read_only() else ""
        modifier_option = chead_volatile_option + chead_const_option
        chead += f"    // Register is normally {'' if self.is_volatile(map_table_entry) else 'non '}volatile\n"
        chead += f"    TS_{map_table_entry.get_noarray_hw_name()} {modifier_option}field;\n" # map register to field structure
        chead += f"    {self.get_ctype()} {modifier_option}reg;\n" # map register to exactly reg bitwidth
        chead += f"    // Alternate Register view : non volatile and volatile\n"
        # provide forced non volatile access to register
        chead += f"    TS_{map_table_entry.get_noarray_hw_name()} {chead_const_option}field_nv;\n" # map register to field structure
        chead += f"    {self.get_ctype()} {chead_const_option}reg_nv;\n" # map register to exactly reg bitwidth
        # non volatile register, provide a way to force access as volatile
        chead += f"    TS_{map_table_entry.get_noarray_hw_name()} volatile {chead_const_option}field_v;\n" # map register to field structure
        chead += f"    {self.get_ctype()} volatile {chead_const_option}reg_v;\n" # map register to exactlyreg bitwidth
        #
        chead += f"}} TU_{map_table_entry.get_noarray_hw_name()};\n"
        chead += f"\n"
        if with_define_constant:
            chead += f"// REG Entry: {map_table_entry.get_regmap_hierarchy(max_range=True)}:\n"
            for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__LSB {f.lsb_bit}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__MSB {f.lsb_bit+f.bit_width-1}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__BITWIDTH {f.bit_width}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__BITMASK "\
                         f"0x{((1<<f.bit_width)-1)<<f.lsb_bit:x}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__LSBBITMASK "\
                         f"0x{1<<f.lsb_bit:x}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET "\
                         f"0x{f.get_reset_value(map_table_entry):x}\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET_VALUE_IS_VALID "\
                         f"{1 if f.is_valid_reset_value(map_table_entry) else 0}\n"

            chead += f"// Register reset value ...\n"
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__RESET "\
                    f"0x{self.get_reset_value(map_table_entry):x}\n"
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__RESET_VALID_MASK "\
                    f"0x{self.get_reset_valid_mask(map_table_entry):x}\n"
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__ADDRESS "\
                    f"0x{map_table_entry.address:x}\n"
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__ADDRESS_WORD "\
                    f"0x{map_table_entry.address//(PyriftBase.dw//8):x}\n"
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__ADDRESS_OFFSET "\
                    f"0x{map_table_entry.address-self.get_root_parent().base_address:x}\n"

            chead += f"\n"
        return chead

    def to_chead_reset_detail(self, map_table_entry, define_prefix=""):
        chead = ""
        any_field_is_special = False

        for f in self.get_all_actual_field(): # sorted LSB first, excluding PAD
            if f.reset and not isinstance(f.reset, int):
                # each item of array with non constant reset value table is detailed here
                if map_table_entry.is_first_of_array():
                    # Array with custom per array item reset value, warning about special info added
                    chead += f"// array have different reset value for some item of array\n"
                    chead += f"// Provide table of reset value for each item\n"
                    chead += f"// previous ...__RESET is valid for first entry only\n"
                    chead += f"// Give constant for field and Register reset value of all array ...\n"
                chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__"\
                         f"{f.get_name().upper()}__RESET_{map_table_entry.get_suffix_array_name().upper()} "\
                         f"0x{f.get_reset_value(map_table_entry):x}\n"
                any_field_is_special = True

        # display register value per register, but only if any field is special reset case
        if any_field_is_special:
            chead += f"#define {define_prefix}{map_table_entry.get_noarray_hw_name().upper()}__RESET"\
                        f"_{map_table_entry.get_suffix_array_name().upper()} "\
                        f"0x{self.get_reset_value(map_table_entry):x}\n"
        return chead

# ----------------------------------------------------------------------------

    def to_regmodel_uvm(self, map_table_entry):
        """
            Generate the register model in uvm (for rif verification)
        """
        regmodel = ""
        regmodel += f"\n"

        # check about need for singlepulse sub-classing
        singlepulse_fields = [field for field in self.get_all_actual_field() if field.singlepulse]
        if singlepulse_fields:
            regmodel += f"// singlepulse fields in {self.name} are implemented here with uvm sub class\n"
            regmodel += f"// Declaring these uvm utility sub-classes here:\n"
            for field in singlepulse_fields:
                class_name = f"uvm_reg_field_pulse_{self.name}_f_{field.get_name()}"
                regmodel += f"// {field.get_name()} is singlepulse : inherit uvm_reg_field\n"
                regmodel += f'class {class_name} extends uvm_reg_field;\n'
                regmodel += f'  `uvm_object_utils({class_name})\n'
                regmodel += f'  \n'
                regmodel += f'  local static bit m_pulse = define_access("PULSE");\n'
                regmodel += f'  \n'
                regmodel += f'  function new(string name = "{class_name}");\n'
                regmodel += f'    super.new(name);\n'
                regmodel += f'  endfunction  \n'
                regmodel += f'endclass // {class_name}\n'
                regmodel += f"\n"
                class_name = f"uvm_reg_cbs_pulse_{self.name}_f_{field.get_name()}"
                regmodel += f"// {field.get_name()} is singlepulse : callback inherit uvm_reg_cbs\n"
                regmodel += f'class {class_name} extends uvm_reg_cbs;\n'
                regmodel += f'  `uvm_object_utils({class_name})\n'
                regmodel += f'  \n'
                regmodel += f'  function new(string name = "{class_name}");\n'
                regmodel += f'    super.new(name);\n'
                regmodel += f'  endfunction\n'
                regmodel += f'  \n'
                regmodel += f'  virtual function void post_predict(input uvm_reg_field  fld,\n'
                regmodel += f'                                     input uvm_reg_data_t previous,\n'
                regmodel += f'                                     inout uvm_reg_data_t value,\n'
                regmodel += f'                                     input uvm_predict_e  kind,\n'
                regmodel += f'                                     input uvm_path_e     path,\n'
                regmodel += f'                                     input uvm_reg_map    map);\n'
                regmodel += f'    if (kind == UVM_PREDICT_WRITE && fld.get_access() == "PULSE")\n'
                regmodel += f'      value = 0;\n'
                regmodel += f'  endfunction\n'
                regmodel += f'endclass // {class_name}\n'

            regmodel += f"\n"

        regmodel += f"class {map_table_entry.get_noarray_hw_name()}_reg extends uvm_reg;\n"
        regmodel += f"\n"
        regmodel += f"   `uvm_object_utils({map_table_entry.get_noarray_hw_name()}_reg)\n"
        regmodel += f"   string reg_hdl_path;\n"
        regmodel += f"   // Field reset value stored in uvm_reg class member after constructor called\n"
        for field in self.get_all_actual_field():
            regmodel += f"   uvm_reg_data_t {field.get_name()}_field_reset_value_var;\n"
        regmodel += f"\n"
        regmodel += f"\n"

        regmodel += f"   function new (string name =\"{map_table_entry.get_noarray_hw_name()}_reg\");\n"
        regmodel += f"      super.new(name, {PyriftBase.dw}, UVM_NO_COVERAGE);\n"
        regmodel += f"      reg_hdl_path = \"\";\n"
        for field in self.get_all_actual_field():
            # for array use value of first register, each instance is to overwrite it after construction
            regmodel += f"      {field.get_name()}_field_reset_value_var = "\
                        f"'h{field.get_reset_value(map_table_entry):0x};\n"
        regmodel += f"   endfunction: new\n"
        regmodel += f"\n"

        regmodel += f"   // Field declaration\n"
        for field in self.get_all_actual_field():
            field_name = field.get_name() # TODO, array_name_suffix
            regmodel += f"   rand uvm_reg_field {field_name};\n"
            regmodel += f"   // each field have a set function for reset value per instance of array\n"
            regmodel += f"   virtual function void set_{field_name}_field_reset_value(uvm_reg_data_t reset_value);\n"
            regmodel += f"     {field_name}_field_reset_value_var = reset_value;\n"
            regmodel += f"   endfunction: set_{field_name}_field_reset_value\n"

        regmodel += f"\n"

        regmodel += f"   virtual function void set_reg_hdl_path(string hdl_path);\n"
        regmodel += f"     reg_hdl_path = hdl_path;\n"
        regmodel += f"   endfunction: set_reg_hdl_path\n"
        regmodel += f"\n"

        regmodel += f"   virtual function void build();\n"
        for field in self.get_all_actual_field():
            field_name = field.get_name()
            # special workaround : singlepulse cast to "WO", not volative
            # (should be more friendly with basic regmodel test sequence)
            if field.is_volatile(map_table_entry) and not field.singlepulse:
                volatile_attr = "1"
            else:
                volatile_attr = "0"
            if field.singlepulse:
                # singlepulse field have a dedicated class in regmodel
                uvm_reg_field_classname = f"uvm_reg_field_pulse_{self.name}_f_{field.get_name()}"
            else:
                uvm_reg_field_classname = f"uvm_reg_field"

            regmodel += f"      {field_name} = {uvm_reg_field_classname}::type_id::create(\"{field_name}\",,get_full_name());\n"
            regmodel += f"      {field_name}.configure(this,\n"
            regmodel += f"         {field.bit_width}, // size\n" # int unsigned
            regmodel += f"         {field.lsb_bit}, // lsb_pos\n" # int unsigned
            regmodel += f"         \"{field.get_uvm_access()}\", // access\n" # string
            regmodel += f"         {volatile_attr}, // volatile\n"
            regmodel += f"         {field.get_name()}_field_reset_value_var,// uvm_reg_data_t reset\n"
            regmodel += f"         {1 if field.is_valid_reset_value(map_table_entry) else 0}, // bit has_reset\n"
            regmodel += f"         1, // is_rand\n"
            regmodel += f"         0); // individually_accessible\n"

            # add_hdl_path_slice(.name("nvmctrl_regmap_sta_f_sta_done"),       .offset(0), .size(1));
            regmodel += f"      add_hdl_path_slice(.name($sformatf(\"%s_f_%s\", reg_hdl_path, \"{field_name}\")),"\
                              f" .offset({field.lsb_bit}), .size({field.bit_width}));\n"
            if field.singlepulse:
                # singlepulse field require a callback for post_predict to work
                regmodel += f'      begin\n' # wrap pulse_cbs as local var creation
                regmodel += f'         // Adding callback to regmodel field for singlepulse return to 0 in post_predict\n'
                regmodel += f'         uvm_reg_cbs_pulse_{self.name}_f_{field_name} pulse_cbs = new("pulse_cbs");\n'
                regmodel += f'         uvm_reg_field_cb::add({field_name}, pulse_cbs);\n'
                regmodel += f'      end\n'
        regmodel += f"\n"
        regmodel += f"   endfunction: build\n"

        regmodel += f"endclass : {map_table_entry.get_noarray_hw_name()}_reg\n"
        return regmodel

# ----------------------------------------------------------------------------

    def get_bit_reset_value(self, bit_id, map_table_entry):
        """
            A register is 8/16/32 bits, for bit_id in dw-1:0 find the field covering this bit
            and return the reset value associated
            (used in markdown to fill reset bit table)
            return a string, "0" if no field with reset value found
        """
        reset_value = "0" # pad field are static 0, so can be considered as 0-reset
        # Check if a field is addressing the bit
        for field in self.get_all_actual_field():
            if bit_id >= field.lsb_bit and bit_id < field.lsb_bit+field.bit_width:
                field_reset_value = field.get_reset_value(map_table_entry)
                reset_value = str((field_reset_value >> (bit_id - field.lsb_bit)) & 1)
                if not field.is_valid_reset_value(map_table_entry):
                    # field has no valid reset value, the integer value 0 is transform to X
                    reset_value = "X"
        return reset_value

# ----------------------------------------------------------------------------

    def get_reset_value(self, map_table_entry):
        """
            A register is 8/16/32 bits, return the reset value of full register
            return an integer
            get value to read just after reset
        """
        reset_value = 0
        # only actual field contribute to register reset value
        # pad register are known to be 0 when reading register
        for f in self.get_all_actual_field():
            reset_value += f.get_reset_value(map_table_entry) << f.lsb_bit
        return reset_value

# ----------------------------------------------------------------------------

    def get_reset_valid_mask(self, map_table_entry):
        """
            A register is 8/16/32 bits,
            return the reset valid mask to detect field without known reset value
            return an integer
        """
        reset_mask = (1<<PyriftBase.dw) - 1 # will reset mask on invalid reset field
        # only actual field contribute to register reset mask
        # pad register are known to be 0 when reading register
        for f in self.get_all_actual_field():
            if not f.is_valid_reset_value(map_table_entry):
                field_mask = ((1 << f.bit_width)-1) << f.lsb_bit
                reset_mask =  reset_mask & ~field_mask
        return reset_mask

# ----------------------------------------------------------------------------

    def fill_excel_row(self, workbook, worksheet, row, column_b0, entry):
        # add method to worksheet so can put string in cell range
        # (either single column, single line or multi)
        # wrap to either merge_range or write_string
        if not hasattr(worksheet, "set_cell"):
            # helper function for xls worksheet
            def set_cell(self, r1, c1, r2, c2, cell_text, fmt):
                if (r1==r2) and (c1==c2):
                    self.write_string(r1, c1, cell_text, fmt)
                else:
                    self.merge_range(r1, c1, r2, c2, cell_text, fmt)
            # print("Add Missing method to class of worksheet")
            worksheet.__class__.set_cell = set_cell

        # define different cell format to be used
        left_format = workbook.add_format({'align': 'left', 'valign': 'vcenter'})
        left_format.set_border(1) # have continuous line as border
        left_format_wrap = workbook.add_format({'align': 'left', 'valign': 'vcenter'})
        left_format_wrap.set_border(1) # have continuous line as border
        left_format_wrap.set_text_wrap() # long one line desc to wrap as multiline cell
        field_monoline_format = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        field_monoline_format.set_font_name('Courier New') # monospace to help clean formatting
        field_monoline_format.set_border(1) # have continuous line as border
        field_monoline_format.set_shrink() # fit in cell (:( exclusive to set_text_wrap ?)
        field_multiline_format = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        field_multiline_format.set_font_name('Courier New') # monospace to help clean formatting
        field_multiline_format.set_shrink() # fit in cell (:( exclusive to set_text_wrap ?)
        field_multiline_format.set_left()
        field_multiline_format.set_right()
        field_multiline_format_top = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        field_multiline_format_top.set_font_name('Courier New') # monospace to help clean formatting
        field_multiline_format_top.set_shrink() # fit in cell (:( exclusive to set_text_wrap ?)
        field_multiline_format_top.set_left()
        field_multiline_format_top.set_right()
        field_multiline_format_top.set_top()
        pad_format = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        pad_format.set_bg_color("silver") # pad have gray background as unused
        pad_format.set_top() # line separation
        pad_format.set_bottom() # line separation
        header_right_format = workbook.add_format({
            'align': 'right', 'bold': True, 'valign': 'vcenter'})
        header_right_format.set_text_wrap() # long field name get multi-line :(

        # field name may be tight to fit in small cell, prepare multi-line spliting
        all_field_name_multiline = []
        for field in self.get_all_actual_field():
            cell_width = PyriftBase.xls_option["bit_char_width"]*field.bit_width
            all_field_name_multiline.append(re.findall(".{1,%d}" % cell_width, field.name))
        row_count = max([len(s) for s in all_field_name_multiline])
        # print(row_count, all_field_name_multiline)

        if row_count>1:
            for row_id in range(row_count):
                worksheet.set_row(row+row_id, PyriftBase.xls_option["multiline_height"]) # small row height if multi-line

        # Now fill the row with all column contents
        # REG name
        worksheet.set_cell(
            row,             0,
            row+row_count-1, 0,
            self.name+entry.get_layer_bracket(), header_right_format)
        # address field (use formatting from PyriftBase.xls_option['register_address_formatting'])
        worksheet.set_cell(
            row,             1,
            row+row_count-1, 1,
            self.get_xls_formatted_address(entry), left_format)

        pad_bit_set = set(range(PyriftBase.dw)) # all bit in register are candidate to be pad bit
                                                # (unless found to be part of a field)
        for field_id, field in enumerate(self.get_all_actual_field()):
            field_name_multiline = all_field_name_multiline[field_id]
            lsb = field.lsb_bit
            msb = field.lsb_bit + field.bit_width - 1
            pad_bit_set = pad_bit_set - set(range(lsb,msb+1))

            if row_count>1 and len(field_name_multiline)>1:
                # skrink does not work on multiline :( , so no manual multiline split
                # longer than n char ? => split to multiline
                # multi line field to fit multiline cell
                for row_id,line in enumerate(field_name_multiline):
                    cell_width = PyriftBase.xls_option["bit_char_width"]*field.bit_width
                    worksheet.set_cell(
                        row+row_id, column_b0-msb,
                        row+row_id, column_b0-lsb,
                        f"{line:<{cell_width}}",
                        field_multiline_format_top if row_id==0 else field_multiline_format)
                # pad unused lines of multiline cell
                for row_id in range(len(field_name_multiline), row_count):
                    worksheet.set_cell(
                        row+row_id, column_b0-msb,
                        row+row_id, column_b0-lsb,
                        f"", # pad with empty string
                        field_multiline_format_top if row_id==0 else field_multiline_format)

            else:
                # single line register, to fit single or multiline cell
                worksheet.set_cell(
                    row,             column_b0-msb,
                    row+row_count-1, column_b0-lsb,
                    field.name, field_monoline_format)

        # gray out all pad bits
        for pad_bit in pad_bit_set:
            worksheet.set_cell(
                row,             column_b0-pad_bit,
                row+row_count-1, column_b0-pad_bit,
                '-', pad_format)

        data_format = '%%0%dX' % (PyriftBase.dw/4)
        worksheet.set_cell(
            row,             column_b0+1,
            row+row_count-1, column_b0+1,
            self.get_sw_access().name, left_format)
        worksheet.set_cell(
            row,             column_b0+2,
            row+row_count-1, column_b0+2,
            f"0x{data_format % self.get_reset_value(entry)}", left_format)
        worksheet.set_cell(
            row,             column_b0+3,
            row+row_count-1, column_b0+3,
            entry.register.get_oneline_desc(), left_format_wrap)

        return row_count

# ----------------------------------------------------------------------------

    def fill_excel_ref_row(self, workbook, worksheet, row, entry, register, field):
        """
            populate the excel row with register/field info
        """
        # Now fill the row with all column contents
        # REG name
        # address field (use formatting from PyriftBase.xls_option['register_address_formatting'])
        worksheet.write_string(row, 0, self.get_xls_formatted_address(entry))
        worksheet.write_string(row, 1, self.name+entry.get_layer_bracket())
        worksheet.write_string(row, 2, field.name)
        worksheet.write_string(row, 3, field.sw.name)
        worksheet.write_number(row, 4, field.bit_width)
        worksheet.write_number(row, 5, field.lsb_bit+field.bit_width-1)
        worksheet.write_number(row, 6, field.lsb_bit)
        worksheet.write_string(row, 7, f"0x{field.get_reset_value(entry):x}")

        worksheet.write_string(row, 8, register.get_oneline_desc()+" : "+field.get_oneline_desc())

# ----------------------------------------------------------------------------

    def document_to_word(self, document, map_table_entry):
        """
            Generate the register documentation in provided word document
            document is a python-docx already open document
            map_table_entry is the register in maptable (extra info : address, ...)
        """
        # print(f"Documenting register {self.name}")
        name_paragraph = document.add_paragraph(f"Register Name: {map_table_entry.get_regmap_hierarchy()}")
        name_paragraph.style = document.styles['Pyrift_Reg_Header']
        data_format = '%%0%dX' % (PyriftBase.dw/4)
        document.add_paragraph(f"Address: 0x{map_table_entry.address:04x}")
        document.add_paragraph(f"Reset value: 0x" + data_format % self.get_reset_value(map_table_entry))
        document.add_paragraph(f"Reset valid mask: 0x" + data_format % self.get_reset_valid_mask(map_table_entry))
        # reset value formatted as 32 bit bitfields
        table = document.add_table(rows=2, cols=PyriftBase.dw+1)
        for i in range(PyriftBase.dw+1):
            table.rows[0].cells[i].width=Cm(0.5)
            table.rows[1].cells[i].width=Cm(0.5)
        table.rows[0].cells[0].text="bit#"
        table.rows[1].cells[0].text="Reset"
        for i in range(PyriftBase.dw):
            table.rows[0].cells[1+i].text=str(PyriftBase.dw-1-i)
            table.rows[1].cells[1+i].text=self.get_bit_reset_value(PyriftBase.dw-1-i, map_table_entry)
            # cell margin can be customized here if needed with following (comment below)
            # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.left_indent=Cm(-0.15)
            # table.rows[0].cells[1+i].paragraphs[0].paragraph_format.right_indent=Cm(-0.15)
        # print("par-fmt=",dir(table.rows[0].cells[0])) # .paragraphs[0].paragraph_format.left_indent))
        table.style = document.styles['Pyrift_Bitfield']
        document.add_paragraph(f"")
        #
        table = document.add_table(rows=1, cols=7)
        # table.columns[0].width=Cm(0.5) # column width not honored
        table.rows[0].cells[0].width=Cm(0.5)
        table.rows[0].cells[1].width=Cm(0.5)
        table.rows[0].cells[2].width=Cm(0.5)
        table.rows[0].cells[3].width=Cm(2.0)
        table.rows[0].cells[4].width=Cm(2.0)
        table.rows[0].cells[5].width=Cm(1.0)
        table.rows[0].cells[6].width=Cm(7.0)
        table.rows[0].cells[0].text = "Bit"
        table.rows[0].cells[1].text = "RW"
        table.rows[0].cells[2].text = "Field"
        table.rows[0].cells[3].text = "Value Id"
        table.rows[0].cells[4].text = "Value"
        table.rows[0].cells[5].text = "Reset"
        table.rows[0].cells[6].text = "Description"

        for field in self.get_all_actual_field():
            field_name = field.get_name() # TODO, array_name_suffix
            row_cells = table.add_row().cells
            row_cells[0].width=Cm(0.5)
            row_cells[1].width=Cm(0.5)
            row_cells[2].width=Cm(0.5)
            row_cells[3].width=Cm(2.0)
            row_cells[4].width=Cm(2.0)
            row_cells[5].width=Cm(1.0)
            row_cells[6].width=Cm(7.0)
            row_cells[0].text = field.bit_range_str()
            row_cells[1].text = field.sw.name
            row_cells[2].text = field_name
            if field.encode:
                row_cells[3].text = f"{field.encode.__name__}:" +\
                                    "".join(["\n"+e.name for e in field.encode])
                row_cells[4].text = f":" +\
                                    "".join(["\n"+f"0x{e.value:0x}" for e in field.encode])
            else:
                row_cells[3].text = "-"
                row_cells[4].text = "-"
            if field.is_valid_reset_value(map_table_entry):
                row_cells[5].text = f"0x{field.get_reset_value(map_table_entry):0x}"
            else:
                row_cells[5].text = "X"
            # trim space at start of line to get nicer formating of cell in case of multiline desc
            row_cells[6].text = cleandoc(field.desc)

        table.style = document.styles['Pyrift_Register']

        document.add_paragraph("")

# ----------------------------------------------------------------------------
