# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_apb.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

# convert host interface (APB,...) signal to generic bus signal used to communicate with rif core
# host bus can be:
# - APB
# - APB_CDC
# - generic (experimental: untested)
# - SPI (future)
# - AHB (experimental: untested)
# - ICYFLEXV (passed basic simulation test)
# - AXI (far future)

# ----------------------------------------------------------------------------

from pyrift_field import PyriftBase

# ----------------------------------------------------------------------------
# host interfacing code generation is handled through a python class Host
# Host is considered abstract, and is to be subclassed according the host interface used : APB, AHB, ICYFLEXV, ...

class Pyrift_Host:
    def __init__(self):
        self.rif_host_bus = None
    def abstract_error(self):
        raise RuntimeError("Unsupported host using abstract class please define 'rif_host_bus' correctly")

    @classmethod
    def get_pyrift_host(cls, rif_host_bus):
        # Class method to build the sub class of pyrift_Host that will handle code generation for rif_host_bus
        # print(f"Subclassing Pyrift_Host for {rif_host_bus}")
        if (rif_host_bus == "APB"):
            return Pyrift_Host_Apb()
        if (rif_host_bus == "APB_CDC"):
            return Pyrift_Host_Apb_Cdc()
        if (rif_host_bus == "GENERIC"):
            return Pyrift_Host_Generic()
        if (rif_host_bus == "AHB"):
            return Pyrift_Host_Ahb()
        if (rif_host_bus == "ICYFLEXV"):
            return Pyrift_Host_Icyflexv()
        raise RuntimeError(f"Unsupported host {rif_host_bus} found in get_pyrift_host subclass constructor, "\
                            "please check rif_host_bus value")
        return None

    def experimental_warn(self):
        print(f"Warning: Using {self.rif_host_bus} host bus interface is still considered experimental")
        print(f"       : {self.rif_host_bus} host bus code pass verilator compilation, but so far was never tested in real design")
        print(f"       : Thanks to report successful use (open pyrift issue on https://gitlab.com/regisc/pyrift/-/issues)")
    def experimental_needed(self):
        raise RuntimeError(f"Error: Using {self.rif_host_bus} host bus interface is still considered experimental\n"\
                           f"     : {self.rif_host_bus} host bus requires to set 'PyriftBase.experimental = True' before use")

    def check_rif_host_bus(self):
        self.abstract_error()
    def host_port_declare(self):
        self.abstract_error()
    def host_port_cdc_layer(self, regmap):
        # Host if with no CDC generate no special code
        verilog = ""
        return verilog
    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_apb (or alternate bus as AHB or spi)
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        verilog = ""
        # build string for be using current DW
        verilog += f"// declare generic host bus signal\n"
        verilog += f"logic host_access_wr;\n"
            # write enable is provided as byte enable or bit mask
        verilog += f"logic [{PyriftBase.dw//8-1}:0] host_access_wr_be;\n"
        verilog += f"logic [{PyriftBase.dw-1}:0] host_access_wr_bmask;\n"

        verilog += f"logic host_access_rd;\n"
        verilog += f"logic host_access_rd_early; // early pulse on read cycle\n"
        verilog += f"logic host_access_ready;\n"
        verilog += f"logic [{PyriftBase.dw-1}:0] rif_rdata;\n"
        verilog += f"logic [{PyriftBase.dw-1}:0] sw_wrdata;\n"
        verilog += f"logic [31:0] host_addr;\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        self.abstract_error()
    def host_bus_unwrap_port(self):
        self.abstract_error()
    def host_bus_unwrap_if_declare(self, regmap):
        self.abstract_error()
    def host_bus_unwrap_instance_port_connect(self):
        self.abstract_error()
    def host_bus_unwrap_connect(self):
        self.abstract_error()

# ----------------------------------------------------------------------------
#        ###    ########  ########
#       ## ##   ##     ## ##     ##
#      ##   ##  ##     ## ##     ##
#     ##     ## ########  ########
#     ######### ##        ##     ##
#     ##     ## ##        ##     ##
#     ##     ## ##        ########

class Pyrift_Host_Apb(Pyrift_Host):
    def __init__(self):
        self.rif_host_bus = "APB"

    def check_rif_host_bus(self):
        pass # nothing special to check for apb

    def host_port_declare(self):
        # declaration to fit inside module port declaration
        # This is the last declaration (last port have no "," at the end)
        verilog = ""

        verilog += f"   // APB used to control all peripherals (synchronous to clk)\n"
        verilog += f"   IF_APB_BUS.slave    if_apb\n"

        return verilog

    def host_port_cdc_layer(self, regmap):
        return "" # no CDC on basic APB

    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_apb (or alternate bus as AHB or spi)
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        # First get signal declaration that are common to all host bus
        verilog = super().host_bus_to_generic_bus(regmap)
        wr_bmask_string = ", ".join(["{8{host_access_wr_be["+str(i)+"]}"+"}" for i in range(PyriftBase.dw//8-1,-1,-1)])
        # print("wr_bmask_string="+wr_bmask_string)

        verilog += f"// APB conversion to generic host interfacing\n"
        verilog += f"//\n"
        verilog += f"\n"
        verilog += f"logic apb_setup;\n"
        verilog += f"logic apb_access;\n"
        verilog += f"assign if_apb.PREADY = host_access_ready; // Wait state APB handling\n"
        verilog += f"                                          // for external register access\n"
        verilog += f"                                          // either a read or a write\n"
        verilog += f"assign if_apb.PSLVERR = 0; // No error supported so far\n"
        verilog += f"\n"
        verilog += f"assign apb_setup = if_apb.PSEL && !if_apb.PENABLE;\n"
        verilog += f"assign apb_access = if_apb.PSEL && if_apb.PENABLE;\n"
        verilog += f"assign host_addr = if_apb.PADDR;\n"
        verilog += f"assign sw_wrdata = if_apb.PWDATA;\n"
        verilog += f"assign host_access_wr = apb_access && if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        verilog += f"// {PyriftBase.dw} bit access, PSTRB are used as byte modifier (optional use)\n"
        verilog += f"// byte enable, bmask signals are used at register/field core implementation level\n"
        verilog += f"assign host_access_wr_be = {{{PyriftBase.dw//8}{{host_access_wr}}}} & if_apb.PSTRB ;\n"
        verilog += f"assign host_access_wr_bmask = {{\n" # expand byte level strobe to single bit level masking
        verilog += f"    {wr_bmask_string}\n"
        verilog += f"}} ; // bitmask enable usage defined per register/field\n"
        verilog += f"assign host_access_rd = apb_access && !if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        verilog += f"assign host_access_rd_early = apb_setup && !if_apb.PWRITE; // early pulse on read cycle\n"
        verilog += f"\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        verilog = ""

        verilog += (
            "// APB processing to generic host interfacing : rdata to host APB\n"
            "//\n"
            "\n"
            # all read register such as regmap_reg_swrdata, ... are muxed to rif_rdata
            "always_comb begin : proc_apb_rdata\n"
            "   if(rif_access_swrd) begin\n"
            "      if_apb.PRDATA = rif_rdata;\n"
            "   end else begin\n"
            "      if_apb.PRDATA = 0;\n"
            "   end\n"
            "end\n"
            "\n"
        )

        return verilog

    def host_bus_unwrap_port(self):
        """
            Define host port for unwrap module port (no interface allowed)
        """
        verilog = ""

        verilog += f"   // unwrap module, APB IO port definition\n"
        verilog += f"   // APB used to control all peripherals (synchronous to clk)\n"
        verilog += f"   input  var logic        psel,\n"
        verilog += f"   input  var logic        penable,\n"
        verilog += f"   input  var logic        pwrite,\n"
        verilog += f"   input  var logic [31:0] paddr,\n"
        verilog += f"   input  var logic [{PyriftBase.dw-1}:0] pwdata,\n"
        verilog += f"   input  var logic  [{PyriftBase.dw//8-1}:0] pstrb,\n"
        verilog += f"\n"
        verilog += f"   output var logic [{PyriftBase.dw-1}:0] prdata,\n"
        verilog += f"   output var logic        pslverr,\n"
        verilog += f"   output var logic        pready\n" # no comma for last port of module
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_if_declare(self, regmap):
        """
            Declare host bus (APB, ...) used between unwrap module declaration and Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_APB_BUS #(.DW({PyriftBase.dw})) if_apb(clk);\n"
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_APB_BUS if_apb(clk);\n"
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_instance_port_connect(self):
        """
            Connect host bus (APB, ...) in Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"   .if_apb(if_apb)\n"

        return verilog

    def host_bus_unwrap_connect(self):
        """
            Connect host port for unwrap module port to apb actual interface
            Code is part of the body of unwrap module
        """
        verilog = ""

        verilog += f"// APB connection of unwrap ports to APB interface\n"
        verilog += f"//\n"
        verilog += f"assign if_apb.PADDR = paddr;\n"
        verilog += f"assign if_apb.PSEL = psel;\n"
        verilog += f"assign if_apb.PENABLE = penable;\n"
        verilog += f"assign if_apb.PWDATA = pwdata;\n"
        verilog += f"assign if_apb.PWRITE = pwrite;\n"
        verilog += f"assign if_apb.PSTRB = pstrb;\n"
        verilog += f"//\n"
        verilog += f"assign pready  = if_apb.PREADY;\n"
        verilog += f"assign pslverr = if_apb.PSLVERR;\n"
        verilog += f"assign prdata  = if_apb.PRDATA;\n"
        verilog += f"\n"

        return verilog


# ----------------------------------------------------------------------------
#        ###    ########  ########           ######  ########   ######
#       ## ##   ##     ## ##     ##         ##    ## ##     ## ##    ##
#      ##   ##  ##     ## ##     ##         ##       ##     ## ##
#     ##     ## ########  ########          ##       ##     ## ##
#     ######### ##        ##     ##         ##       ##     ## ##
#     ##     ## ##        ##     ##         ##    ## ##     ## ##    ##
#     ##     ## ##        ########  #######  ######  ########   ######

class Pyrift_Host_Apb_Cdc(Pyrift_Host):
    def __init__(self):
        self.rif_host_bus = "APB_CDC"

    def check_rif_host_bus(self):
        pass # nothing special to check for apb

    def host_port_declare(self):
        # declaration to fit inside module port declaration
        # This is the last declaration (last port have no "," at the end)
        verilog = ""

        verilog += f"   // APB with CDC clocking used to control all peripherals\n"
        verilog += f"   // if_apb is synchronous to apb_cdc_clk\n"
        verilog += f"   // Rif core is synchronous to clk\n"
        verilog += f"   input  var logic apb_cdc_clk,\n"
        verilog += f"   IF_APB_BUS.slave if_apb_cdc\n"

        return verilog

    def host_port_cdc_layer(self, regmap):
        verilog = ""

        verilog += f"// rif_host_bus is APB_CDC\n"
        verilog += f"// APB at external port is 'if_apb_cdc' on 'apb_cdc_clk'\n"
        verilog += f"// instanciate pyrift_apb_bridge_cdc to cross the clock domain boundary\n"
        verilog += f"\n"
        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_APB_BUS #(.DW({PyriftBase.dw})) if_apb(clk);\n"
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_APB_BUS if_apb(clk);\n"
        verilog += f"\n"
        verilog += f"pyrift_apb_bridge_cdc #(.DW({PyriftBase.dw})) i_pyrift_apb_bridge_cdc (\n"
        if regmap.default_reset_signal.sync:
            raise RuntimeError("Sync reset used is not compatible with APB CDC option\n"
                               "Can you try to use Async reset ?")
        verilog += f"   .rst_n             ({regmap.default_reset_signal.low_name()}      ),\n"
        verilog += f"   .ck_cpu_side_i     (apb_cdc_clk),\n"
        verilog += f"   .ck_periph_side_i  (clk        ),\n"
        verilog += f"   .if_apb_cpu_side   (if_apb_cdc ),\n"
        verilog += f"   .if_apb_periph_side(if_apb     )\n"
        verilog += f");\n"
        verilog += "\n"

        verilog += "// ----------------------------------------------------------------------------\n"
        verilog += "\n"

        return verilog

    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_apb (or alternate bus as AHB or spi)
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        # First get signal declaration that are common to all host bus
        verilog = super().host_bus_to_generic_bus(regmap)
        wr_bmask_string = ", ".join(["{8{host_access_wr_be["+str(i)+"]}"+"}" for i in range(PyriftBase.dw//8-1,-1,-1)])
        # print("wr_bmask_string="+wr_bmask_string)

        verilog += f"// APB conversion to generic host interfacing\n"
        verilog += f"//\n"
        verilog += f"\n"
        verilog += f"logic apb_setup;\n"
        verilog += f"logic apb_access;\n"
        verilog += f"assign if_apb.PREADY = host_access_ready; // Wait state APB handling\n"
        verilog += f"                                          // for external register access\n"
        verilog += f"                                          // either a read or a write\n"
        verilog += f"assign if_apb.PSLVERR = 0; // No error supported so far\n"
        verilog += f"\n"
        verilog += f"assign apb_setup = if_apb.PSEL && !if_apb.PENABLE;\n"
        verilog += f"assign apb_access = if_apb.PSEL && if_apb.PENABLE;\n"
        verilog += f"assign host_addr = if_apb.PADDR;\n"
        verilog += f"assign sw_wrdata = if_apb.PWDATA;\n"
        verilog += f"assign host_access_wr = apb_access && if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        verilog += f"// {PyriftBase.dw} bit access, PSTRB are used as byte modifier (optional use)\n"
        verilog += f"// byte enable, bmask signals are used at register/field core implementation level\n"
        verilog += f"assign host_access_wr_be = {{{PyriftBase.dw//8}{{host_access_wr}}}} & if_apb.PSTRB ;\n"
        verilog += f"assign host_access_wr_bmask = {{\n" # expand byte level strobe to single bit level masking
        verilog += f"    {wr_bmask_string}\n"
        verilog += f"}} ; // bitmask enable usage defined per register/field\n"
        verilog += f"assign host_access_rd = apb_access && !if_apb.PWRITE; // Simple IF, use APB access phase for host\n"
        verilog += f"assign host_access_rd_early = apb_setup && !if_apb.PWRITE; // early pulse on read cycle\n"
        verilog += f"\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        verilog = ""

        verilog += (
            "// APB processing to generic host interfacing : rdata to host APB\n"
            "//\n"
            "\n"
            # all read register such as regmap_reg_swrdata, ... are muxed to rif_rdata
            "always_comb begin : proc_apb_rdata\n"
            "   if(rif_access_swrd) begin\n"
            "      if_apb.PRDATA = rif_rdata;\n"
            "   end else begin\n"
            "      if_apb.PRDATA = 0;\n"
            "   end\n"
            "end\n"
            "\n"
        )

        return verilog

    def host_bus_unwrap_port(self):
        """
            Define host port for unwrap module port (no interface allowed)
        """
        verilog = ""

        verilog += f"   // unwrap module, APB IO port definition\n"
        verilog += f"   // APB used to control all peripherals (synchronous to apb_cdc_clk)\n"
        verilog += f"   input  var logic apb_cdc_clk,\n"
        verilog += f"\n"
        verilog += f"   input  var logic        psel,\n"
        verilog += f"   input  var logic        penable,\n"
        verilog += f"   input  var logic        pwrite,\n"
        verilog += f"   input  var logic [31:0] paddr,\n"
        verilog += f"   input  var logic [{PyriftBase.dw-1}:0] pwdata,\n"
        verilog += f"   input  var logic  [{PyriftBase.dw//8-1}:0] pstrb,\n"
        verilog += f"\n"
        verilog += f"   output var logic [{PyriftBase.dw-1}:0] prdata,\n"
        verilog += f"   output var logic        pslverr,\n"
        verilog += f"   output var logic        pready\n" # no comma for last port of module
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_if_declare(self, regmap):
        """
            Declare host bus (APB, ...) used between unwrap module declaration and Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""

        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_APB_BUS #(.DW({PyriftBase.dw})) if_apb_cdc(apb_cdc_clk);\n"
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_APB_BUS if_apb_cdc(apb_cdc_clk);\n"
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_instance_port_connect(self):
        """
            Connect host bus (APB, ...) in Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""

        verilog += "\n"
        verilog += f"   .apb_cdc_clk(apb_cdc_clk),\n"
        verilog += f"   .if_apb_cdc(if_apb_cdc)\n"

        return verilog

    def host_bus_unwrap_connect(self):
        """
            Connect host port for unwrap module port to apb actual interface
            Code is part of the body of unwrap module
        """
        verilog = ""

        apb_cdc_ext = '_cdc'
        verilog += f"// APB connection of unwrap ports to APB interface\n"
        verilog += f"//\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PADDR = paddr;\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PSEL = psel;\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PENABLE = penable;\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PWDATA = pwdata;\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PWRITE = pwrite;\n"
        verilog += f"assign if_apb{apb_cdc_ext}.PSTRB = pstrb;\n"
        verilog += f"//\n"
        verilog += f"assign pready  = if_apb{apb_cdc_ext}.PREADY;\n"
        verilog += f"assign pslverr = if_apb{apb_cdc_ext}.PSLVERR;\n"
        verilog += f"assign prdata  = if_apb{apb_cdc_ext}.PRDATA;\n"
        verilog += f"\n"

        verilog += f"//  apb_cdc_clk is connected directly to instanciated module\n"
        verilog += f"\n"

        return verilog

# ----------------------------------------------------------------------------
#      ######   ######## ##    ## ######## ########  ####  ######
#     ##    ##  ##       ###   ## ##       ##     ##  ##  ##    ##
#     ##        ##       ####  ## ##       ##     ##  ##  ##
#     ##   #### ######   ## ## ## ######   ########   ##  ##
#     ##    ##  ##       ##  #### ##       ##   ##    ##  ##
#     ##    ##  ##       ##   ### ##       ##    ##   ##  ##    ##
#      ######   ######## ##    ## ######## ##     ## ####  ######

class Pyrift_Host_Generic(Pyrift_Host):
    def __init__(self):
        self.rif_host_bus = "GENERIC"
        if PyriftBase.experimental:
            self.experimental_warn()
        else:
            self.experimental_needed()


    def check_rif_host_bus(self):
        pass # nothing special to check for AHB

    def host_port_declare(self):
        # declaration to fit inside module port declaration
        # This is the last declaration (last port have no "," at the end)
        verilog = ""

        verilog += f"   // GENERIC used to control all peripherals (synchronous to clk)\n"
        verilog += f"   IF_GENERIC_BUS.slave    if_generic\n"

        return verilog

    def host_port_cdc_layer(self, regmap):
        return "" # no CDC on GENERIC

    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_generic
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        # First get signal declaration that are common to all host bus
        verilog = super().host_bus_to_generic_bus(regmap)
        wr_bmask_string = ", ".join(["{8{host_access_wr_be["+str(i)+"]}"+"}" for i in range(PyriftBase.dw//8-1,-1,-1)])
        # print("wr_bmask_string="+wr_bmask_string)

        verilog += f"// GENERIC conversion to generic host interfacing\n"
        verilog += f"//\n"
        verilog += f"\n"
        verilog += f"assign if_generic.gready = host_access_ready; // Wait state GENERIC handling\n"
        verilog += f"                                              // for external register access\n"
        verilog += f"                                              // either a read or a write\n"
        verilog += f"\n"
        verilog += f"assign host_addr = if_generic.gaddr;\n"
        verilog += f"assign sw_wrdata = if_generic.gwdata;\n"
        verilog += f"assign host_access_wr = if_generic.gwr;\n"
        verilog += f"// byte enable, bmask signals are used at register/field core implementation level\n"
        verilog += f"assign host_access_wr_be = {{{PyriftBase.dw//8}{{host_access_wr}}}} & if_generic.gbe ;\n"
        verilog += f"assign host_access_wr_bmask = {{\n" # expand byte level strobe to single bit level masking
        verilog += f"    {wr_bmask_string}\n"
        verilog += f"}} ; // bitmask enable usage defined per register/field\n"
        verilog += f"assign host_access_rd = if_generic.grd;\n"
        verilog += f"assign host_access_rd_early = if_generic.grd_early; // early pulse on read cycle\n"
        verilog += f"\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        verilog = ""
        verilog += (
            "// GENERIC processing to generic host interfacing : rdata to host GENERIC\n"
            "//\n"
            "\n"
            # all read register such as regmap_reg_swrdata, ... are muxed to rif_rdata
            "always_comb begin : proc_generic_rdata\n"
            "   if(rif_access_swrd) begin\n"
            "      if_generic.grdata = rif_rdata;\n"
            "   end else begin\n"
            "      if_generic.grdata = 0;\n"
            "   end\n"
            "end\n"
            "\n"
        )

        return verilog

    def host_bus_unwrap_port(self):
        """
            Define host port for unwrap module port (no interface allowed)
        """
        verilog = ""
        verilog += f"   // unwrap module, GENERIC IO port definition\n"
        verilog += f"   // GENERIC used to control all peripherals (synchronous to clk)\n"
        verilog += f"   input  var logic [31:0] gaddr,\n"
        verilog += f"   input  var logic        grd,\n"
        verilog += f"   input  var logic        grd_early,\n"
        verilog += f"   input  var logic        gwr,\n"
        verilog += f"   input  var logic  [{PyriftBase.dw//8-1}:0] gbe,\n"
        verilog += f"   input  var logic [{PyriftBase.dw-1}:0] gwdata,\n"
        verilog += f"\n"
        verilog += f"   output var logic [{PyriftBase.dw-1}:0] grdata,\n"
        verilog += f"   output var logic        gready\n" # no comma for last port of module
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_if_declare(self, regmap):
        """
            Declare host bus (APB, ...) used between unwrap module declaration and Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_GENERIC_BUS #(.DW({PyriftBase.dw})) if_generic(clk);\n"
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_GENERIC_BUS if_generic(clk);\n"
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_instance_port_connect(self):
        """
            Connect host bus (APB, ...) in Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"   .if_generic(if_generic)\n"

        return verilog

    def host_bus_unwrap_connect(self):
        """
            Connect host port for unwrap module port to apb actual interface
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"// GENERIC connection of unwrap ports to GENERIC interface\n"
        verilog += f"//\n"
        verilog += f"assign if_generic.gaddr     = gaddr;\n"
        verilog += f"assign if_generic.grd       = grd;\n"
        verilog += f"assign if_generic.grd_early = grd_early;\n"
        verilog += f"assign if_generic.gwr       = gwr;\n"
        verilog += f"assign if_generic.gbe       = gbe;\n"
        verilog += f"assign if_generic.gwdata    = gwdata;\n"
        verilog += f"//\n"
        verilog += f"assign grdata = if_generic.grdata;\n"
        verilog += f"assign gready = if_generic.gready;\n"
        verilog += f"\n"

        return verilog

# ----------------------------------------------------------------------------
#     ####  ######  ##    ## ######## ##       ######## ##     ## ##     ##
#      ##  ##    ##  ##  ##  ##       ##       ##        ##   ##  ##     ##
#      ##  ##         ####   ##       ##       ##         ## ##   ##     ##
#      ##  ##          ##    ######   ##       ######      ###    ##     ##
#      ##  ##          ##    ##       ##       ##         ## ##    ##   ##
#      ##  ##    ##    ##    ##       ##       ##        ##   ##    ## ##
#     ####  ######     ##    ##       ######## ######## ##     ##    ###

class Pyrift_Host_Icyflexv(Pyrift_Host):
    def __init__(self):
        self.rif_host_bus = "ICYFLEXV"

    def check_rif_host_bus(self):
        pass # nothing special to check for ICYFLEXV

    def host_port_declare(self):
        # declaration to fit inside module port declaration
        # This is the last declaration (last port have no "," at the end)
        verilog = ""

        verilog += f"   // ICYFLEXV used to control all peripherals (synchronous to clk)\n"
        verilog += f"   IF_ICYFLEXV_BUS.slave    if_icyflexv\n"

        return verilog

    def host_port_cdc_layer(self, regmap):
        return "" # no CDC on ICYFLEXV

    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_apb (or alternate bus as AHB or spi)
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        # First get signal declaration that are common to all host bus
        verilog = super().host_bus_to_generic_bus(regmap)
        wr_bmask_string = ", ".join(["{8{host_access_wr_be["+str(i)+"]}"+"}" for i in range(PyriftBase.dw//8-1,-1,-1)])
        # print("wr_bmask_string="+wr_bmask_string)

        # ICYFLEXV contain addr/ctrl resync flop, this will use the regmap.default_reset_signal
        verilog += f"// ICYFLEXV conversion to generic host interfacing\n"
        verilog += f"// Address and control are recorded during req phase\n"
        verilog += f"// Actual pyrift cycle take place during data rvalid phase after req/gnt (either read or write)\n"
        verilog += f"\n"
        verilog += f"logic [31:0] icyflexv_addr_resync;\n"
        verilog += f"logic        icyflexv_access_resync; // access on-going till rvalid (set on Gnt, else reset on rvalid)\n"
        verilog += f"logic        icyflexv_write_resync;\n"
        verilog += f"logic [{PyriftBase.dw}-1:0] icyflexv_wdata_resync;\n"
        verilog += f"logic  [{PyriftBase.dw//8}-1:0] icyflexv_be_resync;\n"

        verilog += f"// gnt is immediate response to req\n"
        verilog += f"assign if_icyflexv.gnt = if_icyflexv.req;\n"

        # always one default reset signal, just get sure to match (a)sync & polarity: (reset_edge() convert to the sync/async low/async high)
        verilog += f"always_ff @(posedge clk{regmap.default_reset_signal.reset_edge()}) begin : proc_icyflexv_resync_for_access\n"
        verilog += f"   if({regmap.default_reset_signal.high_name()}) begin\n"
        verilog += f"      icyflexv_addr_resync <= 0;\n"
        verilog += f"      icyflexv_access_resync <= 0;\n"
        verilog += f"      icyflexv_write_resync <= 0;\n"
        verilog += f"      icyflexv_wdata_resync <= 0;\n"
        verilog += f"      icyflexv_be_resync <= 0;\n"
        verilog += f"   end else begin\n"
        #                  Resync only engaging a new access (that is when req/gnt is active)
        verilog += f"      if(if_icyflexv.gnt) begin\n"
        verilog += f"         icyflexv_addr_resync <= if_icyflexv.addr;\n"
        verilog += f"         icyflexv_access_resync <= 1;\n"
        verilog += f"         icyflexv_write_resync <= if_icyflexv.we;\n"
        verilog += f"         icyflexv_wdata_resync <= if_icyflexv.wdata;\n"
        verilog += f"         icyflexv_be_resync <= if_icyflexv.be;\n"
        verilog += f"      end else if (if_icyflexv.rvalid) begin\n"
        verilog += f"         // No new gnt cycle, and access completing now\n"
        verilog += f"         icyflexv_access_resync <= 0;\n"
        verilog += f"      end\n"
        verilog += f"   end\n"
        verilog += f"end\n"

        verilog += f"// Wait state ICYFLEXV handling for external register access either a read or a write\n"
        verilog += f"assign if_icyflexv.rvalid = icyflexv_access_resync && host_access_ready;\n"
        verilog += f"assign if_icyflexv.berr = 0; // No error supported so far\n"
        verilog += f"\n"
        verilog += f"assign host_addr = icyflexv_addr_resync;\n"
        verilog += f"assign sw_wrdata = icyflexv_wdata_resync;\n"
        verilog += f"assign host_access_wr = icyflexv_access_resync && icyflexv_write_resync;\n"
        verilog += f"// {PyriftBase.dw} bit access, be are used as byte modifier (optional use)\n"
        verilog += f"// byte enable, bmask signals are used at register/field core implementation level\n"
        verilog += f"assign host_access_wr_be = {{{PyriftBase.dw//8}{{host_access_wr}}}} & icyflexv_be_resync ;\n"
        verilog += f"assign host_access_wr_bmask = {{\n" # expand byte level strobe to single bit level masking
        verilog += f"    {wr_bmask_string}\n"
        verilog += f"}} ; // bitmask enable usage defined per register/field\n"
        verilog += f"assign host_access_rd = icyflexv_access_resync && !icyflexv_write_resync; // Simple IF, use APB access phase for host\n"
        verilog += f"assign host_access_rd_early = if_icyflexv.gnt && !if_icyflexv.we; // early pulse on gnt read cycle\n"
        verilog += f"\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        verilog = ""
        verilog += (
            "// ICYFLEXV processing to generic host interfacing : rdata to host ICYFLEXV\n"
            "//\n"
            "\n"
            # all read register such as regmap_reg_swrdata, ... are muxed to rif_rdata
            "always_comb begin : proc_icyflexv_rdata\n"
            "   if(rif_access_swrd) begin\n"
            "      if_icyflexv.rdata = rif_rdata;\n"
            "   end else begin\n"
            "      if_icyflexv.rdata = 0;\n"
            "   end\n"
            "end\n"
            "\n"
        )

        return verilog

    def host_bus_unwrap_port(self):
        """
            Define host port for unwrap module port (no interface allowed)
        """
        verilog = ""
        verilog += f"   // unwrap module, ICYFLEXV IO port definition\n"
        verilog += f"   // ICYFLEXV used to control all peripherals (synchronous to clk)\n"
        verilog += f"   input  var logic        req,\n"
        verilog += f"   input  var logic        we,\n"
        verilog += f"   input  var logic  [3:0] be,\n"
        verilog += f"   input  var logic [31:0] addr,\n"
        verilog += f"   input  var logic [{PyriftBase.dw-1}:0] wdata,\n" # only 32 bits usable, but DW=32 by default
        verilog += f"\n"
        verilog += f"   output var logic        gnt,\n"
        verilog += f"   output var logic        rvalid,\n"
        verilog += f"   output var logic [{PyriftBase.dw-1}:0] rdata,\n" # only 32 bits usable, but DW=32 by default
        verilog += f"   output var logic        berr\n" # no comma for last port of module
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_if_declare(self, regmap):
        """
            Declare host bus (APB, ...) used between unwrap module declaration and Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""

        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_ICYFLEXV_BUS #(.DW({PyriftBase.dw})) if_icyflexv(clk, {regmap.default_reset_signal.low_name()});\n"
            print(f"Warning: IcyflexV is usually only 32 bits, now required as {PyriftBase.dw}, please check this is as expected")
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_ICYFLEXV_BUS if_icyflexv(clk, {regmap.default_reset_signal.low_name()});\n"
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_instance_port_connect(self):
        """
            Connect host bus (APB, ...) in Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"   .if_icyflexv(if_icyflexv)\n"

        return verilog

    def host_bus_unwrap_connect(self):
        """
            Connect host port for unwrap module port to apb actual interface
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"// ICYFLEXV connection of unwrap ports to ICYFLEXV interface\n"
        verilog += f"//\n"
        verilog += f"assign if_icyflexv.req = req;\n"
        verilog += f"assign if_icyflexv.we = we;\n"
        verilog += f"assign if_icyflexv.be = be;\n"
        verilog += f"assign if_icyflexv.addr = addr;\n"
        verilog += f"assign if_icyflexv.wdata = wdata;\n"
        verilog += f"//\n"
        verilog += f"assign gnt  = if_icyflexv.gnt;\n"
        verilog += f"assign rvalid  = if_icyflexv.rvalid;\n"
        verilog += f"assign berr = if_icyflexv.berr;\n"
        verilog += f"assign rdata  = if_icyflexv.rdata;\n"
        verilog += f"\n"

        return verilog

# ----------------------------------------------------------------------------
#        ###    ##     ## ########
#       ## ##   ##     ## ##     ##
#      ##   ##  ##     ## ##     ##
#     ##     ## ######### ########
#     ######### ##     ## ##     ##
#     ##     ## ##     ## ##     ##
#     ##     ## ##     ## ########

class Pyrift_Host_Ahb(Pyrift_Host):
    def __init__(self):
        self.rif_host_bus = "AHB"
        if PyriftBase.experimental:
            self.experimental_warn()
        else:
            self.experimental_needed()


    def check_rif_host_bus(self):
        pass # nothing special to check for AHB

    def host_port_declare(self):
        # declaration to fit inside module port declaration
        # This is the last declaration (last port have no "," at the end)
        verilog = ""

        verilog += f"   // AHB used to control all peripherals (synchronous to clk)\n"
        verilog += f"   IF_AHB_BUS.slave    if_ahb\n"

        return verilog

    def host_port_cdc_layer(self, regmap):
        return "" # no CDC on AHB

    def host_bus_to_generic_bus(self, regmap):
        """
            host bus side is : if_ahb
            rif side is the generic bus:
                - host_access_addr
                - host_access_rd
                - host_access_rd_early
                - host_access_ready
                - host_access_wr_be
                - host_access_wr_bmask
                - rif_rdata
                - sw_wrdata
        """
        # First get signal declaration that are common to all host bus
        verilog = super().host_bus_to_generic_bus(regmap)
        wr_bmask_string = ", ".join(["{8{host_access_wr_be["+str(i)+"]}"+"}" for i in range(PyriftBase.dw//8-1,-1,-1)])
        # print("wr_bmask_string="+wr_bmask_string)

        # AHB contain addr/ctrl resync flop, this will use the regmap.default_reset_signal
        verilog += f"// AHB conversion to generic host interfacing\n"
        verilog += f"// Address and control are recorded during address phase\n"
        verilog += f"// Actual pyrift cycle take place during data phase (either read or write)\n"
        verilog += f"// HBURST is not used (no anticipation for burst), all cycles are supported as is non burst\n"
        verilog += f"\n"
        verilog += f"logic [31:0] ahb_addr_resync;\n"
        verilog += f"logic        ahb_sel_resync; // data phase will have active cycle (from HSEL+HTRANS)\n"
        verilog += f"logic        ahb_sel_next; // will engage cycle (on ready)\n"
        verilog += f"logic        ahb_write_resync;\n"
        verilog += f"logic [{PyriftBase.dw//8}-1:0] ahb_be_resync;\n"

        # The transfer size set by HSIZE must be less than or equal to the width of the data bus.
        # For example, with a 32-bit data bus, HSIZE must only use the values 0b000, 0b001, or 0b010. (8, 16 or 32 bit transfer)
        # So far pyrift support only little endian RIF on AHB
        # (should be equivalent to what is called in ahb spec: 32-bit byte-invariant big-endian data bus)
        # So far AHB 32-bit word-invariant big-endian data bus is NOT supported
        # TODO: add support if needed (just add parameter to know if target is little or big, then rework BE evaluation)

        # What is the log2(Byte width)-1 ?
        # knowing PyriftBase.dw//8 is power of 2
        log2_dw_m1 = 0
        tmp_byte_count = PyriftBase.dw//8
        while tmp_byte_count>1:
            tmp_byte_count = tmp_byte_count // 2
            log2_dw_m1 +=1
        # here we have log2_dw_m1 =0 for 8 bit, 1 for 16 bit, 2 for 32 bits, ...
        # Find out what are the byte lane used for transfer, this depend only on HSIZE and lsb of address
        verilog += f"// {PyriftBase.dw} bit access, HSIZE and HADDR are used to get the byte modifier (optional use)\n"
        verilog += f"// byte enable, bmask signals are used at register/field core implementation level\n"
        verilog += f"// SIZE is in range 0 to {log2_dw_m1}\n"
        verilog += f"// HADDR[{log2_dw_m1-1}:0] used for Byte enabling\n"
        verilog += f"logic [{PyriftBase.dw//8}-1:0] ahb_be_next;\n"
        # For 32 bit:
        #            BE[3210]
        # size=0=8b @=0 0001
        # size=0=8b @=1 0010
        # size=0=8b @=2 0100
        # size=0=8b @=3 1000
        #             BE[3210]
        # size=1=16b @=0 0011
        # size=1=16b @=2 1100
        #             BE[3210]
        # size=2=32b @=0 1111
        verilog += f"always_comb begin : proc_ahb_be_next\n"
        for ahb_size in range(log2_dw_m1+1):
            verilog += f"   if(if_ahb.HSIZE==3'd{ahb_size}) begin // size is {8*(1<<ahb_size)} bits\n"
            verilog += f"      ahb_be_next = 'b{'1'*(1<<ahb_size)}"
            verilog += f"<<if_ahb.HADDR[{log2_dw_m1-1}:{ahb_size}];\n" if ahb_size<log2_dw_m1 else ";\n"
            verilog += f"   end else\n"
        # put default value all line active if no valid setup found (this is if master used size larger than current pyrift dw)
        verilog += f"   begin\n"
        verilog += f"      // illegal size found on AHB, larger than DW, recover with full DW used \n"
        verilog += f"      ahb_be_next = {PyriftBase.dw//8}'b{'1'*(PyriftBase.dw//8)};\n"
        verilog += f"   end\n"
        verilog += f"end\n"
        verilog += f"\n"

        verilog += f"assign ahb_sel_next = if_ahb.HSEL && (if_ahb.HTRANS==2'b10 || if_ahb.HTRANS==2'b11); // SEQ or NSEQ;\n"
        # always one default reset signal, just get sure to match (a)sync & polarity: (reset_edge() convert to the sync/async low/async high)
        verilog += f"always_ff @(posedge clk{regmap.default_reset_signal.reset_edge()}) begin : proc_ahb_resync_for_data_phase\n"
        verilog += f"   if({regmap.default_reset_signal.high_name()}) begin\n"
        verilog += f"      ahb_sel_resync <= 0;\n"
        verilog += f"      ahb_addr_resync <= 0;\n"
        verilog += f"      ahb_write_resync <= 0;\n"
        verilog += f"      ahb_be_resync <= 0;\n"
        verilog += f"   end else begin\n"
        #                  Resync only when getting from ADDR phase to data phase (valid DATA phase completing with HREADY)
        #                  IDLE, BUSY are ignore and do NOT start anything
        #                  SEQ or NON SEQ start a cycle in pyrift (no difference for RIF)
        verilog += f"      if(if_ahb.HREADY) begin\n"
        verilog += f"         ahb_sel_resync <= ahb_sel_next;\n"
        #                     Control signal are recorded only if valid cycle is engaged (sel get to 1), otherwise keep same unused value
        verilog += f"         if(if_ahb.HSEL && (if_ahb.HTRANS==2'b10 || if_ahb.HTRANS==2'b11)) begin\n"
        verilog += f"            ahb_addr_resync <= if_ahb.HADDR;\n"
        verilog += f"            ahb_write_resync <= if_ahb.HWRITE;\n"
        verilog += f"            ahb_be_resync <= ahb_be_next;\n"
        verilog += f"         end\n"
        verilog += f"      end\n"
        verilog += f"   end\n"
        verilog += f"end\n"

        verilog += f"assign if_ahb.HREADY =\n"
        verilog += f"   ! ahb_sel_resync // No Wait inserted if not cycle engaged (HREADY==0 only during data phase active)\n"
        verilog += f"   ||  host_access_ready; // Wait state AHB handling\n"
        verilog += f"                          // for external register access\n"
        verilog += f"                          // either a read or a write\n"
        verilog += f"assign if_ahb.HRESP = 2'b00; // No error supported so far : 0 is always OKAY\n"
        verilog += f"\n"
        verilog += f"assign host_addr = ahb_addr_resync;\n"
        verilog += f"assign sw_wrdata = if_ahb.HWDATA;\n"
        verilog += f"assign host_access_wr = ahb_sel_resync && ahb_write_resync; // Simple IF, use sel resync as AHB data phase for host\n"
        verilog += f"assign host_access_wr_be = {{{PyriftBase.dw//8}{{host_access_wr}}}} & ahb_be_resync;\n"
        verilog += f"assign host_access_wr_bmask = {{\n" # expand byte level strobe to single bit level masking
        verilog += f"    {wr_bmask_string}\n"
        verilog += f"}} ; // bitmask enable usage defined per register/field\n"
        verilog += f"assign host_access_rd = ahb_sel_resync && !ahb_write_resync; // Simple IF, use sel resync as AHB data phase for host\n"
        verilog += f"assign host_access_rd_early = ahb_sel_next && if_ahb.HREADY && !if_ahb.HWRITE; // early pulse on read cycle\n"
        verilog += f"\n"
        verilog += f"\n"

        return verilog

    def host_bus_read_from_generic(self):
        verilog = ""
        verilog += (
            "// AHB processing to generic host interfacing : rdata to host AHB\n"
            "//\n"
            "\n"
            # all read register such as regmap_reg_swrdata, ... are muxed to rif_rdata
            "always_comb begin : proc_ahb_rdata\n"
            "   if(rif_access_swrd) begin\n"
            "      if_ahb.HRDATA = rif_rdata;\n"
            "   end else begin\n"
            "      if_ahb.HRDATA = 0;\n"
            "   end\n"
            "end\n"
            "\n"
        )

        return verilog

    def host_bus_unwrap_port(self):
        """
            Define host port for unwrap module port (no interface allowed)
        """
        verilog = ""
        verilog += f"   // unwrap module, AHB IO port definition\n"
        verilog += f"   // AHB used to control all peripherals (synchronous to clk)\n"
        verilog += f"   input  var logic        hsel,\n"
        verilog += f"   input  var logic  [1:0] htrans,\n"
        verilog += f"   input  var logic        hwrite,\n"
        verilog += f"   input  var logic [31:0] haddr,\n"
        verilog += f"   input  var logic [{PyriftBase.dw-1}:0] hwdata,\n"
        verilog += f"   input  var logic  [2:0] hburst,\n"
        verilog += f"   input  var logic  [2:0] hsize,\n"
        verilog += f"   // hmastlock : No lock transfer supported\n"
        verilog += f"   // hprot : No protection control supported\n"
        verilog += f"\n"
        verilog += f"   output var logic [{PyriftBase.dw-1}:0] hrdata,\n"
        verilog += f"   output var logic  [1:0] hresp,\n"
        verilog += f"   output var logic        hready\n" # no comma for last port of module
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_if_declare(self, regmap):
        """
            Declare host bus (APB, ...) used between unwrap module declaration and Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""

        if PyriftBase.dw!=32:
            # only non default 32 bit bus get parametrized
            verilog += f"IF_AHB_BUS #(.DW({PyriftBase.dw})) if_ahb(clk);\n"
        else:
            # default 32 bit bus, no DW parameter used (user can use basic interface declaration)
            verilog += f"IF_AHB_BUS if_ahb(clk);\n"
        verilog += f"\n"

        return verilog

    def host_bus_unwrap_instance_port_connect(self):
        """
            Connect host bus (APB, ...) in Rif instanciation
            Code is part of the body of unwrap module
        """
        verilog = ""
        verilog += f"   .if_ahb(if_ahb)\n"

        return verilog

    def host_bus_unwrap_connect(self):
        """
            Connect host port for unwrap module port to apb actual interface
            Code is part of the body of unwrap module
        """
        verilog = ""

        verilog += f"// AHB connection of unwrap ports to AHB interface\n"
        verilog += f"//\n"
        verilog += f"assign if_ahb.HADDR = haddr;\n"
        verilog += f"assign if_ahb.HSEL = hsel;\n"
        verilog += f"assign if_ahb.HBURST = hburst;\n"
        verilog += f"assign if_ahb.HWDATA = hwdata;\n"
        verilog += f"assign if_ahb.HWRITE = hwrite;\n"
        verilog += f"assign if_ahb.HTRANS = htrans;\n"
        verilog += f"// if_ahb.HMASTLOCK : No lock transfer supported\n"
        verilog += f"// if_ahb.HPROT : No protection control supported\n"
        verilog += f"//\n"
        verilog += f"assign hready  = if_ahb.HREADY;\n"
        verilog += f"assign hresp = if_ahb.HRESP;\n"
        verilog += f"assign hrdata  = if_ahb.HRDATA;\n"
        verilog += f"\n"

        return verilog

# ----------------------------------------------------------------------------
