################################################
# pyrift : Python Register InterFace Translate
################################################

Pyrift is a collection of python script to help generating  
Systemverilog hardware description of Register InterFace (RIF).
Generated rtl connect to APB (Advanced Peripheral Bus from AMBA)

Supported features are inspired from SystemRDL.  
This support access properties : read and/or write access on either hw
or sw side (the APB host bus or the user connected hardware).  
The generated rtl includes the storage element (flop).  
Sw option includes mode like "write one clear", or clear on read, or ...  
Hw option have hwset feature, and notification pulse on sw read or write access.  

Support collection register or array of register in a regmap (with addressing table generation for register)  
Regmap or array of regmap can be nested.  
Register is a collection of fields (with bit range) within the 8/16/32 bit width of register.  

Pyrift object are python class constructed in user scripts.
Then the script generated following files:
- Systemverilog rtl implementation of register, host bus interface
- Systemverilog interface between RIF and user hardware
- systemverilog package definition (for enum type)
- C header file (help software access to generated RIF)
- SVD XML description of registers (for debugger input) : very experimental
- Html documentation
- Uvm reg model
 
# Status/Roadmap

This code is provided as GPL licence.  
It is available as is. This is still work in progress.  
Generated code is still experimental for several features.  
Generated code have **NO** licence, and is full property of user generating the file.  
Nonetheless, in actual state, the generated code can save a lot of coding time of rtl verilog.  
(at least this works for me).  

If you use it for real hardware project, 
please do have your own testing and verification 
in your environment.
It is starting to be really usable, and saved me a lot of time in hand writing
verilog code for RIF.
I know it is already use profesionally for several projects to generate 
code running in both simulation and fpga platform.  
It is also passing basic test after IC tape out.  
Ok on silicon in at least two projects (checked on samples).  

Please consider the user script API as still subject to change.
I plan to finetune Field/Register attribute, constructor parameters as see fit 
to improve the overall functionality.  
Upcoming change can still impact user script, and finetuning may be needed (hopefully minor changes).  
The user API stability will be considered as important criteria to reach v1.0.  

Version 0.4 complete addition of a new set of feature like regmap and array nesting,  
cdc layer core rewritten, markdown documentation generator,    
signal renaming in interface, signal aggregation in interface.  

Version 0.5 is now frozen with a set of new features : alias, shadow fields.  

Version v0.6 is completing a few new features (stronger align/pad support on regmap array,  
8/16bit support for data width, enhanced indexing in alias), and experimental support for SVD output.  
This is the last recommended version to be used if you want to start using pyrift.  

Next v0.7 on roadmap will focus on adding new features 
(AHB, SPI support for host, verification, counter support, ... priority still TBD).

Pyrift feature is improving according to my needs for the moment.
Should you have extra need requirement, ideas, ... please let me know.
I will see what I can do to improve the tool. 

Documentation is growing in wiki (most features are now covered already, 
but feedback report on missing points is welcome).

Sample user code is available in test directory.  
(please have a look to sample scripts to see what you can do)  
(I am trying to add example file for each new feature added).  
