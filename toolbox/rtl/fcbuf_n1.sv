// ----------------------------------------------------------------------------
// Pyrift toolbox provides basic block for re-use and help pyrift user integration
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// ----------------------------------------------------------------------------
// This file: fcbuf_n1.sv is provided as a toolbox addon of pyrift project
// fcbuf_n1 : flow control buffer SRC_N data -> 1 data
// valid/ready on both source and destination
// flop based output : enforce timing barrier between src and dst
// DW is parameter of data width used
// INVALID_DATA parameter specify the value to be used when dst_valid_o is 0
//    Use -1 would keep the last value used is previous valid cycle
//    (-1 is recommended to minimize power consumption/number of transition)
//    Use any positive value to drive dst_data_o whenever dst_valid_o is 0
// SRC_N is parameter for number of data provided in parallel as src
// ----------------------------------------------------------------------------

`default_nettype none

module fcbuf_n1 #(
   parameter int DW           = 8, // default data width is 8 bit
   parameter int SRC_N        = 2,
   parameter int INVALID_DATA = -1 // return is specific value when valid==0
) (
   input  var logic          rst_ni           , // Async active low reset
   input  var logic          clk_i            , // reference clock, active rising edge
   //
   input  var logic          src_ce_i         , // source clock enable (can be tie to 1'b1 is unused)
   input  var logic          dst_ce_i         , // destination clock enable (can be tie to 1'b1 is unused)
   // SRC port : source
   input  var logic          src_valid_i      ,
   input  var logic [DW-1:0] src_data_i[SRC_N], // SRC_N data input, D[0] is first tout o dst, D[SRC_N-1] is last out to dst
   output var logic          src_ready_o      ,
   // DST port : destination
   output var logic          dst_valid_o      ,
   output var logic [DW-1:0] dst_data_o       ,
   input  var logic          dst_ready_i
);

// ----------------------------------------------------------------------------

// assert parameter are valid
if(SRC_N<2) begin
   // following code generate only if invalid setting
   $error("fcbuf_n1 only support SRC_N at least 2");
end

// ----------------------------------------------------------------------------

// If buffer is not empty, then SRC is not ready
// When a transfer occur on src (both valid and reay active), then the data get either to
// - D[0]->dst if possible to get a new dat out, D[1]..D[N-1]-> buf[1]..buf[N-1]
// - buffer if dst is busy, D[0]..D[N-1]-> buf[0]..buf[N-1]
// Then buf is shifted to dst starting either from buf[0] or buf[1]
// BUF[0] may be used or be unused according to dst usage on initial storage
// (should minimize amount of buf data muxing,
// and avoid 3-way mux for buf[i] : mux(Src[i],Src[i+1],B[i+1]), only mux(Src[i],B[i+1]))
// DST = mux(Src[0],B[0],B[1])
// BUF[0] = mux(Src[0],B[1])
// BUF[1] = mux(Src[1],B[2])
// ...
// BUF[N-2] = mux(Src[N-2],B[N-1])
// BUF[N-1] = mux(Src[N-1])
// buffer is consider really empty is both lsb are empty: buffer_empty[1:0]==2'b11
// Create extra entry buffer[SRC_N] as dummy to avoid side effect in shifting fr SRC_N==2
// [SRC_N] is init as empty, and never changed, should simplify at synthesis
logic [SRC_N:0][DW-1:0] buffer_data ;
logic [SRC_N:0]         buffer_empty;

// sequence example with SRC_N=4 : New data is DCBA, then dcba
//  time --->
// Dst: ABCDabcd
//  B0:    abcd
//  B1: BCDbcd
//  B2: CD cd
//  B3: D  d

// masking flow control input with CE
logic src_valid_ce;
logic dst_ready_ce;
assign src_valid_ce = src_valid_i && src_ce_i;
assign dst_ready_ce = dst_ready_i && dst_ce_i;

always_ff @(posedge clk_i or negedge rst_ni) begin : proc_src_buffer
   if(~rst_ni) begin
      buffer_data <= {SRC_N+1{DW'(0)}};
      buffer_empty <= '1;
      src_ready_o <= 1;
      dst_valid_o <= 0;
      dst_data_o <= (INVALID_DATA>=0) ? (DW)'(INVALID_DATA) : 0;
   end else begin
      if(src_valid_ce && src_ready_o) begin
         // Here we have a new data coming from src, need to be store somewhere (either buffer or dst)
         // buffer is known to be empty at this point (src_ready_o==1)
         if (! dst_valid_o || dst_ready_ce) begin // dst not busy
            // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
            // dst is going to be available, so move src directly to partly dst, partly buffer
            // buffer is empty, so 1 data to dst (buffer is bypassed and buffer[0] remain empty)
            dst_valid_o <= 1;
            dst_data_o <= src_data_i[0];
            for (int i = 1; i <= SRC_N-1; i++) begin
               buffer_data[i] <= src_data_i[i]; // [SRC_N-1:1], [SRC_N] is not used, so unchanged
            end
            buffer_empty[SRC_N-1:1] <= (SRC_N-1)'(0); // buf was empty, now full with new data
            buffer_empty[0] <= 1; // (except slot 0 getting directly to dst)
            src_ready_o <= 0; // slot 1 full, so src is not ready anymore
         end else begin // dst busy
            // No room on dst, use full temporary storage for src into buffer
            for (int i = 0; i <= SRC_N-1; i++) begin
               buffer_data[i] <= src_data_i[i]; // [SRC_N-1:0], [SRC_N] is not used, so unchanged
            end
            buffer_empty[SRC_N-1:0] <= {SRC_N{1'b0}}; // buffer switch from all empty to all full
                                                      // (unused [SRC_N] untouched, to remain unused)
            src_ready_o <= 0; // buffer partillay used, prevent src to be ready (risk for src to conflict with buf)
         end
      end else begin
         // No new data coming, just handle buffer and dst
         if (! dst_valid_o || dst_ready_ce) begin // dst not busy
            // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
            // dst is going to be available, so may have to move buf if not empty
            if (buffer_empty[0]==1'b0) begin // Buf [...:O] are used
               // buf[0] transfer to dst
               dst_valid_o <= 1;
               dst_data_o <= buffer_data[0];
               // buffer is shifted
               buffer_empty[SRC_N-1] <= 1; // [SRC_N] is unchanged/unused
               buffer_empty[SRC_N-2:0] <= buffer_empty[SRC_N-1:1];
               buffer_data[SRC_N-2:0] <= buffer_data[SRC_N-1:1];
               // slot 0 was used at start so usage of slot 1 is the prediction for fully empty next clock
               src_ready_o <= buffer_empty[1];
            end else if (buffer_empty[1:0] == 2'b01) begin // Buf [...:1] are used, Buf[0] unused
               // buf[1] transfer to dst, keep buf[0] is unused
               dst_valid_o <= 1;
               dst_data_o <= buffer_data[1];
               // buffer is shifted, SRC_N is used as empty source for shifting (avoiding out of range for SRC_N==2)
               buffer_empty[SRC_N-1:1] <= buffer_empty[SRC_N:2];
               buffer_data[SRC_N-1:1] <= buffer_data[SRC_N:2];
               // buffer_empty[0] remain 1 as empty unused
               // slot 0 was unused at start so usage of slot 2 is the prediction for fully empty next clock
               src_ready_o <= buffer_empty[2];
            end else begin
               // buffer is empty, and no new data to dst, but may still have to update dst
               if(dst_valid_o) begin
                  dst_valid_o <= 0;
                  if (INVALID_DATA>=0) begin
                     dst_data_o <= (DW)'(INVALID_DATA);
                  end
               end
            end
         end
         // else : dst_busy : dst is not going to be available
         // keep as is, waiting for change
      end

   end
end

// ----------------------------------------------------------------------------

// debug signal to identify valid transfer transaction at src or destination
// These are not used, should simplify at synthesis
logic debug_src_transaction;
logic debug_dst_transaction;

assign debug_src_transaction = src_valid_i && src_ready_o && src_ce_i;
assign debug_dst_transaction = dst_valid_o && dst_ready_i && dst_ce_i;

// ----------------------------------------------------------------------------

endmodule // fcbuf
