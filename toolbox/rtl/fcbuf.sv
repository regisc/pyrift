// ----------------------------------------------------------------------------
// Pyrift toolbox provides basic block for re-use and help pyrift user integration
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// ----------------------------------------------------------------------------
// This file: fcbuf.sv is provided as a toolbox addon of pyrift project
// fcbuf : flow control buffer
// valid/ready on both source and destination
// flop based output : enforce timing barrier between src and dst
// DW is parameter of data width used
// INVALID_DATA parameter specify the value to be used when dst_valid_o is 0
//    Use -1 would keep the last value used is previous valid cycle
//    (-1 isrecommended to minimize power consumption/number of transition)
//    Use any positive value to drive dst_data_o whenever dst_valid_o is 0
// ----------------------------------------------------------------------------

`default_nettype none

module fcbuf #(
   parameter int DW           = 8,  // default data width is 8 bit
   parameter int INVALID_DATA = -1 // return is specific value when valid==0
) (
   input  var logic          rst_ni     , // Async active low reset
   input  var logic          clk_i      , // reference clock, active rising edge
   //
   input  var logic          src_ce_i   , // source clock enable (can be tie to 1'b1 is unused)
   input  var logic          dst_ce_i   , // destination clock enable (can be tie to 1'b1 is unused)
   // SRC port : source
   input  var logic          src_valid_i,
   input  var logic [DW-1:0] src_data_i ,
   output var logic          src_ready_o,
   // DST port : destination
   output var logic          dst_valid_o,
   output var logic [DW-1:0] dst_data_o ,
   input  var logic          dst_ready_i
);

// ----------------------------------------------------------------------------

// If buffer is not empty, then SRC is not ready
// When a transfer occur on src (both valid and reay active), then the data get either to
// - dst if possible to get a new dat out
// - buffer if dst is busy
logic [DW-1:0] buffer_data;
logic          buffer_empty;

// masking flow control input with CE
logic src_valid_ce;
logic dst_ready_ce;
assign src_valid_ce = src_valid_i && src_ce_i;
assign dst_ready_ce = dst_ready_i && dst_ce_i;

always_ff @(posedge clk_i or negedge rst_ni) begin : proc_src_buffer
   if(~rst_ni) begin
      buffer_data <= 0;
      buffer_empty <= 1;
      dst_valid_o <= 0;
      dst_data_o <= (INVALID_DATA>=0) ? (DW)'(INVALID_DATA) : 0;
   end else begin
      if(src_valid_ce && src_ready_o) begin
         // Here we have a new data coming from src, need to be store somewhere (either buffer or dst)
         // buffer is known to be empty at this point (src_ready_o==1)
         if (! dst_valid_o || dst_ready_ce) begin // dst not busy
            // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
            // dst is going to be available, so move src directly to dst
            // buffer is empty, so new data to dst (buffer is bypassed and remain empty)
            dst_valid_o <= 1;
            dst_data_o <= src_data_i;
         end else begin // dst busy
            // No room on dst, get temporary storage for src into buffer
            buffer_empty <= 0; // buffer switch from empty to full
            buffer_data <= src_data_i;
         end
      end else begin
         // No new data coming, just handle buffer and dst
         if (! dst_valid_o || dst_ready_ce) begin // dst not busy
            // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
            // dst is going to be available, so may have to move buf if not empty
            if (buffer_empty==1'b0) begin
               // buf transfer to dst
               dst_valid_o <= 1;
               dst_data_o <= buffer_data;
               // buffer is cleared
               buffer_empty <= 1;
            end else begin
               // buffer is empty, and no new data to dst, but may have to update dst
               if(dst_valid_o) begin
                  dst_valid_o <= 0;
                  if (INVALID_DATA>=0) begin
                     dst_data_o <= (DW)'(INVALID_DATA);
                  end
               end
            end
         end
         // else : dst_busy : dst is not going to be available
         // keep as is, waiting for change
      end

   end
end
assign src_ready_o = buffer_empty;

// ----------------------------------------------------------------------------

// debug signal to identify valid transfer transaction at src or destination
// These are not used, should simplify at synthesis
logic debug_src_transaction;
logic debug_dst_transaction;

assign debug_src_transaction = src_valid_i && src_ready_o && src_ce_i;
assign debug_dst_transaction = dst_valid_o && dst_ready_i && dst_ce_i;

// ----------------------------------------------------------------------------

endmodule // fcbuf
