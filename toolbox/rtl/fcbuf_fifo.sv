// ----------------------------------------------------------------------------
// Pyrift toolbox provides basic block for re-use and help pyrift user integration
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// ----------------------------------------------------------------------------
// This file: fcbuf_fifo.sv is provided as a toolbox addon of pyrift project
// fcbuf_fifo : flow control buffer with fifo extra storage
// valid/ready on both source and destination
// flop based output : enforce timing barrier between src and dst
// DW is parameter of data width used
// INVALID_DATA parameter specify the value to be used when dst_valid_o is 0
//    Use -1 would keep the last value used is previous valid cycle
//    (-1 isrecommended to minimize power consumption/number of transition)
//    Use any positive value to drive dst_data_o whenever dst_valid_o is 0
// BUF_SIZE gives the number of slot available to store src data when dst stall
//    Minimum is BUF_SIZE=1 for basic fonctionality, larger value works as fifo
//    for extra storage smoothing src--> src transfer
//    BUF_SIZE is required to be power of 2 : valid value are : 2, 4, 8, ...
//    Should you need BUF_SIZE=1 prefer basic fcbuf
// ----------------------------------------------------------------------------

`default_nettype none

module fcbuf_fifo #(
   parameter  int DW           = 8 , // default data width is 8 bit
   parameter  int INVALID_DATA = -1, // return is specific value when valid==0
   parameter  int BUF_SIZE     = 2 , // how many buffer slot available (minimum is 2, power of 2)
   localparam int BUF_SIZE_L2 = $clog2(BUF_SIZE)  // BUF_SIZE convert to log2 as localparam
) (
   input  var logic                 rst_ni      , // Async active low reset
   input  var logic                 clk_i       , // reference clock, active rising edge
   //
   input  var logic                 src_ce_i    , // source clock enable (can be tie to 1'b1 is unused)
   input  var logic                 dst_ce_i    , // destination clock enable (can be tie to 1'b1 is unused)
   // SRC port : source
   input  var logic                 src_valid_i ,
   input  var logic [       DW-1:0] src_data_i  ,
   output var logic                 src_ready_o ,
   // DST port : destination
   output var logic                 dst_valid_o ,
   output var logic [       DW-1:0] dst_data_o  ,
   input  var logic                 dst_ready_i ,
   //
   output var logic [BUF_SIZE_L2:0] free_space_o  // one extra bit for BUF_SIZE (4 buffer => up to 5 free space => 3 bit, not 2)
);

// ----------------------------------------------------------------------------

// If buffer is not empty, then SRC is not ready
// When a transfer occur on src (both valid and reay active), then the data get either to
// - dst if possible to get a new dat out
// - buffer if dst is busy
logic [DW-1:0] buffer_data [BUF_SIZE];
// buffer used as fifo with input/output pointer
// in_ptr == out_ptr => empty buffer
// in_ptr == out_ptr+BUF_SIZE => full buffer
// one extra bit used to differentiate empty/full case
logic [BUF_SIZE_L2:0] buffer_in_ptr; // [BUF_SIZE_L2-1:0] point to next storage slot
logic [BUF_SIZE_L2:0] buffer_out_ptr;// [BUF_SIZE_L2-1:0] point to out extraction slot
logic buffer_empty;
assign buffer_empty = buffer_in_ptr==buffer_out_ptr;
logic buffer_non_full; // as flop to reuse directly for src_ready_o

logic [BUF_SIZE_L2:0] buffer_in_ptr_incr; // buffer_in_ptr + 1
logic [BUF_SIZE_L2:0] buffer_out_ptr_incr;// buffer_out_ptr + 1
assign buffer_in_ptr_incr = buffer_in_ptr + 1;
assign buffer_out_ptr_incr = buffer_out_ptr + 1;
logic [BUF_SIZE_L2:0] buffer_in_ptr_full; // buffer_in_ptr expected value if buffer is to be checked full
assign buffer_in_ptr_full = buffer_out_ptr + BUF_SIZE;

logic [BUF_SIZE_L2:0] free_space_next;
logic [BUF_SIZE_L2:0] buffer_in_ptr_next; // [BUF_SIZE_L2-1:0] point to next storage slot
logic [BUF_SIZE_L2:0] buffer_out_ptr_next;// [BUF_SIZE_L2-1:0] point to out extraction slot
logic dst_valid_next;
assign free_space_next =
   (BUF_SIZE_L2+1)'(BUF_SIZE + 1) - // at start when empty, the free space is 1 (dst stage) + BUF_SIZE for buffer
   (BUF_SIZE_L2+1)'(buffer_in_ptr_next - buffer_out_ptr_next) - // correct by amount of slot used in buffer
   (BUF_SIZE_L2+1)'(dst_valid_next); // correct by -1 is dst is used

// masking flow control input with CE
logic src_valid_ce;
logic dst_ready_ce;
assign src_valid_ce = src_valid_i && src_ce_i;
assign dst_ready_ce = dst_ready_i && dst_ce_i;

logic [DW-1:0] buffer_data_next [BUF_SIZE];
logic buffer_non_full_next; // as flop to reuse directly for src_ready_o
logic [DW-1:0] dst_data_next;

// clock synchronous part of logic
always_ff @(posedge clk_i or negedge rst_ni) begin : proc_src_buffer_sync
   if(~rst_ni) begin
      buffer_data <= '{BUF_SIZE{0}};
      buffer_in_ptr <= 0;
      buffer_out_ptr <= 0;
      buffer_non_full <= 1;
      dst_valid_o <= 0;
      dst_data_o <= (INVALID_DATA>=0) ? (DW)'(INVALID_DATA) : 0;
      free_space_o <= (BUF_SIZE_L2+1)'(BUF_SIZE + 1);
   end else begin
      // Flop sync on clock edge
      buffer_data <= buffer_data_next;
      buffer_in_ptr <= buffer_in_ptr_next;
      buffer_out_ptr <= buffer_out_ptr_next;
      buffer_non_full <= buffer_non_full_next;
      dst_valid_o <= dst_valid_next;
      dst_data_o <= dst_data_next;
      free_space_o <= free_space_next;
   end
end

// Combinatorial part of logic
always_comb begin : proc_comb
   // Default to previous sync state, change to be explicit
   buffer_data_next = buffer_data;
   buffer_in_ptr_next = buffer_in_ptr;
   buffer_out_ptr_next = buffer_out_ptr;
   buffer_non_full_next = buffer_non_full;
   dst_valid_next = dst_valid_o;
   dst_data_next = dst_data_o;

   // next is comb function of current state
   if(src_valid_ce && src_ready_o) begin
      // Here we have a new data coming from src, need to be store somewhere (either buffer or dst)
      // buffer is known to be non full at this point (src_ready_o==1)
      if (! dst_valid_o || dst_ready_ce) begin // dst not busy
         // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
         // dst is going to be available, so move src directly or buffer is not empty to dst
         if (buffer_empty) begin
            // buffer is empty, so new data to dst (buffer is bypassed and remain empty)
            dst_valid_next = 1;
            dst_data_next = src_data_i;
            // buffer status is unchanged, don't touch any of (buffer_data, buffer_in_ptr, buffer_out_ptr, buffer_non_full)
         end else begin
            // buffer is NOT empty, src get to buffer, and buffer to dst
            dst_valid_next = 1;
            dst_data_next = buffer_data[buffer_out_ptr[BUF_SIZE_L2-1:0]];
            // Buffer data exit
            buffer_out_ptr_next = buffer_out_ptr_incr; // buffer extraction
            //  src get temporary storage for src into buffer
            buffer_data_next[buffer_in_ptr[BUF_SIZE_L2-1:0]] = src_data_i;
            // Buffer data entry
            buffer_in_ptr_next = buffer_in_ptr_incr; // buffer refill
            // buffer_non_full is unchanged +1 in, -1 out
         end
      end else begin // dst busy
         // No room on dst, get temporary storage for src into buffer
         buffer_data_next[buffer_in_ptr[BUF_SIZE_L2-1:0]] = src_data_i;
         // Buffer data entry
         buffer_in_ptr_next = buffer_in_ptr_incr; // buffer refill
         buffer_non_full_next = (buffer_in_ptr_incr!=buffer_in_ptr_full);
      end
   end else begin
      // No new data coming, just handle buffer and dst
      if (! dst_valid_o || dst_ready_ce) begin // dst not busy
         // dst is considered not busy if not already loaded (!dst_valid_o) or ready (will get free now if loaded)
         // dst is going to be available, so may have to move buf if not empty
         if (buffer_empty) begin
            // buffer is empty, and no new data to dst, but may have to update dst
            if(dst_valid_o) begin
               dst_valid_next = 0;
               if (INVALID_DATA>=0) begin
                  dst_data_next = (DW)'(INVALID_DATA);
               end
            end
         end else begin
            // buffer not empty transfer to dst
            dst_valid_next = 1;
            dst_data_next = buffer_data[buffer_out_ptr[BUF_SIZE_L2-1:0]];
            // Buffer data exit
            buffer_out_ptr_next = buffer_out_ptr_incr; // buffer extraction
            buffer_non_full_next = 1; // after extraction, it is never full
         end
      end
      // else : dst_busy : dst is not going to be available
      // keep as is, waiting for change
   end
end

assign src_ready_o = buffer_non_full;

// ----------------------------------------------------------------------------

// debug signal to identify valid transfer transaction at src or destination
// These are not used, should simplify at synthesis
logic debug_src_transaction;
logic debug_dst_transaction;

assign debug_src_transaction = src_valid_i && src_ready_o && src_ce_i;
assign debug_dst_transaction = dst_valid_o && dst_ready_i && dst_ce_i;

// ----------------------------------------------------------------------------

endmodule // fcbuf
