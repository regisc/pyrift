# ----------------------------------------------------------------------------
# fcbuf_n1 cocotb testing
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event, Join,Combine
from cocotb.result import TestFailure
from cocotb.binary import BinaryValue
from cocotb_bus.monitors import Monitor
from cocotb_bus.scoreboard import Scoreboard
from cocotb.regression import TestFactory
import random as random

# ----------------------------------------------------------------------------

async def delay_ns(delay):
    await Timer(delay, units='ns')

# ----------------------------------------------------------------------------

async def fcbuf_prepare(dut):
    dut.clk_i.value = 0 # default state
    # put input to dut stable inactive before reset
    dut.src_valid_i.value = 0
    dut.dst_ready_i.value = 0
    for i in range(SRC_N):
        dut.src_data_i[i].value = 0

    dut.rst_ni.value = 1
    await delay_ns(10) # compensate for startup lost 1ns (want edge exactly on 1us multiple)
    dut.rst_ni.value = 0
    await delay_ns(80)
    dut.rst_ni.value = 1
    await delay_ns(10)
    dut.rst_ni._log.info("Reset complete")

    # re-align the first clock edge to be multiple of 100 ns
    # (visual friendly for user: help simulation grid to be properly align)
    current_time = cocotb.utils.get_sim_time(units='ns')
    # realign_offset = 99-(current_time % 100)
    # dut._log.info(f"Time={current_time}ns, re-align by {realign_offset}")
    # await delay_ns(realign_offset)

    # t= 1us, start clock device, freq=10MHz, period=100ns
    clock = Clock(dut.clk_i, 100, units="ns")  # Create a 100ns period clock on port clk
    cocotb.start_soon(clock.start())  # Start the clock

# ----------------------------------------------------------------------------

class DataMonitor(Monitor):
    """
        Capture data transaction at src or dst of dut
        signal used are data, valid, ready
        Inherit from cocotb Monitor class (handle event/callback in standard way)
        width is the number of individual sample data per clock used (SRC_N)
    """

    def __init__(self, name, width, signal_data, signal_valid, signal_ready, signal_ce, clk, callback=None, event=None):
        self.name = name
        self.width = width
        self.signal_data = signal_data
        self.signal_valid = signal_valid
        self.signal_ready = signal_ready
        self.signal_ce = signal_ce
        self.clk = clk
        Monitor.__init__(self, callback, event)

    async def _monitor_recv(self):
        clk_rising_edge = RisingEdge(self.clk)

        while True:
            # Capture signal at rising edge of clock
            await clk_rising_edge
            #self.clk._log.info(f"{self.name} monitor: data={self.signal_data.value.binstr}"
            #                   f" valid={self.signal_valid.value} ready{self.signal_ready.value}")
            # Monitor only consider clock where Valid+Ready+CE are active
            if(self.signal_valid.value and self.signal_ready.value and self.signal_ce.value):
                if self.width>1:
                    for tr_id in range(self.width):
                        self.clk._log.info(f"{self.name} monitor: data[{tr_id}]=0x{self.signal_data[tr_id].value.integer:02X}")
                        transaction = self.signal_data[tr_id].value
                        self._recv(transaction)
                else:
                    # dst monitor, no array on data bus
                    self.clk._log.info(f"{self.name} monitor: data=0x{self.signal_data.value.integer:02X}")
                    transaction = self.signal_data.value
                    self._recv(transaction)
            else:
                pass
            await NextTimeStep()

# ----------------------------------------------------------------------------

async def src_driver(dut, sample_count, valid_probability):
    """
        Generate randomized valid flow control signal from source
        Count and randomize needed Data sample
        Check ready+CE to honor fcbuf core flow control
    """
    dut.src_valid_i.value = 0
    for i in range(SRC_N):
        dut.src_data_i[i].value = 0
    await RisingEdge(dut.clk_i) # resync to clock edge
    await NextTimeStep()
    for index in range(sample_count):
        valid_and_ready_and_ce = False
        while not valid_and_ready_and_ce:
            # loop to generate a new src sample (randomize at each clock of valid, but only keep if ready+ce)
            valid = (random.randint(0,99) < valid_probability)
            if valid:
                dut.src_valid_i.value = 1
                # src_data = BinaryValue(''.join([BinaryValue(random.randint(0, (1<<DW)-1), n_bits=DW).binstr for _ in range(SRC_N)]))
                for i in range(SRC_N):
                    src_data = random.randint(0, (1<<(DW))-1)
                    dut.src_data_i[i].value = src_data
            else:
                dut.src_valid_i.value = 0
                #  No valid data provided ( verilator using 2 state would cast X to 0)
                # keep 2 bit entropy in randomization : 00,F0,0F,FF. unused value are not always 00
                src_data = 0
                for i in range(SRC_N):
                    src_data = random.choice([0x00, 0x0F, 0xF0, 0xFF])
                    # dut._log.info(f"Invalid data assigned !=>{src_data:08X}")
                    dut.src_data_i[i].value = src_data
                #dut.src_data_i.value.binstr("x" * DW)
            # dut._log.info(f"src_driver waiting: data={src_data:04X}")
            await RisingEdge(dut.clk_i) # resync to clock edge
            valid_and_ready_and_ce = valid and dut.src_ready_o.value and dut.src_ce_i.value
            if valid_and_ready_and_ce:
                dut._log.info(f"src_driver sample valid count to {index}: data={src_data:04X}")
            # dut._log.info(f"src_driver read back data={dut.src_data_i.value.binstr}")
            await NextTimeStep()

    # Last sample pushed to fcbuf, wait a few extra clocks
    dut.src_valid_i.value = 0
    src_data = 0
    for i in range(SRC_N):
        src_data = random.choice([0x00, 0x0F, 0xF0, 0xFF])
        dut.src_data_i[i].value = src_data
    dut._log.info(f"Src driver purging.")
    for _ in range(5):
        await RisingEdge(dut.clk_i) # resync to clock edge

# ----------------------------------------------------------------------------

async def dst_driver(dut, sample_count, ready_probability):
    """
        Just generate randomized ready flow control signal from destination
        Also track sample count to complete : sample_count is a countdown to 0
    """

    dut.dst_ready_i.value = 0
    await RisingEdge(dut.clk_i) # resync to clock edge
    while sample_count>0:
        ready = (random.randint(0,99) < ready_probability)
        dut.dst_ready_i.value = ready
        await RisingEdge(dut.clk_i) # resync to clock edge
        if(dut.dst_ready_i.value and dut.dst_valid_o.value and dut.dst_ce_i.value):
            # valid transaction detected, countdown for 1 sample
            sample_count -=1
            dut._log.info(f"dst_driver seen data #{sample_count} = 0x{dut.dst_data_o.value.integer:02X}")
        await NextTimeStep()

    # Last sample pulled from fcbuf, wait a few extra clocks
    dut.dst_ready_i.value = 0
    dut._log.info(f"Dst driver purging.")
    for _ in range(5):
        await RisingEdge(dut.clk_i) # resync to clock edge

# ----------------------------------------------------------------------------

async def dst_invalid_checker(dut, invalid_data):
    """
        On every clock edge where dst_valid_o is 0, check dst_data_o is matching invalid_data
    """
    while True:
        await RisingEdge(dut.clk_i) # resync to clock edge
        if(not dut.dst_valid_o.value and dut.dst_ce_i.value):
            # valid != 1, ce==1, ready don't care
            if(dut.dst_data_o.value.integer != invalid_data):
                dut.dst_data_o._log.error(f"Dst data do not return to INVALID_DATA=0x{invalid_data} on dst_valid_o==0.\n"
                                          f"Found dst_data_o 0x{dut.dst_data_o.value.integer}=0b{dut.dst_data_o.value.binstr}\n"
                                          f"Expected:0x{invalid_data:02X} Get:0x{dut.dst_data_o.value.integer:02X}")
                # raise error because dut _log is not collected as error in regression (still pass test)
                # raise will stop test and get error in regression (to be reported to cocotb)
                raise TestFailure(f"Dst data do not return to INVALID_DATA on dst_valid_o==0.")
        await NextTimeStep()

# ----------------------------------------------------------------------------

async def ce_driver(dut, ce_src_probability, ce_dst_probability):
    """
        Just generate randomized clock enable signal
        Extra flow control feature
    """

    dut._log.info(f"Launching CE coroutine ...")
    while True:
        # dut._log.info(f"CE coroutine falling edge ...")
        await FallingEdge(dut.clk_i) # resync to clock edge : CE is significant only on rising edge
        src_ce = (random.randint(0,99) < ce_src_probability)
        dut.src_ce_i.value = src_ce
        dst_ce = (random.randint(0,99) < ce_dst_probability)
        dut.dst_ce_i.value = dst_ce
        await RisingEdge(dut.clk_i) # resync to clock edge
        # CE are not supposed to be used after rising edge
        dut.src_ce_i.value = 0
        dut.dst_ce_i.value = 0
        await NextTimeStep()

# ----------------------------------------------------------------------------

async def fcbuf_check(dut, sample_count=10, valid_probability=50, ready_probability=50, ce_probability=75):
    """fcbuf testing."""

    global DW
    global SRC_N
    DW = dut.DW.value.integer
    SRC_N = dut.SRC_N.value.integer

    INVALID_DATA = dut.INVALID_DATA.value.signed_integer # signed as could be -1 for unused

    dut._log.info(f"Using {cocotb.SIM_NAME} for simulator");
    dut._log.info(f"Running fcbuf test! DW={DW} N={SRC_N}: sample_count={sample_count}, "
                  f"valid_probability={valid_probability}, ready_probability={ready_probability}, ce_probability={ce_probability}")

    await fcbuf_prepare(dut)
    # out of reset, clock is running in background
    ce_thread = cocotb.start_soon(ce_driver(dut, ce_src_probability=ce_probability, ce_dst_probability=ce_probability))
    dut._log.info(f"CE coroutine launched.")
    dut._log.info(f"CE coroutine launched. {dut.src_data_i[0].value}")

    # -----------------------------------------
    # launch the source and destination driver
    # -----------------------------------------
    # src have data using DW SRC_N basic sample at each clock
    # dst have single data width sample at each clock, so number of sample is scaled by SRC_N from src
    src_drv_thread = cocotb.start_soon(src_driver(dut, sample_count=sample_count,       valid_probability=valid_probability))
    dst_drv_thread = cocotb.start_soon(dst_driver(dut, sample_count=sample_count*SRC_N, ready_probability=ready_probability))

    # -----------------------------------------
    # Source monitor
    # -----------------------------------------
    src_monitor = DataMonitor(
        name="src",
        width=SRC_N,
        signal_data=dut.src_data_i, # contain SRC_N data (width=DW*SRC_N)
        signal_valid=dut.src_valid_i,
        signal_ready=dut.src_ready_o,
        signal_ce=dut.src_ce_i,
        clk=dut.clk_i,
        # Model the fcbuf based on the input *transaction*.
        # Directly append *transaction* to the ``expected_dst_transaction_list`` list
        # detected transaction at src also feed the model (to build expected for scoreboard)
        callback=lambda tr : expected_dst_transaction_list.append(tr)
    )

    # -----------------------------------------
    # Build expected transaction list from model of fcbuf (extracted src monitor pushed to list)
    global expected_dst_transaction_list
    expected_dst_transaction_list = []

    # -----------------------------------------
    # Destination monitor
    # -----------------------------------------
    dst_monitor = DataMonitor(
        name="dst",
        width=1,
        signal_data=dut.dst_data_o, # contain 1 data (width=DW)
        signal_valid=dut.dst_valid_o,
        signal_ready=dut.dst_ready_i,
        signal_ce=dut.dst_ce_i,
        clk=dut.clk_i
    )

    if(INVALID_DATA>=0):
        dst_invalid_checker_thread = cocotb.start_soon(dst_invalid_checker(dut, invalid_data=INVALID_DATA))
    # else no invalid data set by dut, nothing to be checked

    # -----------------------------------------
    # Scoreboard
    # -----------------------------------------
    # Use standard scoreboard for data consistency checks (of what monitors have seen)
    scoreboard = Scoreboard(dut)
    scoreboard.add_interface(dst_monitor, expected_dst_transaction_list)

    # -----------------------------------------
    # Completion : wait for driver src and dst to complete
    await delay_ns(3000)
    await Combine(src_drv_thread, dst_drv_thread)

    await delay_ns(99) # round test duration to multiple of 100 ns
    # -----------------------------------------
    # Print result of scoreboard.
    # -----------------------------------------
    raise scoreboard.result


    # -----------------------------------------
    dut._log.info("Done fcbuf test!")

# ----------------------------------------------------------------------------

factory = TestFactory(fcbuf_check)
factory.add_option("sample_count",          [10,200])
factory.add_option("valid_probability",     [20, 50, 80])
factory.add_option("ready_probability",     [20, 50, 80])
factory.add_option("ce_probability",        [50, 95])
factory.generate_tests()
