# -----------------------------------------------------
# gen_rif_auto_bit_range.py
# this is an example script to illustrate use of auto_bit_range
# bit_range is defaulting to 'AUTO' and
# if not actually setup by user to explicit value,
# then AUTO mode used give Field automatically guessing at validate time
# bit_range guess first try to guess bit_width:
#  - if bit_width is explicit, then it is used
#  - No bit_width but use enum field : enum number of value required bit_width is used
#  - Error missing explicit bit_width otherwise
# With guessed bit_width, the Field is try to fit among other bit already assigned
# find first "lsb" where [lsb+bw-1:lsb] is not used in register.
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

class Cycle_Request(FieldEncode):
    BUS_IDLE = 0
    BUS_READ = 1
    BUS_WRITE = 2
    BUS_ACK = 3

# ----------------------------------------------------------------------------
# Create an example register name "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="Main config register"

# Create an example field to put in register reg_config
fb1 = Field(name="b1", bit_range='1', desc="bit1", sw=Access.RW, hw=Access.R)
fb42 = Field(name="auto3b", bit_width=3, desc='auto:3b', sw=Access.RW, hw=Access.R) # should fit '4:2'
fb6 = Field(name="b6", bit_range='6', desc="bit6", sw=Access.RW, hw=Access.R)
fb87 = Field(name="auto2benum", encode=Cycle_Request, desc='auto:2b enum', sw=Access.RW, hw=Access.R) # should fit '8:7'
# explicit padding field read=0 constant, to re-align the next auto bit range
fb10 = Field(name="pad10", bit_range='10', desc="bit10", reset=0, sw=Access.R, hw=Access.NA)
fb1211 = Field(name="auto2b", bit_width=2, desc='auto:2b', sw=Access.RW, hw=Access.R) # should fit '12:11'
#illegal_field = Field(name="illegal", bit_range='32:30', desc='illegal', sw=Access.RW, hw=Access.R) # out of range : above 31

reg_config.add(fb1)
reg_config.add(fb42)
reg_config.add(fb6)
reg_config.add(fb87)
reg_config.add(fb10)
reg_config.add(fb1211)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_config)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
