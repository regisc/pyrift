# -----------------------------------------------------
# gen_rif_warl.py
# this is a basic testing setup to check write any , read legal field
# heck also side effect in case of alias in use
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register name "warl_basic"
# Basic field with warl will induce volatility for register
reg_warl_basic = Register(name="warl_basic", desc="register with warl field")
# field is RW from sw
reg_warl_basic.add(Field(name="f_basic", bit_range='3:0', desc="rw warl basic",\
                       reset=0x2,
                       warl=[1,2,3],
                       sw=Access.RW, hw=Access.R,
    ),
)
# ----------------------------------------------------------------------------
# Create an example register name "wr_special_alias"
reg_wr_special_alias = Register(name="wr_special_alias", desc="register with warl field")
# field is RW from sw
reg_wr_special_alias.add(Field(name="f_rw_special", bit_range='3:0', desc="rw warl const",\
                       reset=0x2,
                       warl=[1,2,3],
                       sw=Access.RW, hw=Access.R,
    ),
)
reg_w_special = Register(name="w_special", desc="register with warl field")
# field is write Only from sw
reg_w_special.add(Field(name="f_w_special", bit_range='7:4', desc="w warl const",\
                       reset=0x2,
                       warl=[1,2,3],
                       sw=Access.W, hw=Access.R,
    ),
)

# ----------------------------------------------------------------------------
# Alias fld_1, fld_2 register in AliasReg1[2], AliasReg2

alias_f = Field(name="alias_warl", bit_range='3:0')
# Legal can be alias specific
alias_f.warl=[2,4,5] # should contain reset of primary aliased field

alias_f.sw = Access.RW
alias_f.hw = Access.NA
# use advance indexing of target register
# %0 is array index of current register, [] contain python expression based on %0,%1
# 1-%0 remap reg 0,1 to reg 1,0
alias_f.alias = "/test_regmap/wr_special_alias/f_rw_special"
#alias_f.alias = "/"
alias_f.desc = """\
    Alias for field  f_rw_special
"""
reg_alias = Register("AliasReg1")
reg_alias.add(alias_f)
# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_warl_basic)
test_regmap.add(reg_wr_special_alias)
test_regmap.add(reg_w_special)
test_regmap.add(reg_alias)
# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
