# -----------------------------------------------------
# gen_rif_hwset_hwclr.py
# simultaneously have hwclr and hwset on a field
# check precedence tuning
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

HW_SW_PRECEDENCE = False # default is False, True to get alternate precedence

# ----------------------------------------------------------------------------
# Create an example register "config"
reg_1 = Register(name="config")
# register description can be added after constructor call
reg_1.desc="REG1"

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
f0 = Field(name="f0", bit_width=8, reset = 0x12, desc="prio hw clear")
f1 = Field(name="f1", bit_width=8, reset = 0x23, desc="prio hw set")
fx = Field(name="fx", bit_width=8, reset = 0x34, desc="No prio hw clear/set")

f0.sw=Access.RW # read/write access from software (default if not specified)
f0.hw=Access.W # hardware get write access for hwset+hwclr
f0.hwclr = True
f0.hwset = True
f0.hwset_have_precedence_over_hwclr = False
f0.hw_have_wr_precedence_over_fw = HW_SW_PRECEDENCE

f1.sw=Access.RW # read/write access from software (default if not specified)
f1.hw=Access.W # hardware get write access for hwset+hwclr
f1.hwclr = True
f1.hwset = True
f1.hwset_have_precedence_over_hwclr = True
f1.hw_have_wr_precedence_over_fw = HW_SW_PRECEDENCE
fx.sw=Access.RW # read/write access from software (default if not specified)
fx.hw=Access.W # hardware get write access for hwset+hwclr
fx.hwclr = True
fx.hwset = True
fx.hw_have_wr_precedence_over_fw = HW_SW_PRECEDENCE
#fx.hwset_have_precedence_over_hwclr = None # as default : ! will issue a warning
reg_1.add(f0)
reg_1.add(f1)
reg_1.add(fx)

# ----------------------------------------------------------------------------
# test main register with sw=RW and 2 alias reg for woset or woclr
# to check side effect of single bit action (only having 2 bit field)
reg_main = Register(name="main")
reg_alias_set = Register(name="alias_set")
reg_alias_clr = Register(name="alias_clr")
f_main = Field(name="fmain", bit_width=2, desc="main : hwclr, hwset, sw RW",
               sw=Access.R, hw=Access.R,
               hwclr=True, hwset=True,
               hw_have_wr_precedence_over_fw = HW_SW_PRECEDENCE,
               hwset_have_precedence_over_hwclr = True)
reg_main.add(f_main)

f_alias_set = Field(name="fAliasSet", bit_width=2, desc="alias : sw set",
                    sw=Access.RW, hw=Access.NA,
                    woset=True,
                    alias="../main/fmain")
reg_alias_set.add(f_alias_set)

f_alias_clr = Field(name="fAliasClr", bit_width=2, desc="alias : sw clr",
                    sw=Access.RW, hw=Access.NA,
                    woclr=True,
                    alias="../main/fmain")
reg_alias_clr.add(f_alias_clr)

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_1)
test_regmap.add(reg_main)
test_regmap.add(reg_alias_set)
test_regmap.add(reg_alias_clr)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_svd("output/rif_test.svd")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
