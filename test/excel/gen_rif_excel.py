# -----------------------------------------------------
# gen_rif_excel.py
# this is an example script to illustrate the excel documentation of pyrift
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# Switch to 16 bit.
PyriftBase.dw=16

# excel formatting options (default)
# PyriftBase.xls_option["bit_char_width"] = 8
# PyriftBase.xls_option["multiline_height"] = 8
# PyriftBase.xls_option["column_regname_width"] = 25
# PyriftBase.xls_option["column_desc_width"] = 60
# PyriftBase.xls_option["column_bit_width"] = 4
# PyriftBase.xls_option['register_address_formatting']= "0x{address//1:02X}" # format address (//1 for byte @, //2 for 16b word @)

# excel formatting options (custom)
PyriftBase.xls_option["bit_char_width"] = 10
PyriftBase.xls_option["multiline_height"] = 10
PyriftBase.xls_option["column_regname_width"] = 15
PyriftBase.xls_option["column_desc_width"] = 20
PyriftBase.xls_option["column_bit_width"] = 8
PyriftBase.xls_option['register_address_formatting']= "0x{address//1:X}" # format address (//1:02X for byte @, //2:02X for 16b word @)

# ----------------------------------------------------------------------------
# Create an example register "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="""\
    CFG description
    as multiline information
"""

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
cfg1 = Field(name="cfg1", bit_range='7:0', reset = 0x23)
# add field description, can be multiline
cfg1.desc="cfg1 field description" # single line description

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.R # hardware get read access (typical of configuration field)

cfg2 = Field(name="configuration2", bit_range='12', sw=Access.RW, hw=Access.R, reset = 1)
cfg2.desc="cfg2 field description\nand extra cfg2 info" # dual line description

cfg3 = Field(name="cfg3", bit_range='14', sw=Access.RW, hw=Access.NA, reset = 1)
cfg3.desc="cfg3 field description\nand extra cfg3 info" # dual line description
reg_config.add(cfg1)
reg_config.add(cfg2)
reg_config.add(cfg3)

reg_test = Register(name="test")
# register description can be added after constructor call
reg_test.desc="""\
    Test description
    as multiline information
"""
tst1 = Field(name="test1", bit_range='0')
tst2 = Field(name="test2", bit_range='3:2')
tst3 = Field(name="test3", bit_range='7:5')
long_tst1 = Field(name="very_long_field_test_1", bit_range='8')
long_tst2 = Field(name="long_field_name_test_2", bit_range='11:10')
long_tst3 = Field(name="very_very_long_field_test_3", bit_range='15:13')
reg_test.add(tst1)
reg_test.add(tst2)
reg_test.add(tst3)
reg_test.add(long_tst1)
reg_test.add(long_tst2)
reg_test.add(long_tst3)

# ----------------------------------------------------------------------------
# Create an example register name "status"
reg_status = Register(name="STAT", desc="status register of my IP (with long oneline desc)")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(name="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W))

# ----------------------------------------------------------------------------
# R2d register
reg_2d = Register(name="r2d", desc="R2d desc", array=(3,2))
reg_2d.add(Field(name="b14", bit_range='14', desc="b14", sw=Access.RW, hw=Access.NA))
reg_2d.add(Field(name="f2d", bit_range='7:4', desc="f2d desc",
    reset = ( # reset of 2d field can be set to 1d reset vector table
        15, # [0,0]
        14, # [0,1]
        13, # [1,0]
        12, # [1,1]
        11, # [2,0]
        10  # [2,1]
    ),
    swacc=True,
    hwset=True,
    sw=Access.RW, hw=Access.RW))

# R1d register
reg_1d = Register(name="r1d", desc="R1d desc", array=50)
reg_1d.add(Field(name="f1d", bit_range='10:2', desc="f1d desc",
    reset = tuple(range(50)),
    # reset = 12,
    swmod=True,
    sw=Access.RW, hw=Access.RW))


# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.desc = "quick start description"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 10 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_config)
test_regmap.add(reg_test)
test_regmap.add(reg_status, align=32) # re-align status register to address multiple of 32
                                      # will allow adding more config register
                                      # without shifting the status register in address map
test_regmap.add(reg_2d)
test_regmap.add(reg_1d)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()


# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
# test_regmap.write_html("output/rif_test.html")
# test_regmap.write_markdown("output/rif_test.md") # extra param : variant='pandoc', default on 'gitlab')
# test_regmap.write_svd("output/rif_test.svd")
# test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
# test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
# test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")

test_regmap.write_excel("output/rif_test.xlsx")

PyriftBase.xls_option["column_desc_width"] = 80
test_regmap.write_excel_ref("output/rif_test_ref.xlsx")
