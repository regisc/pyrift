# -----------------------------------------------------
# gen_rif_hwbe.py
# have hwbe and rclr in same register field to see how bmask works
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register "config"
reg_1 = Register(name="config")
# register description can be added after constructor call
reg_1.desc="REG1"

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
rc1 = Field(name="rc1", bit_range='15:0', reset = 0x1234)
# add field description, can be multiline
rc1.desc="rc1 field description" # single line description

rc1.sw=Access.RW # read/write access from software (default if not specified)
rc1.hw=Access.R # hardware get read access (typical of configuration field)
rc1.rclr = True
rc1.swmod = True
reg_1.add(rc1)

# ----------------------------------------------------------------------------
# reg/field with swmod and read-only
# Chek swmod can apply as well to read only field
reg_readonly = Register(name="regreadonly", desc="read-only register")

f1 = Field(name="f1", bit_range='3:0', desc="swmod+RO",\
           sw=Access.R, hw=Access.W, swmod=True)
reg_readonly.add(f1)

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_1)
test_regmap.add(reg_readonly)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
