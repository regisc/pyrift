// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
// no import of package without enum // import rif_test_pkg::*; // get enum definitions

interface IF_RIF_TEST;
   //  [00010000]:config in regmap: test_regmap
      //  cfg1
      logic [7:0] cfg1;

      //  cfg2
      logic  cfg2;

   //  [00010000]:config
   // ----------------------------------
   //  [00010020]:STAT in regmap: test_regmap
      //  f_stat
      logic [15:0] f_stat_next;

   //  [00010020]:STAT
   // ----------------------------------

   // ----------------------------------

   modport rif(
      input  f_stat_next,
      output cfg1,
      output cfg2
   );

   // ----------------------------------

   modport hw(
      output f_stat_next,
      input  cfg1,
      input  cfg2
   );

   // ----------------------------------

endinterface // IF_RIF_TEST
