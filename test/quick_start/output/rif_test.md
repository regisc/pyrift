# Address table of regmap, name: test_regmap, SysVerilog module name: rif_test
## test_regmap
Description test_regmap:  
quick start description  
  
| Address | Register | Desc summary |  
|---------|----------|--------------|  
|[0x00010000](#config)|[config](#config)|CFG description|  
|[0x00010020](#stat)|[stat](#stat)|status register of my IP|  
  
------------------------------------  

# Registers details
## config
Register description:  
CFG description    
as multiline information    
Register reset value: 0x00001023  
Reset valid mask: 0xFFFFFFFF  
Address: [0x00010000](#test_regmap)  
  
| Bit | Name | SW | HW | Reset | Store  | Cdc | If          | Desc                   |  
|-----|------|----|----|-------|--------|-----|-------------|------------------------|  
| 7:0 | cfg1 | RW | R  | 0x23  | F.Flop |     | IF_RIF_TEST | cfg1 field description |  
| 12  | cfg2 | RW | R  | 1     | F.Flop |     | IF_RIF_TEST | cfg2 field description <br> and extra cfg2 info    |  
  
## STAT
Register description:  
status register of my IP  
Register reset value: 0x00000000  
Reset valid mask: 0xFFFF0000  
Address: [0x00010020](#test_regmap)  
  
| Bit  | Name   | SW | HW | Reset | Store   | Cdc | If          | Desc                        |  
|------|--------|----|----|-------|---------|-----|-------------|-----------------------------|  
| 15:0 | f_stat | R  | W  | X     | Hw.Wire |     | IF_RIF_TEST | stat field in STAT register |  
  

---------------------------------  

# Timestamp and pyrift version info  

----------------------------------------------------------  
  

----------------------------------------------------------  
  
This code has been generated with pyrift tool (under GPL licence),  
but this generated file is not covered by pyrift licence.  
It is full property and/or responsibility of the user of pyrift that generated this file.  

----------------------------------------------------------
