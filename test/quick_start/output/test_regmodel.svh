// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
// uvm_reg_field:
//
//function void configure(      uvm_reg     parent,
//  int     unsigned    size,
//  int     unsigned    lsb_pos,
//      string      access,
//      bit     volatile,
//      uvm_reg_data_t      reset,
//      bit     has_reset,
//      bit     is_rand,
//      bit     individually_accessible     )

//set_access
//'RO'      W: no effect, R: no effect
//'RW'      W: as-is, R: no effect
//'RC'      W: no effect, R: clears all bits
//'RS'      W: no effect, R: sets all bits
//'WRC'     W: as-is, R: clears all bits
//'WRS'     W: as-is, R: sets all bits
//'WC'      W: clears all bits, R: no effect
//'WS'      W: sets all bits, R: no effect
//'WSRC'    W: sets all bits, R: clears all bits
//'WCRS'    W: clears all bits, R: sets all bits
//'W1C'     W: 1/0 clears/no effect on matching bit, R: no effect
//'W1S'     W: 1/0 sets/no effect on matching bit, R: no effect
//'W1T'     W: 1/0 toggles/no effect on matching bit, R: no effect
//'W0C'     W: 1/0 no effect on/clears matching bit, R: no effect
//'W0S'     W: 1/0 no effect on/sets matching bit, R: no effect
//'W0T'     W: 1/0 no effect on/toggles matching bit, R: no effect
//'W1SRC'   W: 1/0 sets/no effect on matching bit, R: clears all bits
//'W1CRS'   W: 1/0 clears/no effect on matching bit, R: sets all bits
//'W0SRC'   W: 1/0 no effect on/sets matching bit, R: clears all bits
//'W0CRS'   W: 1/0 no effect on/clears matching bit, R: sets all bits
//'WO'      W: as-is, R: error
//'WOC'     W: clears all bits, R: error
//'WOS'     W: sets all bits, R: error
//'W1'      W: first one after HARD reset is as-is, other W have no effects, R: no effect
//'WO1'     W: first one after HARD reset is as-is, other W have no effects, R: error


// ##############################################################
// Register is : config, hierarchy=test_regmap_config
// ##############################################################


class test_regmap_config_reg extends uvm_reg;

   `uvm_object_utils(test_regmap_config_reg)
   string reg_hdl_path;
   // Field reset value stored in uvm_reg class member after constructor called
   uvm_reg_data_t cfg1_field_reset_value;
   uvm_reg_data_t cfg2_field_reset_value;


   function new (string name ="test_regmap_config_reg");
      super.new(name, 32, UVM_NO_COVERAGE);
      reg_hdl_path = "";
      cfg1_field_reset_value = 'h23;
      cfg2_field_reset_value = 'h1;
   endfunction: new

   // Field declaration
   rand uvm_reg_field cfg1;
   // each field have a set function for reset value per instance of array
   virtual function void set_cfg1_field_reset_value(uvm_reg_data_t reset_value);
     cfg1_field_reset_value = reset_value;
   endfunction: set_cfg1_field_reset_value
   rand uvm_reg_field cfg2;
   // each field have a set function for reset value per instance of array
   virtual function void set_cfg2_field_reset_value(uvm_reg_data_t reset_value);
     cfg2_field_reset_value = reset_value;
   endfunction: set_cfg2_field_reset_value

   virtual function void set_reg_hdl_path(string hdl_path);
     reg_hdl_path = hdl_path;
   endfunction: set_reg_hdl_path

   virtual function void build();
      cfg1 = uvm_reg_field::type_id::create("cfg1",,get_full_name());
      cfg1.configure(this,
         8, // size
         0, // lsb_pos
         "RW", // access
         0, // volatile
         cfg1_field_reset_value,// uvm_reg_data_t reset
         1, // bit has_reset
         1, // is_rand
         0); // individually_accessible
      add_hdl_path_slice(.name($sformatf("%s_f_%s", reg_hdl_path, "cfg1")), .offset(0), .size(8));
      cfg2 = uvm_reg_field::type_id::create("cfg2",,get_full_name());
      cfg2.configure(this,
         1, // size
         12, // lsb_pos
         "RW", // access
         0, // volatile
         cfg2_field_reset_value,// uvm_reg_data_t reset
         1, // bit has_reset
         1, // is_rand
         0); // individually_accessible
      add_hdl_path_slice(.name($sformatf("%s_f_%s", reg_hdl_path, "cfg2")), .offset(12), .size(1));

   endfunction: build
endclass : test_regmap_config_reg

// ##############################################################
// Register is : STAT, hierarchy=test_regmap_STAT
// ##############################################################


class test_regmap_STAT_reg extends uvm_reg;

   `uvm_object_utils(test_regmap_STAT_reg)
   string reg_hdl_path;
   // Field reset value stored in uvm_reg class member after constructor called
   uvm_reg_data_t f_stat_field_reset_value;


   function new (string name ="test_regmap_STAT_reg");
      super.new(name, 32, UVM_NO_COVERAGE);
      reg_hdl_path = "";
      f_stat_field_reset_value = 'h0;
   endfunction: new

   // Field declaration
   rand uvm_reg_field f_stat;
   // each field have a set function for reset value per instance of array
   virtual function void set_f_stat_field_reset_value(uvm_reg_data_t reset_value);
     f_stat_field_reset_value = reset_value;
   endfunction: set_f_stat_field_reset_value

   virtual function void set_reg_hdl_path(string hdl_path);
     reg_hdl_path = hdl_path;
   endfunction: set_reg_hdl_path

   virtual function void build();
      f_stat = uvm_reg_field::type_id::create("f_stat",,get_full_name());
      f_stat.configure(this,
         16, // size
         0, // lsb_pos
         "RO", // access
         1, // volatile
         f_stat_field_reset_value,// uvm_reg_data_t reset
         0, // bit has_reset
         1, // is_rand
         0); // individually_accessible
      add_hdl_path_slice(.name($sformatf("%s_f_%s", reg_hdl_path, "f_stat")), .offset(0), .size(16));

   endfunction: build
endclass : test_regmap_STAT_reg
// --------------------------------------------------------------
//                  REGISTER MODEL
// --------------------------------------------------------------

class test_regmodel extends uvm_reg_block;

   rand test_regmap_config_reg config ;
   rand test_regmap_STAT_reg STAT ;

   `uvm_object_utils(test_regmodel)

   function new(string name = "test_regmodel");
      super.new(name, UVM_NO_COVERAGE);
   endfunction: new

   virtual function void build();

      default_map = create_map("reg_model_map", 0, 4, UVM_LITTLE_ENDIAN);

      config = test_regmap_config_reg::type_id::create("config",,get_full_name());
      config.configure(this, null, "");
      config.set_reg_hdl_path("test_regmap_config");
      config.set_cfg1_field_reset_value('h23);
      config.set_cfg2_field_reset_value('h1);
      config.build();
      config.reset();
      default_map.add_reg(config,`UVM_REG_ADDR_WIDTH'h10000,"RW");

      STAT = test_regmap_STAT_reg::type_id::create("STAT",,get_full_name());
      STAT.configure(this, null, "");
      STAT.set_reg_hdl_path("test_regmap_STAT");
      STAT.set_f_stat_field_reset_value('h0);
      STAT.build();
      STAT.reset();
      default_map.add_reg(STAT,`UVM_REG_ADDR_WIDTH'h10020,"RW");

   endfunction: build
endclass : test_regmodel
