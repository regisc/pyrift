// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
// RIF module verilog implementation

// no import of package without enum // import rif_test_pkg::*; // get enum definitions

// enforce check for net type (var or wire is mandatory)
`default_nettype none

module rif_test (
   // Clock and Reset
   input  var logic clk,
   // extra user input signal ...
   input  var logic  rst_n, // async activelow //

   // RIF to user HW interface port
   IF_RIF_TEST.rif if_rif_test,

   // APB used to control all peripherals (synchronous to clk)
   IF_APB_BUS.slave    if_apb
);

// ----------------------------------------------------------------------------

// host_port_cdc_layer()
// -------------------------
// host_bus_to_generic_bus()
// -------------------------
// declare generic host bus signal
logic host_access_wr;
logic [3:0] host_access_wr_be;
logic [31:0] host_access_wr_bmask;
logic host_access_rd;
logic host_access_rd_early; // early pulse on read cycle
logic host_access_ready;
logic [31:0] rif_rdata;
logic [31:0] sw_wrdata;
logic [31:0] host_addr;

// APB conversion to generic host interfacing
//

logic apb_setup;
logic apb_access;
assign if_apb.PREADY = host_access_ready; // Wait state APB handling
                                          // for external register access
                                          // either a read or a write
assign if_apb.PSLVERR = 0; // No error supported so far

assign apb_setup = if_apb.PSEL && !if_apb.PENABLE;
assign apb_access = if_apb.PSEL && if_apb.PENABLE;
assign host_addr = if_apb.PADDR;
assign sw_wrdata = if_apb.PWDATA;
assign host_access_wr = apb_access && if_apb.PWRITE; // Simple IF, use APB access phase for host
// 32 bit access, PSTRB are used as byte modifier (optional use)
// byte enable, bmask signals are used at register/field core implementation level
assign host_access_wr_be = {4{host_access_wr}} & if_apb.PSTRB ;
assign host_access_wr_bmask = {
    {8{host_access_wr_be[3]}}, {8{host_access_wr_be[2]}}, {8{host_access_wr_be[1]}}, {8{host_access_wr_be[0]}}
} ; // bitmask enable usage defined per register/field
assign host_access_rd = apb_access && !if_apb.PWRITE; // Simple IF, use APB access phase for host
assign host_access_rd_early = apb_setup && !if_apb.PWRITE; // early pulse on read cycle


//
// --------------------
// to_verilog_decoder()
// --------------------
// host interface bus cycle decoder
logic [31:0] rif_base_address;
assign rif_base_address = 32'h00010000;
logic rif_access_swwr;
logic rif_access_swrd;
logic rif_access_swrd_early;
logic rif_access_ready;
assign host_access_ready = rif_access_ready;
assign rif_access_swwr = host_access_wr  &&
                         (host_addr[7:6] == rif_base_address[7:6]);
assign rif_access_swrd = host_access_rd  &&
                         (host_addr[7:6] == rif_base_address[7:6]);
assign rif_access_swrd_early = host_access_rd_early  &&
                               (host_addr[7:6] == rif_base_address[7:6]);

// Decoding register config test_regmap_config:@=0x10000
logic test_regmap_config_r_swwr;
logic test_regmap_config_r_swrd;
logic test_regmap_config_r_swrd_early;
logic test_regmap_config_r_ready;// ready only for external register (or unused)
logic [31:0] test_regmap_config_swrdata;
assign test_regmap_config_r_swwr = rif_access_swwr
   && (host_addr[5:2] == 4'('h10000/4));
assign test_regmap_config_r_swrd = rif_access_swrd
   && (host_addr[5:2] == 4'('h10000/4));
assign test_regmap_config_r_swrd_early = rif_access_swrd_early
   && (host_addr[5:2] == 4'('h10000/4));
// Decoding register STAT test_regmap_STAT:@=0x10020
logic test_regmap_STAT_r_swwr;
logic test_regmap_STAT_r_swrd;
logic test_regmap_STAT_r_swrd_early;
logic test_regmap_STAT_r_ready;// ready only for external register (or unused)
logic [31:0] test_regmap_STAT_swrdata;
assign test_regmap_STAT_r_swwr = rif_access_swwr
   && (host_addr[5:2] == 4'('h10020/4));
assign test_regmap_STAT_r_swrd = rif_access_swrd
   && (host_addr[5:2] == 4'('h10020/4));
assign test_regmap_STAT_r_swrd_early = rif_access_swrd_early
   && (host_addr[5:2] == 4'('h10020/4));
// -------------------------------
// generated data muxing based on *_r_swrd for all register
always_comb begin : proc_host_rdata_for_register
   rif_rdata = 0; // rdata is 0 unless actual read reg occurs
   if( test_regmap_config_r_swrd) begin
      rif_rdata = rif_rdata | test_regmap_config_swrdata;
   end
   if( test_regmap_STAT_r_swrd) begin
      rif_rdata = rif_rdata | test_regmap_STAT_swrdata;
   end
end
// -------------------------------
// generated ready muxing for all register
always_comb begin : proc_host_ready
   rif_access_ready = 1;
   // generate ready selection from register ready, either read or write
   if( (test_regmap_config_r_swrd || test_regmap_config_r_swwr) &&
       !test_regmap_config_r_ready) begin
      rif_access_ready = 0; // force to 0 due to accessed register not ready
   end
   // generate ready selection from register ready, either read or write
   if( (test_regmap_STAT_r_swrd || test_regmap_STAT_r_swwr) &&
       !test_regmap_STAT_r_ready) begin
      rif_access_ready = 0; // force to 0 due to accessed register not ready
   end
end
// -------------------------------
//
// ----------------------------
// host_bus_read_from_generic()
// ----------------------------
// APB processing to generic host interfacing : rdata to host APB
//

always_comb begin : proc_apb_rdata
   if(rif_access_swrd) begin
      if_apb.PRDATA = rif_rdata;
   end else begin
      if_apb.PRDATA = 0;
   end
end

//
// ----------------------------------------------------------------------------
//
// to_verilog_register()
//
// ----------------------------------------------
// Declare signals used in all registers of remap
// ----------------------------------------------
// signal declaration for: sw_access layer for field : test_regmap_config_f_cfg1
logic test_regmap_config_f_cfg1_swrd;
logic test_regmap_config_f_cfg1_swrd_early;
logic [7:0] test_regmap_config_f_cfg1_swnext;
logic        test_regmap_config_f_cfg1_swwr;
logic [7:0] test_regmap_config_f_cfg1_swwr_bmask;

logic [7:0] test_regmap_config_f_cfg1;
logic [7:0] test_regmap_config_f_cfg1_cdcif;
localparam [8-1:0] test_regmap_config_f_cfg1_rst_value = 8'h23; // reset value for storage
// Field have storage
logic [7:0] test_regmap_config_f_cfg1_ffnext;
logic [7:0] test_regmap_config_f_cfg1_ffbmask; // for byte enable support
logic test_regmap_config_f_cfg1_swwr_condition0; // before wonce filtering
logic test_regmap_config_f_cfg1_swwr_condition; // after wonce filtering
logic test_regmap_config_f_cfg1_wonce_locked;
logic test_regmap_config_f_cfg1_swrd_condition; // for swacc generation if any
logic test_regmap_config_f_cfg1_swrd_condition_early; // for early_swacc generation if any
logic test_regmap_config_f_cfg1_ready;

logic [4:0] test_regmap_config_f_pad_11_8;
// signal declaration for: sw_access layer for field : test_regmap_config_f_cfg2
logic test_regmap_config_f_cfg2_swrd;
logic test_regmap_config_f_cfg2_swrd_early;
logic [0:0] test_regmap_config_f_cfg2_swnext;
logic        test_regmap_config_f_cfg2_swwr;
logic [0:0] test_regmap_config_f_cfg2_swwr_bmask;

logic [0:0] test_regmap_config_f_cfg2;
logic [0:0] test_regmap_config_f_cfg2_cdcif;
localparam [1-1:0] test_regmap_config_f_cfg2_rst_value = 1'h1; // reset value for storage
// Field have storage
logic [0:0] test_regmap_config_f_cfg2_ffnext;
logic [0:0] test_regmap_config_f_cfg2_ffbmask; // for byte enable support
logic test_regmap_config_f_cfg2_swwr_condition0; // before wonce filtering
logic test_regmap_config_f_cfg2_swwr_condition; // after wonce filtering
logic test_regmap_config_f_cfg2_wonce_locked;
logic test_regmap_config_f_cfg2_swrd_condition; // for swacc generation if any
logic test_regmap_config_f_cfg2_swrd_condition_early; // for early_swacc generation if any
logic test_regmap_config_f_cfg2_ready;

logic [19:0] test_regmap_config_f_pad_31_13;
// signal declaration for: sw_access layer for field : test_regmap_STAT_f_f_stat
logic test_regmap_STAT_f_f_stat_swrd;
logic test_regmap_STAT_f_f_stat_swrd_early;

logic [15:0] test_regmap_STAT_f_f_stat;
logic [15:0] test_regmap_STAT_f_f_stat_cdcif;
localparam [16-1:0] test_regmap_STAT_f_f_stat_rst_value = 16'h0; // reset value for storage
logic [15:0] test_regmap_STAT_f_f_stat_next;
logic [15:0] test_regmap_STAT_f_f_stat_next_cdcif;
logic test_regmap_STAT_f_f_stat_swrd_condition; // for swacc generation if any
logic test_regmap_STAT_f_f_stat_swrd_condition_early; // for early_swacc generation if any
logic test_regmap_STAT_f_f_stat_ready;

logic [16:0] test_regmap_STAT_f_pad_31_16;
// ----------------------------------------------

// ##############################################################
// Register is : config, fullname=test_regmap_config
//    map@:65536,
//    regmap: test_regmap
//    regmap-array=()/()+reg-array=/
// ##############################################################

// Register test_regmap_config control signals are:
//     logic test_regmap_config_r_swwr;
//     logic test_regmap_config_r_swrd;
//     logic test_regmap_config_r_swrd_early;
//     logic [31:0] test_regmap_config_swrdata;

// Register test_regmap_config field implementation
// sw_access layer for field : test_regmap_config_f_cfg1
assign test_regmap_config_f_cfg1_swrd = test_regmap_config_r_swrd && test_regmap_config_r_ready; // field read comes from register read
assign test_regmap_config_f_cfg1_swrd_early = test_regmap_config_r_swrd_early; // field early read (one pulse before ready is available)

assign test_regmap_config_f_cfg1_swwr = test_regmap_config_r_swwr && test_regmap_config_r_ready;
always_comb begin : proc_test_regmap_config_f_cfg1_swnext
    test_regmap_config_f_cfg1_swnext = 0; // default to 0
    test_regmap_config_f_cfg1_swwr_bmask = '1; // No hw byte enable, all bit enable for write
    if(test_regmap_config_f_cfg1_swwr) begin
        test_regmap_config_f_cfg1_swnext = sw_wrdata[7:0]; // normal sw write
    end
end

// --------------------------------------------------------------
// Field in bit range of internal register : 7:0
// no wonce used, no locked needed
assign test_regmap_config_f_cfg1_wonce_locked = 0;
assign test_regmap_config_f_cfg1_swwr_condition0 = test_regmap_config_f_cfg1_swwr;
assign test_regmap_config_f_cfg1_swwr_condition = test_regmap_config_f_cfg1_swwr_condition0 && !test_regmap_config_f_cfg1_wonce_locked;
always_comb begin : proc_test_regmap_config_f_cfg1_ffnext
    // Default to SW, not used if no sw wr or no hw write
    test_regmap_config_f_cfg1_ffnext = test_regmap_config_f_cfg1_swnext; // default to SW logic, overload in hw write case
    test_regmap_config_f_cfg1_ffbmask = test_regmap_config_f_cfg1_swwr_bmask; // default to SW logic, overload in hw write case
    // hwwr
    // swwr for field 'cfg1'
    if(test_regmap_config_f_cfg1_swwr) begin
        // data modification from SW write
        test_regmap_config_f_cfg1_ffnext   = (test_regmap_config_f_cfg1_swnext & test_regmap_config_f_cfg1_swwr_bmask) |  (test_regmap_config_f_cfg1_ffnext & ~test_regmap_config_f_cfg1_swwr_bmask);
        test_regmap_config_f_cfg1_ffbmask |= test_regmap_config_f_cfg1_swwr_bmask;
    end
end
always_ff @(posedge clk or negedge rst_n) begin : proc_test_regmap_config_f_cfg1
    if(~rst_n) begin
        test_regmap_config_f_cfg1 <= test_regmap_config_f_cfg1_rst_value;
    end else
    begin
        // Clock gate the flop write only if sw wr or hw wr is active
        // bit test_regmap_config_f_cfg1_b0
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[0]) begin
            test_regmap_config_f_cfg1[0] <= test_regmap_config_f_cfg1_ffnext[0];
        end
        // bit test_regmap_config_f_cfg1_b1
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[1]) begin
            test_regmap_config_f_cfg1[1] <= test_regmap_config_f_cfg1_ffnext[1];
        end
        // bit test_regmap_config_f_cfg1_b2
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[2]) begin
            test_regmap_config_f_cfg1[2] <= test_regmap_config_f_cfg1_ffnext[2];
        end
        // bit test_regmap_config_f_cfg1_b3
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[3]) begin
            test_regmap_config_f_cfg1[3] <= test_regmap_config_f_cfg1_ffnext[3];
        end
        // bit test_regmap_config_f_cfg1_b4
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[4]) begin
            test_regmap_config_f_cfg1[4] <= test_regmap_config_f_cfg1_ffnext[4];
        end
        // bit test_regmap_config_f_cfg1_b5
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[5]) begin
            test_regmap_config_f_cfg1[5] <= test_regmap_config_f_cfg1_ffnext[5];
        end
        // bit test_regmap_config_f_cfg1_b6
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[6]) begin
            test_regmap_config_f_cfg1[6] <= test_regmap_config_f_cfg1_ffnext[6];
        end
        // bit test_regmap_config_f_cfg1_b7
        if ((test_regmap_config_f_cfg1_swwr_condition) && test_regmap_config_f_cfg1_ffbmask[7]) begin
            test_regmap_config_f_cfg1[7] <= test_regmap_config_f_cfg1_ffnext[7];
        end
    end
end
assign test_regmap_config_f_cfg1_swrd_condition =
    test_regmap_config_f_cfg1_swrd;
assign test_regmap_config_f_cfg1_swrd_condition_early =
    test_regmap_config_f_cfg1_swrd_early;
always_comb begin : proc_test_regmap_config_f_cfg1_ready
    test_regmap_config_f_cfg1_ready = 1; // no wait state without swmod or swacc
end

assign test_regmap_config_f_pad_11_8 = 0;

// sw_access layer for field : test_regmap_config_f_cfg2
assign test_regmap_config_f_cfg2_swrd = test_regmap_config_r_swrd && test_regmap_config_r_ready; // field read comes from register read
assign test_regmap_config_f_cfg2_swrd_early = test_regmap_config_r_swrd_early; // field early read (one pulse before ready is available)

assign test_regmap_config_f_cfg2_swwr = test_regmap_config_r_swwr && test_regmap_config_r_ready;
always_comb begin : proc_test_regmap_config_f_cfg2_swnext
    test_regmap_config_f_cfg2_swnext = 0; // default to 0
    test_regmap_config_f_cfg2_swwr_bmask = '1; // No hw byte enable, all bit enable for write
    if(test_regmap_config_f_cfg2_swwr) begin
        test_regmap_config_f_cfg2_swnext = sw_wrdata[12:12]; // normal sw write
    end
end

// --------------------------------------------------------------
// Field in bit range of internal register : 12:12
// no wonce used, no locked needed
assign test_regmap_config_f_cfg2_wonce_locked = 0;
assign test_regmap_config_f_cfg2_swwr_condition0 = test_regmap_config_f_cfg2_swwr;
assign test_regmap_config_f_cfg2_swwr_condition = test_regmap_config_f_cfg2_swwr_condition0 && !test_regmap_config_f_cfg2_wonce_locked;
always_comb begin : proc_test_regmap_config_f_cfg2_ffnext
    // Default to SW, not used if no sw wr or no hw write
    test_regmap_config_f_cfg2_ffnext = test_regmap_config_f_cfg2_swnext; // default to SW logic, overload in hw write case
    test_regmap_config_f_cfg2_ffbmask = test_regmap_config_f_cfg2_swwr_bmask; // default to SW logic, overload in hw write case
    // hwwr
    // swwr for field 'cfg2'
    if(test_regmap_config_f_cfg2_swwr) begin
        // data modification from SW write
        test_regmap_config_f_cfg2_ffnext   = (test_regmap_config_f_cfg2_swnext & test_regmap_config_f_cfg2_swwr_bmask) |  (test_regmap_config_f_cfg2_ffnext & ~test_regmap_config_f_cfg2_swwr_bmask);
        test_regmap_config_f_cfg2_ffbmask |= test_regmap_config_f_cfg2_swwr_bmask;
    end
end
always_ff @(posedge clk or negedge rst_n) begin : proc_test_regmap_config_f_cfg2
    if(~rst_n) begin
        test_regmap_config_f_cfg2 <= test_regmap_config_f_cfg2_rst_value;
    end else
    begin
        // Clock gate the flop write only if sw wr or hw wr is active
        // bit test_regmap_config_f_cfg2_b0
        if ((test_regmap_config_f_cfg2_swwr_condition) && test_regmap_config_f_cfg2_ffbmask[0]) begin
            test_regmap_config_f_cfg2[0] <= test_regmap_config_f_cfg2_ffnext[0];
        end
    end
end
assign test_regmap_config_f_cfg2_swrd_condition =
    test_regmap_config_f_cfg2_swrd;
assign test_regmap_config_f_cfg2_swrd_condition_early =
    test_regmap_config_f_cfg2_swrd_early;
always_comb begin : proc_test_regmap_config_f_cfg2_ready
    test_regmap_config_f_cfg2_ready = 1; // no wait state without swmod or swacc
end

assign test_regmap_config_f_pad_31_13 = 0;

// --------------------------------------------------------------
// CDC layer for field : test_regmap_config_f_cfg1

// fld_type of test_regmap_config_f_cfg1 is logic [7:0]
assign test_regmap_config_f_cfg1_cdcif = test_regmap_config_f_cfg1;

// END of CDC layer for field : test_regmap_config_f_cfg1
// --------------------------------------------------------------

// --------------------------------------------------------------
// CDC layer for field : test_regmap_config_f_cfg2

// fld_type of test_regmap_config_f_cfg2 is logic
assign test_regmap_config_f_cfg2_cdcif = test_regmap_config_f_cfg2;

// END of CDC layer for field : test_regmap_config_f_cfg2
// --------------------------------------------------------------


// --------------------------------------------------------------

// Register test_regmap_config field aggregation for sw rdata
assign test_regmap_config_swrdata = {
    test_regmap_config_f_pad_31_13[18:0],
    test_regmap_config_f_cfg2[0:0],
    test_regmap_config_f_pad_11_8[3:0],
    test_regmap_config_f_cfg1[7:0]
};

// --------------------------------------------------------------

// Register test_regmap_config ready aggregation for all field
assign test_regmap_config_r_ready =
    test_regmap_config_f_cfg1_ready &
    test_regmap_config_f_cfg2_ready;
// --------------------------------------------------------------


// ##############################################################
// Register is : STAT, fullname=test_regmap_STAT
//    map@:65568,
//    regmap: test_regmap
//    regmap-array=()/()+reg-array=/
// ##############################################################

// Register test_regmap_STAT control signals are:
//     logic test_regmap_STAT_r_swwr;
//     logic test_regmap_STAT_r_swrd;
//     logic test_regmap_STAT_r_swrd_early;
//     logic [31:0] test_regmap_STAT_swrdata;

// Register test_regmap_STAT field implementation
// sw_access layer for field : test_regmap_STAT_f_f_stat
assign test_regmap_STAT_f_f_stat_swrd = test_regmap_STAT_r_swrd && test_regmap_STAT_r_ready; // field read comes from register read
assign test_regmap_STAT_f_f_stat_swrd_early = test_regmap_STAT_r_swrd_early; // field early read (one pulse before ready is available)


// --------------------------------------------------------------
// Field in bit range of internal register : 15:0
// Field have NO storage
//No storage for the hw writable field : field = field_next in combinatorial
always_comb begin : proc_test_regmap_STAT_f_f_stat
    test_regmap_STAT_f_f_stat = test_regmap_STAT_f_f_stat_next;
end
assign test_regmap_STAT_f_f_stat_swrd_condition =
    test_regmap_STAT_f_f_stat_swrd;
assign test_regmap_STAT_f_f_stat_swrd_condition_early =
    test_regmap_STAT_f_f_stat_swrd_early;
always_comb begin : proc_test_regmap_STAT_f_f_stat_ready
    test_regmap_STAT_f_f_stat_ready = 1; // no wait state without swmod or swacc
end

assign test_regmap_STAT_f_pad_31_16 = 0;

// --------------------------------------------------------------
// CDC layer for field : test_regmap_STAT_f_f_stat

// fld_type of test_regmap_STAT_f_f_stat is logic [15:0]
assign test_regmap_STAT_f_f_stat_next = test_regmap_STAT_f_f_stat_next_cdcif;

// END of CDC layer for field : test_regmap_STAT_f_f_stat
// --------------------------------------------------------------


// --------------------------------------------------------------

// Register test_regmap_STAT field aggregation for sw rdata
assign test_regmap_STAT_swrdata = {
    test_regmap_STAT_f_pad_31_16[15:0],
    test_regmap_STAT_f_f_stat[15:0]
};

// --------------------------------------------------------------

// Register test_regmap_STAT ready aggregation for all field
assign test_regmap_STAT_r_ready =
    test_regmap_STAT_f_f_stat_ready;
// --------------------------------------------------------------

//
// ----------------------------------------------------------------------------
//
// to_verilog_connect_interface()
//
// Connect register to interface IF_RIF_TEST
   //  [00010000]:test_regmap_config
      //  cfg1
      assign if_rif_test.cfg1[7:0] = test_regmap_config_f_cfg1_cdcif;

      //  cfg2
      assign if_rif_test.cfg2 = test_regmap_config_f_cfg2_cdcif;

   //  [00010000]:test_regmap_config
   // ----------------------------------
   //  [00010020]:test_regmap_STAT
      //  f_stat
      assign test_regmap_STAT_f_f_stat_next_cdcif = if_rif_test.f_stat_next[15:0];

   //  [00010020]:test_regmap_STAT
   // ----------------------------------
//
// ----------------------------------------------------------------------------
endmodule
