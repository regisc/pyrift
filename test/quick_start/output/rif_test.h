// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
#ifndef __CHEAD_DEF_RIF_TEST
#define __CHEAD_DEF_RIF_TEST

// C header definition for register map : test_regmap
// module name :  rif_test

#include <stdint.h>

// typedef enum for all enum field used in any register

// ##############################################################
// Register is : config, hierarchy=test_regmap/config
// ##############################################################

// --------------------------------------------------------------

// register is volatile : False
// register is const : False
// Register Description: CFG description
// Register Description: as multiline information
// Field Description cfg1[7:0]: cfg1 field description
// Field Description cfg2[12]: cfg2 field description
// Field Description cfg2[12]: and extra cfg2 info
typedef struct
{
    unsigned int cfg1:8;
    unsigned int pad_11_8:4;
    unsigned int cfg2:1;
    unsigned int pad_31_13:19;
} TS_test_regmap_config;

typedef union
{
    // Register is normally non volatile
    TS_test_regmap_config field;
    uint32_t reg;
    // Alternate Register view : non volatile and volatile
    TS_test_regmap_config field_nv;
    uint32_t reg_nv;
    TS_test_regmap_config volatile field_v;
    uint32_t volatile reg_v;
} TU_test_regmap_config;

// REG Entry: test_regmap/config:
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__LSB 0
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__MSB 7
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__BITWIDTH 8
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__BITMASK 0xff
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__LSBBITMASK 0x1
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__RESET 0x23
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG1__RESET_VALUE_IS_VALID 1
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__LSB 12
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__MSB 12
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__BITWIDTH 1
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__BITMASK 0x1000
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__LSBBITMASK 0x1000
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__RESET 0x1
#define RIF_TEST__TEST_REGMAP_CONFIG__CFG2__RESET_VALUE_IS_VALID 1
// Register reset value ...
#define RIF_TEST__TEST_REGMAP_CONFIG__RESET 0x1023
#define RIF_TEST__TEST_REGMAP_CONFIG__RESET_VALID_MASK 0xffffffff
#define RIF_TEST__TEST_REGMAP_CONFIG__ADDRESS 0x10000
#define RIF_TEST__TEST_REGMAP_CONFIG__ADDRESS_WORD 0x4000


// ##############################################################
// Register is : STAT, hierarchy=test_regmap/STAT
// ##############################################################

// --------------------------------------------------------------

// register is volatile : True
// register is const : True
// Register Description: status register of my IP
// Field Description f_stat[15:0]: stat field in STAT register
typedef struct
{
    unsigned int f_stat:16;
    unsigned int pad_31_16:16;
} TS_test_regmap_STAT;

typedef union
{
    // Register is normally volatile
    TS_test_regmap_STAT volatile const field;
    uint32_t volatile const reg;
    // Alternate Register view : non volatile and volatile
    TS_test_regmap_STAT const field_nv;
    uint32_t const reg_nv;
    TS_test_regmap_STAT volatile const field_v;
    uint32_t volatile const reg_v;
} TU_test_regmap_STAT;

// REG Entry: test_regmap/STAT:
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__LSB 0
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__MSB 15
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__BITWIDTH 16
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__BITMASK 0xffff
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__LSBBITMASK 0x1
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__RESET 0x0
#define RIF_TEST__TEST_REGMAP_STAT__F_STAT__RESET_VALUE_IS_VALID 0
// Register reset value ...
#define RIF_TEST__TEST_REGMAP_STAT__RESET 0x0
#define RIF_TEST__TEST_REGMAP_STAT__RESET_VALID_MASK 0xffff0000
#define RIF_TEST__TEST_REGMAP_STAT__ADDRESS 0x10020
#define RIF_TEST__TEST_REGMAP_STAT__ADDRESS_WORD 0x4008

// ##############################################################
// REGMAP Structure definition for hierarchy
// ##############################################################

// map_table [2:5]
typedef struct
{
    // Register is : config, addr=65536=0x10000, Array()
    TU_test_regmap_config config;
    // -------------------------------------------------- 
    // Align pad for STAT: addr=65540=0x10004, Size=+28Bytes => new addr=0x10020
    uint32_t pad_10004[7];
    // Register is : STAT, addr=65568=0x10020, Array()
    TU_test_regmap_STAT STAT;
} TS_test_regmap;

// --------------------------------------------------------------

// Parameter also put in verilog package are dump here

// --------------------------------------------------------------

// Address mapping information for regmap:
// RegisterMap: test_regmap
#define TEST_REGMAP__BASE_ADDRESS 0x00010000
#define TEST_REGMAP__SIZE 0x00000024

// --------------------------------------------------------------

#endif // __CHEAD_DEF_RIF_TEST

