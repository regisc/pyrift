// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
// RIF module verilog unwrapper implementation
// Port should have only basic type : logic, enum, ... (no interface, struct, ...)

// no import of package without enum // import rif_test_pkg::*; // get enum definitions

// enforce check for net type (var or wire is mandatory)
`default_nettype none

module rif_test_unwrap (
   // Clock and Reset
   input  var logic clk,
   // extra user input signal ...
   input  var logic  rst_n, // async activelow //

   // RIF to user HW interface port
   // declare ports for interface : IF_RIF_TEST.rif if_rif_test
   output var logic [7:0] if_rif_test_cfg1,
   output var logic  if_rif_test_cfg2,
   input  var logic [15:0] if_rif_test_f_stat_next,

   // unwrap module, APB IO port definition
   // APB used to control all peripherals (synchronous to clk)
   input  var logic        psel,
   input  var logic        penable,
   input  var logic        pwrite,
   input  var logic [31:0] paddr,
   input  var logic [31:0] pwdata,
   input  var logic  [3:0] pstrb,

   output var logic [31:0] prdata,
   output var logic        pslverr,
   output var logic        pready

);

// ----------------------------------------------------------------------------

// Interface declaration for RIF instanciation
IF_RIF_TEST if_rif_test();

IF_APB_BUS if_apb(clk);

// RIF instanciation
rif_test i_rif_test (
   .clk(clk),
   // reset, extra user input signal ...
   .rst_n (rst_n), // async activelow //

   .if_rif_test(if_rif_test.rif),
   .if_apb(if_apb)
);
// APB connection of unwrap ports to APB interface
//
assign if_apb.PADDR = paddr;
assign if_apb.PSEL = psel;
assign if_apb.PENABLE = penable;
assign if_apb.PWDATA = pwdata;
assign if_apb.PWRITE = pwrite;
assign if_apb.PSTRB = pstrb;
//
assign pready  = if_apb.PREADY;
assign pslverr = if_apb.PSLVERR;
assign prdata  = if_apb.PRDATA;

// interface connection (modport .hw) to unwrap module port
// connect ports for interface : IF_RIF_TEST.hw if_rif_test.hw
assign if_rif_test_cfg1 = if_rif_test.cfg1;
assign if_rif_test_cfg2 = if_rif_test.cfg2;
assign if_rif_test.f_stat_next = if_rif_test_f_stat_next;


endmodule // rif_test_unwrap
