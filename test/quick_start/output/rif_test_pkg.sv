// ----------------------------------------------------------

// ----------------------------------------------------------

// This code has been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsibility of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ---------------------------------------------------------------------------
// package definition for rif_test and IF_RIF_TEST
// ---------------------------------------------------------------------------
package rif_test_pkg;

// --------------------------------------------------------------
// --------------------------------------------------------------

// Address mapping information for regmap:
// RegisterMap: test_regmap
localparam TEST_REGMAP__BASE_ADDRESS = 32'h00010000;
localparam TEST_REGMAP__SIZE = 32'h00000024;

localparam TEST_REGMAP__HOST_BUS_DECODE_MSB = 7;
localparam TEST_REGMAP__HOST_BUS_DECODE_MASK = 32'hfc;

// --------------------------------------------------------------

// Extra constant information on registers:
// Register is : config, hierarchy=test_regmap/config
// Register Description: CFG description
// Register Description: as multiline information
// Field Description cfg1[7:0]: cfg1 field description
// Field Description cfg2[12]: cfg2 field description
// Field Description cfg2[12]: and extra cfg2 info
localparam TEST_REGMAP_CONFIG__CFG1__LSB = 0;
localparam TEST_REGMAP_CONFIG__CFG1__MSB = 7;
localparam TEST_REGMAP_CONFIG__CFG1__BITWIDTH = 8;
localparam TEST_REGMAP_CONFIG__CFG1__BITMASK = 32'hff;
localparam TEST_REGMAP_CONFIG__CFG1__LSBBITMASK = 32'h1;
localparam TEST_REGMAP_CONFIG__CFG1__RESET = 8'h23;
localparam TEST_REGMAP_CONFIG__CFG1__RESET_VALUE_IS_VALID = 1;
localparam TEST_REGMAP_CONFIG__CFG2__LSB = 12;
localparam TEST_REGMAP_CONFIG__CFG2__MSB = 12;
localparam TEST_REGMAP_CONFIG__CFG2__BITWIDTH = 1;
localparam TEST_REGMAP_CONFIG__CFG2__BITMASK = 32'h1000;
localparam TEST_REGMAP_CONFIG__CFG2__LSBBITMASK = 32'h1000;
localparam TEST_REGMAP_CONFIG__CFG2__RESET = 1'h1;
localparam TEST_REGMAP_CONFIG__CFG2__RESET_VALUE_IS_VALID = 1;
// Register reset value ...
localparam TEST_REGMAP_CONFIG__RESET = 32'h1023;
localparam TEST_REGMAP_CONFIG__RESET_VALID_MASK = 32'hffffffff;
localparam TEST_REGMAP_CONFIG__ADDRESS = 32'h00010000;
// - - - - -
// Register is : STAT, hierarchy=test_regmap/STAT
// Register Description: status register of my IP
// Field Description f_stat[15:0]: stat field in STAT register
localparam TEST_REGMAP_STAT__F_STAT__LSB = 0;
localparam TEST_REGMAP_STAT__F_STAT__MSB = 15;
localparam TEST_REGMAP_STAT__F_STAT__BITWIDTH = 16;
localparam TEST_REGMAP_STAT__F_STAT__BITMASK = 32'hffff;
localparam TEST_REGMAP_STAT__F_STAT__LSBBITMASK = 32'h1;
localparam TEST_REGMAP_STAT__F_STAT__RESET = 16'h0;
localparam TEST_REGMAP_STAT__F_STAT__RESET_VALUE_IS_VALID = 0;
// Register reset value ...
localparam TEST_REGMAP_STAT__RESET = 32'h0;
localparam TEST_REGMAP_STAT__RESET_VALID_MASK = 32'hffff0000;
localparam TEST_REGMAP_STAT__ADDRESS = 32'h00010020;
// - - - - -

// --------------------------------------------------------------

endpackage
