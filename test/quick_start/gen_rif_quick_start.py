# -----------------------------------------------------
# gen_rif_quick_start.py
# this is an example script to illustrate a quick start of pyrift
# This can be considered the hello world of pyrift
# this instanciate register in a registermap and generate typical output
# This should give a realistic idea of how to setup a basic RIF for user
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="""\
    CFG description
    as multiline information
"""

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
cfg1 = Field(name="cfg1", bit_range='7:0', reset = 0x23)
# add field description, can be multiline
cfg1.desc="cfg1 field description" # single line description

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.R # hardware get read access (typical of configuration field)

cfg2 = Field(name="cfg2", bit_range='12', sw=Access.RW, hw=Access.R, reset = 1)
cfg2.desc="cfg2 field description\nand extra cfg2 info" # dual line description
reg_config.add(cfg1)
reg_config.add(cfg2)

# ----------------------------------------------------------------------------
# Create an example register name "status"
reg_status = Register(name="STAT", desc="status register of my IP")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(name="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.desc = "quick start description"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_config)
test_regmap.add(reg_status, align=32) # re-align status register to address multiple of 32
                                      # will allow adding more config register
                                      # without shifting the status register in address map

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md") # extra param : variant='pandoc', default on 'gitlab')
test_regmap.write_svd("output/rif_test.svd")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
test_regmap.write_excel("output/rif_test.xlsx")
