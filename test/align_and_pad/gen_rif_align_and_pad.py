# -----------------------------------------------------
# gen_rif_align_and_pad.py
# this is an example script to illustrate the use of
# register/regmap padding and alignment options
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 12 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000

# ----------------------------------------------------------------------------
# Register : use compact Field instanciation for field/register sharing name
reg_r0 = Register(Field(name="r0", bit_range='3:0', desc="R0"))
reg_r1 = Register(Field(name="r1", bit_range='3:0', desc="R1"))
reg_r2 = Register(Field(name="r2", bit_range='3:0', desc="R2"))

# Current@ => 0x10000
# R0 is already aligned to start @ == 0x10000, no align field added
test_regmap.add(reg_r0, align=0x100)

# Current@ => 0x10004
reg_r1.pre_pad = 12 # Not used, overloaded by add option: pad=4
reg_r1.post_pad = 8 # Pad after R1 from 0x1000C to 0x10014
test_regmap.add(reg_r1, pad=4)       # R1 is pre-pad (+4) to 0x10008
# 0x10008+post_pad=8 => 0x10014
# Current@ => 0x10014
test_regmap.add(reg_r2)       # R1 is added normally at 0x10014
# Current@ => 0x10018

# ----------------------------------------------------------------------------
# Register array with padding

reg_arr0 = Register(Field(name="arr0", bit_range='3:0', desc="Array0"), array=2)
reg_arr1 = Register(Field(name="arr1", bit_range='3:0', desc="Array1"), array=2)
reg_arr2 = Register(Field(name="arr2", bit_range='3:0', desc="Array2"), array=(2,3))
reg_arr3 = Register(Field(name="arr3", bit_range='3:0', desc="Array3"), array=2)

# Current@ => 0x10018
# Align=0x20 ==> 0x10020
# Pre_Pad+4 ==> 0x10024
reg_arr0.align = 0x20
test_regmap.add(reg_arr0, pad=4)
# array size = 4*2
# Current@ => 0x1002C
reg_arr1.post_pad = 4
test_regmap.add(reg_arr1)
# array size = 4*2 [0x1002C,0x10034[, +4 post_pad ==>0x10038
# Current@ => 0x10038
test_regmap.add(reg_arr2, pad=4) # 2d array with pre_pad
# Pre_Pad+4 ==> 0x1003C
# array size = 4*6 [0x1003C,0x10054[,  ==>0x10054
# Current@ => 0x10054
test_regmap.add(reg_arr3) # basic array with no pad, no align
# Current@ => 0x1005C

# ----------------------------------------------------------------------------
# Sub Register map array with padding

# Align=0x40 ==> 0x10080
sub_regmap_A = RegisterMap("sub_rmA", array=2)
sub_regmap_A.align = 0x100
sub_regmap_A.regmap_length_align = 0x80 # would be override to at least 0x40 from A_r2.align if missing
sub_regmap_A.pre_pad = 0x10
sub_regmap_A.post_pad = 0x8
sub_regmap_A.add(Register(Field(name="A_r1", bit_range='3:0', desc="A_r1")))
sub_regmap_A.add(Register(Field(name="A_r2", bit_range='3:0', desc="A_r2")), align=0x40)
sub_regmap_A.add(Register(Field(name="A_r3", bit_range='3:0', desc="A_r3")))

test_regmap.add(sub_regmap_A)
#   @base        : x100
#   @base+prepad : x110, x100
# -----------------------------
#R1:             : x110, x190
#R2:align x40    : x140, x1C0
#R3:             : x144, x1C4
#rm_length_align : x190, x210 (align to @R1+0x80*N)
# -----------------------------
#   +postpad : x218

# ----------------------------------------------------------------------------
# Sub Register map non array with padding

# Align=0x400 ==> 0x10400
sub_regmap_B = RegisterMap("sub_rmB")
sub_regmap_B.align = 0x400 # re-align to 0x10400 with size >> 0
sub_regmap_B.add(Register(Field(name="B_r1", bit_range='3:0', desc="B_r1")))
test_regmap.add(sub_regmap_B)

# ----------------------------------------------------------------------------

test_regmap.add(Register(Field(name="Final_Reg", bit_range='3:0', desc="Final reg")))

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
