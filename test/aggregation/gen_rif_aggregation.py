# -----------------------------------------------------
# gen_rif_aggregation.py
# this is a basic testing for field aggregation options
# have  32 x 2 bit fit in 2 registers (32bit contain only 16 x 2 bit fields)
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

class Unused_Enum(FieldEncode):
    BUS_IDLE = 0
    BUS_READ = 1
    BUS_WRITE = 2
    BUS_ACK = 3

# ----------------------------------------------------------------------------
# test_id_part02 register

fld_test_id_part02 = Field(name="test_id_part02", bit_range='31:0')
fld_test_id_part02.sw = Access.RW
fld_test_id_part02.hw = Access.RW
fld_test_id_part02.hwclr = True
fld_test_id_part02.swwe = True
fld_test_id_part02.swmod = True
fld_test_id_part02.name_in_if = "@test_id[0]" # aggregation at interface level
# fld_test_id_part02.multipart_if_pattern = r"(.*)_part\d*"
fld_test_id_part02.if_reshape = (2,)
fld_test_id_part02.desc = """\
    TEST ID register 1
"""
reg_test_id_part02 = Register(fld_test_id_part02, array=(3,))

# ----------------------------------------------------------------------------
# test_id_part3 register

fld_test_id_part3 = Field(name="test_id_part3", bit_range='31:0')
fld_test_id_part3.sw = Access.RW
fld_test_id_part3.hw = Access.RW
fld_test_id_part3.hwclr = True
fld_test_id_part3.swwe = True
fld_test_id_part3.swmod = True
fld_test_id_part3.name_in_if = "@test_id[1]" # aggregation at interface level
# fld_test_id_part3.multipart_if_slave = True
fld_test_id_part3.if_reshape = (2,) # TODO: no reshape needed, Should use part0 reshape definition
fld_test_id_part3.desc = """\
    TEST ID register 2
"""
reg_test_id_part3 = Register(fld_test_id_part3)

# ----------------------------------------------------------------------------

f1=Field("f1", bit_width=1, name_in_if="@g[1]")
f2=Field("f2", bit_width=2, name_in_if="@g[2]") #, encode=Unused_Enum)
f3=Field("f3", bit_width=1, name_in_if="@g[3]")

# add an extra field that infer a special interface
f_special=Field("f_special", bit_width=1)
f_special.interface_name = "IF_RIF_ALTERNATE"

f1.swacc = True
f2.swacc = True
f3.swacc = True

r1 = Register("r1")
r2 = Register("r2")
r1.add(f1)
r1.add(f3)
r1.add(f_special)
r2.add(f2)

# ----------------------------------------------------------------------------

# special use case to get aggregate flattening an array of field on interface
fieldA=Field("fieldA", bit_width=2, name_in_if="@tab[0]", interface_name="IF_TAB")
rA = Register("rA", array=3)
rA.add(fieldA)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_test_id_part02)
test_regmap.add(reg_test_id_part3)
test_regmap.add(r1)
test_regmap.add(r2)
test_regmap.add(rA)

test_regmap.validate()

#test_regmap.write_map_table("output/rif_test.map_table.txt")


test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
