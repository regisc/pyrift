# -----------------------------------------------------
# gen_rif_wonce.py
# this is a basic testing setup to check wonce option on a field (non aliased)
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register name "w1" ith wonce field
reg_wonce = Register(name="w1", desc="register with wonce field")
# field is RW from sw
reg_wonce.add(Field(name="f_w1", bit_range='1:0', desc="rw wonce const",\
                       reset=0x2,
                       wonce=True,
                       sw=Access.RW, hw=Access.R,
    ),
)
# field is write Only from sw
reg_wonce.add(Field(name="f_ro_w1", bit_range='5:4', desc="w wonce const",\
                       reset=0x2,
                       wonce=True,
                       sw=Access.W, hw=Access.R,
    ),
)
# field without reset cannot be wonce : Field definition below is commented as illegal
# reg_wonce.add(Field(name="f_w1_noreset", bit_range='9:8', desc="w wonce no reset",\
#                        reset=None,
#                        wonce=True,
#                        sw=Access.RW, hw=Access.R,
#     ),
# )

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_wonce)
# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
