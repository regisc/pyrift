# -----------------------------------------------------
# gen_rif_field_collision.py
# this is an example script to illustrate field name collision
# (2 fields in different register with same name)
# cfg1 is colliding (fixed with alternate name in interface : name_in_if)
# cfg2 have 2 different interfaces and so are OK
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register name "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="Main config register"

# Create an example field to put in register reg_config
cfg1 = Field(name="cfg1", bit_range='7:0',  reset = 1, desc="cfg1 main field", sw=Access.RW, hw=Access.R)
cfg2 = Field(name="cfg2", bit_range='15:8', reset = 2, desc="cfg2 main field", sw=Access.RW, hw=Access.R)

reg_config.add(cfg1)
reg_config.add(cfg2)

# ----------------------------------------------------------------------------
# Create an example register name "alternate_onfig"
reg_alternate_config = Register(name="alternate_config", desc="alternate config register")

# Create a duplicate field to put in register alternate_onfig
alt_cfg1 = Field(name="cfg1", bit_range='7:0',  reset = 11, desc="cfg1 Alt field", sw=Access.RW, hw=Access.R)
alt_cfg2 = Field(name="cfg2", bit_range='15:8', reset = 12, desc="cfg2 Alt field", sw=Access.RW, hw=Access.R)

# alt_cfg1.cfg1 field is in same interface as reg_config.cfg1 field
# but this field is renamed alt_cfg1 in the system_verilog interface
# So name conflict is removed
alt_cfg1.name_in_if = "alt_cfg1"

# alt_cfg2 is using a different interface : No collision
alt_cfg2.interface_name = "IF_RIF_ALTERNATE"

reg_alternate_config.add(alt_cfg1)
reg_alternate_config.add(alt_cfg2)

# ----------------------------------------------------------------------------
# register with same name : config (not instantiated for no error)
reg_colliding = Register(name="config", desc="colliding with config register")
cfg3 = Field(name="cfg3", bit_range='7:0',  reset = 1, desc="cfg3 in collidedregister", sw=Access.RW, hw=Access.R)
reg_colliding.add(cfg3)

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_config)
test_regmap.add(reg_alternate_config)
# register with same name should trig collision if instanciated
# test_regmap.add(reg_colliding)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
