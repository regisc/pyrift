# -----------------------------------------------------
# gen_rif_word.py
# this is an example script to illustrate the generation of word documentation
# This is using a basic quick start example with word output added
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Switch to 16 bit.
PyriftBase.dw=16

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

class my_enum(FieldEncode):
    E_IDLE = 0
    E_READ = 1
    E_WRITE = 2
    E_ACK = 3
    __value_desc_dict__ = {
        0: "*unintended italic*",
    }
    def get_desc():
        return """ \
            enum description with special char
            char = [+/-]
        """


# ----------------------------------------------------------------------------
# Create an example register name "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="CFG\nExtra cfg description"

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
cfg1 = Field(name="cfg1", bit_range='1:0')
# add field description, can be multiline
cfg1.desc="cfg1 field description\n   Extra cfg1 description\nmath example: (y=a*x+b)" # single line description

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.R # hardware get read access (typical of configuration field)
cfg1.reset = my_enum.E_WRITE # Enum type can be used as reset value
cfg1.encode = my_enum

cfg2 = Field(name="cfg2", bit_range='8', sw=Access.RW, hw=Access.R)
cfg2.desc="cfg2 field description" # single line description

reg_config.add(cfg1)
reg_config.add(cfg2)

# ----------------------------------------------------------------------------
# Create an example register name "status"
reg_status = Register(name="STAT", desc="status register of my IP")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(name="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W))
reg_status.desc="STAT\nExtra stat description"

# ----------------------------------------------------------------------------
# Create an example register name "status"
reg_special_desc = Register(name="Special_desc",
    desc="""\
        This register desc is using special char colliding with markdown language
        italic are expanded, so need escaping when getting to markdown
        *unintended italic*,
        _unintended italic_
        other special seems not interpreted
        ## unintended header
        1. Looks like first list item
        7. unwanted list item renumbering
        * unwanted list rendering
        + unwanted list rendering
        - unwanted list rendering
    """
)
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_special_desc.add(Field(
    name="special_field", bit_range='15:0',
    sw=Access.RW, hw=Access.R,
    desc="""\
        This field desc is using special char
        colliding with markdown language
        bracket and parenthesis needs escaping
        x*+-.!`_
        \\
        []()xxx][)({}}{
"""
))

# Create an special empty register (not used)
reg_empty = Register(name="empty")
reg_empty.desc="Empty register with no field"

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add(reg_config)
test_regmap.add(reg_status, align=32) # re-align status register to address multiple of 32
                                      # will allow adding more config register
                                      # without shifting the status register in address map
test_regmap.add(reg_special_desc)
# test_regmap.add(reg_empty)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

PyriftBase.md_option['register_address_formatting']= "0x{address//2:02X}" # format address (//1 for byte @, //2 for 16b word @)
PyriftBase.md_option['with_reset_bit_table']= True
PyriftBase.md_option['with_register_reset_value']= True # register reset value provided in register section
PyriftBase.md_option['with_reset_valid_mask']= True # register reset valid mask provided in register section
PyriftBase.md_option['header_address_map']= 'ADDRESS MAP, regmap name: %(name)s, SV name: %(module_name)s'
PyriftBase.md_option['header_register_detail']= 'Registers details\n'
PyriftBase.md_option['header_version_info']= None #skip version info section
PyriftBase.md_option['desc_escape_markdown_special']= True #escape special markdown char from desc
# choose header level at top of markdown (1 is default, 2 to generate "## header" instead of "# header")
# PyriftBase.md_option['regmap_column_desc']= True # add desc in regmap table
PyriftBase.md_option['regmap_column_oneline_desc']= True # add desc first line in regmap table
PyriftBase.md_option['header_start_level']= 1
#PyriftBase.md_option['Column_SW']= False
#PyriftBase.md_option['Column_HW']= False
#PyriftBase.md_option['Column_Store']= False
PyriftBase.md_option['Column_Cdc']= False
#PyriftBase.md_option['Column_If']= False
#PyriftBase.md_option['link_to_reg']= False  # no links from regmap list to reg description
#PyriftBase.md_option['link_to_addr_map']= False # No link from @in reg description to address map
#
# Header text for register description in markdown : default
#PyriftBase.md_option['Header_Bit']   = 'Bit'
#PyriftBase.md_option['Header_Name']  = 'Name'
#PyriftBase.md_option['Header_SW']    = 'SW'
#PyriftBase.md_option['Header_HW']    = 'HW'
#PyriftBase.md_option['Header_Reset'] = 'Reset'
#PyriftBase.md_option['Header_Store'] = 'Store'
#PyriftBase.md_option['Header_Cdc']   = 'Cdc'
#PyriftBase.md_option['Header_If']    = 'If'
#PyriftBase.md_option['Header_Desc']  = 'Desc'
# update for custom legend
PyriftBase.md_option['Header_Reset'] = 'Reset Value'
PyriftBase.md_option['Header_Desc']  = 'Description'
#


# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md.gitlab", variant='gitlab') # gitlab markdown is not compatible with pandoc markdown !!!
test_regmap.write_markdown("output/rif_test.md", variant='pandoc') # gitlab markdown is not compatible with pandoc markdown !!!
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")

# Generate the word document with register map information
# template can be an empty word document (.docx) provided by user
# requirement is to have a table stype defined with name = "pyrift_register"
# if template="", then pyrift try to provide its own basic template

# write word is now deprecated, please try to switch to markdown generation followed by pandoc conversion
# command line example:
# pandoc -f markdown -t docx --reference-doc=../doc_template/pyrift_pandoc_template.docx -o word/output/rif_test.docx word/output/rif_test.md
# test_regmap.write_word("output/rif_test.unsupported.docx", template="")
