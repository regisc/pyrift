# -----------------------------------------------------
# swwr2hwif.py
# this is a basic testing for field swwr2hwif options
# check options: basic field, aggregation, array, ...
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

HAVE_SWWR2HWIF = True

# ----------------------------------------------------------------------------
# basic config register bit, single/dual bit with R/W/RW variant on sw/hw
reg_config = Register(name="config")
cfg0 = Field(name="cfg0",   bit_range='0',    sw=Access.W,  hw=Access.RW, reset=1, swwr2hwif=True)
reg_config.add(cfg0)
cfg1 = Field(name="cfg1",   bit_range='1',    sw=Access.RW, hw=Access.NA, reset=1, swwr2hwif=True)
reg_config.add(cfg1)
cfg2 = Field(name="cfg2",   bit_range='2',    sw=Access.RW, hw=Access.RW, reset=1, swwr2hwif=True)
reg_config.add(cfg2)
cfg3 = Field(name="cfg3",   bit_range='3',    sw=Access.RW, hw=Access.R,  reset=1, swwr2hwif=True)
reg_config.add(cfg3)
cfg4 = Field(name="cfg4",   bit_range='5:4',  sw=Access.W,  hw=Access.RW, reset=1, swwr2hwif=True)
reg_config.add(cfg4)
cfg6 = Field(name="cfg6",   bit_range='7:6',  sw=Access.RW, hw=Access.NA, reset=1, swwr2hwif=True)
reg_config.add(cfg6)
cfg8 = Field(name="cfg8",   bit_range='9:8',  sw=Access.RW, hw=Access.RW, reset=1, swwr2hwif=True)
reg_config.add(cfg8)
cfg10 = Field(name="cfg10", bit_range='11:10', sw=Access.RW, hw=Access.R, reset=1, swwr2hwif=True)
reg_config.add(cfg10)

# ----------------------------------------------------------------------------
# test reg aggregate (rag) of field + array of field

fld_fag1 = Field(name="fag1", bit_range='1:0', swwr2hwif=True)
fld_fag1.sw = Access.RW
fld_fag1.hw = Access.RW
fld_fag1.hwclr = True
fld_fag1.swwe = True
fld_fag1.swmod = True
fld_fag1.name_in_if = "@ag[0]" # aggregation at interface level

reg_rag1 = Register(name="rag1", array=(2,))
reg_rag1.add(fld_fag1)

fld_fag2 = Field(name="fag2", bit_range='9:8', swwr2hwif=True)
fld_fag2.sw = Access.RW
fld_fag2.hw = Access.RW
fld_fag2.hwclr = True
fld_fag2.swwe = True
fld_fag2.swmod = True
fld_fag2.name_in_if = "@ag[1]" # aggregation at interface level

reg_rag2 = Register(name="rag2")
reg_rag2.add(fld_fag2)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_config)
test_regmap.add(reg_rag1)
test_regmap.add(reg_rag2)

test_regmap.validate()

#test_regmap.write_map_table("output/rif_test.map_table.txt")


test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
