# -----------------------------------------------------
# gen_rif_sw_type.py
# this is an example script to illustrate the modification of type seen from sw in cheader
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Field.default_support_hwbe = True

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# Create an example register "config"
reg_uconfig = Register(name="uconfig", desc="unsigned cfg")

# Use auto bit_range allocation
reg_uconfig.add(Field(name="ucfg1", bit_width=8, reset = 0x23, desc="ucfg1 is unsigned from sw"))
reg_uconfig.add(Field(name="ucfg2s", bit_width=8, reset = 0x23, sw_signed=True, desc="ucfg2s is signed from sw"))

# ----------------------------------------------------------------------------

reg_sconfig = Register(name="sconfig", desc="unsigned cfg", sw_signed=True)

# Use auto bit_range allocation
reg_sconfig.add(Field(name="scfg1", bit_width=8, reset = 0x23, desc="scfg1 is unsigned from sw"))
reg_sconfig.add(Field(name="scfg2s", bit_width=8, reset = 0x23, sw_signed=True, desc="ucfg2s is signed from sw"))

# ----------------------------------------------------------------------------

# reg with overloaded type (example to pointer type)
reg_ptr = Register(Field(name="ptr", bit_width=32, desc="ptr field"), sw_type='void *')


# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000;

test_regmap.add(reg_uconfig)
test_regmap.add(reg_sconfig)
test_regmap.add(reg_ptr)

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum and param definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
