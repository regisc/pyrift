# -----------------------------------------------------
# gen_rif_shadow.py
# this is a basic testing for field shadow options
# have a primary register with a shadow version
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# primary register

pf1 = Field(name="pf1", bit_range='3:0')
pf1.sw = Access.RW
pf1.rclr = True
pf1.hw = Access.R
pf1.hwclr = True
# pf1.swwe = True
# pf1.swmod = True
pf1.desc = """\
    PRIMARY field  1
"""
primary1 = Register("prim1") #, array=2)
primary1.add(pf1)

# ----------------------------------------------------------------------------
# pf2 register

pf2 = Field(name="pf2", bit_range='3:0')
pf2.sw = Access.W
pf2.hw = Access.RW
pf2.hwclr = True
pf2.swwe = True
pf2.swmod = True
pf2.swacc = True
pf2.desc = """\
    PRIMARY field  2
"""
primary2 = Register("prim2")
primary2.add(pf2)

# ----------------------------------------------------------------------------
# shadow pf1, pf2 register

shadow_1 = Field(name="shadow_1", bit_range='3:0')
shadow_1.sw = Access.NA # shadow don't need any normal SW access
shadow_1.hw = Access.RW
shadow_1.shadow = "/test_regmap/prim1/pf1"
shadow_1.desc = """\
    shadow for field  1
"""

reg_shadow1 = Register("shadowReg1")
reg_shadow1.add(shadow_1)

shadow_2 = Field(name="shadow_2", bit_range='3:0')
shadow_2.sw = Access.RW
shadow_2.hw = Access.R
shadow_2.shadow = "../prim2/pf2"
shadow_2.swacc = True
shadow_2.desc = """\
    shadow for field  2
"""

reg_shadow2 = Register("shadowReg2")
reg_shadow2.add(shadow_2)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(primary1)
test_regmap.add(primary2)
test_regmap.add(reg_shadow1)
test_regmap.add(reg_shadow2)

test_regmap.validate()

test_regmap.write_map_table("output/rif_test.map_table.txt")


test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
