# -----------------------------------------------------
# gen_rif_alias.py
# this is a basic testing for field alias options
# have  2 x 16 bit fields and fit in 2 alias in 32 bit registers
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

class Unused_Enum(FieldEncode):
    BUS_IDLE = 0
    BUS_READ = 1
    BUS_WRITE = 2
    BUS_ACK = 3

# ----------------------------------------------------------------------------
# fld_1 register r1[2]

fld_1 = Field(name="fld_1", bit_range='3:0')
fld_1.sw = Access.R
fld_1.hw = Access.R
# fld_1.hwclr = True
# fld_1.swwe = True
# fld_1.swmod = True
fld_1.desc = """\
    TEST field  1
"""
reg_1 = Register("r1", array=(2,3))
reg_1.add(fld_1)

# ----------------------------------------------------------------------------
# fld_2 register r2

fld_2 = Field(name="fld_2", bit_range='3:0')
fld_2.sw = Access.W
fld_2.hw = Access.RW
fld_2.hwclr = True
fld_2.swwe = True
fld_2.swmod = True
fld_2.swacc = True
fld_2.desc = """\
    TEST field  2
"""
reg_2 = Register("r2")
reg_2.add(fld_2)

# ----------------------------------------------------------------------------
# Alias fld_1, fld_2 register in AliasReg1[2], AliasReg2

alias_1 = Field(name="alias_1", bit_range='3:0')
alias_1.sw = Access.RW
alias_1.hw = Access.NA
# use advance indexing of target register
# %0 is array index of current register, [] contain python expression based on %0,%1
# 1-%0 remap reg 0,1 to reg 1,0
alias_1.alias = "/test_regmap/r1[1-%0,2-%1]/fld_1"
#alias_1.alias = "/"
alias_1.desc = """\
    Alias for field  1
"""

alias_2 = Field(name="alias_2", bit_range='19:16')
alias_2.sw = Access.RW
alias_2.hw = Access.NA
# alias_2.interface_name = "invalid" # cannot redefine interface_name for alias
alias_2.alias = "./../r2/./fld_2"
alias_2.desc = """\
    Alias for field  2
"""
reg_alias1 = Register("AliasReg1", array=(2,3))
reg_alias1.add(alias_1)
reg_alias2 = Register("AliasReg2")
reg_alias2.add(alias_2)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_1)
test_regmap.add(reg_2)
test_regmap.add(reg_alias1)
test_regmap.add(reg_alias2)

test_regmap.validate()

test_regmap.write_map_table("output/rif_test.map_table.txt")


test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
