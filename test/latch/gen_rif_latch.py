# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# intended latch usage, do not want the verbose warning
Field.silent_latch_usage = True

# ----------------------------------------------------------------------------
# R1 register
reg_r1 = Register(
    Field(name="f11", bit_range='3:0', desc="f11 desc", sw=Access.RW, hw=Access.R, latch=True),
    desc="R1")
reg_r1.add(Field(name="f12", bit_range='7:4', desc="f12 desc", sw=Access.RW, hw=Access.RW, latch=True))
reg_r1.add(Field(name="f13", bit_range='15:8',  desc="f13 desc", sw=Access.RW, hw=Access.R, latch=True, reset=None))
reg_r1.add(Field(name="f14", bit_range='19:16', desc="f14 desc", sw=Access.RW, hw=Access.R, latch=True, reset=5,
                 woclr=True))

# ----------------------------------------------------------------------------
# R2 register
reg_r2 = Register(
    Field(name="f21", bit_range='7:4', desc="f21 desc", sw=Access.RW, hw=Access.R, latch=True),
    desc="R2")

# ----------------------------------------------------------------------------
# R3 register : LDCE/LDPE primitive used
reg_r3 = Register(name="R3", desc="R3 desc")
f31=Field(name="f31", bit_range='1:0', desc="f31 desc", sw=Access.RW, hw=Access.R, latch=True,
          reset=2)
f32=Field(name="f32", bit_range='2', desc="f32 desc", sw=Access.RW, hw=Access.R, latch=True,
          reset=1)
f33=Field(name="f33", bit_range='9:8', desc="f33 desc", sw=Access.RW, hw=Access.R, latch=True,
          reset=None,
          wzs=True)
f31.latch_use_lib_ldce = True
f32.latch_use_lib_ldce = True
f33.latch_use_lib_ldce = True
reg_r3.add(f31)
reg_r3.add(f32)
reg_r3.add(f33)

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.add(reg_r1)
test_regmap.add(reg_r2, pad=12)
test_regmap.add(reg_r3)

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_svd("output/rif_test.svd")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
# test_regmap.write_word("output/rif_test.docx", template="")
