# -----------------------------------------------------
# gen_rif_hwce.py
# Instanciate register with example of hwce activation
# have affected mode by hwce : swmod, swacc, external register, ...
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

Field.default_support_hwbe = True

# ----------------------------------------------------------------------------
# Create an example register name "config"
reg_config = Register(name="config")
# register description can be added after constructor call
reg_config.desc="CFG"

# Create an example field to put in register r1.register
# name f1
# First call the constructor of field, then set all wanted feature
# Only mandatory parameter of constructor is name and bitfield
cfg1 = Field(name="cfg1", bit_range='1:0')
cfg1.desc="cfg1 field description"

cfg1.sw=Access.RW # read/write access from software (default if not specified)
cfg1.hw=Access.RW # hardware get read/w access
cfg1.swmod = True
cfg1.hwclr = True
cfg1.early_swacc = True
cfg1.hwce_signal_name = "hwce"

cfg2 = Field(name="cfg2", bit_range='3:2')
cfg2.desc="cfg2 field description"

cfg2.sw=Access.RW # read/write access from software (default if not specified)
cfg2.hw=Access.R # hardware get read access (typical of configuration field)
#cfg2.swmod = True
cfg2.delayed_swmod = True
cfg2.swacc = True
cfg2.hwce_signal_name = "hwce"

cfg3 = Field(name="cfg3", bit_range='5:4')
cfg3.desc="cfg3 field single pulse"

cfg3.sw=Access.W # write access from software for single pulse setting
cfg3.hw=Access.R # hardware get read access (typical of single pulse)
cfg3.singlepulse = True
cfg3.woset = True
cfg3.hwce_signal_name = "hwce"

cfg_ext8 = Field(name="cfge8", bit_range='8', sw=Access.RW, hw=Access.NA,
                 external=True,
                 hwce_signal_name="hwce")
cfg_ext9 = Field(name="cfge9", bit_range='9', sw=Access.RW, hw=Access.NA,
                 external=True,
                 hwce_signal_name="hwce")
cfg_ext10 = Field(name="cfge10", bit_range='10', sw=Access.RW, hw=Access.NA,
                 external=True, extready_rd=True, extready_wr=True,
                 hwce_signal_name="hwce")
cfg_ext11 = Field(name="cfge11", bit_range='11', sw=Access.RW, hw=Access.NA,
                 external=True, extready_rd=True, extready_wr=True,
                 hwce_signal_name="hwce")
reg_config.add(cfg1)
reg_config.add(cfg2)
reg_config.add(cfg3)
reg_config.add(cfg_ext8)
reg_config.add(cfg_ext9)
reg_config.add(cfg_ext10)
reg_config.add(cfg_ext11)

# ----------------------------------------------------------------------------
# Create an example register name "status"
reg_status = Register(name="STAT", desc="status register of my IP")
# field is status register is readable by software, writable by hardware
# This makes a non storage, software accessible signal in hardware
reg_status.add(Field(name="f_stat", bit_range='15:0', desc="stat field in STAT register",\
                     sw=Access.R, hw=Access.W,
                     # hwce would not change anything for status read from hw
                     hwce_signal_name = "hwce"))

# ----------------------------------------------------------------------------
# create a register map to collect the available set of register
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
# set the address range to be decoded on APB
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant

test_regmap.add_signal(Signal("hwce", activelow=False, sync=True))
test_regmap.add(reg_config)
test_regmap.add(reg_status, align=32) # re-align status register to address multiple of 32
                                      # will allow adding more config register
                                      # without shifting the status register in address map

# validate consistency of register map content, compute the map table
# (address lookup for register in address space decoded on the host bus)
test_regmap.validate()

# generate output file
# if directory is missing, it will be created
# Existing file is to be overwriten without warning
test_regmap.write_html("output/rif_test.html")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
# test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
