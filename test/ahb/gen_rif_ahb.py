# -----------------------------------------------------
# gen_rif_ahb.py
# this is an example script to illustrate the use of
# AHB host interface
# -----------------------------------------------------

# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# So far in v0.7 wip, AHB is still experimental,
# not yet really tested
# Acknowledge the risk and unlock the AHB feature
PyriftBase.experimental = True

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 12 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000

# AHB is the selected interface for host to connect to RIF
# ------------------------------
test_regmap.rif_host_bus = "AHB"
# ------------------------------

# ----------------------------------------------------------------------------
# Register : use compact Field instanciation for field/register sharing name
reg_cfg = Register(Field(name="cfg", bit_range='7:0', reset = 0x23, sw=Access.RW, hw=Access.R))
reg_stat = Register(Field(name="stat", bit_range='15:0', desc="stat field", sw=Access.R, hw=Access.W))

test_regmap.add(reg_cfg)
test_regmap.add(reg_stat)

# ----------------------------------------------------------------------------

test_regmap.validate()

test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
