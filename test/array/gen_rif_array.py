# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# supress variable timestamp generation (help track change in generated files)
Field.pyrift_generation_info = False
Field.pyrift_version_info = False

# ----------------------------------------------------------------------------
# R1 register
reg_r1 = Register(name="r1", desc="R1 desc")
reg_r1.add(Field(name="f11", bit_range='3:0', desc="f11 desc", sw=Access.RW, hw=Access.R))

# ----------------------------------------------------------------------------
# R1d register
reg_1d = Register(name="r1d", desc="R1d desc", array=6)
reg_1d.add(Field(name="f1d", bit_range='7:4', desc="f1d desc",
    reset = (1,2,3,4,5,6),
    # reset = 12,
    swmod=True,
    sw=Access.RW, hw=Access.RW))

# R2d register
reg_2d = Register(name="r2d", desc="R2d desc", array=(3,2))
reg_2d.add(Field(name="f2d", bit_range='7:4', desc="f2d desc",
    reset = ( # reset of 2d field can be set to 1d reset vector table
        15, # [0,0]
        14, # [0,1]
        13, # [1,0]
        12, # [1,1]
        11, # [2,0]
        10  # [2,1]
    ),
    swacc=True,
    hwset=True,
    sw=Access.RW, hw=Access.RW))

# ----------------------------------------------------------------------------
test_regmap = RegisterMap("test_regmap")
test_regmap.interface_name = "IF_RIF_TEST"
test_regmap.module_name = "rif_test"
test_regmap.uvm_regmodel_classname = "test_regmodel"
test_regmap.rif_host_bus_msb = 7 # 256B allocated to TEST, 8 bit address decode are significant
test_regmap.base_address = 0x10000
test_regmap.desc = """\
    This is my description text for top level regmap: test_regmap
"""

test_regmap.add(reg_r1, pad=4)
test_regmap.add(reg_1d, align=32)
test_regmap.add(reg_2d, align=32)

test_regmap.validate()

PyriftBase.md_option['register_address_formatting']= "0x{address//2:02X}" # format address (//1 for byte @, //2 for 16b word @)
PyriftBase.md_option['link_to_addr_map']= False # No link from @in reg description to address map

test_regmap.write_html("output/rif_test.html")
test_regmap.write_markdown("output/rif_test.md")
test_regmap.write_map_table("output/rif_test.map_table.txt")
test_regmap.write_svd("output/rif_test.svd")
test_regmap.write_verilog_interface("output/if_rif_test.sv")
test_regmap.write_verilog_module("output/rif_test.sv")
test_regmap.write_verilog_unwrap("output/rif_test_unwrap.sv")
test_regmap.write_verilog_package("output/rif_test_pkg.sv") # pkg for enum definitions
test_regmap.write_chead("output/rif_test.h", with_define_constant=True)
test_regmap.write_regmodel_uvm("output/test_regmodel.svh")
