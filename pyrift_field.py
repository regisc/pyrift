# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_field.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from enum import Enum, auto, IntEnum
from re import match
import datetime
import platform
import sys

# library to support xml generation
import xml.etree.ElementTree as ET
from inspect import cleandoc

# ----------------------------------------------------------------------------


class PyriftBase():
    """Base PyriftBase for other sub class"""

    # Add alternate version ID, planned to be incremented at each commit
    # better tracking for user which have no git tracking of the tool
    # to be incremented at each commit even if pyrift_version is unchanged
    # If branching from master without merging, to be defined how to handle ...
    # Please try to keep (pyrift_version, PYRIFT_REGMAP_VERSION) unique
    PYRIFT_REGMAP_VERSION = "98"

    def pyrift_version(self):
        return f"Pyrift v0.8- work in progress"

    # PyriftBase is base inheritage for all pyrift classes
    # It is used to host global variable setting that can be easily used anywhere in pyrift code
    # These global setting are defined hereafter

    # Data bus size used is defined here as dw class member
    dw = 32

    # have detailed generation info in generated file
    # Field.pyrift_generation_info = False to remove these infos in generated file
    pyrift_generation_info = True
    # have detailed version info in generated file
    # Field.pyrift_version_info = False to remove version infos in generated file
    pyrift_version_info = True

    # using latch is hot topic, usually it is a good idea to warn user
    # Set this one to true if you know what you are doing
    silent_latch_usage = False

    # Experimental feature are dangerous, the user have to enable it
    # You want it, you get it
    # Set to True only if you know what you are doing
    experimental=False

    # define default option for excel output
    # User can change unfitting option with PyriftBase.xls_option['opt']=value
    xls_option = {
        "bit_char_width" : 8, # Number of char per bit column
        "multiline_height" : 8,
        "column_regname_width" : 25,
        "column_desc_width" : 60,
        "column_bit_width" : 4, # single bit column width in excel unit
        "register_address_formatting" : "0x{address//1:04X}", # address formatting for XLS documentation
                                                              # will be used as :
                                                              # eval(f"f'{PyriftBase.xls_option['register_address_formatting']}'", {"address":...})
                                                              # through register.get_xls_formatted_address()
    }

    chead_option = {
        "wrap_enum_with_ifdef" : False, # if same .H code can be multiple included (enum call from multiple RIF)
        "no_constant_for_base_address" : False, # Change to True is you want the regmap__BASE_ADDRESS NOT to be generated
                                                # (case of multiple RIF with different address and you don't want conflict, or use base_address==0)
    }

    svpkg_option = {
        "no_constant_for_base_address" : False, # Change to True is you want the regmap__BASE_ADDRESS NOT to be generated
                                                # (case of multiple RIF with different address and you don't want conflict, or use base_address==0)
    }

    # define default option for markdown output
    # User can change unfitting option with PyriftBase.md_option['opt']=False
    md_option = {
        "with_regmap_header" : True,
        "with_regmap_footer" : True,
        "with_regmap_array_header" : True,
        "with_reset_bit_table" : False,
        "with_register_reset_value" : True,
        "with_reset_valid_mask" : True,
        "register_address_formatting" : "0x{address//1:08X}", # address formatting for MD documentation
                                                              # will be used as :
                                                              # eval(f"f'{PyriftBase.md_option['register_address_formatting']}'", {"address":...})
                                                              # through register.get_md_formatted_address()
        "regmap_separator" : '/',
        "regmap_column_desc" : False, # option no yet supported, please do NOT use (yet)
        "regmap_column_oneline_desc" : True,
        "desc_escape_markdown_special" : True, # special char in desc : *, _, ... are escape to avoid markdown formatting
                                               # False will allow MD formatting to be used (warning: can corrupt html output)
        "header_start_level" : 1,
        "header_address_map" : 'Address table of regmap, name: %(name)s, SysVerilog module name: %(module_name)s',
        "header_register_detail" : 'Registers details\n',
        "header_version_info" : 'Timestamp and pyrift version info  \n\n',
        'Column_SW' : True,
        'Column_HW' : True,
        'Column_Store' : True,
        'Column_Cdc' : True,
        'Column_If' : True,
        # Header text for register description in markdown
        'Header_Bit'   : 'Bit',
        'Header_Name'  : 'Name',
        'Header_SW'    : 'SW',
        'Header_HW'    : 'HW',
        'Header_Reset' : 'Reset',
        'Header_Store' : 'Store',
        'Header_Cdc'   : 'Cdc',
        'Header_If'    : 'If',
        'Header_Desc'  : 'Desc',
        #
        'link_to_reg' : True,
        'link_to_addr_map' : True,
    }

    def __init__(self):
        super(PyriftBase, self).__init__()
        self.name = "Uninitialized PyriftBase"
        self.desc = "Please provide description of class that inherit PyriftBase"
        self.ispresent = True  # present in final generation

    def pyrift_generation_message(self, user_notice, comment_header="// ", separator='-', force_version_and_gen_info=False):
        platform_summary = platform.sys.version.replace('\n', ' ') # can be multiline of some platform (linux)
        msg_separator = f"{comment_header}{separator*58}\n\n"
        msg_pyrift_version =  (
            f"{comment_header}Code generated automatically with {self.pyrift_version()}\n"
            f"{comment_header}Incremental version id: {self.PYRIFT_REGMAP_VERSION}\n"
            f"{comment_header}Pyrift can be found at https://gitlab.com/regisc/pyrift\n"
        ) if Field.pyrift_version_info or force_version_and_gen_info else ""
        msg_gen_info =  (
            f"{comment_header}Python platform: {platform_summary}\n"
            f"{comment_header}System: {platform.platform()}\n"
            f"{comment_header}Generate Date: {datetime.datetime.now()}\n"
        ) if Field.pyrift_generation_info or force_version_and_gen_info else ""
        msg = msg_separator + msg_pyrift_version + msg_gen_info + msg_separator
        with open(user_notice) as f:
            notice_text = f.readlines()
            # remove whitespace characters like `\n` at the end of each line
            # add header
            notice_text = [comment_header+x.strip()+"\n" for x in notice_text]
        msg += ''.join(notice_text)
        msg += f"{comment_header}{separator*58}\n"
        return msg

# ----------------------------------------------------------------------------


class Access(Enum):
    RW = auto()
    R = auto()
    W = auto()
    NA = auto()

    def is_readable(self):
        return (self == Access.RW) or (self == Access.R)

    def is_writable(self):
        return (self == Access.RW) or (self == Access.W)


    def __or__(self, other):
        """
            Implement mixing of 2 access mode together (result is self|other)
            For instance, 2 aliased field with sw access mode gives the ored acces mode
            (result is R is any of self or other is R)
            (result is W is any of self or other is W)
              other | NA |  R |  W | RW |
            --------+----+----+----+----+
            self NA | NA |  R |  W | RW |
                  R |  R |  R | RW | RW |
                  W |  W | RW |  W | RW |
                 RW | RW | RW | RW | RW |
            --------+----+----+----+----+
        """
        if not isinstance(other, Access):
            raise RuntimeError(f"Access.__or__(other) found unexpected type as other=({other}), class=({other.__class__}) ")
        if self.is_readable() or other.is_readable():
            if self.is_writable() or other.is_writable():
                result = Access.RW
            else:
                result = Access.R
        else:
            # None readable
            if self.is_writable() or other.is_writable():
                result = Access.W
            else:
                result = Access.NA

        return result
# Basic debugging test for Access __or__ operator implementation
# for a in Access:
#     for b in Access:
#         print(f"a={a.name},b={b.name},a|b={(a|b).name}",)


class Cdc(Enum):
    NA = auto()
    HOST_CE = auto() # resync in CDC as simple CE on host clock
    FULL_ASYNC = auto() # resync in CDC assuming full async clock

# Create subclass of IntEnum to handle encode of field enum
class FieldEncode(IntEnum):
    @classmethod
    def get_bit_length(cls):
        """
            helper method to evaluate the range of value used to encode field
        """
        enum_range = [x.value for x in cls]
        if min(enum_range) < 0:
            raise RuntimeError("Enum type for verilog package generation is only supported for positive values")
        # assume range 0 to 2^N - 1 supported in N bit logic vector
        # Find N from max
        bit_length = max(enum_range).bit_length()
        # print(f"bw={bit_length} for class={cls.__name__}")
        return bit_length

    @classmethod
    def get_value_desc(cls, value):
        # getattr have special handling of normal enum member, not used
        if hasattr(cls, '__value_desc_dict__'):
            return cls.__value_desc_dict__.get(value, "")
        else:
            return ""

    @classmethod
    def get_desc(cls):
        # this class method can be overloaded by user enum class if prefered to __desc__ declaration
        # This allow as well to be backward compatible to get_desc() definition by user (historical way to describe enum)
        # getattr have special handling of normal enum member, not used
        if hasattr(cls, '__desc__'):
            return cls.__desc__
        else:
            return ""

# ----------------------------------------------------------------------------


class Field(PyriftBase):
    """
        The most basic PyriftBase object.
        Fields serve as an abstraction of hardware storage elements.
    """

    # Field class variables
    # Used to set default values for any instance
    default_latch_transparent_state = 0 # latch transparent on 0 or 1
    default_latch_use_lib_ldce = False # latch generation use pyrift lib primitive (help in hold time closure in vivado)
    default_support_hwbe = False # support byte enable masking on host bus write if True

    def __init__(self,
        name=None,
        bit_range='AUTO', # normally explicit by user, "AUTO" : try auto guess bit_range
        bit_width=None, # integer can be provided by user in case of AUTO bit_range
        shortname=None,
        name_in_if=None,
        interface_name=None,
        desc=None,
        # bring all field to constructor parameter with default value
        # should help user for concise Field creation
        hw=Access.RW,
        sw=Access.RW,
        sw_signed=False, # unsigned view from cheader is default
        ispresent=True,
        prop=None, # add user specific properties to field (to be provided as dictionary)
        multipart_if_pattern = None,
        multipart_if_slave = None,
        alias=None,
        shadow=None,
        if_reshape = None,
        external = False,
        extready_rd = False,
        extready_wr = False,
        latch = False,
        donttest = False,
        dontcompare = False,
        reset = 0, # field by default have a reset to 0, set to None to remove reset feature
        resetsignal = None,
        hwset = False,
        hwclr = False,
        hwset_have_precedence_over_hwclr = None, # None is not defined (assume no simultaneous hwclr/hwset)
                                                 # False: same as None, without warning (priority to hwclr)
                                                 # True: priority to hwset
        swwe = False,
        swwel = False,
        swacc = False,
        early_swacc = False,
        swmod = False,
        delayed_swmod = False,
        swwr2hwif = False, # export write field information to hw IF
        rclr = False,
        rset = False,
        woclr = False,
        woset = False,
        wot = False,
        wzc = False,
        wzs = False,
        wzt = False,
        wclr = False,
        wset = False,
        warl = None,
        wonce = False,
        encode = False,
        hw_have_wr_precedence_over_fw = False,
        support_hwbe = None, # if None, fallback to default_support_hwbe
                             # if False or True, use value for this current register
        singlepulse = False,
        hwce_signal_name = None, # None for not used as default,
                                 # use the string name of signal to use
                                 # if None fallback to default_hwce_signal of top regmap owner
                                 # hw is clock enabled when this signal is active
                                 # To be used to delay the host cycle accordingly
        cdc_out = Cdc.NA,
        cdc_swmod = Cdc.NA,
        cdc_swacc = Cdc.NA,
        cdc_out_signal_name = None, # None for not used as default,
                                    # use the string name of signal to use (clock or ce)
        cdc_in = Cdc.NA,
        cdc_in_signal_name = None, # None for not used as default,
                                   # use the string name of signal to use (clock or ce)
        ):
        super().__init__()
        if shortname and name:
            raise RuntimeError("Field constructor error: both name AND shortname provided !")
        if not shortname and not name:
            raise RuntimeError("Field constructor makes either name or shortname mandatory !")
        if shortname:
            # shortname provided in __init__, make default name
            self.name = shortname
            print(f"Field {shortname} constructor is using deprecated 'shortname' parameter,")
            print(f"Please update your script to use 'name' instead")
        if name:
            self.name = name
        self.name_in_if = name_in_if

        # parent is used when field is included in a parent register
        # allow check for single parent inclusion
        self.parent = None

        # desc is provided as constructor "desc" param
        # else carry default setting from base class
        if desc:
            self.desc = desc
                                 #

        if(PyriftBase.dw % 8 != 0):
            raise RuntimeError(
                f"Data bus width is mandatory to be multiple of 8, found unexpected dw={PyriftBase.dw}")

        if bit_range == 'AUTO':
            # Auto bit_range mode, try to guess bit_width here
            if type(bit_width) is int:
                if (bit_width <0) or (bit_width>PyriftBase.dw):
                    raise RuntimeError(f"Unexpected integer bit_width for AUTO bit_range {bit_width}")
            elif bit_width == None:
                # no bit_width, may try to guess for enum field (encode provided in constructor)
                if encode and issubclass(encode, FieldEncode):
                    bit_width = encode.get_bit_length()
                else:
                    raise RuntimeError(f"Unexpected AUTO bit_range without either bit_range or valid encode in constructor")
            # print (f"Guess bit width = {bit_width} ")
            self.bit_width = bit_width
            # Field may not be added to register yet
            # Can not get lsb position till the register validate_check_premap is called
            # add a marker till this is done
            self.lsb_bit = 'TBV' # To be validated in validate_check_premap on register
        else: #bit_range provided by user
            # check bit field as either '2' or '4:3'
            m1 = match(r'^(\d*)$', bit_range)
            m2 = match(r'^(\d*):(\d*)$', bit_range)
            if m2:
                msb_bit = m2.group(1)
                lsb_bit = m2.group(2)
                self.lsb_bit = int(lsb_bit)
                self.bit_width = int(msb_bit) - self.lsb_bit + 1
            elif m1:
                self.lsb_bit = int(m1.group(1))
                self.bit_width = 1
            else:
                raise RuntimeError("Unexpected bit_range for field creation:" + bit_range)
            if(self.lsb_bit<0):
                raise RuntimeError("Unexpected bit_range for field creation:" + bit_range +
                                   ". lsb should be positive")
            if(self.bit_width<=0):
                raise RuntimeError("Unexpected bit_range for field creation:" + bit_range +
                                   ". msb:lsb should get msb>=lsb")
            if(self.lsb_bit+self.bit_width>PyriftBase.dw):
                raise RuntimeError(f"Unexpected bit_range for field creation:{bit_range}."\
                                   f" Msb exceeding bit[{PyriftBase.dw}] for {PyriftBase.dw} bit host bus !")

        self.hw = hw
        self.sw = sw
        self.sw_signed = sw_signed
        self.ispresent = ispresent
        if prop and isinstance(prop, dict):
            self.prop = prop
        else:
            self.prop = {}
        self.interface_name = interface_name

        self.latch = latch # if you want to generate latch based storage element
                           # Use with extreme caution, better you know what your are doing
        self.latch_transparent_state = Field.default_latch_transparent_state # from Field class var
        self.latch_use_lib_ldce = Field.default_latch_use_lib_ldce # from Field class var

        # multipart is still present to catch user error. Not supported anymore since v0.5
        self.multipart_if_pattern = multipart_if_pattern
        self.multipart_if_slave = multipart_if_slave

        self.alias = alias # None if no alias, or string for path to alias target
        self.shadow = shadow # None if no shadow, or string for path to shadow primary target
                             # if self is a shadow, it collects and apply write access from primary target
        self.if_reshape = if_reshape # could be a list a extra dimension to reshape on
                                     # = (2,) to have [...:0][1:0]
        self.external = external
                      # no verilog/rtl generation for the register internal storage if external
                      # provide host bus signal in interface to allow implementation as external
                      # Note: using that prevent several option (hw properties or related)
                      #       No hwclr, hwset, woclr, woset, swwe, swmod, wz*, ...
        self.extready_rd = extready_rd # external ready signal for read access
        self.extready_wr = extready_wr # external ready signal for write access
        #
        self.donttest = donttest
        self.dontcompare = dontcompare
        # HW properties
        self.reset = reset
        self.resetsignal = resetsignal # string, name of signal to be used for reset (otherwise "rst_n")
        self.hwset = hwset # makes hw writable (can be hw=R/NA)
        self.hwclr = hwclr # makes hw writable (can be hw=R/NA)
        self.hwset_have_precedence_over_hwclr = hwset_have_precedence_over_hwclr
        self.swwe = swwe  # hw enable sw to write into register
        self.swwel = swwel
        self.swacc = swacc # pulse when sw is accessing register (read)
        self.early_swacc = early_swacc # swaac is early pulse when sw is accessing register (read)
        self.swmod = swmod # pulse when sw is modifying register
                           # (write or read with side effect (rset, rclr))
        self.delayed_swmod = delayed_swmod # swmod is delayed by 1 clock on interface
                                           # modified field data already valid on pulse
        self.swwr2hwif = swwr2hwif
        # SW properties
        self.rclr = rclr
        self.rset = rset
        #
        self.woclr = woclr  # write one clear
        self.woset = woset  # write one set
        self.wot = wot    # write one toggle
        self.wzc = wzc  # write zero clear
        self.wzs = wzs  # write zero set
        self.wzt = wzt  # write zero toggle
        #
        self.wclr = wclr  # write clear
        self.wset = wset  # write set
        self.warl = warl  # write any, read legal : warl is list of legal value, first is default value written if illegal
        self.wonce = wonce  # write once, next write are ignored

        self.encode = encode

        self.hw_have_wr_precedence_over_fw = hw_have_wr_precedence_over_fw
        self.singlepulse = singlepulse

        if(support_hwbe == None):
            # Constructor parameter not given, use default value
            self.support_hwbe = Field.default_support_hwbe
        else:
            # use constructor param
            self.support_hwbe = support_hwbe

        self.hwce_signal_name = hwce_signal_name # string name to find top regmap signal
        self.hwce_signal = None # Signal pointer set during validate_check_premap if applicable
        self.cdc_out = cdc_out
        self.cdc_swmod = cdc_swmod
        self.cdc_swacc = cdc_swacc
        self.cdc_out_signal_name = cdc_out_signal_name
        self.cdc_out_signal = None # Signal pointer set during validate_check_premap if applicable
        self.cdc_in = cdc_in
        self.cdc_in_signal_name = cdc_in_signal_name
        self.cdc_in_signal = None # Signal pointer set during validate_check_premap if applicable

    def get_oneline_desc(self):
        reg_oneline_description = self.desc.replace("\r\n", "\n").splitlines()
        if len(reg_oneline_description)>=1:
            reg_oneline_description = reg_oneline_description[0].strip()
        else:
            reg_oneline_description = ""
        return reg_oneline_description

    def get_src_alias(self, map_table_entry):
        """
            return iterator on all aliased field for self (excluding self)
            That is all field that are src_alias in field_idb of register containg the field
            Each iterator item is a tuple (entry_map, field_name_string)
            alias source exploration is using field_idb that is populated during regmap validate()
                self.field_idb['field_name']["alias_src"] = [{"entry":entry, "field":f}]
        """
        # print(f"Lookup idb for field={self.name}, entry_t={map_table_entry.type} entry={map_table_entry.register.name}")
        # print(f"aliased_idb={map_table_entry.field_idb[self.name]['alias_src']}")
        return map_table_entry.field_idb[self.name]["alias_src"]

    def get_all_alias(self, map_table_entry):
        """
            return iterator on all aliased field for self (including source and target)
            Can be called on either a source of alias or on a target of alias
            Can also be called on non alias field (and here return empty iterator)
            alias source exploration is using field_idb that is populated during regmap validate()
        """

        # if on a source alias, find out the target and process from there
        if self.alias:
            # field is src alias, find target to call get_alias_ored_attr or this target
            target_entry, target_field_name = map_table_entry.resolve_field_path(self.alias)
            target_field = target_entry.register.get_field_by_name(target_field_name)
        else:
            # either target alias or no alias at all
            target_entry = map_table_entry
            target_field = self

        # Build alias list with : [src] + target
        # all list item is a dict {"entry":entry, "field":f}
        alias_dict_list=[{"entry":target_entry, "field":target_field}]
        alias_dict_list += list(target_field.get_src_alias(target_entry))

        return alias_dict_list

    def get_all_alias_or_shadow(self, map_table_entry):
        # first find all field that are aliased related
        alias_or_shadow_dict_list = self.get_all_alias(map_table_entry)

        # second add shadow as another source of sw access
        if self.shadow:
            target_entry, target_field_name = map_table_entry.resolve_field_path(self.shadow)
            target_field = target_entry.register.get_field_by_name(target_field_name)
            alias_or_shadow_dict_list += [{"entry":target_entry, "field":target_field}]

        return alias_or_shadow_dict_list

    def get_alias_ored_attr(self, attr, aliased=False, map_table_entry=None):
        """
            Get attribute value for field
            Primary field version is called as field.get_alias_ored_attr('attr') or
                                              field.get_alias_ored_attr('attr', aliased=False, map_table_entry=None)
            Primary version return the attr value for the field itself (ignoring any alias field)
            Aliased field version is called as
                    field.get_alias_ored_attr('attr', aliased=True, map_table_entry=<entry of instanciating register>)
            and return the ored of (primary value, all aliased values)
            Shadow field case are handled automatically behind get_alias_ored_attr
            (shadow sw access is the same path as alias sw access)
        """

        # first get primary sw access mode
        attribute = getattr(self, attr)  # assume attribute do exist
        if aliased:
            if not map_table_entry:
                raise RuntimeError(f"Aliased get attribute required map_table_entry of instanciating register")

            # modify primary attribute with alias field if any exist
            # primary will be reprocessed (once for initial attribute setting, once to be ored when cumulating)
            # x or a  = (x or a) or a (don't cére because (a or a) == a), so self can be processed twice
            # Aliased evaluation can be called from field in either category:
            #   - non aliased field
            #   - target aliased field (self.get_src_alias() will find src_alias field)
            #   - src aliased field (self.alias point to target alias field)
            # Current function explore get_all_alias to find all src_alias field, so every field
            # related to aliasing in processing (either src or target alias)
            # reuse get_all_alias to simplify code in current method
            #
            for alias_dict in self.get_all_alias_or_shadow(map_table_entry):
                # alias_dict is a dict:{"entry":entry, "field":f}
                alias_reg=alias_dict["entry"].register
                alias_field = alias_dict["field"]
                alias_field_name=alias_field.name
                if alias_field == self:
                    continue # self is already accounted and pad field are not tracked in register
                # print(f"self={self.name} alias-reg={alias_reg.name} alias_field={alias_field_name}")
                # add current alias contribution to swa
                attribute |= alias_reg.get_field_by_name(alias_field_name).get_alias_ored_attr(attr)
            # print(f"Final aliased attr={attr} for {self.get_full_hw_name(map_table_entry)}:{getattr(self, attr, False)}->{attribute}")


        return attribute


    def bit_range_str(self):
        if(self.bit_width == 1):
            result = str(self.lsb_bit)
        else:
            result = str(self.lsb_bit + self.bit_width - 1) + ':' + str(self.lsb_bit)
        return result

    def has_storage(self, aliased=False, map_table_entry=None):
        """
            Check if the implementation have a storage element associated
            This is the case if field or any alias have:
                - SW is rw
                - SW is w and HW is r or rw
            SW=w and HW=w is illegal
            SW read with side effect (rclr/rset) are considerer as SW=W for storage needs
            For SW is r, HW special :hwset, hwclr, or SW special rclr/rset force the storage
            if aliased==True, then consider shadow path as extra write access (storage if HW is r or rw)
        """


        # get sw access mode and rclr/rset extension (from aliased view if requested)
        alias_swa  = self.get_alias_ored_attr(attr='sw',   aliased=aliased, map_table_entry=map_table_entry)
        alias_rclr = self.get_alias_ored_attr(attr='rclr', aliased=aliased, map_table_entry=map_table_entry)
        alias_rset = self.get_alias_ored_attr(attr='rset', aliased=aliased, map_table_entry=map_table_entry)
        is_extended_sw_writable = (alias_swa == Access.R) and (alias_rclr or alias_rset)
        return alias_swa == Access.RW or \
            (alias_swa == Access.W and self.hw.is_readable()) or \
            (is_extended_sw_writable and self.hw.is_readable()) or \
            (alias_swa == Access.R and (self.hwset or self.hwclr or self.hw == Access.RW or is_extended_sw_writable))


    def is_volatile(self, map_table_entry):
        """
            Get the volatile feature of the field
            Volatile means the SW readback value have a chance to differ from SW written value
            Not the case if: not has_storage
            Is the case if:
                - not a sw read/write
                - rclr, rset, woclr, woset, wot, wzc, wzs, wzt, wclr, wset, warl affect content not in usual way
                - hw has write access
                - swwe : prevent sw write to execute (make unpredictable, so is volatile)
                - external register (behaviour handle in hw is unknown, assume volatile)
                - swmod/delayed_swmod : hw is using the write event to trig side effect, so don't want
                                        write to be optimized by software compiler (all write event is precious)

            + Check how volatility is affected by aliasing (either src or target)
            If field has aliases, then use aliased version of sw, rclr, rset
            (also for wo*, wz*, wclr, wset ?)
            If alias field is involved, any write flavour on alias source make the current field volatile



        """
        # first check the current/primary field for volatility (ignoring any alias)
        primary_is_volatile =\
                (self.sw != Access.RW) or \
                self.rclr or \
                self.rset or \
                self.woclr or \
                self.woset or \
                self.wot or \
                self.wzc or \
                self.wzs or \
                self.wzt or \
                self.wclr or \
                self.wset or \
                self.warl or \
                self.hw.is_writable() or \
                self.hwset or \
                self.hwclr or \
                self.swwe or self.swwel or \
                self.external or \
                self.delayed_swmod or \
                self.swmod # swmod force volatile, so get sure write access is not optimized by compiler
        # print (f"Field {self.get_name()} is nat-vol={primary_is_volatile}")

        # now check for all other field contributing to aliasing (self excluded)
        other_alias_have_write_support = False
        for alias_dict in self.get_all_alias(map_table_entry):
            # alias_dict is a dict:{"entry":entry, "field":f} => unmap to basic vars
            alias_entry=alias_dict["entry"]
            alias_field=alias_dict["field"]
            # exclude the self case (get_all_alias include self)
            if alias_field == self:
                continue

            other_alias_have_write_support = other_alias_have_write_support or \
                (alias_field.sw.is_writable()) or \
                alias_field.rclr or \
                alias_field.rset or \
                alias_field.hw.is_writable() or \
                alias_field.hwset or \
                alias_field.hwclr or \
                alias_field.delayed_swmod or \
                alias_field.swmod
            # alternate way of write : woclr,woset,wot,wzc,wzs,wzt,wclr,wset, swwe, swwel are don't care
            # as sw.is_writable() is already True in such option
            # external is not considered for alias (not supported)

        # if field is shadow of a primary register, the primary write path makes the field volatile
        other_alias_have_write_support = other_alias_have_write_support or self.shadow

        result = primary_is_volatile or other_alias_have_write_support
        # print (f"Field {self.get_name()} is volatile={result}:---:"\
        #        f"{primary_is_volatile}/{other_alias_have_write_support} (nat/other)")
        return result

    def is_value_volatile(self, map_table_entry):
        """
            Get the volatile feature of the field by value (excluding side-effect like swmod and predictable woclr, ...)
            Volatile means the SW readback value have a chance to readback from SW as unpredictable
            Not the case if: not has_storage
            Is the case if:
                - not a sw read/write
                - hw has write access
                - swwe : prevent sw write to execute (make unpredictable, so is volatile)
                - external register (behaviour handle in hw is unknown, assume volatile)

            + Check how volatility is affected by aliasing (either src or target)
            If field has aliases, then use aliased version of sw, rclr, rset
            (also for wo*, wz*, wclr, wset ?)
            If alias field is involved, any write flavour on alias source make the current field volatile

            This function was intended to adjust the modifiedWriteValues attribute in SVD generation
            but now not used anymore
        """
        # first check the current/primary field for volatility (ignoring any alias)
        primary_is_volatile =\
                (self.sw != Access.RW) or \
                self.hw.is_writable() or \
                self.hwset or \
                self.hwclr or \
                self.swwe or self.swwel or \
                self.external
        # print (f"Field {self.get_name()} is nat-vol={primary_is_volatile}")

        # now check for all other field contributing to aliasing (self excluded)
        other_alias_have_write_support = False
        for alias_dict in self.get_all_alias(map_table_entry):
            # alias_dict is a dict:{"entry":entry, "field":f} => unmap to basic vars
            alias_entry=alias_dict["entry"]
            alias_field=alias_dict["field"]
            # exclude the self case (get_all_alias include self)
            if alias_field == self:
                continue

            other_alias_have_write_support = other_alias_have_write_support or \
                (alias_field.sw.is_writable()) or \
                alias_field.rclr or \
                alias_field.rset or \
                alias_field.hw.is_writable() or \
                alias_field.hwset or \
                alias_field.hwclr
            # alternate way of write : woclr,woset,wot,wzc,wzs,wzt,wclr,wset, swwe, swwel are don't care
            # as sw.is_writable() is already True in such option
            # external is not considered for alias (not supported)

        # if field is shadow of a primary register, the primary write path makes the field volatile
        other_alias_have_write_support = other_alias_have_write_support or self.shadow

        result = primary_is_volatile or other_alias_have_write_support
        # print (f"Field {self.get_name()} is volatile={result}:---:"\
        #        f"{primary_is_volatile}/{other_alias_have_write_support} (nat/other)")
        return result

    def get_name(self):
        """
            return name
        """
        return self.name

    def get_name_in_if(self):
        """
            return name inside verilog interface
        """
        if self.name_in_if:
            return self.name_in_if
        return self.get_name()

    def is_agregate_field(self, with_name=""):
        """
            aggegate field have name_in_if starting with '@'
        """
        pattern = "@"+with_name
        return match(pattern, self.get_name_in_if())

    def get_aggregate_info(self):
        """
            aggegate field have name_in_if such as '@name[123]'
            return (name, "123")
        """
        m = match(r"@(\w+)\[(\d+)\]", self.get_name_in_if())
        if m and len(m.groups())==2:
            return (m.group(1), int(m.group(2)))
        else:
            print("### Aggregation pattern should match @AGGREGATIONNAME.# : with @ an identifier a dot and a number")
            print("### Please fix script to modify the name_in_if to somthing else")
            raise RuntimeError(f"Unexpected failure in searching for aggregate name in "\
                               f"name_in_if={self.get_name_in_if()} name={self.name} ")

    def get_full_hw_name(self, map_table_entry):
        # get hierarchical name using register info from map_table_entry
        return map_table_entry.get_full_hw_name() + "_f_" + self.get_name()

    def get_interface_name(self):
        """
            Field is part of a register hierarchy,
            Search first ancestor that have an interface_name
            (to be used for verilog interface connection)
            root parent may have several interface, so find out where it belong
        """
        if getattr(self, 'interface_name', None):
            return self.interface_name
        else:
            return self.parent.get_interface_name() # parent of field is a register

    def get_reset_signal(self):
        # find reset signal to be used for field storage (if any)
        # Could be regmap default, or a spcific reset for this field
        # This is used for core field, and also for cdc flop (in, out)
        if self.reset != None:
            # Field flops have a reset feature
            if self.resetsignal:
                # locate the signal in root regmap to get signal attribute
                field_rst_signal = self.parent.get_root_parent().get_signal_by_name(self.resetsignal)
            else:
                # no explicit reset signal for this field, use default reset
                field_rst_signal = self.parent.get_root_parent().default_reset_signal
        else:
            field_rst_signal = None
        return field_rst_signal

    def get_reset_edge(self):
        if self.get_reset_signal():
            return self.get_reset_signal().reset_edge()
        else:
            return ""

    def get_reset_high_name(self):
        if self.get_reset_signal():
            return self.get_reset_signal().high_name()
        else:
            return "!? unable to find reset !?"

    def get_reset_value(self, map_table_entry):
        if self.reset:
            # self.reset is a non 0 integer or a tuple list of reset value
            if isinstance(self.reset, int):
                reset_value = self.reset
            elif isinstance(self.reset, tuple):
                # Reset value to be customized in array of register
                # Use map_table_entry to index self.reset as
                # array of reset value to apply
                # print(f"Look up reset in {self.reset} at {map_table_entry.linear_index}")
                array_size = map_table_entry.get_array_size_in_hierarchy()
                reg_array_size = map_table_entry.get_array_layer_size()
                if len(self.reset) == array_size:
                    reset_value = self.reset[map_table_entry.linear_index]
                elif len(self.reset) == reg_array_size:
                    reset_value = self.reset[map_table_entry.linear_index % reg_array_size]
                else:
                    raise  RuntimeError(
                        f"Field:{self.get_name()}: mismatch in reset value size={len(self.reset)}"\
                        f" for {self.reset}, expect size of either 1, or {reg_array_size} or {array_size}")
            else:
                raise RuntimeError(
                    f"No support of reset value {self.reset} type field : " + self.get_name())
        else:
            # None or 0 as reset, default to 0 for result
            reset_value = 0
        return reset_value

    def is_valid_reset_value(self, map_table_entry):
        """
            get_reset_value() always provide an integer value (default to 0 if unknown)
            is_valid_reset_value report False if get_reset_value() is inconsistent/unknwon
            Check the storage type and option, and self.reset to identify the different cases
        """
        if self.external:
            valid = False
        elif self.has_storage(aliased=True, map_table_entry=map_table_entry):
            # internal storage, flop or latch ?
            if self.latch:
                valid = (self.reset != None)
            else:
                valid = (self.reset != None)
        else: # no storage
            if self.hw.is_writable():
                # no storage, hw writable, this is status direct from hw
                valid = False
            else:
                # no storage, const to reset
                valid = True
        return valid

    def get_storage_type_str(self, map_table_entry):
        # storage_type = "?"
        # storage type is reported in documentation
        # The actual storage type is relevant only for non alias type
        # Alias field are reported as sotrage="Alias"
        if self.external:
            storage_type = "Extern"
        elif self.alias:
            # actual storage is in another aliased field
            storage_type = "Alias"
        elif self.has_storage(aliased=True, map_table_entry=map_table_entry):
            # internal storage, flop or latch ?
            if self.latch:
                storage_type = "Latch"
            else:
                storage_type = "F.Flop"
        else: # no storage
            if self.hw.is_writable():
                # no storage, hw writable, this is status direct from hw
                storage_type = "Hw.Wire"
            else:
                # no storage, const to reset
                storage_type = "Const"
        return storage_type

# ----------------------------------------------------------------------------

    def validate_check_premap(self):
        # implement error checking to validate_check_premap the field parameter are consistent
        # This is call for each field of register in Register.validate_check_premap()
        # No maptable entry is needed at this point, only intrinsic properties are checked
        # (case of aliased software access, or option involving knowledge of aliased field are not cehcked here)

        if self.external:
            if self.cdc_out != Cdc.NA or self.cdc_swmod != Cdc.NA or self.cdc_swacc != Cdc.NA or\
               self.cdc_in != Cdc.NA:
                raise RuntimeError(
                    f"No support of CDC option on external field : " + self.get_name())
            # no hw option supported
            for option in ["hwset", "hwclr", "swwe", "swwel", "swacc", "early_swacc",
                    "swmod", "delayed_swmod", "rclr", "rset",
                    "woclr", "woset", "wot", "wzc", "wzs", "wzt", "wclr", "wset",
                    ]:
                if getattr(self, option):
                    raise RuntimeError(f"{option} is not supported on external field : {self.get_name()}")
        if self.singlepulse:
            if self.cdc_out != Cdc.NA and self.cdc_out != Cdc.HOST_CE:
                raise RuntimeError(
                    f"Using cdc_out={self.cdc_out} option with singlepulse is not supported on {self.get_name()}")
        if self.swacc and self.early_swacc:
            raise RuntimeError(
                f"swacc and early_swacc are exclusive options on field : {self.get_name()}")
        if self.swmod and self.delayed_swmod:
            print (f"Warning: both swmod and delayed_swmod option used on {self.get_name()}")
            print (f"         This is deprecated use, please activate only one option")
            print (f"         Assuming delayed_swmod is needed.")
        if self.swmod:
            if self.cdc_swmod != Cdc.NA and self.cdc_swmod != Cdc.HOST_CE:
                print (f"Warning: using cdc_swmod option with swmod may be not supported on {self.get_name()}:cdc_swmod={self.cdc_swmod}")
                print (f"         Please double check the CDC on swmod signal on interface")
            if self.cdc_swmod == Cdc.HOST_CE and self.delayed_swmod:
                # delayed swmod and cdc out CE are 2 ways to generated swmod on internal :
                # required to select one or the other
                raise RuntimeError(
                    f"Delayed_swmod and cdc_swmod == Cdc.HOST_CE are exclusive options on field : " + self.get_name())
        # check reset type (None, 0, integer, or tuple of integer)
        if self.reset:
            if isinstance(self.reset, int) or isinstance(self.reset, tuple):
                pass
            else:
                raise RuntimeError(f"reset type unexpected for field : " + self.get_name())
            # Check all reset value (single value or table)
            reset_value_list = (self.reset,) if isinstance(self.reset, int) else self.reset
            for reset_value in reset_value_list:
                if reset_value < 0:
                    raise RuntimeError(f"{self.get_name()} reset expected as positive, found unsupported negative: {self.reset}")
                if reset_value.bit_length() > self.bit_width:
                    raise RuntimeError(f"{self.get_name()} reset={reset_value} too large (need {reset_value.bit_length()} bit)"\
                                       f" to fit field bitwidth ({self.bit_width} bit)")
        if self.encode:
            if not issubclass(self.encode, FieldEncode):
                raise RuntimeError(f"Field.encode for {self.get_name()} is required to use FieldEncode (Please note IntEnum not supported anymore)"\
                                   f" encode={self.encode.__name__}:{self.encode.__bases__}")
            # Check that the encode bit width is matching the field bit width
            if self.encode.get_bit_length() > self.bit_width:
                raise RuntimeError(f"Field {self.get_name()} bitwidth({self.bit_width}) too small to fit enum "\
                                   f"encode:{self.encode.__name__}, "\
                                   f"required : {self.encode.get_bit_length()} bit")
            elif self.encode.get_bit_length() < self.bit_width:
                print (f"Warning : field {self.get_name()} is using {self.bit_width} bit where "\
                       f"only {self.encode.get_bit_length()} needed for encode:{self.encode.__name__}")

        if self.multipart_if_pattern or self.multipart_if_slave:
            print (f"Multipart was deprecated use since v0.4, and support is now removed since v0.5")
            print (f"Please switch to field aggregation for equivalent features")
            print (f"Using name_in_if='@agg[?]' should do the job")
            if self.multipart_if_pattern:
                raise RuntimeError(f"Unsupported multipart_if_pattern option used on {self.get_name()}")
            if self.multipart_if_slave:
                raise RuntimeError(f"Unsupported multipart_if_slave option used on {self.get_name()}")

        if self.alias:
            # alias re-use implemetation of other field, so no hw connection
            if self.hw != Access.NA:
                raise RuntimeError(f"Alias Field {self.get_name()} hw access={self.hw} should be NA")

            # no hw option supported
            for option in ["if_reshape", "external", "extready_rd", "extready_wr",
                    "latch", "reset", "resetsignal", "hwset", "hwclr",
                    "swwe", "swwel", "swacc", "early_swacc", "swmod", "delayed_swmod", "interface_name",
                    "singlepulse", "cdc_out_signal_name",  "cdc_in_signal_name"]:
                if getattr(self, option):
                    raise RuntimeError(f"Alias Field {self.get_name()} cannot define .{option}, "\
                                       f"it must share same alias target (found:{getattr(self, option)})")

            for option in ["cdc_out", "cdc_swmod", "cdc_swacc", "cdc_in"]:
                if getattr(self, option) != Cdc.NA:
                    raise RuntimeError(f"Alias Field {self.get_name()} cannot define .{option}, "\
                                       f"it must share same alias target (expect Cdc.NA, found:{getattr(self, option)})")
        if self.hwclr and self.hwset:
            # check for simultaneous hwclr and hwset, require precedence self.hwset_have_precedence_over_hwclr
            # to be defined to be warning less
            if self.hwset_have_precedence_over_hwclr==None:
                print(f"Warning: hwset and hwclr are both active on field : {self.get_name()}")
                print(f"         hwset_have_precedence_over_hwclr is not defined (None),")
                print(f"         Please check current hwclr precedence over hwset is what you want.")
                print(f"         You can define hwset_have_precedence_over_hwclr to either False or True")
                print("          to suppress this warning.")

        # isascii check is silently skipped in version less than 3.7 (where isascii has been added)
        if sys.hexversion >= 0x3070000 and not self.desc.isascii():
            print (f"Warning: Non ascii char detected in desc of field {self.get_name()}")

# ----------------------------------------------------------------------------

    def validate_check_postmap(self, map_table_entry):
        # implement error checking to validate_check the field parameter are consistent
        # This is call for each field of register in Register.validate_check_postmap()
        # No maptable entry is needed at this point, only intrinsic properties are checked
        # (case of aliased software access, or option involving knowledge of aliased field are not checked here)

        if not self.has_storage(aliased=True, map_table_entry=map_table_entry) and self.cdc_out != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC out option without storage for field : " + self.get_name())
        if not self.has_storage(aliased=True, map_table_entry=map_table_entry) and self.cdc_swmod != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC swmod option without storage for field : " + self.get_name())
        if not self.has_storage(aliased=True, map_table_entry=map_table_entry) and self.cdc_swacc != Cdc.NA:
            raise RuntimeError(
                f"No support of CDC swacc option without storage for field : " + self.get_name())
        if not self.has_storage(aliased=True, map_table_entry=map_table_entry) and self.latch:
            raise RuntimeError(
                f"Latch option is not applicable for field without storage : {self.get_name()} in reg:{map_table_entry.register.name}")

        # sw access mode is to be checked versus the aliased fields view
        alias_swa = self.get_alias_ored_attr(attr='sw', aliased=True, map_table_entry=map_table_entry)

        # storage based field is already check for valid systemRdl sw/hw combination
        # check only for remaining combination of sw/hw access of internal register fields
        if not self.has_storage(aliased=True, map_table_entry=map_table_entry) and not self.external:
            # report invalid line for systemRdl standard
            if (alias_swa==Access.W and self.hw==Access.W) or \
               (alias_swa==Access.W and self.hw==Access.NA):
                raise RuntimeError(
                    f"sw={alias_swa} and hw={self.hw} considered 'Error-Meaningless' on field : " + self.get_name())
            if (alias_swa==Access.NA and self.hw==Access.RW) or \
               (alias_swa==Access.NA and self.hw==Access.R):
                raise RuntimeError(
                    f"sw={alias_swa} and hw={self.hw} considered 'Undefined' on field : " + self.get_name())
            if alias_swa==Access.NA and self.hw==Access.W:
                raise RuntimeError(
                    f"sw={alias_swa} and hw={self.hw} considered 'Unloaded net' on field : " + self.get_name())
            if alias_swa==Access.NA and self.hw==Access.NA:
                raise RuntimeError(
                    f"sw={alias_swa} and hw={self.hw} considered 'Non existent net' on field : " + self.get_name())
        if alias_swa.is_readable() and (self.swacc or self.early_swacc):
            if self.cdc_swacc != Cdc.NA and self.cdc_swacc != Cdc.HOST_CE:
                print (f"Warning: using cdc_swacc option with swacc may be not supported on {self.get_name()}:cdc_swacc={self.cdc_swacc}")
                print (f"         Please double check the CDC on swacc signal on interface")
        if not self.sw.is_writable() and self.woclr:
            # woclr required sw write access to be active
            raise RuntimeError(
                f"woclr option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.woset:
            # woset required sw write access to be active
            raise RuntimeError(
                f"woset option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.wot:
            # wot required sw write access to be active
            raise RuntimeError(
                f"wot option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.wzc:
            # wzc required sw write access to be activate
            raise RuntimeError(
                f"wzc option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.wzs:
            # wzs required sw write access to be activate
            raise RuntimeError(
                f"wzs option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.wzt:
            # wzt required sw write access to be activate
            raise RuntimeError(
                f"wzt option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if not self.sw.is_writable() and self.warl:
            # warl required sw write access to be activated
            raise RuntimeError(
                f"warl option requires software write access (sw=Access.*W) on field : " + self.get_name())
        if self.warl:
            # warl parameter consistency checks
            if not type(self.warl) in (list, tuple):
                raise RuntimeError(
                    f"warl option requires list of value to be provided on field : {self.get_name()},"
                    f" found {self.warl}, type {type(self.warl)}")
            # check each value of warl looks good
            for warl_value in self.warl:
                if warl_value.bit_length() > self.bit_width:
                    raise RuntimeError(
                        f"warl value require too many bits for {warl_value} on field : {self.get_name()}")
            # check warl contains reset value
            # if on a source alias, find out the target and process from there
            if self.alias:
                # field is src alias, find target to call get_alias_ored_attr or this target
                target_entry, target_field_name = map_table_entry.resolve_field_path(self.alias)
                target_field = target_entry.register.get_field_by_name(target_field_name)
            else:
                # either target alias or no alias at all
                target_entry = map_table_entry
                target_field = self
            if not (target_field.get_reset_value(target_entry) in self.warl):
                raise RuntimeError(
                    f"Storage field reset value ={target_field.get_reset_value(target_entry)}"
                    f" is not a legal warl value ={self.warl} on field : {self.get_name()}")
        if self.alias and self.wonce:
            # wonce required on field primary, not on alias
            raise RuntimeError(
                f"wonce option required to be on primary field, not on aliased field. Found on field : " + self.get_name())
        if not alias_swa.is_writable() and self.wonce:
            # wonce required sw write access to be activate
            raise RuntimeError(
                f"wonce option requires software write access (aliased sw=Access.*W) on field : " + self.get_name())
        if self.wonce and self.reset==None:
            # wonce required reset support
            raise RuntimeError(
                f"wonce option requires to have reset on field  (reset needed to detect first write consistently): " + self.get_name())
        # check swwr2hwif have write option to export interface
        if self.swwr2hwif:
            if not self.has_storage(aliased=True, map_table_entry=map_table_entry) or \
                    not(self.sw.is_writable() or self.rclr or self.rset):
                # asked to export write field info to hardware interface, but not such info is available
                raise RuntimeError(
                    f"swwr2hwif asked to export write field info to hardware interface, but field not standard: " + self.get_name())


        if self.hw.is_writable() and self.has_storage(aliased=True, map_table_entry=map_table_entry):
            if self.cdc_in != Cdc.NA and self.cdc_in != Cdc.HOST_CE:
                # writable storage have hwwe, this is not supported in full_async mode
                print (f"Warning: using cdc_in option with hwwe field may be not supported on {self.get_name()}:cdc_in={self.cdc_in}")
                print (f"         Please double check the CDC on hwwe signal on interface")
        if self.hwset or self.hwclr:
            if self.cdc_in != Cdc.NA and self.cdc_in != Cdc.HOST_CE:
                # hw writable storage have hwset/hwclr, this is not supported in full_async mode
                print (f"Warning: using cdc_in option with hwset/hwclr field may be not supported on {self.get_name()}:cdc_in={self.cdc_in}")
                print (f"         Please double check the CDC on hwset/hwclr signal on interface")
        # the config of singlepulse with hwce require woset
        # (check postmap, as cannot be check premap because hwce_signal only set at end of register validate_check_premap)
        if self.singlepulse and not self.woset and self.hwce_signal != None:
            print (f"Warning: single pulse and hwce without woset detected on {self.get_name()}")
            print (f"         You probably have to have field.woset=True to avoid quick reset on singlepulse")
            print (f"         Please double check the field woset is correct")
# ----------------------------------------------------------------------------

    def to_verilog_external(self, map_table_entry):
        reg_name = map_table_entry.get_full_hw_name()
        fld_hw_name = self.get_full_hw_name(map_table_entry)

        verilog = ""
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=(f"// External Field in bit range of register : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")

        # field value to be read in register is field_name signal
        # generic name used to connect inside register (field either internal or external)
        verilog += f"logic [{self.bit_width - 1}:0] {fld_hw_name};\n"
        # field value to be read in register is assigned in to_verilog_connect_interface
        if not self.sw.is_readable():
            # field is non readable, assign a default 0 value to replace non available external value
            verilog += f"assign {fld_hw_name} = 0; // external non readable is tied to 0\n"

        if self.sw.is_writable():
            # signal for wdata at verilog implementation level (translate bit position in register to [*:0])
            verilog += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_extwdata;\n"
            verilog += f"assign {fld_hw_name}_extwdata = sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}];\n"
            # (interface will connect directly to sw_wrdata)
            verilog += f"logic {fld_hw_name}_extwr;\n"
            verilog += f"assign {fld_hw_name}_extwr = {reg_name}_r_swwr;\n"
            if self.support_hwbe:
                # _wr_bmask provided as bitmask to external field
                verilog += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_extwr_bmask;\n"
                verilog += f"assign {fld_hw_name}_extwr_bmask = host_access_wr_bmask[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}];\n"

        # extready handling
        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have either read or write ready option
            verilog += f"logic {fld_hw_name}_extready_from_if; // signal connected from interface\n"
            verilog += f"logic {fld_hw_name}_extready; // demuxed for applicable read/write\n"
            # Need demux for read or write cycle
            verilog += f"always_comb begin : proc_{fld_hw_name}_extready\n"
            verilog += f"    {fld_hw_name}_extready = 1; // default\n"
            if self.extready_rd:
                # force ready to 0 if !_extready && _extrd
                verilog += f"    if({reg_name}_r_swrd && !{fld_hw_name}_extready_from_if)\n"
                verilog += f"        {fld_hw_name}_extready = 0;\n"
            if self.extready_wr:
                # force ready to 0 if !_extready && _extwr
                verilog += f"    if({fld_hw_name}_extwr && !{fld_hw_name}_extready_from_if)\n"
                verilog += f"        {fld_hw_name}_extready = 0;\n"
            verilog += f"end\n"

        verilog += f"logic {fld_hw_name}_ready;\n" # ready either on read or write
        if self.hwce_signal != None:
            hwce_condition = f"{self.hwce_signal.name}"
        else:
            hwce_condition = "1"
        verilog += f"always_comb begin : proc_{fld_hw_name}_ready\n"
        verilog += f"    {fld_hw_name}_ready = 1; // ready unless found condition for not ready\n"
        # external r/w get ready condition from external, + may be mask with !hwce
        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            verilog += f"    {fld_hw_name}_ready = {fld_hw_name}_ready && {hwce_condition} && {fld_hw_name}_extready; // external ready\n"
        if (self.sw.is_readable() and not self.extready_rd) or \
           (self.sw.is_writable() and not self.extready_wr):
            verilog += f"    {fld_hw_name}_ready = {fld_hw_name}_ready && {hwce_condition}; // no extready, masked only with hwce\n"
        verilog += f"end\n"

        verilog += "\n"
        return verilog


    # // Typical storage implementation with most options:
    # logic [N:0] field; // current flop storage state
    # //
    # // hw related signals
    # logic [N:0] field_next; // next data from hardware
    # logic [N:0] field_hwset; // hwset from hardware (bitwise ored)
    # logic [N:0] field_hwclr; // hwclr from hardware (bitwise anded)
    # logic field_hwwe;
    # logic [N:0] field_hwnext; // next value for storage coming from hw
    # //
    # // sw related signals
    # // cpu wr data bus comes from sw_wrdata[N+L:L]
    # logic field_swwr; // wr pulse from cpu
    # logic field_swrd; // rd pulse from cpu
    # logic [N:0] field_swnext; // next value for storage coming from sw
    # // woclr, wzc is option to alter swwr effect
    # // woset, wzs is option to alter swwr effect
    # // wot, wzt is option to alter swwr effect
    # // wclr is option to alter swwr effect
    # // wset is option to alter swwr effect
    # //
    # // flop related signals
    # logic [N:0] field_ffnext;// next value for storage coming from either hwnext or swnext
    # // singlepulse activate auto return to 0 of flop signal the cycle after set
    # //
    # always_comb begin : proc_field_hwnext
    #     field_hwnext = field; // default
    #     // all 3 options are sequenced
    #     field_hwnext = (field_hwwe) ? field_next : field_hwnext; // normal hw write (trig by field_hwwe)
    #     field_hwnext = field_hwnext | field_hwset; // hwset
    #     field_hwnext = field_hwnext & ~field_hwclr; // hwclr
    # end
    # always_comb begin : proc_field_swnext
    #     // 3 exclusive options ...
    #     field_swnext = sw_wrdata[N+L:L]; // normal sw write : field_swwr
    #         field_swnext = 0; // rclr && field_swrd
    #         field_swnext = '1; // rset && field_swrd
    #     field_swnext = field | sw_wrdata[N+L:L]; // woset
    #     field_swnext = field & ~sw_wrdata[N+L:L]; // woclr
    #     field_swnext = field | ~sw_wrdata[N+L:L]; // wzs
    #     field_swnext = field & sw_wrdata[N+L:L]; // wzc
    #     field_swnext = '1; // wset
    #     field_swnext = 0; // wclr
    #     field_swnext = field ^ sw_wrdata[N+L:L]; // wot : toggle
    #     field_swnext = field ^ ~sw_wrdata[N+L:L]; // wzt : toggle
    # end
    # assign field_swwr = register_r_swwr; // field swwr comes from register level
    # always_comb begin : proc_field_ffnext
    #     // Default keep flop data unchanged (no sw wr, no hw write)
    #     field_ffnext = field;
    #     if(field_swwr) begin
    #         // data modification from SW
    #         field_ffnext = field_swnext;
    #     end else if (field_hwwe) begin
    #         // write data from HW
    #         field_ffnext = field_next;
    #     end
    # end
    # always_ff @(posedge clk or negedge rst_n) begin : proc_field
    #     if(~rst_n) begin
    #         field <= 0;
    #     end else begin
    #         field <= 0; // singlepulse auto reset to 0
    #         // Clock gate the flop write only if sw wr or hw wr
    #         if (field_swwr|| field_hwwe) begin
    #             field <= field_ffnext;
    #         end
    #     end
    # end


    def to_verilog_internal_sw_access(self, map_table_entry):
        """
            Generate verilog code to implement field software access to core
            (this includes signal definition, sw read/write signals, and connection to storage core)
            (this exclude the storage core signal declaration and connection : see to_verilog_internal_storage)
            [CORE] <-> field signal <-> [CDC layer] <-> field_cdcif signal <-> [hw IF]
            Generation is dependant on alias/shadow setting for register
            sw_access is generated for normal field and alias field, skipped for shadow register
                (implement the sw write section to field storage, ...)
            Storage is generated only for non alias field (storage, interface connection)
            (to_verilog_internal_storage method is not called for an alias field)
            Shadow register has usual sw access
            (shadow storage difference is just extra write path from primary shadowed field sw_access)
                      |Normal|Alias |Shadow|
            ----------+------+------+------+
            sw access | YES  | YES  | YES  |
            storage   | YES  | NO   | YES  |
            output signal declared and setup are:
            - {fld_hw_name}_swnext
            - {fld_hw_name}_swrd
            - {fld_hw_name}_swwr
            - {fld_hw_name}_swwr_bmask
        """
        reg_name = map_table_entry.get_full_hw_name()
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        verilog_declaration = ""
        verilog = ""
        verilog_declaration += f"// signal declaration for: sw_access layer for field : {fld_hw_name}\n"
        verilog += f"// sw_access layer for field : {fld_hw_name}\n"

        if self.sw.is_readable():
            verilog_declaration += f"logic {fld_hw_name}_swrd;\n"
            verilog_declaration += f"logic {fld_hw_name}_swrd_early;\n"
            verilog += f"assign {fld_hw_name}_swrd = {reg_name}_r_swrd && {reg_name}_r_ready; // field read comes from register read\n"
            verilog += f"assign {fld_hw_name}_swrd_early = {reg_name}_r_swrd_early; // field early read (one pulse before ready is available)\n"
            verilog += f"\n"

        # sw access generation depend on aliased has_storage
        # but only primary (=not aliased) sw, rclr, rset used to tune the access connection
        if self.has_storage(aliased=True, map_table_entry=map_table_entry):
            if self.sw.is_writable() or self.rclr or self.rset:
                verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_swnext;\n"
                verilog_declaration += f"logic        {fld_hw_name}_swwr;\n"
                verilog += f"assign {fld_hw_name}_swwr = {reg_name}_r_swwr && {reg_name}_r_ready;\n"
                # _wr_bmask provided as bitmask to storage field
                verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_swwr_bmask;\n"
                # swnext, swwr_bmask generation
                verilog += f"always_comb begin : proc_{fld_hw_name}_swnext\n"
                verilog += f"    {fld_hw_name}_swnext = 0; // default to 0\n"
                if self.support_hwbe:
                    verilog += f"    {fld_hw_name}_swwr_bmask = host_access_wr_bmask[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // support_hwbe : mask from byte strobe\n"
                else:
                    verilog += f"    {fld_hw_name}_swwr_bmask = '1; // No hw byte enable, all bit enable for write\n"
                if self.sw.is_writable() and \
                        not self.woset and not self.woclr and not self.wot and \
                        not self.wzs and not self.wzc and not self.wzt and \
                        not self.wset and not self.wclr:
                    verilog += f"    if({fld_hw_name}_swwr) begin\n"
                    verilog += f"        {fld_hw_name}_swnext = sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // normal sw write\n"
                    if self.warl:
                        # sw access is warl, check if illegal value, and cast to default legal : self.warl[0]
                        warl_illegal_cond = " &&\n            ".join([f"({fld_hw_name}_swnext!={val})" for val in self.warl])

                        verilog += f"        if ( // check for warl legal\n"
                        verilog += f"            {warl_illegal_cond}\n"
                        verilog += f"        ) {fld_hw_name}_swnext = {self.warl[0]}; // convert illegal to first legal in warl\n"
                    verilog += f"    end\n"
                if self.sw.is_writable() and self.woset:
                    verilog += f"    {fld_hw_name}_swnext = '1; // woset : write 1 in storage\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // write 1 only if wrdata is 1\n"
                if self.sw.is_writable() and self.woclr:
                    verilog += f"    {fld_hw_name}_swnext = 0; // woclr : write 0 in storage\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // write 0 only if wrdata is 1\n"
                if self.sw.is_writable() and self.wot:
                    verilog += f"    {fld_hw_name}_swnext = {fld_hw_name} ^ sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wot\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // toggle only if wrdata is 1\n"
                if self.sw.is_writable() and self.wzs:
                    verilog += f"    {fld_hw_name}_swnext = '1; // wzset : write 1 in storage\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // write 1 only if wrdata is 0\n"
                if self.sw.is_writable() and self.wzc:
                    verilog += f"    {fld_hw_name}_swnext = 0; // wzc : write 0 in storage\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // write 0 only if wrdata is 0\n"
                if self.sw.is_writable() and self.wzt:
                    verilog += f"    {fld_hw_name}_swnext = {fld_hw_name} ^ ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // wzt\n"
                    verilog += f"    {fld_hw_name}_swwr_bmask &= ~sw_wrdata[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // toggle only if wrdata is 0\n"
                if self.sw.is_writable() and self.wset:
                    verilog += f"    {fld_hw_name}_swnext = '1; // wset\n"
                    # bmask not masked, all bit affected (only hwbe masked)
                if self.sw.is_writable() and self.wclr:
                    verilog += f"    {fld_hw_name}_swnext = 0; // wclr\n"
                    # bmask not masked, all bit affected (only hwbe masked)
                if (self.rclr):
                    verilog += f"    if({fld_hw_name}_swrd) {fld_hw_name}_swnext = 0; // rclr\n"
                    # all bit affected (no _swwr_bmask, it is a read, not write)
                if (self.rset):
                    verilog += f"    if({fld_hw_name}_swrd) {fld_hw_name}_swnext = '1; // rset\n"
                    # all bit affected (no _swwr_bmask, it is a read, not write)
                verilog += f"end\n"
        else: # end if self.has_storage():
            # No storage, special case of swmod (needed for read only register)
            # Read-only register but swmod need the field _swwr, _swwr_condition and _ffbmask signals
            # Generate a simpliflied definition for this swmod case on no storage
            if self.swmod or self.delayed_swmod:
                verilog_declaration += f"logic {fld_hw_name}_swwr;\n"
                verilog_declaration += f"logic {fld_hw_name}_swwr_condition;\n"
                verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_ffbmask;\n"
                verilog += f"assign {fld_hw_name}_swwr = {reg_name}_r_swwr && {reg_name}_r_ready;\n"
                verilog += f"assign {fld_hw_name}_swwr_condition = {fld_hw_name}_swwr;\n"
                if self.support_hwbe:
                    verilog += f"assign {fld_hw_name}_ffbmask = host_access_wr_bmask[{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}]; // support_hwbe : mask from byte strobe\n"
                else:
                    verilog += f"assign {fld_hw_name}_ffbmask = '1; // No hw byte enable, all bit enable for write\n"

        verilog_declaration += "\n"
        verilog += "\n"
        return (verilog_declaration, verilog)

    def to_verilog_internal_storage(self, map_table_entry):
        """
            Generate verilog code to implement field core
            (this includes signal definition, storage logic if any (either latch of flop))
            signal declaration to interface connection through CDC layer
            (this exclude the software access signal declaration and connection : see to_verilog_internal_sw_access)
            (now add as well the declaration of cdc signal on hw interface side :
            assign connection or CDC function implemented in CDC layer)
            [CORE] <-> field signal <-> [CDC layer] <-> field_cdcif signal <-> [hw IF]
            Generation is dependant on alias/shadow setting for register
            sw_access is generated for normal field and alias field, skipped for shadow register
                (implement the sw write section to field storage, ...)
            Storage is generated only for non alias field (storage, interface connection)
            (to_verilog_internal_storage method is not called for an alias field)
            Shadow register has usual sw access
            (shadow storage difference is just extra write path from primary shadowed field sw_access)
                      |Normal|Alias |Shadow|
            ----------+------+------+------+
            sw access | YES  | YES  | YES  |
            storage   | YES  | NO   | YES  |
            input signal already declared and setup are:
            - {fld_hw_name}_swnext
            - {fld_hw_name}_swrd
            - {fld_hw_name}_swwr
            - {fld_hw_name}_swwr_bmask
        """
        reg_name = map_table_entry.get_full_hw_name()
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        verilog_declaration = ""
        verilog = ""

        # sw access mode is to be checked versus the aliased/shadow fields view in mode usage below
        alias_swa  = self.get_alias_ored_attr(attr='sw',   aliased=True, map_table_entry=map_table_entry)
        alias_rclr = self.get_alias_ored_attr(attr='rclr', aliased=True, map_table_entry=map_table_entry)
        alias_rset = self.get_alias_ored_attr(attr='rset', aliased=True, map_table_entry=map_table_entry)

        # --------------------------------------------------------------------

        # in some side effect of field access, wait state can be added at register level
        # (hwce based field on slow hardware will postpone access to full register)
        # {reg_name}_r_ready is to be used to delay cycle on the field

        # hwce is not active if a slow hw is not available on every clock of the rif
        # the hwce signal is use to mask the field_ready condition to pause the host bus
        # This is needed only if the hw have special signal used (swmod, swacc)
        # a write to internal storage without swmod or swacc is not pausing the bus
        # hw change of storage (hwwe, hwset, hwclr) is also masked by hwce
        if self.hwce_signal != None:
            hwce_condition = f"{self.hwce_signal.name}"
            hwce_and_condition = f" && {self.hwce_signal.name}"
        else:
            hwce_condition = "1"
            hwce_and_condition = ""


        # verilog_signal_swnext_name = fld_hw_name + "_swnext"
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=(f"// Field in bit range of internal register : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")
        verilog_declaration += f"logic [{self.bit_width-1}:0] {fld_hw_name};\n"
        verilog_declaration += f"logic [{self.bit_width-1}:0] {fld_hw_name}_cdcif;\n"
        # Create parameter value for reset (create signal even if not reset used)
        # No storage can also use init value in tie to constant mode
        verilog_declaration += f"localparam [{self.bit_width}-1:0] {fld_hw_name}_rst_value = {self.bit_width}'h{(self.get_reset_value(map_table_entry)):0x}; // reset value for storage\n"

        if self.hw.is_writable() and self.has_storage(aliased=True, map_table_entry=map_table_entry):
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_next;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_next_cdcif;\n"
            verilog_declaration += f"logic {fld_hw_name}_hwwe;\n"
            verilog_declaration += f"logic {fld_hw_name}_hwwe_cdcif;\n"
        if self.hw.is_writable() and not self.has_storage(aliased=True, map_table_entry=map_table_entry):
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_next;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_next_cdcif;\n"
        if alias_swa.is_writable() or alias_rclr or alias_rset:
            if self.swwe or self.swwel: # hardware enabling field modification
                verilog_declaration += f"logic {fld_hw_name}_swwe;\n"
                verilog_declaration += f"logic {fld_hw_name}_swwe_cdcif;\n"
        if (self.has_storage(aliased=True, map_table_entry=map_table_entry) and self.hw.is_writable()) or self.hwset or self.hwclr:
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwnext;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwbmask;\n"
        if self.hwset:
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwset;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwset_cdcif;\n"
        if self.hwclr:
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwclr;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_hwclr_cdcif;\n"
        if self.has_storage(aliased=True, map_table_entry=map_table_entry):
            verilog_declaration += f"// Field have storage\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_ffnext;\n"
            verilog_declaration += f"logic [{self.bit_width - 1}:0] {fld_hw_name}_ffbmask; // for byte enable support\n"
            if self.hw.is_writable() or self.hwset or self.hwclr:
                # hwnext generation
                verilog += f"always_comb begin : proc_{fld_hw_name}_hwnext\n"
                verilog += f"    {fld_hw_name}_hwbmask = 0; // default to no bit changed, explicit bmasked ored below if used\n"
                if self.latch:
                    # Avoid all form of feedback in case of latch (latch and hwset/hwclr is not valid combination)
                    verilog += f"    {fld_hw_name}_hwnext = 0; // default to 0\n"
                else:
                    verilog += f"    {fld_hw_name}_hwnext = {fld_hw_name}; // default\n"
                # mux the hwwe if any
                if self.hw.is_writable():
                    verilog += f"    {fld_hw_name}_hwnext = ({fld_hw_name}_hwwe) ? {fld_hw_name}_next : {fld_hw_name}_hwnext; // normal hw write (trig by {fld_hw_name}_hwwe)\n"
                    verilog += f"    {fld_hw_name}_hwbmask = {{{self.bit_width}{{({fld_hw_name}_hwwe)}}}}; // hwwe control bmask\n"
                if (self.hwset):
                    verilog_hwset =  f"    {fld_hw_name}_hwnext = {fld_hw_name}_hwnext | {fld_hw_name}_hwset; // hwset\n"
                    verilog_hwset += f"    {fld_hw_name}_hwbmask |= {fld_hw_name}_hwset; // affect only bits with hwset\n"
                else:
                    verilog_hwset = ""
                if (self.hwclr):
                    verilog_hwclr =  f"    {fld_hw_name}_hwnext = {fld_hw_name}_hwnext & ~{fld_hw_name}_hwclr; // hwclr\n"
                    verilog_hwclr += f"    {fld_hw_name}_hwbmask |= {fld_hw_name}_hwclr; // affect only bits with hwclr\n"
                else:
                    verilog_hwclr = ""
                if self.hwset_have_precedence_over_hwclr:
                    # hwset get priority over hwclr, so hwset comes after to override hwclr
                    verilog += verilog_hwclr
                    verilog += verilog_hwset
                else:
                    # hwclr get priority over hwset, so hwclr comes after to override hwset
                    verilog += verilog_hwset
                    verilog += verilog_hwclr
                verilog += f"end\n"

            # create a swwr_condition signal for any field with storage (tie to 0 if not used)
            verilog_declaration += f"logic {fld_hw_name}_swwr_condition0; // before wonce filtering\n"
            verilog_declaration += f"logic {fld_hw_name}_swwr_condition; // after wonce filtering\n"
            verilog_declaration += f"logic {fld_hw_name}_wonce_locked;\n"

            # have extra flop to detect first write in case of wonce
            if self.wonce:
                verilog += f"// Add extra flop to track first write on a wonce field\n"
                verilog += f"// wonce_done share same reset as storage\n"
                # warl field have been check to have a reset and a legal reset value
                # normal flop based implementation with reset in sensitivity
                verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_wonce_locked\n"

                # No latch used for wonce locking
                verilog += f"    if({self.get_reset_high_name()}) begin\n"
                verilog += f"        {fld_hw_name}_wonce_locked <= 0;\n"
                verilog += f"    end else begin\n"
                verilog += f"        if({fld_hw_name}_swwr_condition0)\n"
                verilog += f"            {fld_hw_name}_wonce_locked <= 1;\n"
                verilog += f"    end\n"
                verilog += f"end\n"
            else:
                verilog += f"// no wonce used, no locked needed\n"
                verilog += f"assign {fld_hw_name}_wonce_locked = 0;\n"

            verilog_swwr = ""
            swwr_condition = []
            # get sw_access involved for current field, and for all alias field if any alias source exist
            # get all alias field using get_all_alias() that return array of dict : {"entry":entry, "field":f}
            # connect sw_access to storage for primary field and its aliases
            # for loop iterate on all alias, first iteration working on primary field (un-aliased)
            # and include also shadow that is alternate write path in sw access
            for alias_dict in self.get_all_alias_or_shadow(map_table_entry):
                fld_sw_access = alias_dict["entry"].register.get_field_by_name(alias_dict["field"].name)
                entry_sw_access = alias_dict["entry"]
                #print (f"   int storage({self.name}) => alias item={fld_sw_access.name}/{entry_sw_access.register.name}")
                if fld_sw_access==self:
                    verilog_swwr += f"    // swwr for field '{self.name}'\n"
                else:
                    verilog_swwr += f"    // swwr for field '{self.name}' from alias '{fld_sw_access.name}'\n"

                fld_aliased_name = fld_sw_access.get_full_hw_name(entry_sw_access)

                if fld_sw_access.sw.is_writable(): # either swwr or woset or woclr or wot or
                                          # wzs or wzc or wzt or wset or wclr
                    verilog_swwr += f"    if({fld_aliased_name}_swwr"
                    if self.swwe or self.swwel:
                        verilog_swwr += f" && {fld_hw_name}_swwe"
                    verilog_swwr +=                         f") begin\n"
                    verilog_swwr += f"        // data modification from SW write\n"
                    # next data is coming from : (allowed bit level precedence, and parallel hange of bits)
                    # current ffnext if _swwr_bmask==0 (unchanged)
                    # current hwnext if _swwr_bmask==1 (changed)
                    verilog_swwr += f"        {fld_hw_name}_ffnext   = ({fld_aliased_name}_swnext & {fld_aliased_name}_swwr_bmask) |  ({fld_hw_name}_ffnext & ~{fld_aliased_name}_swwr_bmask);\n"
                    verilog_swwr += f"        {fld_hw_name}_ffbmask |= {fld_aliased_name}_swwr_bmask;\n"
                    verilog_swwr += f"    end\n"
                if fld_sw_access.rclr or fld_sw_access.rset:
                    verilog_swwr += f"    if({fld_aliased_name}_swrd"
                    if self.swwe or self.swwel:
                        verilog_swwr += f" && {fld_hw_name}_swwe"
                    verilog_swwr +=                         f") begin\n"
                    verilog_swwr += f"        // data modification from side effect read : rclr/rset\n"
                    verilog_swwr += f"        {fld_hw_name}_ffnext = {fld_aliased_name}_swnext;\n"
                    # ffbmask is not masking (rclr/rset read side effect if full bitwidth bit applied) (no _swwr_bmask available on read)
                    verilog_swwr += f"        {fld_hw_name}_ffbmask = '1;\n"
                    verilog_swwr += f"    end\n"

                if fld_sw_access.sw.is_writable(): # either swwr or woset or woclr or wot or
                                          # wzs or wzc or wzt or wset or wclr
                    if self.swwe or self.swwel:
                        swwr_condition.append(f"({fld_aliased_name}_swwr && {fld_hw_name}_swwe)")
                    else:
                        swwr_condition.append(f"{fld_aliased_name}_swwr")
                if fld_sw_access.rclr or fld_sw_access.rset:
                    if self.swwe or self.swwel:
                        swwr_condition.append(f"({fld_aliased_name}_swrd && {fld_hw_name}_swwe)")
                    else:
                        swwr_condition.append(f"{fld_aliased_name}_swrd")
            swwr_condition = " ||\n                ".join(swwr_condition)
            verilog += f"assign {fld_hw_name}_swwr_condition0 = "
            if swwr_condition != '':
                verilog += f"{swwr_condition};\n"
            else:
                verilog += "0;\n"
            verilog += f"assign {fld_hw_name}_swwr_condition = {fld_hw_name}_swwr_condition0 && !{fld_hw_name}_wonce_locked;\n"

            hwwr_condition = []
            if self.hw.is_writable():
                hwwr_condition.append(f"{fld_hw_name}_hwwe")
            if self.hwset:
                hwwr_condition.append(f"|{fld_hw_name}_hwset")
            if self.hwclr:
                hwwr_condition.append(f"|{fld_hw_name}_hwclr")
            hwwr_condition = " || ".join(hwwr_condition)

            verilog_hwwr = "    // hwwr\n"
            if self.hw.is_writable() or self.hwset or self.hwclr:
                verilog_hwwr += f"    if({hwwr_condition}) begin\n"
                verilog_hwwr += f"        // write data from HW\n"
                # next data is coming from : (allowed bit level precedence, and parallel hange of bits)
                # current ffnext if hwbmask==0 (unchanged)
                # current hwnext if hwbmask==1 (changed)
                verilog_hwwr += f"        {fld_hw_name}_ffnext   = ({fld_hw_name}_hwnext & {fld_hw_name}_hwbmask) | ({fld_hw_name}_ffnext & ~{fld_hw_name}_hwbmask);\n"
                verilog_hwwr += f"        {fld_hw_name}_ffbmask |= {fld_hw_name}_hwbmask; // select bit to change from hw\n"
                verilog_hwwr += f"    end\n"


            # Generate the signal next for the field storage (ff)
            #     get either from swwr or hwwr, select order from precedence
            verilog += f"always_comb begin : proc_{fld_hw_name}_ffnext\n"
            verilog += f"    // Default to SW, not used if no sw wr or no hw write\n"
            # default value used have impact on gate count in latch case
            # So try to have default value matching the swwr case, this help synthesis to optimize
            # (no special treatment in case of aliased fields)
            if self.sw.is_writable() or self.rclr or self.rset:
                verilog += f"    {fld_hw_name}_ffnext = {fld_hw_name}_swnext; // default to SW logic, overload in hw write case\n"
                verilog += f"    {fld_hw_name}_ffbmask = {fld_hw_name}_swwr_bmask; // default to SW logic, overload in hw write case\n"
            else:
                verilog += f"    {fld_hw_name}_ffnext = 0; // No default to SW logic available tie to 0\n"
                verilog += f"    {fld_hw_name}_ffbmask = '0; // default to unchanged unless explicit change source found\n"
            if self.hw_have_wr_precedence_over_fw:
                # First fw then hw take precedence
                verilog += verilog_swwr
                verilog += verilog_hwwr
            else:
                # First hw then fw take precedence
                verilog += verilog_hwwr
                verilog += verilog_swwr
                #
            verilog += f"end\n"

            ff_condition = []
            if hwwr_condition != '':
                # mask the hwwr condition is hwce is not active
                ff_condition.append("(("+hwwr_condition+")"+hwce_and_condition+")")
            # hwce already included in swwr_condition
            ff_condition.append(f"{fld_hw_name}_swwr_condition")
            ff_condition = " ||\n                ".join(ff_condition)

            # Check error case for latch
            # must avoid any form of feedback from latch output to input
            if self.latch:
                if self.singlepulse:
                    raise RuntimeError(f"Unsupported singlepulse for latch based storage: {self.get_name()}")
                if self.hwclr or self.hwset:
                    raise RuntimeError(f"Unsupported hwset/hwclr for latch based storage: {self.get_name()}")
                if self.wot:
                    raise RuntimeError(f"Unsupported wot for latch based storage: {self.get_name()}")
                if self.wzt:
                    raise RuntimeError(f"Unsupported wzt for latch based storage: {self.get_name()}")
                if not self.silent_latch_usage:
                    print(f"Warning: using latch based storage for field: {self.get_name()}")

            if self.latch and self.latch_use_lib_ldce:
                # storage is generated with latch as LDCE/LDPE lib primitive
                verilog += f"// /!\\ Field requested for LATCH implementation using LDCE lib !!!\n"
                for ff_bit in range(self.bit_width):
                    verilog += f"// bit {fld_hw_name}_b{ff_bit}\n"
                    verilog += f"pyrift_latch_as_ldxe #(\n"
                    verilog += f"   .WIDTH(1),\n"
                    verilog += f"   .RESET_VALUE({fld_hw_name}_rst_value[{ff_bit}])\n"
                    verilog += f") i_pyrift_latch_as_ldxe_{fld_hw_name}_b{ff_bit} (\n"
                    if self.reset != None:
                        verilog += f"   .RESET({self.get_reset_high_name()}),\n"
                    else:
                        verilog += f"   .RESET(1'b0), // No reset for this field\n"
                    verilog += f"   .G(clk=={self.latch_transparent_state}), "\
                                       f"// /!\\ latch is transparent if clk is {self.latch_transparent_state} !!!\n"
                    verilog += f"   .GE(({ff_condition}) && {fld_hw_name}_ffbmask[{ff_bit}]), "\
                                       "// Clock gate the flop write only if sw wr or hw wr is active\n"
                    verilog += f"   .D({fld_hw_name}_ffnext[{ff_bit}]),\n"
                    verilog += f"   .Q({fld_hw_name}[{ff_bit}])\n"
                    verilog += f");\n"
            else:
                # storage is generated with verilog (either FF or latch)

                # Flop for the field
                if self.latch:
                    # Try to use latch for storage implementation
                    # could be better in IC for area implementation (/!\ put severe constraint on clocking)
                    verilog += f"// /!\\ Field requested for LATCH implementation !!!\n"
                    verilog += f"always_latch begin : proc_{fld_hw_name}\n"
                    # sensitivity list is * because ffnext, or many hwwr/swwr condition can trig action
                else:
                    if self.reset != None:
                        # normal flop based implementation with reset in sensitivity
                        verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}\n"
                    else:
                        # normal flop based implementation with NO reset
                        verilog += f"// This flop is requested to have NO reset feature\n"
                        verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}\n"

                # reset condition apply for both latch and flop case
                if self.reset != None:
                    verilog += f"    if({self.get_reset_high_name()}) begin\n"
                    verilog += f"        {fld_hw_name} <= {fld_hw_name}_rst_value;\n"
                    verilog += f"    end else\n"

                verilog += f"    begin\n"
                if self.singlepulse:
                    if self.hwce_signal != None:
                        verilog += f"        if({hwce_condition}) // singlepulse auto reset only if hwce\n"
                    else:
                        verilog += f"        // if(1) // No hw ce condition, all clock edge may reset pulse\n"
                    # if cdc_out is used (HOST_CE), the single pulse last until CDC use it
                    if self.cdc_out == Cdc.HOST_CE:
                        verilog += f"            if({self.cdc_out_signal.name}) {fld_hw_name} <= 0; // singlepulse auto reset to 0 after CDC use it\n"
                    else:
                        verilog += f"            {fld_hw_name} <= 0; // singlepulse auto reset to 0\n"

                verilog += f"        // Clock gate the flop write only if sw wr or hw wr is active\n"
                for ff_bit in range(self.bit_width):
                    verilog += f"        // bit {fld_hw_name}_b{ff_bit}\n"
                    if self.latch:
                        verilog += f"        // /!\\ latch is transparent if clk is {self.latch_transparent_state} !!!\n"
                        verilog += f"        if (({ff_condition}) && {fld_hw_name}_ffbmask[{ff_bit}] &&\n"
                        verilog += f"                (clk=={self.latch_transparent_state})) begin\n"
                    else:
                        # normal flop based implementation
                        verilog += f"        if (({ff_condition}) && {fld_hw_name}_ffbmask[{ff_bit}]) begin\n"
                    verilog += f"            {fld_hw_name}[{ff_bit}] <= {fld_hw_name}_ffnext[{ff_bit}];\n"
                    verilog += f"        end\n"

                verilog += f"    end\n"
                verilog += f"end\n"
        else: #  not self.has_storage()
            verilog += f"// Field have NO storage\n"
            if self.hw.is_writable():
                verilog += f"//No storage for the hw writable field : field = field_next in combinatorial\n"
                verilog += f"always_comb begin : proc_{fld_hw_name}\n"
                verilog += f"    {fld_hw_name} = {fld_hw_name}_next;\n"
                verilog += f"end\n"
            else:
                # No storage for non hardware writable field :
                # this is a "Wire/Bus – constant value" as of systemRdl
                verilog += f"//No storage for constant field : field = reset value in combinatorial\n"
                verilog += f"always_comb begin : proc_{fld_hw_name}\n"
                verilog += f"    {fld_hw_name} = {fld_hw_name}_rst_value;\n"
                verilog += f"end\n"

        if alias_swa.is_readable():
            # field or any alias have read access
            # create a swrd_condition signal for possible used to create swacc in cdc layer
            # Done for both swrd_condition and swrd_condition_early (in case of early_swacc)
            verilog_declaration += f"logic {fld_hw_name}_swrd_condition; // for swacc generation if any\n"
            verilog_declaration += f"logic {fld_hw_name}_swrd_condition_early; // for early_swacc generation if any\n"

            # iterate on self+all alias src and get any *_swrd. Result are ored to swrd_condition string
            swrd_condition = []
            swrd_condition_early = []
            # get sw_access involved for current field, and for all alias field if any alias source exist
            # get all alias field using get_all_alias() that return array of dict : {"entry":entry, "field":f}
            # check readable sw_access for primary field and its aliases
            # for loop iterate on all alias, first iteration working on primary field (un-aliased)
            # shadow do NOT contribute to read access (only write), so shadow are not considered, only alias
            # (shadow rclr/rset are already converted to write access in sw_access, and considered only write in shadow
            # host is read the primary field, not the shadow, so no swacc generated seems the best implementation (?))
            for alias_dict in self.get_all_alias(map_table_entry):
                fld_sw_access = alias_dict["entry"].register.get_field_by_name(alias_dict["field"].name)
                entry_sw_access = alias_dict["entry"]
                # print (f"   swrd_condition({self.name}) => alias item={fld_sw_access.name}/{entry_sw_access.register.name}")

                fld_aliased_name = fld_sw_access.get_full_hw_name(entry_sw_access)
                if fld_sw_access.sw.is_readable():
                    swrd_condition.append(f"{fld_aliased_name}_swrd")
                    swrd_condition_early.append(f"{fld_aliased_name}_swrd_early")
            swrd_condition = " ||\n    ".join(swrd_condition)
            swrd_condition_early = " ||\n    ".join(swrd_condition_early)

            verilog += f"assign {fld_hw_name}_swrd_condition =\n    "
            if swrd_condition != '':
                verilog += f"{swrd_condition};\n"
            else:
                verilog += "0;\n"
            verilog += f"assign {fld_hw_name}_swrd_condition_early =\n    "
            if swrd_condition_early != '':
                verilog += f"{swrd_condition_early};\n"
            else:
                verilog += "0;\n"

        verilog_declaration += f"logic {fld_hw_name}_ready;\n" # ready either on read or write

        verilog += f"always_comb begin : proc_{fld_hw_name}_ready\n"
        if(self.swmod or self.delayed_swmod or self.swacc or self.early_swacc):
            # wait state on host bus only needed is swmod or swacc is used
            verilog += f"    {fld_hw_name}_ready = {hwce_condition}; // swacc/swmod : ready is copy of hwce\n"
        else:
            verilog += f"    {fld_hw_name}_ready = 1; // no wait state without swmod or swacc\n"
        verilog += f"end\n"

        verilog_declaration += "\n"
        verilog += "\n"
        return (verilog_declaration, verilog)

    def to_verilog_internal_alias_storage(self, map_table_entry):
        """
            Generate verilog code to implement field core in case of alias field
            just need basic connection of {fld_hw_name} and {fld_hw_name}_ready
            to the target alias equivalent signals (sw_access handling write cycle, read cycle still needed)
        """
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        # field the target of current alias field
        target_entry, target_field_name = map_table_entry.resolve_field_path(self.alias)
        target_alias_name = target_entry.register.get_field_by_name(target_field_name).get_full_hw_name(target_entry)

        verilog_declaration = ""
        verilog_declaration += "// signal declaration for Alias storage\n"
        verilog_declaration += f"logic [{self.bit_width-1}:0] {fld_hw_name};\n"
        verilog_declaration += f"logic {fld_hw_name}_ready;\n" # ready either on read or write

        verilog = ""
        verilog += "// begin Alias storage wrapper (for read cycle access to target alias)\n"
        verilog += f"always_comb begin : proc_{fld_hw_name}\n"
        verilog += f"    {fld_hw_name} = {target_alias_name};\n"
        verilog += f"end\n"
        verilog += f"\n"
        verilog += f"always_comb begin : proc_{fld_hw_name}_ready\n"
        verilog += f"    {fld_hw_name}_ready = {target_alias_name}_ready;\n"
        verilog += f"end\n"
        verilog += "// end Alias storage wrapper\n"

        return (verilog_declaration, verilog)


    def to_verilog(self, map_table_entry):
        if self.external:
            verilog_declaration = ""
            verilog = self.to_verilog_external(map_table_entry)
        else:
            verilog_declaration = ""
            verilog = ""
            (swa_declaration, swa_verilog) = self.to_verilog_internal_sw_access(map_table_entry)
            verilog_declaration += swa_declaration
            verilog += swa_verilog
            if not self.alias:
                (int_declaration, int_verilog) = self.to_verilog_internal_storage(map_table_entry)
                verilog_declaration += int_declaration
                verilog += int_verilog
            else:
                # for alias case, no internal storage is actually implemented,
                # just need basic connection of {fld_hw_name} and {fld_hw_name}_ready
                # to the target alias equivalent signals (sw_access handling write cycle, read cycle still needed)
                (tmp_declaration, tmp_verilog) = self.to_verilog_internal_alias_storage(map_table_entry)
                verilog_declaration += tmp_declaration
                verilog += tmp_verilog

        return (verilog_declaration, verilog)

# ----------------------------------------------------------------------------

    def to_verilog_internal_cdc_layer(self, map_table_entry):
        """
            verilog implementation of cdc layer : CDC between CORE and hw interface
            [CORE] <-> field signal <-> [CDC layer] <-> field_cdcif signal <-> [hw IF]
            Check if CDC conversion is needed ? yes : generate CDC core, No just transparent assign
            Aggregation field are not considered here
            (CDC done on each field of aggregation, connect_interface will handle the specifc of aggregation)
        """
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        # get field type expression (single item for array) for verilog coding "logic", "logic [...]"
        if self.bit_width <= 1:
            fld_type = f"logic"
        else:
            # vector
            fld_type = f"logic [{self.bit_width-1}:0]"
        # sw access mode is to be checked versus the aliased fields view in mode usage below
        alias_swa  = self.get_alias_ored_attr(attr='sw',   aliased=True, map_table_entry=map_table_entry)
        alias_rclr = self.get_alias_ored_attr(attr='rclr', aliased=True, map_table_entry=map_table_entry)
        alias_rset = self.get_alias_ored_attr(attr='rset', aliased=True, map_table_entry=map_table_entry)

        verilog = ""
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=(f"// CDC layer for field : {fld_hw_name}\n")
        verilog +=("\n")

        if self.hw.is_readable():
            # {fld_hw_name}_cdcif already declared as verilog signal in to_verilog() when generating field rtl
            verilog += f"// fld_type of {fld_hw_name} is {fld_type}\n"
            if self.cdc_out == Cdc.NA:
                # Non cdc, cdc field named is falling back to direct field name without cdc
                verilog += f"assign {fld_hw_name}_cdcif = {fld_hw_name};\n"
            elif  self.cdc_out == Cdc.HOST_CE:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_out option on {fld_hw_name} requires a valid cdc_out_signal to resync output")
                verilog += f"// Cdc output is resync with flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_cdc_flop\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}_cdc_flop\n"
                # reset condition if needed
                if self.reset != None:
                    verilog += f"   if({self.get_reset_high_name()}) begin\n"
                    verilog += f"      // cdc reset value use same as field\n"
                    verilog += f"      {fld_hw_name}_cdcif <= {fld_hw_name}_rst_value;\n"
                    verilog += f"   end else\n"

                verilog += f"   if({self.cdc_out_signal.name}) begin\n"
                verilog += f"       {fld_hw_name}_cdcif <= {fld_hw_name};\n"
                verilog += f"   end\n"
                verilog += f"end\n"
            elif  self.cdc_out == Cdc.FULL_ASYNC:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"FULL_ASYNC cdc_out option on {fld_hw_name} requires a valid cdc_out_signal for resync clock of output")
                meta_name = fld_hw_name + "_meta"
                verilog += f"{fld_type} {meta_name}; // metastable first stage cdc resync signal declaration\n"
                verilog += f"// Cdc output is resync with 2 stage of flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge {self.cdc_out_signal.name}{self.get_reset_edge()}) begin : proc_{fld_hw_name}_fasync_flop\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge {self.cdc_out_signal.name}) begin : proc_{fld_hw_name}_fasync_flop\n"
                # reset condition if needed
                if self.reset != None:
                    verilog += f"   if({self.get_reset_high_name()}) begin\n"
                    verilog += f"      // cdc reset value use same as field\n"
                    verilog += f"      {meta_name} <= {fld_hw_name}_rst_value;\n"
                    verilog += f"      {fld_hw_name}_cdcif <= {fld_hw_name}_rst_value;\n"
                    verilog += f"   end else\n"
                verilog += f"   begin\n"
                verilog += f"      {meta_name} <= {fld_hw_name}; // first stage CDC flop\n"
                verilog += f"      {fld_hw_name}_cdcif <= {meta_name}; // second stage CDC flop\n"
                verilog += f"   end\n"
                verilog += f"end\n"
            else:
                raise RuntimeError(f"Unexpected Cdc_out option option on {fld_hw_name}:cdc_out={self.cdc_out}")



        # swmod cdc generation is not altered by aggregation
        if (self.swmod or self.delayed_swmod):
            # swmod_condition is re-using the {fld_hw_name}_swwr_condition build for internal storage
            # swmod_condition has a extra filtering layer according to _swwr_bmask (if write)
            # or read bmask='1 for rclr/rset' : this is merged into {fld_hw_name}_ffbmask
            swmod_condition = f"{fld_hw_name}_swwr_condition &&  |{fld_hw_name}_ffbmask";

            # signal are local to cdc layer
            verilog += f"logic {fld_hw_name}_swmod_nodelay;\n"
            verilog += f"logic {fld_hw_name}_swmod_cdcif;\n"
            verilog += f"assign {fld_hw_name}_swmod_nodelay = {swmod_condition};\n"

            if self.delayed_swmod:
                verilog += f"// delayed swmod output to interface.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_swmod_delay\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}_swmod_delay\n"
                # reset conditionif applicable
                if self.reset != None:
                    verilog += f"   if({self.get_reset_high_name()}) begin\n"
                    verilog += f"      {fld_hw_name}_swmod_cdcif <= 0;\n"
                    verilog += f"   end else\n"

                verilog += f"   begin\n"
                if self.hwce_signal != None:
                    # if hwce is in use, the clocking delay should be gated by hwce
                    verilog += f"      if({self.hwce_signal.name})\n"
                verilog += f"         {fld_hw_name}_swmod_cdcif <= {swmod_condition};\n"
                verilog += f"   end\n"
                verilog += f"end // always ff\n"
            elif self.cdc_swmod == Cdc.HOST_CE:
                if self.cdc_out_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_swmod option on {fld_hw_name}+swmod requires a valid cdc_out_signal to resync output")
                verilog += f"// HOST_CE cdc_swmod swmod output to interface.\n"
                verilog += f"logic {fld_hw_name}_swmod_pending; // record swmod until cdc CE\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_swmod_pending\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}_swmod_pending\n"
                # reset condition if applicable
                if self.reset != None:
                    verilog += f"   if({self.get_reset_high_name()}) begin\n"
                    verilog += f"      {fld_hw_name}_swmod_pending <= 0;\n"
                    verilog += f"      {fld_hw_name}_swmod_cdcif <= 0;\n"
                    verilog += f"   end else\n"
                verilog += f"   begin\n"
                verilog += f"      if({fld_hw_name}_swmod_nodelay)\n" # precedence is to re-arm pending rather than reset
                verilog += f"           {fld_hw_name}_swmod_pending <= 1;\n"
                verilog += f"      else if({self.cdc_out_signal.name})\n"
                verilog += f"           {fld_hw_name}_swmod_pending <= 0;\n"
                verilog += f"      if({self.cdc_out_signal.name})\n"
                verilog += f"         {fld_hw_name}_swmod_cdcif <= {fld_hw_name}_swmod_pending;\n"
                verilog += f"   end\n"
                verilog += f"end\n"
            else:
                # if hwce is in use, the swmod_condition already include the gating
                verilog += f"assign {fld_hw_name}_swmod_cdcif = {fld_hw_name}_swmod_nodelay;\n"

        # swacc cdc generation is not altered by aggregation
        if (self.swacc or self.early_swacc):
            verilog += f"logic {fld_hw_name}_swacc_cdcif;\n"
            # Generation of swacc from all aliased field
            # (already collected aliased into {fld_hw_name}_swrd_condition).
            if(alias_swa.is_readable()):
                if self.cdc_swacc == Cdc.HOST_CE:
                    if self.cdc_out_signal == None:
                        raise RuntimeError(f"HOST_CE cdc_swacc option on {fld_hw_name}+swacc requires a valid cdc_out_signal to resync output")
                    verilog += f"// HOST_CE cdc_swacc swacc output to interface.\n"
                    verilog += f"logic {fld_hw_name}_swacc_pending; // record swacc until cdc CE\n"
                    if self.reset != None:
                        # normal flop based implementation with reset in sensitivity
                        verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_swacc_pending\n"
                    else:
                        # normal flop based implementation with NO reset
                        verilog += f"// This flop is requested to have NO reset feature\n"
                        verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}_swacc_pending\n"
                    # reset condition if applicable
                    if self.reset != None:
                        verilog += f"   if({self.get_reset_high_name()}) begin\n"
                        verilog += f"      {fld_hw_name}_swacc_pending <= 0;\n"
                        verilog += f"      {fld_hw_name}_swacc_cdcif <= 0;\n"
                        verilog += f"   end else\n"
                    verilog += f"   begin\n"
                    if self.early_swacc:
                        verilog += f"      if({fld_hw_name}_swrd_condition_early)\n" # precedence is to re-arm pending rather than reset
                    else:
                        verilog += f"      if({fld_hw_name}_swrd_condition)\n" # precedence is to re-arm pending rather than reset
                    verilog += f"           {fld_hw_name}_swacc_pending <= 1;\n"
                    verilog += f"      else if({self.cdc_out_signal.name})\n"
                    verilog += f"           {fld_hw_name}_swacc_pending <= 0;\n"
                    verilog += f"      if({self.cdc_out_signal.name})\n"
                    verilog += f"         {fld_hw_name}_swacc_cdcif <= {fld_hw_name}_swacc_pending;\n"
                    verilog += f"   end\n"
                    verilog += f"end\n"
                else:
                    # if hwce is in use, the _swrd already include the gating
                    if self.early_swacc:
                        verilog += f"assign {fld_hw_name}_swacc_cdcif = {fld_hw_name}_swrd_condition_early; // access reported on any sw read\n"
                    else:
                        verilog += f"assign {fld_hw_name}_swacc_cdcif = {fld_hw_name}_swrd_condition; // access reported on any sw read\n"
            else:
                verilog += f"assign {fld_hw_name}_swacc_cdcif = 0; // swacc for non readable field\n"

        if self.hw.is_writable():
            verilog += f"// fld_type of {fld_hw_name} is {fld_type}\n"

            if self.cdc_in == Cdc.NA or (self.cdc_in == Cdc.HOST_CE and\
                   self.has_storage(aliased=True, map_table_entry=map_table_entry)):
                # Non cdc, _next is assign as normal signal directly from interface
                # In storage case, only the hwwe pulse are gated, data are comb assigned directly
                verilog += f"assign {fld_hw_name}_next = {fld_hw_name}_next_cdcif;\n"
            elif self.cdc_in == Cdc.HOST_CE and not self.has_storage(aliased=True, map_table_entry=map_table_entry):
                # HOST_CE and no storage, resync the data with single flop
                if self.cdc_in_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_in option on {fld_hw_name} requires a valid cdc_in_signal to resync input")

                verilog += f"// Cdc input is resync with flop on clk. This uses same reset as field if any\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge clk{self.get_reset_edge()})\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge clk)\n"
                # reset condition if applicable
                if self.reset != None:
                    verilog += f"    if({self.get_reset_high_name()})\n"
                    verilog += f"         {fld_hw_name}_next <= 0; // reset to 0 the cdc in flop \n"
                    verilog += f"    else\n"
                verilog += f"    if({self.cdc_in_signal.name})\n"
                verilog += f"         {fld_hw_name}_next <= {fld_hw_name}_next_cdcif;\n"
            elif  self.cdc_in == Cdc.FULL_ASYNC:
                if self.cdc_in_signal != None:
                    raise RuntimeError(f"FULL_ASYNC cdc_in option on {fld_hw_name} provide unneeded cdc_in_signal input '{self.cdc_in_signal.name}'")
                if self.bit_width>1:
                    print(f"Warning: Detected CDC_IN as FULL_ASYNC on multibit for field: {self.get_name()}, transient inconsistent value may propagate to host bus /!\\")
                    print(f"         Dual flop prevent metastable to propagate, but you can double check this is as intended !")

                verilog += f"{fld_type} {fld_hw_name}_next_meta; // metastable first stage cdc resync signal declaration\n"

                verilog += f"// Cdc input is resync with 2 stage of flop.\n"
                if self.reset != None:
                    # normal flop based implementation with reset in sensitivity
                    verilog += f"always_ff @(posedge clk{self.get_reset_edge()}) begin : proc_{fld_hw_name}_cdcin_fasync\n"
                else:
                    # normal flop based implementation with NO reset
                    verilog += f"// This flop is requested to have NO reset feature\n"
                    verilog += f"always_ff @(posedge clk) begin : proc_{fld_hw_name}_cdcin_fasync\n"
                # reset condition apply for both latch and flop case
                if self.reset != None:
                    verilog += f"   if({self.get_reset_high_name()}) begin\n"
                    verilog += f"      {fld_hw_name}_next_meta <= 0;\n"
                    verilog += f"      {fld_hw_name}_next <= 0;\n"
                    verilog += f"   end else\n"

                verilog += f"   begin\n"
                verilog += f"      // first stage CDC flop\n"
                verilog += f"      {fld_hw_name}_next_meta <= {fld_hw_name}_next_cdcif;\n"
                verilog += f"      // second stage CDC flop\n"
                verilog += f"      {fld_hw_name}_next <= {fld_hw_name}_next_meta;\n"
                verilog += f"   end\n"
                verilog += f"end\n"
            else:
                raise RuntimeError(f"Unexpected Cdc_in option option on {fld_hw_name}:cdc_in={self.cdc_in}")

        if self.hw.is_writable() and self.has_storage(aliased=True, map_table_entry=map_table_entry):
            # hw writable and have storage
            if self.cdc_in == Cdc.NA:
                # Non cdc, _next is assign as normal signal directly from interface
                verilog += f"assign {fld_hw_name}_hwwe = {fld_hw_name}_hwwe_cdcif;\n"
            elif  self.cdc_in == Cdc.HOST_CE:
                if self.cdc_in_signal == None:
                    raise RuntimeError(f"HOST_CE cdc_in option on {fld_hw_name}_hwwe requires a valid cdc_in_signal to resync input")
                verilog += f"assign {fld_hw_name}_hwwe = {self.cdc_in_signal.name} && {fld_hw_name}_hwwe_cdcif; // HOST_CE CDC_IN gating\n"
            # elif  self.cdc_in == Cdc.FULL_ASYNC: # write pulse is not supported in FULL Async mode
            else:
                raise RuntimeError(f"Unexpected Cdc_in option option on {fld_hw_name}_hwwe: cdc_in={self.cdc_in}")

        if self.swwe and self.swwel:
            raise RuntimeError(f"Unexpected simultaneous options : swwe and swwel on {fld_hw_name}. "\
                               f"These are normally exclusive.")
        # hardware enabling field modification (swwe, swwel flavour will be handled in interface connect)
        # No CDC special support used on swwe
        if (alias_swa.is_writable() or alias_rclr or alias_rset) and (self.swwe or self.swwel):
            verilog += f"assign {fld_hw_name}_swwe = {fld_hw_name}_swwe_cdcif;\n"

        # hwset, hwclr get HOST_CE support (interface hwset/clr gets masking with external cdc_in_signal)
        if self.hwset:
            if self.cdc_in == Cdc.HOST_CE and self.cdc_in_signal == None:
                raise RuntimeError(f"HOST_CE cdc_in option on {fld_hw_name} with hwset requires a valid cdc_in_signal to mask input")

            if self.cdc_in == Cdc.HOST_CE:
                verilog += f"assign {fld_hw_name}_hwset = {{{self.bit_width}{{{self.cdc_in_signal.name}}}}} &"\
                                                      f" {fld_hw_name}_hwset_cdcif;\n"
            else:
                verilog += f"assign {fld_hw_name}_hwset = {fld_hw_name}_hwset_cdcif;\n"

        if self.hwclr:
            if self.cdc_in == Cdc.HOST_CE and self.cdc_in_signal == None:
                raise RuntimeError(f"HOST_CE cdc_in option on {fld_hw_name} with hwclr requires a valid cdc_in_signal to mask input")

            if self.cdc_in == Cdc.HOST_CE:
                verilog += f"assign {fld_hw_name}_hwclr = {{{self.bit_width}{{{self.cdc_in_signal.name}}}}} &"\
                                                      f" {fld_hw_name}_hwclr_cdcif;\n"
            else:
                verilog += f"assign {fld_hw_name}_hwclr = {fld_hw_name}_hwclr_cdcif;\n"

        verilog +=("\n")
        verilog +=(f"// END of CDC layer for field : {fld_hw_name}\n")
        verilog += f"// --------------------------------------------------------------\n"
        verilog +=("\n")
        return verilog

# ----------------------------------------------------------------------------

    # interface content in case the register is external
    # register is external:
    # interface if sw access is writable
    #   logic field_extwr; // wr pulse from cpu
    #   logic [N:0] field_extwdata; // data from cpu, valid on field_swwr
    # interface if sw access is readable
    # logic field_extrd; // rd pulse from cpu
    # logic [N:0] field_extrdata; // data to cpu, needed valid on field_swrd

    def to_verilog_interface_external(self, if_name, map_table_entry):
        # generate the verilog body of interface
        fld_name_in_if = self.get_name_in_if() # array_name_suffix='' real array in interface
        root_regmap = map_table_entry.get_root_entry().register
        array_range_definition = map_table_entry.get_array_range_definition()
        verilog = ""
        if_bit_width = self.bit_width

        # print(f"to_interface external {self.get_name()} reshape={self.if_reshape}")
        extra_dim = ""
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]
            if if_bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.get_name()}")
            if_bit_width //= reshape_size
            extra_dim = f"[{reshape_size-1}:0]"
            # print(f"to_interface external {self.get_name()} extra_dim={extra_dim} ifw={if_bit_width}")

        if self.encode:
            if self.sw.is_readable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name_in_if}_extrdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_extrdata",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
            if self.sw.is_writable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name_in_if}_extwdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}_extwdata",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
        elif(self.bit_width <= 1):
            if self.sw.is_readable():
                verilog += f"      logic {array_range_definition} {fld_name_in_if}_extrdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_extrdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.sw.is_writable():
                verilog += f"      logic {array_range_definition} {fld_name_in_if}_extwdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}_extwdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        else:
            # more than 1 bit in the field, reshape support even for external register
            if self.sw.is_readable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_extrdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_extrdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if self.sw.is_writable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_extwdata;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}_extwdata",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        if self.sw.is_writable() and self.support_hwbe:
            # wr_bmask, ignore encode option, just consider bit width
            if(self.bit_width <= 1):
                if_field_type = f"logic {array_range_definition}"
            else:
                if_field_type = f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
            verilog += f"      {if_field_type} {fld_name_in_if}_extwr_bmask; // hwbe supported\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_extwr_bmask",
                "if_name" : if_name,
                "type" : f"{if_field_type}"
            })

        if self.hw != Access.NA:
            raise RuntimeError(
                f"external register should have hw=Access.NA setup. Non consistent setup found in {self.get_name()} "\
                f"(unexpect hw={self.hw} found !?)")

        if self.sw.is_readable():
            # only external register with read access is interested in read pulse
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_extrd; // read pulse for readable external field\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_extrd",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.sw.is_writable():
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_extwr; // write pulse for writable external field\n\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_extwr",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })


        if self.extready_rd and not self.sw.is_readable():
            raise RuntimeError(
                f"'extready_rd' found without sw read access in {self.get_name()} "\
                f"(sw as Access.R or Access.RW expected !?)")
        if self.extready_wr and not self.sw.is_writable():
            raise RuntimeError(
                f"'extready_wr' found without sw write access in {self.get_name()} "\
                f"(sw as Access.W or Access.RW expected !?)")

        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have either read or write ready option
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_extready; // ready on _extrd/_extwr\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{fld_name_in_if}_extready",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })

        return verilog

    def to_verilog_interface_internal(self, if_name, map_table_entry):
        """\
            Generate field signal in verilog hw interface
            following signal are handled as single bit or vector[] root link shown
              ---------------------------------------------------------
              FIELD rtl level   # multi IF level (root)   # multi IF for slave
              ---------------------------------------------------------
            - field[:0]         |       root[:N]          |   Nothing
            - field_next[:0]    |       root_next[:N]     |   Nothing
            - field_hwset[:0]   |       root_hwset[:N]    |   Nothing
            - field_hwclr[:0]   |       root_hwclr[:N]    |   Nothing
            - field_hwwe        |       root_hwwe         |   Nothing
            - field_swmod       |       field_swmod       |   field_swmod
            - field_swacc       |       field_swacc       |   field_swacc
            if signal can be reshaped: .if_reshape = (2,) get [n-1:0][1:0]if_signal
            (from a 2*n packed field)
        """
        # generate the verilog body of interface
        fld_name_in_if = self.get_name_in_if() # array_name_suffix='' real array in interface
        root_regmap = map_table_entry.get_root_entry().register
        array_range_definition = map_table_entry.get_array_range_definition()
        if self.is_agregate_field():
            # aggregate field expected to be processed by other method
            # (RegisterMap.to_verilog_interface_internal_aggregate())
            raise RuntimeError("Unexpected aggregate field in to_verilog_interface_internal")
        verilog = ""
        if_bit_width = self.bit_width

        extra_dim = ""
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]
            if if_bit_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.get_name()}")
            if_bit_width //= reshape_size
            extra_dim = f"[{reshape_size-1}:0]"


        # --------------------------------------------------------------------
        # signal generation for internal field
        # verilog += f"// wr={self.hw.is_writable()}\n"
        if self.encode:
            if self.hw.is_readable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name_in_if};\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
            if self.hw.is_writable():
                verilog += f"      Tenum_{self.encode.__name__} {array_range_definition} {fld_name_in_if}_next;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_next",
                    "if_name" : if_name,
                    "type" : f"Tenum_{self.encode.__name__} {array_range_definition}"
                })
        elif(self.bit_width <= 1):
            if self.hw.is_readable():
                verilog += f"      logic {array_range_definition} {fld_name_in_if};\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.hw.is_writable():
                verilog += f"      logic {array_range_definition} {fld_name_in_if}_next;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_next",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        else:
            if self.hw.is_readable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if};\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : True,
                    "name" : f"{fld_name_in_if}",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if self.hw.is_writable():
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_next;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_next",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        # signal hwset and hwclr declaration : enum encoded field are kept as logic vector
        # as bitwise set or clr don't make much sense for enum field
        # (likely an enum based field would not have hwset/hwclr, but if any keep raw logic definition)
        if(self.bit_width <= 1):
            if self.hwset:
                verilog += f"      logic {array_range_definition} {fld_name_in_if}_hwset;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_hwset",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
            if self.hwclr:
                verilog += f"      logic {array_range_definition} {fld_name_in_if}_hwclr;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_hwclr",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}"
                })
        else:
            if (self.hwset):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_hwset;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_hwset",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
            if (self.hwclr):
                verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_hwclr;\n"
                root_regmap.interface_io_list.append( {
                    "rif_out_dir" : False,
                    "name" : f"{fld_name_in_if}_hwclr",
                    "if_name" : if_name,
                    "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
                })
        if (self.hw.is_writable() and self.has_storage(aliased=True, map_table_entry=map_table_entry)):
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_hwwe;\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{fld_name_in_if}_hwwe",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        # swmod is not altered by aggregation
        if (self.swmod or self.delayed_swmod):
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_swmod;\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_swmod",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        # swacc is not altered by aggregation
        if (self.swacc or self.early_swacc):
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_swacc;\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_swacc",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.swwr2hwif:
            # export software write cycle information to the hw interface (bypassing any CDC layer)
            # Storage internal are : - {fld_hw_name}_swwr_condition
            #                        - {fld_hw_name}_swnext
            #                        - {fld_hw_name}_swwr_bmask
            # signal name in interface :
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_ifsw_wr;\n"
            verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_ifsw_wr_data;\n"
            verilog += f"      logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim} {fld_name_in_if}_ifsw_wr_bmask;\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_ifsw_wr",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_ifsw_wr_data",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
            })
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : True,
                "name" : f"{fld_name_in_if}_ifsw_wr_bmask",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}[{if_bit_width - 1}:0]{extra_dim}"
            })
        # sw access mode is to be checked versus the aliased fields view in mode usage below
        alias_swa  = self.get_alias_ored_attr(attr='sw',   aliased=True, map_table_entry=map_table_entry)
        alias_rclr = self.get_alias_ored_attr(attr='rclr', aliased=True, map_table_entry=map_table_entry)
        alias_rset = self.get_alias_ored_attr(attr='rset', aliased=True, map_table_entry=map_table_entry)
        if self.swwe or self.swwel: # hardware enabling field modification
            if not(alias_swa.is_writable() or alias_rclr or alias_rset):
                raise RuntimeError(f"found swwe option for field  {fld_name_in_if} that is"\
                                   f" not sw writable, nor rclr , nor rset ?")
        if self.swwe: # hardware enabling field modification
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_swwe;\n"
            root_regmap.interface_io_list.append( {
                "rif_out_dir" : False,
                "name" : f"{fld_name_in_if}_swwe",
                "if_name" : if_name,
                "type" : f"logic {array_range_definition}"
            })
        if self.swwel: # hardware enabling field modification (active low)
            verilog += f"      logic {array_range_definition} {fld_name_in_if}_swwel;\n"
        verilog += "\n"
        return verilog

    def to_verilog_interface(self, if_name, map_table_entry):
        if self.external:
            return self.to_verilog_interface_external(
                if_name=if_name,
                map_table_entry=map_table_entry)
        else:
            return self.to_verilog_interface_internal(
                if_name=if_name,
                map_table_entry=map_table_entry)

# ----------------------------------------------------------------------------

    def to_verilog_connect_interface_external(self, map_table_entry):
        array_name_suffix = map_table_entry.get_suffix_array_name()
        array_index_bracket = map_table_entry.get_array_index_bracket()
        reg_name = map_table_entry.get_full_hw_name()
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        fld_name_in_if = self.get_name_in_if() # no array extension
        if_portname = self.get_interface_name().lower()
        verilog = ""

        reshape_size = 1
        if self.if_reshape:
            # so far support only single extra dimension
            reshape_size = self.if_reshape[0]

        verilog += (f"    // External Field interface connection : "
                       f"{self.lsb_bit + self.bit_width - 1}:{self.lsb_bit}\n")
        if self.sw.is_readable():
            # field value to be read in register is field_name signal
            # generic name used to connect inside register (field either internal or external)
            verilog += f"      assign {fld_hw_name} =\n"
            if self.encode:
                # internal signal is bit vector (interface is enum : cast needed)
                verilog += f"          {self.bit_width}'({if_portname}.{fld_name_in_if}_extrdata{array_index_bracket}); // cast enum to proper bitwidth integer\n"
            elif self.bit_width <= 1:
                verilog += f"          {if_portname}.{fld_name_in_if}_extrdata{array_index_bracket};\n"
            else:
                # vector
                verilog += f"          {if_portname}.{fld_name_in_if}_extrdata{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"
            # external hw implementation requires the read pulse
            verilog += f"      assign {if_portname}.{fld_name_in_if}_extrd{array_index_bracket} = {reg_name}_r_swrd;\n"

        if self.sw.is_writable():
            if self.bit_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwdata{array_index_bracket} =\n"
            else:
                # vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwdata{array_index_bracket}[{self.bit_width//reshape_size-1}:0] =\n"
            if self.encode:
                # internal signal is vector to be cast to enum
                verilog += f"          Tenum_{self.encode.__name__}'({fld_hw_name}_extwdata);\n"
            else:
                verilog += f"          {fld_hw_name}_extwdata;\n"
            verilog += f"      assign {if_portname}.{fld_name_in_if}_extwr{array_index_bracket} = {fld_hw_name}_extwr;\n"
            if self.support_hwbe:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_extwr_bmask{array_index_bracket} = {fld_hw_name}_extwr_bmask;\n"

        if (self.sw.is_readable() and self.extready_rd) or \
           (self.sw.is_writable() and self.extready_wr):
            # external register have _extready signal (used either for read or write)
            verilog += f"      assign {fld_hw_name}_extready_from_if = {if_portname}.{fld_name_in_if}_extready{array_index_bracket};\n"

        verilog += "\n"
        return verilog

    def to_verilog_connect_interface_internal(self, map_table_entry):
        """
            Generate verilog code to connect the internal signal (storage around storage, host bus)
            to the verilog hw interface.
        """
        array_index_bracket = map_table_entry.get_array_index_bracket()
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        fld_name_in_if = self.get_name_in_if() # no array extension
        fld_width = self.bit_width
        # name in interface will be like  {if_name}.{fld_name_in_if}_swmod{array_index_bracket}
        if_portname = self.get_interface_name().lower()
        root_regmap = map_table_entry.get_root_entry().register
        verilog = ""

        if self.is_agregate_field():
            aggregate_name = self.get_aggregate_info()[0]
            aggregate_info_list = map_table_entry.field_idb[self.name]["aggregate_info"]

            #print(aggregate_info_list)
            aggregate_bit_lsb = 0
            aggregate_position = 0
            for aggregate_info in aggregate_info_list:
                if aggregate_info['field'] == self and aggregate_info['reg_entry'] == map_table_entry:
                    fld_name_in_if = aggregate_info['aggregate_name']
                    #print(f"ConnectIF: Found aggreg: {fld_name_in_if}, Pos={aggregate_position}, Bit={aggregate_bit_lsb}")
                    if fld_width != aggregate_info['bit_width']:
                        raise RuntimeError(
                            f"Unexpected condition: field size is not matching in aggregate info:{aggregate_info} "\
                            f"for field {fld_hw_name}:bw={fld_width}")
                    break
                else:
                    aggregate_bit_lsb += aggregate_info['bit_width']
                    aggregate_position += 1
                    # Aggregate_position to be the position in all aggregated fields
                    # (unit is +1 for each field aggregation)
            else:
                # The field is part of aggregate, so I should be found
                raise RuntimeError(
                    f"Unexpected condition: Unable to locate aggregate field in aggregate info:{aggregate_info} "\
                    f"for field {fld_hw_name}:{fld_name_in_if}")
            # print(f"Field={self.name}: Locate aggregation in bit_range={aggregate_bit_lsb+fld_width-1}:{aggregate_bit_lsb}")

        reshape_size = 1
        # reshape condition from current field
        reshape_condition = self.if_reshape
        if self.is_agregate_field():
            reshape_condition = aggregate_info_list[0]['field'].if_reshape

        if reshape_condition:
            # so far support only single extra dimension
            reshape_size = reshape_condition[0]
            if fld_width % reshape_size != 0:
                raise RuntimeError(f"vector dimension is not multiple of reshape size in {self.get_name()}")
            if      self.is_agregate_field() and\
                    aggregate_bit_lsb % reshape_size != 0:
                raise RuntimeError(f"aggregate offset is not multiple of reshape size in {self.get_name()}")

        if self.hw.is_readable():
            if self.is_agregate_field():
                verilog += f"      assign {if_portname}.{aggregate_name}"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:"+\
                           f"{aggregate_bit_lsb//reshape_size}] = "
            elif fld_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}{array_index_bracket} = "
            else:
                # vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}{array_index_bracket}"+\
                                 f"[{fld_width//reshape_size-1}:0] = "
            if self.encode:
                # internal signal is vector to be cast to enum
                verilog += f"Tenum_{self.encode.__name__}'({fld_hw_name}_cdcif);\n"
            else:
                verilog += f"{fld_hw_name}_cdcif;\n"

        if (self.swmod or self.delayed_swmod):
            if self.is_agregate_field():
                # one swmod signal bit for each aggregate_position
                verilog += f"      assign {if_portname}.{aggregate_name}_f_swmod[{aggregate_position}] = {fld_hw_name}_swmod_cdcif;\n"
                # global swmod signal is generate while processing first aggregated position
                if aggregate_position==0:
                    verilog += f"      assign {if_portname}.{aggregate_name}_swmod = |{if_portname}.{aggregate_name}_f_swmod;\n"
            else:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_swmod{array_index_bracket} = {fld_hw_name}_swmod_cdcif;\n"

        if (self.swacc or self.early_swacc):
            if self.is_agregate_field():
                # one swacc signal bit for each aggregate_position
                verilog += f"      assign {if_portname}.{aggregate_name}_f_swacc[{aggregate_position}] = {fld_hw_name}_swacc_cdcif;\n"
                # global swacc signal is generate while processing first aggregated position
                if aggregate_position==0:
                    verilog += f"      assign {if_portname}.{aggregate_name}_swacc = |{if_portname}.{aggregate_name}_f_swacc;\n"
            else:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_swacc{array_index_bracket} = {fld_hw_name}_swacc_cdcif;\n"

        if self.swwe or self.swwel:
            if self.is_agregate_field():
                # aggregate is handled as single field on interface, drop array that are aggregated
                verilog += f"      assign {fld_hw_name}_swwe_cdcif = {if_portname}.{aggregate_name}_swwe;\n"
            else:
                verilog += f"      assign {fld_hw_name}_swwe_cdcif = {if_portname}.{fld_name_in_if}_swwe{array_index_bracket};\n"

        if (self.hwset):
            if self.is_agregate_field():
                verilog += f"      assign {fld_hw_name}_hwset_cdcif = {if_portname}.{aggregate_name}_hwset"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:"+\
                           f"{aggregate_bit_lsb//reshape_size}];\n"
            else:
                verilog += f"      assign {fld_hw_name}_hwset_cdcif = {if_portname}.{fld_name_in_if}_hwset{array_index_bracket};\n"

        if (self.hwclr):
            if self.is_agregate_field():
                verilog += f"      assign {fld_hw_name}_hwclr_cdcif = {if_portname}.{aggregate_name}_hwclr"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:"+\
                           f"{aggregate_bit_lsb//reshape_size}];\n"
            else:
                verilog += f"      assign {fld_hw_name}_hwclr_cdcif = {if_portname}.{fld_name_in_if}_hwclr{array_index_bracket};\n"

        if self.hw.is_writable():
            verilog += f"      assign {fld_hw_name}_next_cdcif = "
            # now complete the assign signal = ..., using signal from interface
            # assign target is either fld_hw_name or cdc_name
            if self.encode:
                # internal signal is bit vector (interface is enum : cast needed)
                verilog += f"{fld_width}'({if_portname}.{fld_name_in_if}_next{array_index_bracket}); // cast enum to proper bitwidth integer\n"
            elif self.is_agregate_field():
                # TODO: add support of encode on aggregate of field ...
                verilog += f"{if_portname}.{aggregate_name}_next"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:{aggregate_bit_lsb//reshape_size}];\n"
            elif fld_width <= 1:
                verilog += f"{if_portname}.{fld_name_in_if}_next{array_index_bracket};\n"
            else:
                # vector
                verilog += f"{if_portname}.{fld_name_in_if}_next{array_index_bracket}[{self.bit_width//reshape_size-1}:0];\n"

            if self.has_storage(aliased=True, map_table_entry=map_table_entry):
                if self.is_agregate_field():
                    verilog += f"      assign {fld_hw_name}_hwwe_cdcif = {if_portname}.{aggregate_name}_hwwe;\n"
                else:
                    verilog += f"      assign {fld_hw_name}_hwwe_cdcif = {if_portname}.{fld_name_in_if}_hwwe{array_index_bracket};\n"

        if self.swwr2hwif:
            if self.is_agregate_field():
                verilog += f"      assign {if_portname}.{aggregate_name}_ifsw_wr_data"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:"+\
                           f"{aggregate_bit_lsb//reshape_size}] = {fld_hw_name}_swnext;\n"
            elif fld_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_ifsw_wr_data{array_index_bracket} = {fld_hw_name}_swnext;\n"
            else:
                # vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}_ifsw_wr_data{array_index_bracket}"+\
                                 f"[{fld_width//reshape_size-1}:0] = {fld_hw_name}_swnext;\n"

            if self.is_agregate_field():
                verilog += f"      assign {if_portname}.{aggregate_name}_ifsw_wr_bmask"+\
                           f"[{fld_width//reshape_size+aggregate_bit_lsb//reshape_size-1}:"+\
                           f"{aggregate_bit_lsb//reshape_size}] = {fld_hw_name}_swwr_bmask;\n"
            elif fld_width <= 1:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_ifsw_wr_bmask{array_index_bracket} = {fld_hw_name}_swwr_bmask;\n"
            else:
                # vector
                verilog += f"      assign {if_portname}.{fld_name_in_if}_ifsw_wr_bmask{array_index_bracket}"+\
                                 f"[{fld_width//reshape_size-1}:0] = {fld_hw_name}_swwr_bmask;\n"

            if self.is_agregate_field():
                # one wr signal bit for each aggregate_position
                verilog += f"      assign {if_portname}.{aggregate_name}_f_ifsw_wr[{aggregate_position}] = {fld_hw_name}_swwr_condition;\n"
                # global swmod signal is generate while processing first aggregated position
                if aggregate_position==0:
                    verilog += f"      assign {if_portname}.{aggregate_name}_ifsw_wr = |{if_portname}.{aggregate_name}_f_ifsw_wr;\n"
            else:
                verilog += f"      assign {if_portname}.{fld_name_in_if}_ifsw_wr{array_index_bracket} = {fld_hw_name}_swwr_condition;\n"

        verilog += "\n"
        return verilog

    def to_verilog_connect_interface(self, map_table_entry):
        if self.external:
            return self.to_verilog_connect_interface_external(map_table_entry)
        else:
            return self.to_verilog_connect_interface_internal(map_table_entry)

# ----------------------------------------------------------------------------

    def get_uvm_access(self):
        """
            get_uvm_access :Return the acces tpe for the field that match uvm reg model definition
                //set_access
                //'RO'      W: no effect, R: no effect
                //'RW'      W: as-is, R: no effect
                //'RC'      W: no effect, R: clears all bits
                //'RS'      W: no effect, R: sets all bits
                //'WRC'     W: as-is, R: clears all bits
                //'WRS'     W: as-is, R: sets all bits
                //'WC'      W: clears all bits, R: no effect
                //'WS'      W: sets all bits, R: no effect
                //'WSRC'    W: sets all bits, R: clears all bits
                //'WCRS'    W: clears all bits, R: sets all bits
                //'W1C'     W: 1/0 clears/no effect on matching bit, R: no effect
                //'W1S'     W: 1/0 sets/no effect on matching bit, R: no effect
                //'W1T'     W: 1/0 toggles/no effect on matching bit, R: no effect
                //'W0C'     W: 1/0 no effect on/clears matching bit, R: no effect
                //'W0S'     W: 1/0 no effect on/sets matching bit, R: no effect
                //'W0T'     W: 1/0 no effect on/toggles matching bit, R: no effect
                //'W1SRC'   W: 1/0 sets/no effect on matching bit, R: clears all bits
                //'W1CRS'   W: 1/0 clears/no effect on matching bit, R: sets all bits
                //'W0SRC'   W: 1/0 no effect on/sets matching bit, R: clears all bits
                //'W0CRS'   W: 1/0 no effect on/clears matching bit, R: sets all bits
                //'WO'      W: as-is, R: error
                //'WOC'     W: clears all bits, R: error
                //'WOS'     W: sets all bits, R: error
                //'W1'      W: first one after HARD reset is as-is, other W have no effects, R: no effect
                //'WO1'     W: first one after HARD reset is as-is, other W have no effects, R: error
        """
        if self.sw == Access.RW:
            if self.rclr:
                return "WRC"
            elif self.rset:
                return "WRS"
            elif self.woclr:
                return "W1C"
            elif self.woset:
                return "W1S"
            elif self.wot:
                return "W1T"
            elif self.wzc:
                return "W0C"
            elif self.wzs:
                return "W0S"
            elif self.wzt:
                return "W0T"
            elif self.wclr:
                return "WC" # readable, but not effect on read on field content
            elif self.wset:
                return "WS" # readable, but not effect on read on field content
            elif self.singlepulse:
                # Single pulse field is set to 1 on sw write,
                # with return to 0 on next active hw clock edge
                # The 2 steps collapse is not supported by default uvm regmodel
                # Pyrift regmodel implementation is adding sub-class to the uvm base to create a new PULSE access mode for the field
                return "PULSE"
            else:
                return "RW"
        elif  self.sw == Access.R:
            if self.rclr:
                return "RC"
            elif self.rset:
                return "RS"
            else:
                return "RO"
        elif  self.sw == Access.W:
            if self.rclr:
                return "WRC"
            elif self.rset:
                return "WRS"
            elif self.woclr:
                return "W1C"
            elif self.woset:
                return "W1S"
            elif self.wot:
                return "W1T"
            elif self.wzc:
                return "W0C"
            elif self.wzs:
                return "W0S"
            elif self.wzt:
                return "W0T"
            elif self.wclr:
                return "WOC" # write only clear
            elif self.wset:
                return "WOS" # write only set
            elif self.singlepulse:
                # Single pulse field is set to 1 on sw write,
                # with return to 0 on next active hw clock edge
                # The 2 steps collapse is not supported by default uvm regmodel
                # Pyrift regmodel implementation is adding sub-class to the uvm base to create a new PULSE access mode for the field
                return "PULSE"
            else:
                return "WO"
        elif  self.sw == Access.NA:
            if self.shadow:
                print(f"Note: get_uvm_access: generate uvm access model for shadow field, marked as 'NA' for field {self.name}")
            else:
                print("Warning: generate uvm access model, with no software access")
                print("         Woud be error or undefined in systemrdl spec,")
                print("         mark uvm reg model as NA, please check register definition")
            return "NA" # no uvm way to say read as 0 ???

# ----------------------------------------------------------------------------

    def svd_fill_field(self, map_table_entry, fields_et):
        field_et = ET.SubElement(fields_et, 'field')
        ET.SubElement(field_et, 'name').text = self.name
        field_description = self.desc
        if not field_description:
            field_description = '-'
        ET.SubElement(field_et, 'description').text = \
            ''.join([ f"{line}\n" for line in cleandoc(field_description).splitlines()]).strip()
        # field bit range is defined from lsb_bit, bit_width
        ET.SubElement(field_et, 'bitOffset').text = str(self.lsb_bit)
        ET.SubElement(field_et, 'bitWidth').text = str(self.bit_width)
        # access depend on field.sw property
        if self.sw == Access.RW:
            ET.SubElement(field_et, 'access').text = "read-write"
        elif  self.sw == Access.R:
            ET.SubElement(field_et, 'access').text = "read-only"
        elif  self.sw == Access.W:
            ET.SubElement(field_et, 'access').text = "write-only"
        elif  self.sw == Access.NA:
            # normally unsupported, could be a shadow setup with no sw access
            pass
        else:
            raise RuntimeError(f"SVD generation found field:{self.name} with unexpected access:sw={self.sw}")

        # SVD:modifiedWriteValues
        #     Describe the manipulation of data written to a field.
        #     If not specified, the value written to the field is the value stored in the field.
        # The other options are bitwise operations:
        # - oneToClear: write data bit of one shall clear (set to zero) the corresponding bit in the field.
        # - oneToSet: write data bit of one shall set (set to one) the corresponding bit in the field.
        # - oneToToggle: write data bit of one shall toggle (invert) the corresponding bit in the field.
        # - zeroToClear: write data bit of zero shall clear (set to zero) the corresponding bit in the field.
        # - zeroToSet: write data bit of zero shall set (set to one) the corresponding bit in the field.
        # - zeroToToggle: write data bit of zero shall toggle (invert) the corresponding bit in the field.
        # - clear: after a write operation all bits in the field are cleared (set to zero).
        # - set: after a write operation all bits in the field are set (set to one).
        # - modify: after a write operation all bit in the field may be modified (default).

        # Check only with is_value_volatile() not is_volatile():
        # (excluding side-effect like swmod and predictable woclr, ...) that are handled explicitly
        if self.sw.is_writable(): # report modifiedWriteValues attribute only for writable field
            # Report the special way to write value in register (in a predictable way)
            # Have precedence to volatility and other side effect of what can happen to the write value inside register later
            # SVD semantic is still to be checked that is is the initial intend of modifiedWriteValues
            if self.woclr:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "oneToClear"
            elif self.woset:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "oneToSet"
            elif self.wot:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "oneToToggle"
            elif self.wzc:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "zeroToClear"
            elif self.wzs:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "zeroToSet"
            elif self.wzt:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "zeroToToggle"
            elif self.wclr:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "clear"
            elif self.wset:
                ET.SubElement(field_et, 'modifiedWriteValues').text = "set"
            else:
                # check for unpredictable write
                if self.swwe or self.swwel or self.external:
                    ET.SubElement(field_et, 'modifiedWriteValues').text = "modify"
                else:
                    pass # Nothing to report on write special : modifiedWriteValues

            # Other options are to be explicit from user configuration in pyrift script : not yet supported
            # if side effect like change by hwwrite, ... are to be reflected in SVD output, then help from script is TBD


        # SVD:readAction
        #     If set, it specifies the side effect following a read operation.
        #     If not set, the register is not modified.
        # The defined side effects are:
        # - clear: The register is cleared (set to zero) following a read operation.
        # - set: The register is set (set to ones) following a read operation.
        # - modify: The register is modified in some way after a read operation.
        # - modifyExternal: One or more dependent resources other than the current register are immediately affected by a read operation
        #                   (it is recommended that the register description specifies these dependencies).
        #                   Debuggers are not expected to read this register location unless explicitly instructed by the user.
        if self.sw.is_readable(): # report readAction attribute only for readable field
            # Report the special way to read value from register that affect the register content (predictable)
            # Have precedence to volatility and other side effect of what can happen to the write value inside register later
            # SVD semantic is still to be checked that is is the initial intend of modifiedWriteValues
            if self.rclr:
                ET.SubElement(field_et, 'readAction').text = "clear"
            elif self.rset:
                ET.SubElement(field_et, 'readAction').text = "set"

            # Other options are to be explicit from user configuration in pyrift script : not yet supported
            # ET.SubElement(field_et, 'readAction').text = "modifyExternal"
            # ET.SubElement(field_et, 'readAction').text = "modify"

        # enum used ?
        if self.encode:
            # self.encode have been check to be a subclass of FieldEncode
            # create enumeratedValues attribute for SVD
            enumeratedvalues_et = ET.SubElement(field_et, 'enumeratedValues')
            # enumeratedValues can be declared either for read or write or both read/write, pyrift only support RW for the moment
            ET.SubElement(enumeratedvalues_et, 'name').text = self.encode.__name__
            ET.SubElement(enumeratedvalues_et, 'usage').text = "read-write"
            # SVD have no description at enum level (only at enum value level)
            # So add the enum description as comment
            if self.encode.get_desc():
                enumeratedvalues_et.append(ET.Comment(
                    ''.join([f"description={line}\n" for line in cleandoc(self.encode.get_desc()).splitlines()]).strip()
                ))

            for val in self.encode:
                val_et = ET.SubElement(enumeratedvalues_et, 'enumeratedValue')
                ET.SubElement(val_et, 'name').text = val.name
                ET.SubElement(val_et, 'value').text = str(val.value)

                desc_txt = self.encode.get_value_desc(val.value)
                if not desc_txt: # description is kind of mandatory to remove verbose info in SVDConv
                    desc_txt = "-"
                ET.SubElement(val_et, 'description').text =\
                    ''.join([ f"{line}\n" for line in cleandoc(desc_txt).splitlines()]).strip()

        # SVD has no way to define reset value per field, so will have to rely on register reset value :(

# ----------------------------------------------------------------------------

class Pad_Field(Field):
    """docstring for Pad_Field"""

    def __init__(self, name, bit_range):
        super().__init__(name=name, bit_range=bit_range)
        # Pad field is degenerated : readonly from sw (read constant reset value)
        self.hw = Access.NA
        self.sw = Access.R

        self.reset = 0

    def is_volatile(self, map_table_entry):
        return False
    def is_value_volatile(self, map_table_entry):
        return False

    def get_interface_name(self):
        return ""

    def get_src_alias(self, map_table_entry):
        # dummy definition in case scan for alias of current pad field
        # There is never any alias for a pad field
        return []

    def get_full_hw_name(self, map_table_entry):
        # get hierarchical name using register info from map_table_entry
        return map_table_entry.get_full_hw_name() + "_f_" + self.name

    def to_verilog(self, map_table_entry):
        fld_hw_name = self.get_full_hw_name(map_table_entry)
        # Padding: verilog_signal_name is tie to 0
        verilog_declaration = ""
        verilog = ""

        verilog_declaration += f"logic [{self.bit_width}:0] {fld_hw_name};\n"
        verilog += f"assign {fld_hw_name} = 0;\n"
        verilog += "\n"

        return (verilog_declaration, verilog)


# ----------------------------------------------------------------------------
