// ----------------------------------------------------------------------------
// interface definition of generic pyrift host bus
// This intended to support interface very close to pyrift internals
// This should allow high performance connection to pyrift core
// This is intended to be used for external host interface module like SPI bridge
// The used of Address is to decode LSB of each register mapped
// Top level decoing is assumed already done externally and provided to wr and rd pulse
// clk use in rif core is not getting through the interface
// ----------------------------------------------------------------------------

// DW is data bus width parameter 32 bit is nominal, option for 8, 16, or 64, 128, ...
interface IF_GENERIC_BUS #(parameter DW = 32) (input var logic clk);
   // Master --> Slave
   logic [    31:0] gaddr    ; //
   logic            grd      ; // read pulse
   logic            grd_early; // read pulse early by 1 clock (to allow special rif data resync)
   logic            gwr      ; // write pulse
   logic [DW/8-1:0] gbe      ; // byte enable (bmask is not needed and re-construct inside pyrift generated code)
   logic [  DW-1:0] gwdata   ; //

   // Slave --> Master
   logic [DW-1:0] grdata; //
   logic          gready; //

      // Master Side
   //***************************************
   modport master (
      output gaddr,
      output grd,
      output grd_early,
      output gwr,
      output gbe,
      output gwdata,

      input  grdata,
      input  gready
   );

   // Slave Side
   //***************************************
   modport slave (
      input gaddr,
      input grd,
      input grd_early,
      input gwr,
      input gbe,
      input gwdata,

      output  grdata,
      output  gready
  );

`ifndef SYNTHESIS
   // Used for SVA checker
   modport monitor(
      // input clk,

      input gaddr,
      input grd,
      input grd_early,
      input gwr,
      input gbe,
      input gwdata,

      input  grdata,
      input  gready
  );
`endif

endinterface // IF_GENERIC_BUS
