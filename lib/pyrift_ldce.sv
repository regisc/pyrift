// ---------------------------------------------------------------------------- -
// Pyrift library to implement latch as LDCE
// This achive a low level mapping of latch template to match Xilinx LDCE
// Workaround to limitation of vivado that fail to get timing clean mapping of LDCE
// This module is 1 bit width LDCE (interface compatible with Xilinx LDCE primitive)
// Can be used for simulation or IC synthesis
// In Xilinx fpga vivado flow, should be replaced by using directly Xilinx primitive : LDCE
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_ldce (
   input  var logic CLR, // Async reset : active high
   input  var logic G  , // Latch gating : active high
   input  var logic GE , // Latch gating enable : active high
   input  var logic D  , // data input
   output var logic Q    // data output
);

// synopsys async_set_reset "CLR"
// synopsys one_hot "CLR"

// behavioural model for latch bit cell
always_latch begin : proc_ldce
   if(CLR) begin
      Q <= 'h0;
   end else begin
      // /!\ latch is transparent if G==1 (masked by gate enable : GE==1)
      if (G==1 && GE==1) begin
         Q <= D;
      end
    end
end

endmodule // pyrift_ldce
