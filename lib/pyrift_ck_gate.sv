// ---------------------------------------------------------------------------- -
// Pyrift library to implement clock gating
// This achieves a low level mapping of CG cell to simulation model
// Intended to be black box in synthesis and replaced by proper techno specific cell
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_ck_gate (
   input  var logic clk_i    , // clock input
   input  var logic ce_i     , // clock enable
   input  var logic test_en_i, // test enable, force ce during test. Tie to 0 is unused
   output var logic clk_o      // clock output, gated by ce_i
);

// ---------------------------------------------------------------------------

// 2 stage processing of clock gating :
// step 1: clock_enable is transparent latch from ce_i on clk_i==0
logic clock_enable;

always_latch begin
   if (clk_i == 1'b0)
      clock_enable <= ce_i | test_en_i; // test force a clock to be enabled;
end

// step 2: comb and logic of clk_i and clock_enable
assign clk_o = clk_i & clock_enable;

// ---------------------------------------------------------------------------
endmodule // pyrift_ck_gate
