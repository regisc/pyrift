// ----------------------------------------------------------------------------
// interface definition of generic AMBA AHB bus (ARM AMBA 5 AHB Protocol Specification)
// This intended to support AMBA as AHB-Lite view
// No intend to handle bus arbitration and multi-layer (no multi-master intended)
// to be used as point-2-point between athe external master and RIF
// The used of HSEL or Address decode module is not explicit
// HSEL used only on slave as decode of HADDR msb.
// HCLK, HRESETn is handled outside the interface
// ----------------------------------------------------------------------------

// basic description from amba spec
// MASTER signal
// HADDR[31:0] The 32-bit system address bus.
// HBURST[2:0] The burst type indicates if the transfer is a single transfer or forms part of a burst.
//    Fixed length bursts of 4, 8, and 16 beats are supported. The burst can be incrementing or wrapping.
//    Incrementing bursts of undefined length are also supported.See Burst operation on page 3-9 for more information.
// HMASTLOCK When HIGH, this signal indicates that the current transfer is part of a locked sequence.
//    It has the same timing as the address and control signals.See Locked transfers on page 3-7 for more information.
// HPROT[3:0] The protection control signals provide additional information about a bus access and
//    are primarily intended for use by any module that wants to implement some level of protection.
//    The signals indicate if the transfer is an opcode fetch or data access,
//    and if the transfer is a privileged mode access or user mode access.
//    For masters with a memory management unit these signals also indicate whether the current access is cacheable or bufferable.
//    See Protection control on page 3-22 for more information.
// HSIZE[2:0] Indicates the size of the transfer, that is typically byte, halfword, or word.
//    The protocol allows for larger transfer sizes up to a maximum of 1024 bits.
//    See Transfer size on page 3-8 for more information.
// HTRANS[1:0] Indicates the transfer type of the current transfer.
//    This can be:-IDLE -BUSY -NONSEQUENTIAL -SEQUENTIAL.
//    See Transfer types on page 3-5 for more information.
// HWDATA[31:0] The write data bus transfers data from the master to the slaves during write operations.
//    A minimum data bus width of 32 bits is recommended.
//    However, this can be extended to enable higher bandwidth operation.
//    See Data buses on page 6-2 for more information.
// HWRITE Indicates the transfer direction.
//    When HIGH this signal indicates a write transfer and when LOW a read transfer.
//    It has the same timing as the address signals, however, it must remain constant throughout a burst transfer.
//    See Basic transfers on page 3-2 for more information.

// SLAVE signal
// HRDATA[31:0] During read operations, the read data bus transfers data from the selected slave to the multiplexor.
//    The multiplexor then transfers the data to the master.
//    A minimum data bus width of 32 bits is recommended.
//    However, this can be extended to enable higher bandwidth operation.See Data buses on page 6-2 for more information.
// HREADYOUT When HIGH, the HREADYOUT signal indicates that a transfer has finished on the bus.
//    This signal can be driven LOW to extend a transfer.
//    See Bus interconnection on page 4-3 for more information.
// HRESP The transfer response, after passing through the multiplexor,
//    provides the master with additional information on the status of a transfer.
//    When LOW, the HRESP signal indicates that the transfer status is OKAY.
//    When HIGH, the HRESP signal indicates that the transfer status is ERROR.
//    See Slave transfer responses on page 5-2 for more information.

// HSEL Decoder output Slave input : put as is in the interface (OK for LITE point2point connection master to slave)


// Info on HRESP 2 bit in full AHB, single on AHB lite
// AHB-Lite (AMBA 3.0) does not support the SPLIT and RETRY slave responses that are supported in full AHB (from AMBA 2.0).
// Looking at the HRESP signals in full AHB:
//
// HRESP[1:0]
// 00 - OKAY
// 01 - ERROR
// 10 - RETRY
// 11 - SPLIT
// Because RETRY and SPLIT are not supported, the only possible responses are OKAY and ERROR
// (which can be determined using only bit 0 of HRESP).

// DW is data bus width parameter 32 bit is nominal, option for 8, 16, or 64, 128, ...
interface IF_AHB_BUS #(parameter DW = 32) (input var logic clk);
   // Master --> Slave
   logic [31:0] HADDR; //
   logic [2:0] HBURST; //
   logic [2:0] HSIZE; //
   logic [1:0] HTRANS; //
   logic [DW-1:0] HWDATA; //
   logic HWRITE; //

   // logic HMASTLOCK; // No lock transfer supported
   // logic [3:0] HPROT; // No protection control supported

   // Normally just slave input
   logic HSEL;

   // Slave --> Master
   logic [DW-1:0] HRDATA; //
   logic HREADY; //
   logic [1:0] HRESP; //

      // Master Side
   //***************************************
   modport master (
      output HADDR,
      output HBURST,
      output HSIZE,
      output HTRANS,
      output HWDATA,
      output HWRITE,

      output HSEL,

      // output HMASTLOCK,
      // output HPROT,

      input  HRDATA,
      input  HREADY,
      input  HRESP
   );

   // Slave Side
   //***************************************
   modport slave (
      input HADDR,
      input HBURST,
      input HSIZE,
      input HTRANS,
      input HWDATA,
      input HWRITE,

      input HSEL,

      // input HMASTLOCK,
      // input HPROT,

      output  HRDATA,
      output  HREADY,
      output  HRESP
  );

`ifndef SYNTHESIS
   // Used for SVA checker
   modport monitor(
      // input clk,

      input HBURST,
      input HSIZE,
      input HTRANS,
      input HWDATA,
      input HWRITE,

      input HSEL,

      // input HMASTLOCK,
      // input HPROT,

      input  HRDATA,
      input  HREADY,
      input  HRESP
  );
`endif

endinterface // IF_AHB_BUS
