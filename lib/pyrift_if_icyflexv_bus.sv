// ----------------------------------------------------------------------------
// interface definition of IcyflexV CPU bus (instruction or data)
// This is generic memory bus used by icyflex V cpu, variant of risc-V
// Known to be used by CSEM, originating from ETHZ risc-V work
// Probably customized a bit differently (here is just definition of signal, not specification)
// ----------------------------------------------------------------------------

interface IF_ICYFLEXV_BUS #(parameter DW = 32)(input var logic clk, input var logic rst_n);

   // -------------------------------------------------------------------------
   // Both on risc-v data interface, AND instruction (read only)

   // Address, Data
   logic [31:0]      addr;
   logic [DW-1:0]    rdata;

   // Master control
   logic             req;

   // Slave control
   logic             gnt;
   logic             rvalid;
   logic             berr; // bus error (bus fault) qualifier of rvalid

   // -------------------------------------------------------------------------
   // Only on risc-v data interface, not instruction (read only)

   // Write Data
   logic [DW-1:0]    wdata;
   // Master control
   logic             we;
   logic [3:0]       be;


   // Master Side
   //***************************************
   modport master (
      output req,
      output we,
      output be,
      output addr,
      output wdata,

      input  gnt,
      input  rvalid,
      input  berr,
      input  rdata
   );

   // Slave Side
   //***************************************
   modport slave (
      input  req,
      input  we,
      input  be,
      input  addr,
      input  wdata,

      output gnt,
      output rvalid,
      output berr,
      output rdata
   );

// Disable extra definition, not yet OK for verilator pyrift testing
`ifdef USE_IF_CLOCKING_AND_CHECKER

`ifndef SYNTHESIS
   // clocking block to be used in interface monitoring
   clocking cb @ (posedge clk);
      // Master driven
      input  req;
      input  we;
      input  be;
      input  addr;
      input  wdata;

      //Slave driven
      input gnt;
      input rvalid;
      input berr;
      input rdata;
     endclocking : cb

   // Monitoring modport
   //***************************************
   modport cb_mon(clocking cb);

   // Used for SVA checker
   modport monitor(
      // input clk,

      // Master driven
      input  req,
      input  we,
      input  be,
      input  addr,
      input  wdata,

      //Slave driven
      input gnt,
      input rvalid,
      input berr,
      input rdata
   );

   // instanciate checker interface (checker is provided as interface instead of a module)
   // (interface can be nested where module cannot)
   `ifndef CPUMEM_BUS_NOCHECKER
      IF_CPUMEM_BUS_CHECKER if_cpumem_bus_checker(
            .clk(clk),
            .rst_n(rst_n),
            .addr(addr),
            .rdata(rdata),
            .req(req),
            .gnt(gnt),
            .rvalid(rvalid),
            .wdata(wdata),
            .berr(berr),
            .we(we),
            .be(be)
         );
   `endif


`endif // SYNTHESIS
`endif // USE_IF_CLOCKING_AND_CHECKER

endinterface : IF_ICYFLEXV_BUS
