# ----------------------------------------------------------------------------
# Copyright 2019 Regis Cattenoz, regis@ip666.org
#
# This file is part of pyrift : https://gitlab.com/regisc/pyrift
#
# pyrift is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyrift is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyrift.  If not, see <https://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------------
# File: pyrift_maptableentry.py
# Python Register InterFace Translation
# ----------------------------------------------------------------------------

from enum import Enum, auto
import re

# ----------------------------------------------------------------------------

class MapTableType(Enum):
    ROOT = auto()
    REG = auto()
    REG_MAP = auto()
    REG_MAP_END = auto()
    PRE_PAD = auto()
    POST_PAD = auto()
    ALIGN_PAD = auto()
    REGMAP_LENGTH_ALIGN_PAD = auto()
    REG_ARRAY = auto()
    REG_ARRAY_END = auto()
    REG_MAP_ARRAY = auto()
    REG_MAP_ARRAY_END = auto()

class MapTableEntry():
    """
        Pseudo class use as data structure for map_table construction in reg_map
    """
    def __init__(self):
        self.type = None
        self.parent = None # None means root entry for regmap
        self.child_list = {} # dict of all child of current entry (filled when creating the child)
                             # "child_name" : map_entry
        self.address = None
        self.register = None
        self.related_entry = None # other entry related to current (used for align, pad)
        self.size = None # address occupation
        self.layer = 0 # Nesting level for regmap/reg
        self.layer_range = () # empty tuple if not an array
        self.layer_index = () # empty tuple if not an array
        self.linear_index = 0 # default if not used
    def __str__(self):
        return f"type={self.type}, @=[{self.address}:+{self.size}:{self.address+self.size}"\
               f"[ Arr={self.layer_index}/{self.layer_range}\n"
    def __repr__(self):
        return self.__str__()
    def get_type(self):
        return self.type.name
    def get_summary(self):
        return self.get_type()+":"+self.register.name
    def is_first_of_array(self):
        # first of multi dimensional array have all index as 0 (is False)
        return not any(self.get_regmap_array(range=False, include_leaf_reg=True))
    def get_root_entry(self):
        # explore parent tree of self entry,
        # Find the root entry parent and return it
        # Calling self.get_root_entry().register.map_table can be use to access
        # the root map_table from current entry
        parent = self
        while parent.type != MapTableType.ROOT:
            if not isinstance(parent.parent, MapTableEntry):
                # reach a not validate map_table, with entry = None (not correctly instialized)
                # or any other invalid case
                print(f"MapTableEntry root search failed on {self.get_summary()}")
                raise RuntimeError("Parent entry expexted to be MapTableEntry, unexpected error !")
            # progress through parent tree to root parent (parent is None)
            parent = parent.parent
        return parent
    def get_array_size_in_hierarchy(self):
        # Get 1 if no array, or product of all dimension (count of register in array)
        # normally called only for register, if regmap, return size fo regmap hierarchy
        if self.type in [ MapTableType.REG_MAP_ARRAY, MapTableType.REG_MAP, \
                          MapTableType.REG_MAP_ARRAY_END, MapTableType.REG_MAP_END]:
            full_hierarchy_array_range = self.get_regmap_array(range=True, include_leaf_reg=False)
        else: #likely a register
            full_hierarchy_array_range = self.get_regmap_array(range=True, include_leaf_reg=True)
            # print(f"Linear size for {self.type}:{self.register.name} : {full_hierarchy_array_range}")
        array_size = 1
        for dim_size in full_hierarchy_array_range:
            array_size = array_size * dim_size
        # print ("array_size", array_size)
        return array_size
    def get_array_layer_size(self):
        # Get 1 if no array, or product of all dimension
        array_size = 1
        for dim_size in self.layer_range:
            array_size = array_size * dim_size
        # print ("array_size", array_size)
        return array_size

    def get_regmap_linear_index(self, include_leaf_reg=False):
        # return the linear index position of a regmap inside the hierarchy to root
        # Get linear offset within multi dim array
        # example: range is (4,5,6) and index is (1,2,3)
        # offset is 1*(5*6) + 2*6 + 3
        # if use for register (not a regmap), use include_leaf_reg=True to include register in indexing
        array_range = self.get_regmap_array(range=True, include_leaf_reg=include_leaf_reg)
        array_index = self.get_regmap_array(range=False, include_leaf_reg=include_leaf_reg)
        offset = 0
        for dim_size,index in zip(array_range, array_index):
            offset *= dim_size
            offset += index
            # print (f"dim_size={dim_size} index={index} offset={offset}")
        # print ("offset", offset)
        return offset

    def get_regmap_hierarchy(self, max_range=False, include_root_regmap=True):
        """
            Called for entry being a REG_MAP or REG_MAP_ARRAY (otherwise return empty string)
            return the hierarchy of regmap from root to reach current regmap entry
            Example: in regmapA[4].regmapB[6] and called from index regmapA[1].regmapB[3]
            Should return something like "regmapA[1].regmapB[3] or regmapA[4].regmapB[6]"
            max_range is used to select either the current index or the size of [array]
            if leaf regmap is array, then give "regmapA[1].regmapB[0:5]""
        """
        # Use default formatting :
        #   rm2rm_separator='.', rm2reg_separator = ':', leaf_rm_range_fmt="[0:{}]", index_fmt="[{}]"
        return self.get_hierarchy_formatted(max_range=max_range, include_root_regmap=include_root_regmap)

    def get_full_hw_name(self):
        return self.get_hierarchy_formatted(max_range=False, include_root_regmap=True,\
            rm2rm_separator='_', rm2reg_separator = '_', leaf_rm_range_fmt="_x{}", index_fmt="_x{}")

    def get_noarray_hw_name(self):
        return self.get_hierarchy_formatted(max_range=False, include_root_regmap=True,\
            rm2rm_separator='_', rm2reg_separator = '_', leaf_rm_range_fmt="", index_fmt="")

    def get_short_hw_name(self):
        return self.register.name

    def get_hierarchy_formatted(self, \
            max_range=False, include_root_regmap=True, \
            rm2rm_separator='/', rm2reg_separator = '/', \
            leaf_rm_range_fmt="[0:{}]", index_fmt="[{}]"):
        # get string representation of map table entry (REG, REGMAP, ...)
        # explore map_table hierarchy, and customize output with several options
        hier_info = self.get_hierarchy_info()
        if len(hier_info['regmap_range_list'])>=1 and not include_root_regmap and\
                len(hier_info['regmap_range_list'][0])==0:
            # regmap is not an array at root, so drop if include_root_regmap
            hier_info['regmap_entry_list'].pop(0)
            hier_info['regmap_name_list'].pop(0)
            hier_info['regmap_range_list'].pop(0)
            hier_info['regmap_index_list'].pop(0)

        if max_range:
            rm_index = hier_info['regmap_range_list']
            rm_name = hier_info['regmap_name_list']
            reg_index = hier_info['register_range']
        else:
            if hier_info['leaf_is_regmap_array']:
                # skip last entry that will be handled as special case
                rm_index = hier_info['regmap_index_list'][:-1]
                rm_name = hier_info['regmap_name_list'][:-1]
                reg_index = hier_info['register_index']
            else:
                rm_index = hier_info['regmap_index_list']
                rm_name = hier_info['regmap_name_list']
                reg_index = hier_info['register_index']
        # rm_index is something like [(),(1,2),(3)], convert to ["","[1][2]","[3]"]
        index_formatted = [''.join([index_fmt.format(id) for id in tup]) for tup in rm_index]
        rm_formatted = rm2rm_separator.join([f"{name}{idfmt}" for name, idfmt in zip(rm_name, index_formatted)])
        # print("Rh=",  hier_info['regmap_name_list'], rm_index, hier_info['register_name'])
        # print("Rhstr=",  rm_formatted, hier_info['register_name'])
        # print("Idstr=",  index_formatted)

        if not max_range and hier_info['leaf_is_regmap_array']:
            # last regmap as range : [0:#]
            if rm_formatted:
                rm_formatted += rm2rm_separator # if parent exist, join with a '.'
            rm_formatted += hier_info['regmap_name_list'][-1]
            rm_formatted += ''.join([leaf_rm_range_fmt.format(id-1) for id in hier_info['regmap_range_list'][-1]])

        # add register info if present
        rm_hierachy = rm_formatted
        if hier_info['register_name']:
            if rm_hierachy:
                rm_hierachy += rm2reg_separator # if parent exist, join with a '.'
            rm_hierachy += hier_info['register_name'] + ''.join([index_fmt.format(id) for id in reg_index])

        return rm_hierachy

# ----------------------------------------------------------------------------

    def get_hierarchy_info(self):
        """
            Explore hierarchy from current entry to root entry
            return all info for all regmaps, and final register:
                - regmap_entry_list[]= list of all regmap maptable entry
                - regmap_name_list[]= list of all regmap name
                - regmap_range_list[]= list of all regmap array_range, () if not array
                - regmap_index_list[]= list of all regmap array_index, () if not array
                - register_entry= leaf register maptable entry
                - register_name= leaf register name
                - register_range= leaf register array_range, () if not array
                - register_index= leaf register array_index, () if not array
            if called from register/regmap ARRAY, report in leaf_is_array
            return info is organized as dictionary with entry: {
                    "regmap_entry_list", "regmap_name", "regmap_range",
                    "regmap_index", "register_entry", "register_range", "register_index",
                    "leaf_is_array"
            }
        """

        # initial call can be done on any of REG/REG_ARRAY/REGMAP/REGMAP_ARRAY or relevant _END version
        # then when walking through parent list to root, only REGMAP entry should be reached
        # whatever try to handle all cases of starting point

        # recursion break condition
        if self.type == MapTableType.ROOT:
            return { # empty contribution when reaching root entry
                'regmap_entry_list' : [],
                'regmap_name_list' : [],
                'regmap_range_list' : [],
                'regmap_index_list' : [],
                'register_entry' : None,
                'register_name' : "",
                'register_range' : (),
                'register_index' : (),
                'leaf_is_regmap_array' : False,
                'leaf_is_register_array' : False,
            }

        # unexpected starting point for call : just return parent info
        if self.type in [ MapTableType.PRE_PAD, MapTableType.POST_PAD, \
                          MapTableType.ALIGN_PAD, MapTableType.REGMAP_LENGTH_ALIGN_PAD]:
            return self.parent.get_hierarchy_info()

        # called from regmap* type
        # recursive call to get parent info, then concatenate to current regmap info
        if self.type in [MapTableType.REG_MAP,       MapTableType.REG_MAP_END, \
                         MapTableType.REG_MAP_ARRAY, MapTableType.REG_MAP_ARRAY_END]:
            regmap_parent_info = self.parent.get_hierarchy_info()
            regmap_parent_info['regmap_entry_list'] += [self.register]
            regmap_parent_info['regmap_name_list']  += [self.register.name]
            regmap_parent_info['regmap_range_list'] += [self.layer_range]
            regmap_parent_info['regmap_index_list'] += [self.layer_index]
            # register entries unchanged : 'register_entry','register_range','register_index'
            if self.type in [MapTableType.REG_MAP_ARRAY, MapTableType.REG_MAP_ARRAY_END]:
                regmap_parent_info['leaf_is_regmap_array'] = True

            return regmap_parent_info

        # called from reg* type
        # recursive call to get parent info, then concatenate to current register info
        if self.type in [MapTableType.REG, \
                         MapTableType.REG_ARRAY, MapTableType.REG_ARRAY_END]:
            register_parent_info = self.parent.get_hierarchy_info()
            register_parent_info['register_entry'] = self.register
            register_parent_info['register_name']  = self.register.name
            register_parent_info['register_range'] = self.layer_range
            register_parent_info['register_index'] = self.layer_index
            if self.type in [MapTableType.REG_ARRAY, MapTableType.REG_ARRAY_END]:
                register_parent_info['leaf_is_register_array'] = True

            return register_parent_info

        # catchup unexpected case as error !
        raise RuntimeError("unexpected entry type found while exploring maptable entry hierarchy")

# ----------------------------------------------------------------------------

    def get_regmap_array(self, range=True, include_leaf_reg=False):
        """
            Get the array range (possibly multi dimensionnal array) of reg or regmap in hierarchy
            If called from a register (not a regmap), then extract result for the parent regmap
            If register is an array, it contribute to result only if include_leaf_reg
            If called for a regmap, this return the sequence of significant dimension (regmap is array)
            of all nested regmap from root to self
            If called on regmap1[3].regmap2.regmap3[5], return tuple as (3,5)
        """
        hier_info = self.get_hierarchy_info()
        if range:
            result_array = tuple(range for layer_range in hier_info['regmap_range_list'] for range in layer_range )
        else:
            result_array = tuple(range for layer_range in hier_info['regmap_index_list'] for range in layer_range )
        if include_leaf_reg:
            if range:
                result_array += tuple(range for range in hier_info['register_range'])
            else:
                result_array += tuple(range for range in hier_info['register_index'])

        return result_array

    def get_layer_bracket(self, range=False):
        # return [2][1] for layer_index==(2,1,) if range=False
        # return [4][2] for layer_range==(4,2,) if range=True
        layer_tuple = self.layer_range if range else self.layer_index
        return "".join(f"[{id}]" for id in layer_tuple)
    def get_layer_range_definition(self):
        # return [0:3][0:1] for layer_range==(4,2,)
        return "".join(f"[0:{id-1}]" for id in self.layer_range)

    def get_suffix_array_name(self):
        # apply to REG or REGMAP
        # verilog signal name have a special suffix when part of array signal
        # return _x2_x1 for hierarchical index==(2,1,)
        return "".join(f"_x{id}" for id in self.get_regmap_array(range=False, include_leaf_reg=True))
    def get_array_range_definition(self):
        # verilog signal name instanciation have a range defined
        # return [3:0][1:0] for hierarchy range==(4,2,)
        return "".join(f"[{id-1}:0]" for id in self.get_regmap_array(range=True, include_leaf_reg=True))
    def get_array_index_bracket(self):
        # verilog signal name instanciation is an array in interface
        # return [2][1] for hirarchy index==(2,1,)
        return "".join(f"[{id}]" for id in self.get_regmap_array(range=False, include_leaf_reg=True))

    def get_all_register_array_entry(self):
        # given one register, find all array entry that it is part of
        if self.type != MapTableType.REG:
            raise  RuntimeError("Entry should be register, unexpected error !")
        # print(f"Lookup all register entry for {self.get_summary()}")
        root_map_table = self.get_root_entry().register.map_table
        all_reg_entry = [entry for entry in root_map_table if entry.type==MapTableType.REG]
        return [entry for entry in all_reg_entry if entry.register == self.register]

# ----------------------------------------------------------------------------


    @staticmethod
    def resolve_index(reg_path_part, array_index):
        # helper function for resolve_field_path
        # there is at least one braquet pair with "%" in it (like reg[2*%0+1])

        # local functions
        def resolve_percent(m):
            index_id = int(m.group(2))
            #print("Found %:", index_id, " with index:", array_index)
            if index_id >= len(array_index):
                # print(f"Found %{index_id}, with index:{array_index} in {reg_path_part}")
                raise RuntimeError(f"Found alias index out of range. Index=%{index_id}, max is {len(array_index)-1}")
            return m.group(1) + f"{array_index[index_id]}" + m.group(3)

        # local functions
        def resolve_inside_braquet(m):
            inside_braquet = m.group(2)
            # print(f"Found inside_braquet:1<{m.group(1)}>:{inside_braquet}:3<{m.group(3)}>", " with index:", array_index)

            # substitute %0, %1, ... with value in array_index[0],[1], ...
            inside_braquet = re.sub(r"(.*?)%(\d)(.*?)", resolve_percent, inside_braquet)

            # inside_braquet now expected to be expression that can eval to integer
            inside_braquet_value = eval(inside_braquet)

            result = m.group(1) + f"{inside_braquet_value}" + m.group(3)
            # print(f"Result inside_braquet: <{result}>")
            return result

        # print("resolve_index on ", reg_path_part, " with index:", array_index)
        # remove space, tab
        reg_path_part = re.sub(r"\s+", '', reg_path_part)
        # print("resolve_index space purge:", reg_path_part)

        # multidimension array may be use, iterate on each dimension,
        # use "[" and "," as start separator
        # use "," and "]" as start separator
        # this should match "field" inside 4 patterns:  [field] [field, ,field, ,field]
        # only field that match %\d inside are processed
        # Processing is done with local function that can access the local array_index scope
        # Processing is done calling resolve_inside_braquet for each matching pattern in re.sub
        while True:
            fixed_reg_path_part = re.sub(r"([\[,])([^\[,\]]*?%\d[^\[,\]]*?)([,\]])", resolve_inside_braquet, reg_path_part)
            # print("resolve_index changed to :", reg_path_part, " with index:", array_index)
            if(reg_path_part != fixed_reg_path_part):
                reg_path_part = fixed_reg_path_part
            else:
                break
        # from sys import exit
        # exit()

        return reg_path_part

    def resolve_field_path(self, alias_path):
        # print(f"resolve_field_path: wd={self.register.name}, path={alias_path}")

        # resolve alias should be called from a source alias field of a register
        # self must be the REG regmap entry of register containing the alias field
        if self.type != MapTableType.REG:
            raise  RuntimeError("Entry should be register, unexpected error !")

        # alias path is a set of '/' separated item
        # '.', '..' are relative step from current entry
        # "reg", "reg[#]" are child step to next entry register (from a regmap entry)
        # "field" is target field for a reg entry

        # in reg[#], where '#' can be a python expression that resolve to integer
        # current register array_index can be used as %0, %1, ... and are eval to get target index value
        # if current register is regmap1[3].regmap2.regmap3[5], array_index is the tupple (3,5)
        # %0 is replaced by 3, %1 is replace by 5
        # So if reg alias part is target[2*%0+%1] then resolve index will substitute as 2*3+5-->11 so alias part is target[11]

        array_index = self.get_regmap_array(range=False, include_leaf_reg=True)

        # print("Searching alias for field in reg:"+self.get_regmap_hierarchy())
        # print("Index of current register to alias:", array_index)

        path_part = alias_path.split('/')

        if (path_part[0] == ""):
            # path starts with a '/'
            current_entry = self.get_root_entry()
            path_part.pop(0)
            # print(f"Found /, remain = {path_part}" )
        else:
            # start with current reg (., .., reg)
            current_entry = self

        if len(path_part) == 0:
            raise RuntimeError(f"Found empty item to parse in remaining path. Starting from {alias_path}")

        field_part = path_part[-1]
        path_part.pop(-1) # remove last item

        if field_part == '':
            raise RuntimeError(f"Found empty item at end of path path, looking for field. Starting from {alias_path}")

        for part in path_part:
            child_str = [child for child, reg in current_entry.child_list.items()]

            # print("Starting field search from base reg:"+current_entry.register.name+'#'+current_entry.type.name+str(current_entry.layer_index))
            # print(f"Field = {field_part}, Searching for '{part}', known child are : {child_str}")

            if part == '.':
                # print("'.' is just ignored")
                continue
            if part == '..':
                # print("Processing to parent")
                current_entry = current_entry.parent
                continue
            # part may have %0, %1, ... target field indexing from current reg index : resolve index will eval
            if re.search(r".*?\[.*?%\d.*?\]", part):
                # parsing is done only when alias indexing is involved
                part=MapTableEntry.resolve_index(reg_path_part=part, array_index=array_index)

            if part in current_entry.child_list.keys():
                current_entry = current_entry.child_list[part]
                continue
            else:
                print(f"Field = {field_part}, Searching for '{part}', known child are : {child_str}")
                raise RuntimeError(f"Unable to found child '{part}' for reg or regmap : "+\
                                   current_entry.register.name+'#'+current_entry.type.name+str(current_entry.layer_index))

        # reach target entry register, final checks
        if current_entry.type != MapTableType.REG:
            raise RuntimeError(f"Alias path failed to find a register. Found {current_entry.type.name}")

        field_name_list_in_target_reg = [f.name for f in current_entry.register.get_all_actual_field()]
        if not field_part in field_name_list_in_target_reg:
            raise RuntimeError(f"Alias path failed to find field inside target register."\
                              f" Looking for {field_part}, found {field_name_list_in_target_reg}")


        target_entry = current_entry
        target_field_name = field_part
        return (target_entry, target_field_name)

# ----------------------------------------------------------------------------
