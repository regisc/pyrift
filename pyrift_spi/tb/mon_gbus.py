# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge, FallingEdge, NextTimeStep, First, Event
from cocotb.result import TestFailure

from tb_shared import delay_ns

# ----------------------------------------------------------------------------

# gbus transaction monitoring
evt_gbus_start = Event() # on start req rising (no rdata yet)
evt_gbus_complete = Event() # on transaction finalisation at req falling (with rdata when read)

gbus_transaction_check_count = 0
gbus_rdata_transaction_check_count = 0
gbus_wdata_transaction_check_count = 0

# ----------------------------------------------------------------------------

async def gbus_probe(dut):
    global gbus_req
    global gbus_ack
    global gbus_address
    global gbus_wdata
    global gbus_wr
    global gbus_rd
    global gbus_be
    global gbus_rdata
    global gbus_clk

    gbus_req = dut.i_pyrift_spi_host.gbus_req
    gbus_ack = dut.i_pyrift_spi_host.gbus_ack
    gbus_address = dut.i_pyrift_spi_host.gbus_address
    gbus_wdata = dut.i_pyrift_spi_host.gbus_wdata
    gbus_wr = dut.i_pyrift_spi_host.gbus_wr
    gbus_rd = dut.i_pyrift_spi_host.gbus_rd
    gbus_be = dut.i_pyrift_spi_host.gbus_be
    gbus_rdata = dut.i_pyrift_spi_host.gbus_rdata
    #gbus_clk = dut.spi_clk_i
    gbus_clk = dut.i_pyrift_spi_host.rif_clk_i

    dut._log.info("Forking Monitoring gbus ...")
    # Run gbus monitoring concurrently
    global gbus_monitor_thread
    global gbus_monitor_rdata_thread
    gbus_monitor_thread = cocotb.fork(gbus_monitor())
    gbus_monitor_rdata_thread = cocotb.fork(gbus_monitor_rdata())

# ----------------------------------------------------------------------------

async def gbus_monitor_rdata():
    """
        Monitor task to track activity on gbus rdata:
        RIF CDC request interface is reference
        Sample rdata on rising edge of clk, last sampling on req==1 is probably the correct rdata
        (this is OK for null CDC rdata smapling)
        (adding CDC will require to adjust this sampling ...TBC)

        Collect:
            - Rdata and put it in global gbus_rdata_sampled
              (could be checked after req falling edge in main monitor)
    """
    global gbus_rdata_sampled

    gbus_req._log.info("Start Monitoring GBUS rdata ...")
    tr_id = 0
    while True:
        # trig event is rising edge of clock
        # await RisingEdge(gbus_clk)
        await FallingEdge(gbus_clk) # should be rising edge
        # but rising edge seems to fail (unless : gbus_clk = dut.spi_clk_i)
        # looks like a delta sim time added to clock when through tb (rif_clk not yet used)
        # TODO: Cleanup, in meanwhile monitor on falling edge is OK (working even if not as expected)
        #       Full rework needed for CDC addition in a clean way ...

        # req is qualifier of cycle in progress
        if gbus_req.value.integer == 1 and gbus_ack.value.integer == 1 and gbus_rd.value.integer == 1:
            # both req and ack are active, rdata should be valid
            if not gbus_rdata.value.is_resolvable:
                raise TestFailure(f"gbus_monitor_rdata: unresolved gbus_rdata sampled: {gbus_rdata.value}")
            gbus_rdata_sampled = gbus_rdata.value.integer
            gbus_req._log.info(f"Monitoring gbus rdata found transaction #{tr_id}: rdata=0x{gbus_rdata_sampled:x}")
        else:
            gbus_rdata_sampled = -1 # not a valid sampling, clear to invalid
            # gbus_req._log.info(f"Monitoring gbus rdata Invalid #{tr_id}: rd={gbus_rd.value.integer} req={gbus_req.value.integer} ack={gbus_ack.value.integer} rdata=0x{gbus_rdata_sampled:x}")


        await NextTimeStep()
        tr_id+=1

# ----------------------------------------------------------------------------

async def gbus_monitor():
    """
        Monitor task to track activity on gbus : RIF CDC request interface to identify the requested transaction
        Collect:
            - Address
            - Write/Read
            - Write BE
            - Wdata
            - Rdata
        Collected transaction is set to global evt, then clear one time step later
    """
    gbus_req._log.info("Start Monitoring GBUS ...")
    tr_id = 0
    while True:
        # trig event is rising edge on request to CDC layer
        await RisingEdge(gbus_req)
        # req activated as same time as gbus info (at least not earlier)
        # allow one ns to delay the sampling of info for transaction
        await delay_ns(1)

        # Now gbus should be stable, sample as needed
        if not gbus_address.value.is_resolvable:
            raise TestFailure(f"gbus_monitor: unresolved gbus_address sampled: {gbus_address.value}")
        if not gbus_wr.value.is_resolvable:
            raise TestFailure(f"gbus_monitor: unresolved gbus_wr sampled: {gbus_wr.value}")
        if not gbus_be.value.is_resolvable:
            raise TestFailure(f"gbus_monitor: unresolved gbus_be sampled: {gbus_be.value}")

        monitor_data = {
            'tr_id':tr_id,
            'address':gbus_address.value.integer,
            'write':gbus_wr.value.integer,
            'write_be':gbus_be.value.integer,
            'wdata':gbus_wdata.value.integer
        }
        gbus_req._log.info(f"Monitoring gbus found req transaction #{tr_id}: {', '.join([f'{k}:{v:x}' for k,v in monitor_data.items()])}")
        evt_gbus_start.set(data=monitor_data)
        await NextTimeStep()

        # Clear the event after 1 time step, all coroutine wait for the event should have get it already
        evt_gbus_start.clear()
        # Now ready for next transaction monitoring

        await FallingEdge(gbus_req)
        await delay_ns(1)

        # after falling edge of req, the gbus_monitor_rdata should have already sampled the rdata
        # in global : gbus_rdata_sampled

        monitor_data['rdata'] = gbus_rdata_sampled
        gbus_req._log.info(f"Monitoring gbus found req finalization #{tr_id}: {', '.join([f'{k}:{v:x}' for k,v in monitor_data.items()])}")
        evt_gbus_complete.set(data=monitor_data)
        await NextTimeStep()
        # Clear the event after 1 time step, all coroutine wait for the event should have get it already
        evt_gbus_complete.clear()
        # Now ready for next transaction monitoring

        # waste time to get ready for next edge
        await NextTimeStep()
        tr_id+=1

# ----------------------------------------------------------------------------

async def gbus_start_check_task(rif_chk=None, msg=""):
    global gbus_transaction_check_count
    global gbus_rdata_transaction_check_count
    global gbus_wdata_transaction_check_count

    gbus_req._log.info(f"gbus_start_check_task : launched against: {', '.join([f'{k}:{v:x}' for k,v in rif_chk.items()])}")
    timeout = Timer(5000, 'ns') # 5 bytes max of 1us, should allow completion
    result = await First(evt_gbus_start.wait(),timeout)
    if result==timeout:
        raise TestFailure(f"gbus_start_check_task: evt_gbus_start timeout: {', '.join([f'{k}:{v:x}' for k,v in rif_chk.items()])}")
    actual_gbus_transaction = evt_gbus_start.data
    gbus_req._log.info(f"Trig gbus start transaction: {', '.join([f'{k}:{v:x}' for k,v in actual_gbus_transaction.items()])}")

    # check available values (missing in dict mean ignore check)
    if 'address' in rif_chk:
        # user may ask for check as negative value, convert to unsigned 32 bit before check
        if rif_chk['address'] <0:
            rif_chk['address'] += 2**32
        if rif_chk['address'] != actual_gbus_transaction['address']:
            raise TestFailure(f"gbus_start_check_task: transaction address mismatch detected:"
                              f" found=0x{actual_gbus_transaction['address']:08X}, expected=0x{rif_chk['address']:08X}")
    if 'write' in rif_chk:
        gbus_req._log.info(f"gbus_start_check_task : Checking write option")
        # check for read or write option
        if rif_chk['write'] != actual_gbus_transaction['write']:
            raise TestFailure(f"gbus_start_check_task: transaction r/w mismatch detected:"
                              f" found={actual_gbus_transaction['write']}, expected={rif_chk['write']}")
        if rif_chk['write']:
            # write have special checks to do : byte enable
            if 'write_be' in rif_chk:
                if rif_chk['write_be'] != actual_gbus_transaction['write_be']:
                    raise TestFailure(f"gbus_start_check_task: transaction write BE mismatch detected:"
                                      f" found={actual_gbus_transaction['write_be']}, expected={rif_chk['write_be']}")
            # write may require a check of write data (write data check is skipped on read)
            if 'wdata' in rif_chk:
                if rif_chk['wdata'] != actual_gbus_transaction['wdata']:
                    raise TestFailure(f"gbus_start_check_task: transaction wdata mismatch detected:"
                                      f" found={actual_gbus_transaction['wdata']:02X}, expected={rif_chk['wdata']:02X}")
                gbus_wdata_transaction_check_count += 1

    # rdata is collected in another event, wait on it only if rdata check is required
    if 'rdata' in rif_chk:
        gbus_req._log.info(f"gbus_start_check_task : Checking rdata option")
        timeout = Timer(5000, 'ns') # max of 1us, should allow completion of CDC
        result = await First(evt_gbus_complete.wait(),timeout)
        if result==timeout:
            raise TestFailure(f"gbus_start_check_task: evt_gbus_complete rdata check timeout: "
                              f"{', '.join([f'{k}:{v:x}' for k,v in rif_chk.items()])}")
        actual_gbus_transaction = evt_gbus_complete.data
        gbus_req._log.info(f"gbus_start_check_task : evt_gbus_complete={', '.join([f'{k}:{v:x}' for k,v in evt_gbus_complete.data.items()])}")
        if rif_chk['rdata'] != actual_gbus_transaction['rdata']:
            raise TestFailure(f"gbus_start_check_task: transaction rdata mismatch detected:"
                              f" found={actual_gbus_transaction['rdata']:02X}, expected={rif_chk['rdata']:02X}")
        gbus_rdata_transaction_check_count += 1

    gbus_transaction_check_count += 1

    gbus_req._log.info(f"gbus_start_check_task : Completed")

# ----------------------------------------------------------------------------

def gbus_monitor_report(dut):
    dut._log.info("Gbus Monitoring Report:")
    dut._log.info(f"    Total Gbus transaction checked : {gbus_transaction_check_count}")
    dut._log.info(f"    Total Gbus wdata transaction checked : {gbus_wdata_transaction_check_count}")
    dut._log.info(f"    Total Gbus rdata transaction checked : {gbus_rdata_transaction_check_count}")

# ----------------------------------------------------------------------------
