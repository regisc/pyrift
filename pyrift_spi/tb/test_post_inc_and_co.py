# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb.py
# Check how rif access impact pointer values (base/last) following pre/post inc/dec
# Randomize command, offset unit, step unit, offset
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_monitor import spi_monitor_report
from tb_monitor import spi_rx_check_task, spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report, gbus_monitor_report

from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from drv_reset import reset_driver_connect, reset_dut

# ----------------------------------------------------------------------------

async def randomize_offset_unit():
    """
        randomize last_offset_unit and base_offset_unit in CFG[4]
        range is 0..7 in CFG or 1.2.4,...,128 bytes actual offset
    """
    # randomize, but have more probable 1B offset (trig more side effect on BE odd/even addressing)
    last_offset_unit = random.choice([0,0,0,0, 0,0,0,0, 1,2,3, 4,5,6,7])
    base_offset_unit = random.choice([0,0,0,0, 0,0,0,0, 1,2,3, 4,5,6,7])
    # Set offset unit to 1<<(0..7)
    # (replace default setting as DW in byte unit
    # [4][7:4]: Last Offset unit 3b,
    # [4][3:0]: Base Offset unit 3b
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+4, (last_offset_unit<<4) | base_offset_unit] )

    return (1<<last_offset_unit,1<<base_offset_unit)

# ----------------------------------------------------------------------------

@cocotb.test()
async def test_post_inc_and_co(dut):
    """Try RIF access and check base/last pointer updates"""

    # get info about limited bitwidth for hw ressources
    # register may be less 32 bits for low gate count if not needed
    RIF_ADDR_BW = dut.RIF_ADDR_BW.value.integer
    BASE_PTR_BW = dut.BASE_PTR_BW.value.integer
    LAST_PTR_BW = dut.LAST_PTR_BW.value.integer
    RIF_ADDRESS_MASK = (1<<RIF_ADDR_BW)-1;
    BASE_PTR_MASK = (1<<BASE_PTR_BW)-1;
    LAST_PTR_MASK = (1<<LAST_PTR_BW)-1;

    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_monitor_prepare(
        clk=dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_driver_set_csn(0)
    await gbus_probe(dut)
    dut._log.info("Running test_post_inc_and_co test!")
    dut._log.info(f"RIF_ADDRESS_MASK=0x{RIF_ADDRESS_MASK:x}")
    dut._log.info(f"BASE_PTR_MASK=0x{BASE_PTR_MASK:x}")
    dut._log.info(f"LAST_PTR_MASK=0x{LAST_PTR_MASK:x}")

    # Switch to 8 bit data cycle on rif
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+1, 0] ) # [1][1:0] = access width = 0 = 8bit, [1][2]=burst mode =0
    # Set inc step to 1B (replace default from 2 byte as DW is 16 to 1 byte)
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+2, 0] ) # [2]Base Inc step
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+3, 0] ) # [3]Last Inc step
    # Set offset unit to 1B (replace default from 2 byte as DW is 16 to 1 byte)
    last_offset_unit,base_offset_unit = 1,1
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+4, (0<<4) | 0] )

    dut._log.info(f"Initial check of Base offset write access[2] : even address")
    await spi_command(cmd=[SPI_CMD_RIF_WR_BASE_AT+2, 0x22],
        rif_chk={'address':2*base_offset_unit, 'write':1, 'write_be':0b01, 'cycle':1}
    )
    dut._log.info(f"Initial check of Base offset write access[3] : odd address")
    await spi_command(cmd=[SPI_CMD_RIF_WR_BASE_AT+3, 0x33],
        rif_chk={'address':3*last_offset_unit, 'write':1, 'write_be':0b10, 'cycle':1}
    )
    # here last pointer is 3
    current_last = 3 & LAST_PTR_MASK
    write = 1
    target_address = current_last-5*1
    write_be = (1 << (target_address&1)) * write

    # rif wr[-5], data=0xD0
    await spi_command(
        cmd=[SPI_CMD_RIF_WR_LAST_AT+16-5, 0xD0],
        rif_chk={'address':(current_last-5*1) & RIF_ADDRESS_MASK, 'write':1, 'write_be':write_be, 'cycle':1}
    )
    # here last pointer is -2
    current_last = (current_last-5*1) & LAST_PTR_MASK

    # Check several access, checking gbus transaction is as expected (base on current base/last, and adressing mode used)
    current_base = 0
    # current_last already set to -2

    for iteration in range(100):
        last_offset_unit, base_offset_unit = await randomize_offset_unit()
        dut._log.info(f"Offset size in cfg is Base={base_offset_unit:04X} Last={last_offset_unit:04X}")
        base_inc_step = random.randint(1,256)
        await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+2, base_inc_step-1] ) # [2]Base Inc step
        last_inc_step = random.randint(1,256)
        await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+3, last_inc_step-1] ) # [3]Last Inc step
        dut._log.info(f"Inc step in cfg is Base={base_inc_step:02X} Last={last_inc_step:02X}")
        #randomize the access mode command
        command = random.choice([
            # exclude Read so far (only write implemented)
            SPI_CMD_RIF_RD_BASE_AT, SPI_CMD_RIF_RD_LAST_AT,
            SPI_CMD_RIF_RD_BASE_POSTINC, SPI_CMD_RIF_RD_BASE_POSTDEC,
            SPI_CMD_RIF_RD_LAST_POSTINC, SPI_CMD_RIF_RD_LAST_POSTDEC,
            SPI_CMD_RIF_RD_LAST_PREINC, SPI_CMD_RIF_RD_LAST_PREDEC,
            SPI_CMD_RIF_WR_BASE_AT, SPI_CMD_RIF_WR_LAST_AT,
            SPI_CMD_RIF_WR_BASE_POSTINC, SPI_CMD_RIF_WR_BASE_POSTDEC,
            SPI_CMD_RIF_WR_LAST_POSTINC, SPI_CMD_RIF_WR_LAST_POSTDEC,
            SPI_CMD_RIF_WR_LAST_PREINC, SPI_CMD_RIF_WR_LAST_PREDEC
        ])

        post_base = current_base # post inc/dec value is current value if not changed by any command
        post_last = current_last # post inc/dec value is current value if not changed by any command
        # base addressing (offset is 5 bit unsigned : 0..31)
        if command in [SPI_CMD_RIF_RD_BASE_AT, SPI_CMD_RIF_WR_BASE_AT]:
            write = 1 if command == SPI_CMD_RIF_WR_BASE_AT else 0
            target_address = current_base
            offset = random.randint(0,31)
            command += offset
            target_address += offset*base_offset_unit
            write_be = (1 << (target_address&1)) * write
            post_base = current_base # no auto inc
            post_last = target_address & LAST_PTR_MASK # current access gives future last

        # last addressing (offset is 4 bit signed : -8..+7)
        if command in [SPI_CMD_RIF_RD_LAST_AT, SPI_CMD_RIF_WR_LAST_AT]:
            write = 1 if command == SPI_CMD_RIF_WR_LAST_AT else 0
            target_address = current_last
            offset = random.randint(-8,7)
            command += offset & 0xF # cast signed integer to 4 bit unsigned
            target_address += offset*last_offset_unit
            write_be = (1 << (target_address&1)) * write
            post_base = current_base # no auto inc
            post_last = target_address & LAST_PTR_MASK # current access gives future last

        # Post base addressing
        if command in [ SPI_CMD_RIF_RD_BASE_POSTINC, SPI_CMD_RIF_RD_BASE_POSTDEC,
                        SPI_CMD_RIF_WR_BASE_POSTINC, SPI_CMD_RIF_WR_BASE_POSTDEC]:
            write = 1 if command in [SPI_CMD_RIF_WR_BASE_POSTINC, SPI_CMD_RIF_WR_BASE_POSTDEC] else 0
            target_address = current_base
            write_be = (1 << (target_address&1)) * write
            if command in [SPI_CMD_RIF_RD_BASE_POSTDEC, SPI_CMD_RIF_WR_BASE_POSTDEC]:
                post_base = target_address - base_inc_step # predict auto inc for base
            if command in [SPI_CMD_RIF_RD_BASE_POSTINC, SPI_CMD_RIF_WR_BASE_POSTINC]:
                post_base = target_address + base_inc_step # predict auto inc for base
            post_last = target_address & LAST_PTR_MASK  # current access gives future last

        # Post last addressing
        if command in [ SPI_CMD_RIF_RD_LAST_POSTINC, SPI_CMD_RIF_RD_LAST_POSTDEC,
                        SPI_CMD_RIF_WR_LAST_POSTINC, SPI_CMD_RIF_WR_LAST_POSTDEC]:
            write = 1 if command in [SPI_CMD_RIF_WR_LAST_POSTINC, SPI_CMD_RIF_WR_LAST_POSTDEC] else 0
            target_address = current_last
            write_be = (1 << (target_address&1)) * write
            post_base = current_base # no auto inc on base
            if command in [SPI_CMD_RIF_RD_LAST_POSTDEC, SPI_CMD_RIF_WR_LAST_POSTDEC]:
                post_last = target_address - last_inc_step # current access gives future base
            if command in [SPI_CMD_RIF_RD_LAST_POSTINC, SPI_CMD_RIF_WR_LAST_POSTINC]:
                post_last = (target_address + last_inc_step) & LAST_PTR_MASK # current access gives future base

        # Pre last addressing
        if command in [ SPI_CMD_RIF_RD_LAST_PREINC, SPI_CMD_RIF_RD_LAST_PREDEC,
                        SPI_CMD_RIF_WR_LAST_PREINC, SPI_CMD_RIF_WR_LAST_PREDEC]:
            write = 1 if command in [SPI_CMD_RIF_WR_LAST_PREINC, SPI_CMD_RIF_WR_LAST_PREDEC] else 0
            target_address = current_last
            if command in [SPI_CMD_RIF_RD_LAST_PREINC, SPI_CMD_RIF_WR_LAST_PREINC]:
                target_address += last_inc_step
            if command in [SPI_CMD_RIF_RD_LAST_PREDEC, SPI_CMD_RIF_WR_LAST_PREDEC]:
                target_address -= last_inc_step
            write_be = (1 << (target_address&1)) * write
            post_base = current_base # no auto inc on base
            post_last = target_address & LAST_PTR_MASK  # pre inc/dec, post_last is the current address used

        # get sure offset overflow are wrapped as in hw
        # clip due to limited bitwidth of hw
        target_address = target_address & 0xFFFF_FFFF if target_address>=0 else target_address + 2**32
        target_address = target_address & RIF_ADDRESS_MASK

        cmd_array = [command]
        if write:
            wdata = random.randint(0,255)
            cmd_array.append(wdata)
            # write is 8 bits to 16 bit bus
            chk_wdata_16b = wdata if (target_address&1)==0 else( wdata<<8)
            # rif_rdata_16b = -1 # invalid, should not be checked on write
        else:
            # for read on spi, no piped of next command, pad with a NOP to slave while slave sending back data
            cmd_array.append(SPI_CMD_NOP)
            chk_wdata_16b = 0
            # if target_address&1 >=0 and target_address&1<128:
            #     # only target in rif_test is checked
            #     rif_rdata_16b = (((target_address&1)+1)<<8) + (target_address&1)
            # else:
            #     rif_rdata_16b = -1 # invalid address, should not be checked on read

        dut._log.info(f"Launching from current Base={current_base:04X} Last={current_last:04X}")
        dut._log.info(f"Launching random commmand {', '.join([f'{d:02X}' for d in cmd_array])}")
        rif_chk={
            'address': target_address,
            'write':write, 'write_be':write_be, 'wdata':chk_wdata_16b,
            'cycle':write # on read cycle, the req is issued with command, on write req is issue after wdata is collected
        }
        # rif content is not tracked for write, so not yet able to track rdata
        # (we don't know the current content of rif at read time)
        # if not write and rif_rdata_16b != -1:
        #     # check read only if read cycle within the rif_test address range
        #     rif_chk['rdata'] = rif_rdata_16b

        await spi_command(
            cmd=cmd_array,
            rif_chk=rif_chk
        )

        # post incrent/decrement may have changed the base/last address after command launched
        # keep track of current pointer to get aligned with rtl for next transfer
        # keep current a positive address
        current_base = post_base & 0xFFFF_FFFF if post_base>=0 else post_base + 2**32
        current_last = post_last & 0xFFFF_FFFF if post_last>=0 else post_last + 2**32
        current_base = current_base & BASE_PTR_MASK
        current_last = current_last & LAST_PTR_MASK

        # Check the base/last pointer update correctly inside rtl
        # check directly with backdoor register access
        if dut.i_pyrift_spi_host.i_pyrift_spi_bridge.cfg_reg8_base_addr.value.integer != current_base:
            raise TestFailure(
                f"Unexpected value in Base pointer : "
                f"backdoor read={dut.i_pyrift_spi_host.i_pyrift_spi_bridge.cfg_reg8_base_addr.value.integer:08X} "
                f"expected={current_base:08X}")
        if dut.i_pyrift_spi_host.i_pyrift_spi_bridge.cfg_reg12_last_addr.value.integer != current_last:
            raise TestFailure(
                f"Unexpected value in Last pointer : "
                f"backdoor read={dut.i_pyrift_spi_host.i_pyrift_spi_bridge.cfg_reg12_last_addr.value.integer:08X} "
                f"expected={current_last:08X}")


    await spi_command(SPI_CMD_NOP) # NOP

    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done test_post_inc_and_co test!")

# ----------------------------------------------------------------------------
