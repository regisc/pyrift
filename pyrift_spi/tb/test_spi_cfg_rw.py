# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb.py
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from tb_monitor import spi_monitor_report
from tb_monitor import spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report
from drv_reset import reset_driver_connect, reset_dut

# ----------------------------------------------------------------------------

@cocotb.test()
async def test_spi_cfg_rw(dut):
    """Try accessing the CFG register in spi IP."""

    # get info about limited bitwidth for hw ressources
    # register may be less 32 bits for low gate count if not needed
    BASE_PTR_BW = dut.BASE_PTR_BW.value.integer
    LAST_PTR_BW = dut.LAST_PTR_BW.value.integer
    BASE_PTR_MASK = (1<<BASE_PTR_BW)-1;
    LAST_PTR_MASK = (1<<LAST_PTR_BW)-1;


    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_monitor_prepare(
        clk=dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_driver_set_csn(0)
    dut._log.info("Running SPI CFG test!")

    # rif wr[0], data=0x1234
    await spi_command(cmd=[SPI_CMD_RIF_WR_BASE_AT+0, 0x12, 0x34])

    await spi_command(SPI_CMD_NOP) # NOP

    # CFG wr (can be 1 byte/2 byte/4 byte sequence)
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+0xB, 0x9A]) # Wr8(B)
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+0xA, 0x78]) # Wr8(A)
    await spi_command(cmd=[SPI_CMD_CFG_WR_16_AT+8, 0x34, 0x56]) # Wr16(8)
    await spi_command(cmd=[SPI_CMD_CFG_WR_32_AT+0xC, 0x11, 0x22, 0x33, 0x44]) # Wr32(C)

    await spi_command(SPI_CMD_NOP) # NOP

    # expected base, last may be truncated due to narrow hw (assume little endian option used)
    expected_base_at_8 = 0x9A785634 & BASE_PTR_MASK
    expected_last_at_12 = 0x44332211 & LAST_PTR_MASK

    # CFG
    await spi_command(
        cmd=[
            SPI_CMD_CFG_RD_AT+0x0C,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT], # Rd(12, next, next, next, next)
        chk=[
            (expected_last_at_12 >>  0) & 0xFF,
            (expected_last_at_12 >>  8) & 0xFF,
            (expected_last_at_12 >> 16) & 0xFF,
            (expected_last_at_12 >> 24) & 0xFF,
            0x12], # Read 4B of Last ptr, then Test of CFG registers [12],[13],[14],[15],[0]
        msg="Reading CFG Regs")
    await spi_command(cmd=SPI_CMD_CFG_RD_AT+0x0B, chk=(expected_base_at_8 >> 24) & 0xFF, msg="CFG read B") # Rd(B)
    await spi_command(cmd=SPI_CMD_CFG_RD_AT+0x0A, chk=(expected_base_at_8 >> 16) & 0xFF, msg="CFG read A") # Rd(A)
    await spi_command(cmd=SPI_CMD_CFG_RD_AT+0x09, chk=(expected_base_at_8 >>  8) & 0xFF, msg="CFG read 9") # Rd(9)
    await spi_command(cmd=SPI_CMD_CFG_RD_AT+0x08, chk=(expected_base_at_8 >>  0) & 0xFF, msg="CFG read 8") # Rd(8)

    await spi_command(SPI_CMD_NOP) # NOP

    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done SPI CFG test!")

# ----------------------------------------------------------------------------
