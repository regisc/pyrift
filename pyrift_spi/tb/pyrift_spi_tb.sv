// ----------------------------------------------------------------------------
// pyrift SPI host interface testing test bench
// This is the top level TB in verilog
// Instanciate :
//  - the dut : pyrift_spi_bridge
//  - the test rif : ...
//  - CDC interface between bridge and RIF : TBD
//
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_spi_tb (
   input  var logic por_rst_ni   , // Async active low reset
   input  var logic spi_clk_i    , // SPI reference clock from master
   //
   input  var logic spi_cs_ni    , // SPI selection from master (active low)
   input  var logic spi_mosi_i   , // SPI Data from master
   output var logic spi_miso_o   , // SPI Data to master
   output var logic spi_miso_oe_o  // SPI Data output enable control to master
);

// ----------------------------------------------------------------------------

localparam DW =16;
localparam RIF_ADDR_BW /*verilator public_flat_rd*/ = 8;
localparam BASE_PTR_BW /*verilator public_flat_rd*/= 8;
localparam LAST_PTR_BW /*verilator public_flat_rd*/= 8;
// ----------------------------------------------------------------------------

logic [3:0] gpo; // GPO from spi bridge, unused
logic [3:0] gpi; // GPI to spi bridge, unused
assign gpi = 4'h5; // dummy value if to be tested

//
IF_GENERIC_BUS  #(
    .DW(DW)
) if_generic(spi_clk_i);

pyrift_spi_host #(
   .RIF_ADDR_BW(RIF_ADDR_BW),
   .BASE_PTR_BW(BASE_PTR_BW),
   .LAST_PTR_BW(LAST_PTR_BW),
   .CDC        (0          ),
   .BE         (0          ),
   .DW         (DW         )
) i_pyrift_spi_host (
   .por_rst_ni   (por_rst_ni   ),
   //
   .spi_clk_i    (spi_clk_i    ),
   .spi_cs_ni    (spi_cs_ni    ),
   .spi_mosi_i   (spi_mosi_i   ),
   .spi_miso_o   (spi_miso_o   ),
   .spi_miso_oe_o(spi_miso_oe_o),
   // GPIO
   .gpo_o        (gpo          ),
   .gpi_i        (gpi          ),
   // No CDC, re-used spi clock for rif
   .rif_clk_i    (spi_clk_i    ),
   .if_generic   (if_generic   )
);


// ----------------------------------------------------------------------------

IF_RIF_TEST if_rif_test();

rif_spi_test i_rif_spi_test (
   .clk        (spi_clk_i  ),
   .rst_n      (por_rst_ni ),
   .if_rif_test(if_rif_test),
   .if_generic (if_generic )
);


// ----------------------------------------------------------------------------

// if_rif_test to connect to hw user design


// ----------------------------------------------------------------------------

endmodule // pyrift_spi_tb
