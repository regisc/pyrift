# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import Timer
from cocotb.triggers import RisingEdge, FallingEdge, NextTimeStep, First, Event
from cocotb.result import TestFailure
from tb_shared import * # want all constant defined without prefix needed

# ----------------------------------------------------------------------------

# Create a glowbal event for spi monitor to report valid Rx byte detected
evt_spi_rx_byte = Event()

spi_rx_byte_check_count = 0

# ----------------------------------------------------------------------------

async def spi_monitor():
    """
        Basic monitoring probe on Rx SPI
        This track cs as reset, clock pins of SPI interface
        Collect received byte
        Incorrect MISO OE or containing X based value are returned as -1
    """

    spi_cs_n._log.info("Start Monitoring spi ...")
    # clk_rising =  RisingEdge(spi_clk)
    # rst_fall =  FallingEdge(spi_cs_n)
    edge_count = 0
    monitor_data = 0
    oe_checked_so_far = True
    while True:
        # spi_cs_n._log.info("spi_monitor waiting for event ...")
        # edge count increment on rising edge of clock
        # But reset on cs=1
        clk_rising =  RisingEdge(spi_clk)
        rst_fall =  FallingEdge(spi_cs_n)
        evt = await First(clk_rising, rst_fall)
        # spi_cs_n._log.info("spi_monitor event Trigged")
        if (evt == rst_fall):
            #cs \_ : reset count
            edge_count = 0
            monitor_data = 0
            oe_checked_so_far = True
            spi_cs_n._log.info("Monitoring spi reseting")
        if(spi_cs_n.value != 0):
            # clock edge while cs==0, not usable
            edge_count = 0
            monitor_data = 0
            oe_checked_so_far = True
            # spi_cs_n._log.info("Monitoring spi clock edge while active reset (cs_n==0)")
        elif (evt == clk_rising):
            # spi_cs_n._log.info("Monitoring spi clock edge")
            # clock rising edge with cs_n==1, thsi is a valid clock for rx data
            edge_count = (edge_count+1) % 8
            monitor_data = (monitor_data << 1) | spi_miso.value
            # check MISO_OE ....
            oe_checked_so_far &= spi_miso_oe.value
            if edge_count == 0:
                # spi_cs_n._log.info(f"Monitoring spi byte complete with OE checked : {monitor_data:02x}")
                evt_spi_rx_byte.set(data=(monitor_data if oe_checked_so_far else -1))
                await NextTimeStep()
                # Clear the event after 1 time step, all coroutine wait for the event should have get it already
                evt_spi_rx_byte.clear()
                # Now ready for next byte monitoring

                # data extracted, get ready for next byte
                monitor_data = 0
                oe_checked_so_far = True


        # waste time to get ready for next edge
        await NextTimeStep()

        # spi_cs_n._log.info("Monitoring spi rising edge")

# ----------------------------------------------------------------------------

async def spi_rx_check_task(chk=None, msg=""):
    global spi_rx_byte_check_count

    spi_clk._log.info(f"spi_rx_check_task : launched against:{', '.join([f'{d:02X}' for d in chk])}")
    for expected_rx_byte in chk:
        timeout = Timer(2000, 'ns') # max of 10 bit * 200 ns clock period, should allow completion of byte rx
        result = await First(evt_spi_rx_byte.wait(),timeout)
        if(result == timeout):
            raise TestFailure(f"spi_rx_check_task:{msg}:Byte monitored Timeout")

        actual_rx_byte = evt_spi_rx_byte.data

        # count number checks processed by monitor (increment global var for status report at end of test)
        spi_rx_byte_check_count+= 1

        spi_clk._log.info(f"spi_rx_check_task:Trig Rx byte event 0x{actual_rx_byte:02X}")
        if actual_rx_byte != expected_rx_byte:
            raise TestFailure(f"spi_rx_check_task:{msg}:Byte monitored on spi is unexpected :"
                              f" read=0x{actual_rx_byte:02x}, expected=0x{expected_rx_byte:02x}")

        # Allow the monitor some time step to clear the event, and get sure the next trig is on the next rx_byte
        await NextTimeStep()
        await NextTimeStep()
    spi_clk._log.info(f"spi_rx_check_task : Completed")

    # TODO: have a check that rx check task have properly complete ?
    #       Check may run long after the command complete, so need an extra parameter ?
    #       Or put a timeout to get sure no check task stuck in background ...

# ----------------------------------------------------------------------------

async def spi_monitor_prepare(clk, cs_n, mosi, miso, miso_oe):
    print("Running spi_monitor_prepare ...")
    global spi_clk
    global spi_cs_n
    global spi_mosi # data output master to slave, Not really used in
    global spi_miso
    global spi_miso_oe

    spi_clk = clk # dut.spi_clk_i
    spi_cs_n = cs_n # dut.spi_cs_ni
    spi_mosi = mosi # dut.spi_mosi_i
    spi_miso = miso # dut.spi_miso_o
    spi_miso_oe = miso_oe # dut.spi_miso_oe_o

    spi_cs_n._log.info("Forking Monitoring spi ...")
    # Run spi monitoring concurrently
    global spi_monitor_thread
    spi_monitor_thread = cocotb.fork(spi_monitor())

# ----------------------------------------------------------------------------

def spi_monitor_report(dut):
    dut._log.info("SPI Monitoring Report:")
    dut._log.info(f"    Total SPI Rx Byte checked : {spi_rx_byte_check_count}")

# ----------------------------------------------------------------------------
