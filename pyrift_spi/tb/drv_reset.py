# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import RisingEdge, FallingEdge, NextTimeStep, Event
from cocotb.result import TestFailure

from tb_shared import * # want all constant defined without prefix needed

# ----------------------------------------------------------------------------

async def reset_dut(pre_duration_ns, duration_ns, post_duration_ns):
    por_rst_n._log.info("Reset generation now")
    por_rst_n <= 1
    await delay_ns(pre_duration_ns)
    por_rst_n <= 0
    await delay_ns(duration_ns)
    por_rst_n <= 1
    await delay_ns(post_duration_ns)
    por_rst_n._log.info("Reset complete")

    # t= 100ns
    await delay_ns(900)
    # t= 1us

# ----------------------------------------------------------------------------

async def reset_driver_connect(rst):
    rst._log.info("Reset connecting driver")
    global por_rst_n
    por_rst_n = rst

    reset_task = cocotb.fork(reset_dut(10-1, 80, 10))

# ----------------------------------------------------------------------------
