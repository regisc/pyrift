# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is the top level TB in python using cocotb framework
# Basic SPI test agent, allow to lauch SPI command and check spi read is as expected
# This is using async for coroutine def of cocotb, this require advance python version  : 3.5 or 3.6
# and cocotb v1.4+
# ----------------------------------------------------------------------------



import cocotb

# ----------------------------------------------------------------------------

# import all test to be run in regression

from test_spi_rif_read import test_spi_rif_read
from test_spi_cfg_rw import test_spi_cfg_rw
from test_post_inc_and_co import test_post_inc_and_co
from test_spi_cfg_ptr_inc import test_spi_cfg_ptr_inc
from test_spi_rif_rw import test_spi_rif_rw

# ----------------------------------------------------------------------------

