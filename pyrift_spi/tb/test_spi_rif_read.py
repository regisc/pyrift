# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb.py
# Check read of rif register through SPI
# - 16b read : aligned address
# - 16b read : odd/even address
# - 8b read : odd/even address
# Checks are based on reset value of rif register (known vs address)
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from tb_monitor import spi_monitor_report
from tb_monitor import spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report
from drv_reset import reset_driver_connect, reset_dut

# ----------------------------------------------------------------------------

@cocotb.test()
async def test_spi_rif_read(dut):
    """
        Try Read accessing the RIF register.
    """

    dut._log.info("About to launch reset")
    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_monitor_prepare(
        clk=dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_driver_set_csn(0)
    await gbus_probe(dut)
    dut._log.info("Running SPI RIF read test!")

    # set base address to non default : B=0x10
    base_address = 0x0000_0010
    await spi_command(cmd=[SPI_CMD_CFG_WR_32_AT+0x8, 0x10, 0x00, 0x00, 0x00]) # Wr32(8) = 0x0000_0010


    dut._log.info("---------------------------------------------")
    dut._log.info("Running SPI RIF read as 16 bit aligned access")
    dut._log.info("---------------------------------------------")
    base_offset_unit =2
    for iteration in range(10):
        # rif read using 16 bit access (default at reset)
        offset_16b = base_offset_unit*random.randint(0,15) # random for even value for base[offset], offset _unit is 2 (DW/8) at reset
        # rif content is init with rif[@]=@
        expected_read_data = base_address + offset_16b
        expected_read_data_plus_1 = base_address + offset_16b + 1
        rif_addr_16b = base_address + offset_16b # Always even align address
        rif_rdata_16b = ((rif_addr_16b+1)<<8) + rif_addr_16b
        await spi_command(
            cmd=[SPI_CMD_RIF_RD_BASE_AT+offset_16b//base_offset_unit, SPI_CMD_NOP, SPI_CMD_NOP],
            chk=[expected_read_data, expected_read_data_plus_1],
            rif_chk={
                    'address': base_address + offset_16b, #get sure offset overflow are wrapped as in hw
                    'write':0, 'write_be':0,
                    'rdata':rif_rdata_16b,
                    'cycle':0 # on read cycle, the req is issued with command
                }
        )

    dut._log.info("------------------------------------------------")
    dut._log.info("Running SPI RIF read 16b as 8 bit aligned access")
    dut._log.info("------------------------------------------------")
    # change base_offset_unit
    base_offset_unit =1
    last_offset_unit =1
    # [4][7:4]: Last Offset unit 3b,
    # [4][3:0]: Base Offset unit 3b
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+4, ((0)<<4) | (0)] )
    for iteration in range(10):
        # rif read using 16 bit access (default at reset)
        offset_8b = base_offset_unit*random.randint(0,31) # random for even value for base[offset], offset _unit is 2 (DW/8) at reset
        # rif content is init with rif[@]=@
        expected_read_data = base_address + offset_8b
        # read at odd offset can not overlap to next 16b word (paddd with 0)
        print (offset_8b & 1)
        expected_read_data_plus_1 = (base_address + offset_8b + 1) if ((offset_8b & 1) == 0) else 0
        rif_addr_16b = (base_address + offset_8b) & (~1) # keep even align address
        rif_rdata_16b = ((rif_addr_16b+1)<<8) + rif_addr_16b
        await spi_command(
            cmd=[SPI_CMD_RIF_RD_BASE_AT+offset_8b//base_offset_unit, SPI_CMD_NOP, SPI_CMD_NOP],
            chk=[expected_read_data, expected_read_data_plus_1],
            rif_chk={
                    'address': base_address + offset_8b, #get sure offset overflow are wrapped as in hw
                    'write':0, 'write_be':0,
                    'rdata':rif_rdata_16b,
                    'cycle':0 # on read cycle, the req is issued with command
                }
        )

    dut._log.info("-----------------------------------------------")
    dut._log.info("Running SPI RIF read 8b as 8 bit aligned access")
    dut._log.info("-----------------------------------------------")
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+1, 0] ) # [1][1:0] = access width = 0 = 8bit, [1][2]=burst mode =0
    # change base_offset_unit
    base_offset_unit =1
    last_offset_unit =1
    # [4][7:4]: Last Offset unit 3b,
    # [4][3:0]: Base Offset unit 3b
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+4, ((0)<<4) | (0)] )
    for iteration in range(10):
        # rif read using 8 bit access (default at reset)
        offset_8b = base_offset_unit*random.randint(0,31) # random for even value for base[offset], offset _unit is 2 (DW/8) at reset
        # rif content is init with rif[@]=@
        expected_read_data = base_address + offset_8b
        rif_addr_16b = (base_address + offset_8b) & (~1) # keep even align address
        rif_rdata_16b = ((rif_addr_16b+1)<<8) + rif_addr_16b
        await spi_command(
            cmd=[SPI_CMD_RIF_RD_BASE_AT+offset_8b//base_offset_unit, SPI_CMD_NOP],
            chk=[expected_read_data],
            rif_chk={
                    'address': base_address + offset_8b, #get sure offset overflow are wrapped as in hw
                    'write':0, 'write_be':0,
                    'rdata':rif_rdata_16b,
                    'cycle':0 # on read cycle, the req is issued with command
                }
        )


    await spi_command(SPI_CMD_NOP) # NOP

    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done SPI RIF read test!")

# ----------------------------------------------------------------------------
