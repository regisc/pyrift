# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import Timer

# ----------------------------------------------------------------------------

async def delay_ns(delay):
    await Timer(delay, units='ns')

# ----------------------------------------------------------------------------
# command constants
SPI_CMD_CFG_RD_AT = 0x00 # address in 4 lsb
SPI_CMD_CFG_RD_NEXT = 0x10
SPI_CMD_CFG_WR_8_AT = 0x40  # address in 4 lsb
SPI_CMD_CFG_WR_16_AT = 0x50  # address in 4 lsb
SPI_CMD_CFG_WR_32_AT = 0x60  # address in 4 lsb
SPI_CMD_CFG_INC_BASE = 0x70  # inc/dec in 3 lsb
SPI_CMD_CFG_INC_LAST = 0x78  # inc/dec in 3 lsb

SPI_CMD_RIF_RD_BASE_AT = 0x80  # address in 5 lsb
SPI_CMD_RIF_RD_LAST_AT = 0xA0  # address in 4 lsb
SPI_CMD_RIF_RD_BASE_POSTINC = 0xB0
SPI_CMD_RIF_RD_BASE_POSTDEC = 0xB1
SPI_CMD_RIF_RD_LAST_POSTINC = 0xB4
SPI_CMD_RIF_RD_LAST_POSTDEC = 0xB5
SPI_CMD_RIF_RD_LAST_PREINC = 0xB6
SPI_CMD_RIF_RD_LAST_PREDEC = 0xB7
SPI_CMD_RIF_WR_BASE_AT = 0xC0  # address in 5 lsb
SPI_CMD_RIF_WR_LAST_AT = 0xE0  # address in 4 lsb
SPI_CMD_RIF_WR_BASE_POSTINC = 0xF0
SPI_CMD_RIF_WR_BASE_POSTDEC = 0xF1
SPI_CMD_RIF_WR_LAST_POSTINC = 0xF4
SPI_CMD_RIF_WR_LAST_POSTDEC = 0xF5
SPI_CMD_RIF_WR_LAST_PREINC = 0xF6
SPI_CMD_RIF_WR_LAST_PREDEC = 0xF7

SPI_CMD_NOP = 0xFF


