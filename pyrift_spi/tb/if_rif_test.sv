// ----------------------------------------------------------
// Code generated automatically with Pyrift v0.7-work-in-progress
// Incremental version id: 63
// Pyrift can be found at https://gitlab.com/regisc/pyrift
// Python platform: 3.6.1 (v3.6.1:69c0db5, Mar 21 2017, 18:41:36) [MSC v.1900 64 bit (AMD64)]
// System: Windows-10-10.0.19041-SP0
// Generate Date: 2020-07-11 11:45:23.966526
// ----------------------------------------------------------
// This code have been generated with pyrift tool (under GPL licence),
// but this generated file is not covered by pyrift licence.
// It is full property and/or responsability of the user of pyrift that generated this file.
// ----------------------------------------------------------
// Autogeneration may overwrite this file :
// You should probably avoid modifying this file directly
// ------------------------------------------------------
// no import of package without enum // import rif_spi_test_pkg::*; // get enum definitions

interface IF_RIF_TEST;
   //  [00000000]:rif_reg[0] in regmap: spi_test_regmap
      //  even_field
      logic [63:0][7:0] even_field;

      //  odd_field
      logic [63:0][7:0] odd_field;

   //  [00000000]:rif_reg
   // ----------------------------------
   //  [00000002]:rif_reg[1] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000004]:rif_reg[2] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000006]:rif_reg[3] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000008]:rif_reg[4] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000000A]:rif_reg[5] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000000C]:rif_reg[6] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000000E]:rif_reg[7] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000010]:rif_reg[8] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000012]:rif_reg[9] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000014]:rif_reg[10] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000016]:rif_reg[11] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000018]:rif_reg[12] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000001A]:rif_reg[13] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000001C]:rif_reg[14] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000001E]:rif_reg[15] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000020]:rif_reg[16] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000022]:rif_reg[17] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000024]:rif_reg[18] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000026]:rif_reg[19] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000028]:rif_reg[20] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000002A]:rif_reg[21] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000002C]:rif_reg[22] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000002E]:rif_reg[23] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000030]:rif_reg[24] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000032]:rif_reg[25] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000034]:rif_reg[26] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000036]:rif_reg[27] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000038]:rif_reg[28] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000003A]:rif_reg[29] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000003C]:rif_reg[30] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000003E]:rif_reg[31] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000040]:rif_reg[32] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000042]:rif_reg[33] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000044]:rif_reg[34] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000046]:rif_reg[35] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000048]:rif_reg[36] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000004A]:rif_reg[37] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000004C]:rif_reg[38] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000004E]:rif_reg[39] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000050]:rif_reg[40] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000052]:rif_reg[41] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000054]:rif_reg[42] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000056]:rif_reg[43] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000058]:rif_reg[44] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000005A]:rif_reg[45] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000005C]:rif_reg[46] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000005E]:rif_reg[47] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000060]:rif_reg[48] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000062]:rif_reg[49] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000064]:rif_reg[50] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000066]:rif_reg[51] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000068]:rif_reg[52] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000006A]:rif_reg[53] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000006C]:rif_reg[54] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000006E]:rif_reg[55] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000070]:rif_reg[56] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000072]:rif_reg[57] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000074]:rif_reg[58] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000076]:rif_reg[59] in regmap: spi_test_regmap
   // ----------------------------------
   //  [00000078]:rif_reg[60] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000007A]:rif_reg[61] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000007C]:rif_reg[62] in regmap: spi_test_regmap
   // ----------------------------------
   //  [0000007E]:rif_reg[63] in regmap: spi_test_regmap
   // ----------------------------------

   // ----------------------------------

   modport rif(
      output even_field,
      output odd_field
   );

   // ----------------------------------

   modport hw(
      input  even_field,
      input  odd_field
   );

   // ----------------------------------

endinterface // IF_RIF_TEST
