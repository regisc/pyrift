# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import RisingEdge, FallingEdge, NextTimeStep, Event
from cocotb.result import TestFailure

from tb_shared import * # want all constant defined without prefix needed
from tb_monitor import spi_rx_check_task
from mon_gbus import gbus_start_check_task

# ----------------------------------------------------------------------------

# spi clock period is 100 ns : 10 MHz
spi_low_period_ns = 60
spi_high_period_ns = 40

spi_byte_pre_wait_ns = 100
spi_byte_post_wait_ns = 100

# ----------------------------------------------------------------------------

async def spi_driver_setup(clk, cs, mosi, miso, miso_oe, drive_clock=True, ext_ref_clk=None):
    """
        Setup IO connection of spi driver, and mode onfiguration
        Pin connection are collected to global variables
        drive_clock allow to switch to passive clock
        (drive_clock==False => use clock from spi_clk provided externaly, just drive data and mosi during spi_command)
    """
    clk._log.info("Spi connecting driver")
    global spi_clk
    global spi_ext_ref_clk
    global spi_cs_n
    global spi_mosi
    global spi_miso
    global spi_miso_oe
    global driver_control_clock

    spi_clk = clk
    spi_ext_ref_clk = ext_ref_clk
    spi_cs_n = cs
    spi_mosi = mosi
    spi_miso = miso
    spi_miso_oe = miso_oe
    driver_control_clock = drive_clock

    spi_cs_n <= 0 # enable spi before reset
    if driver_control_clock:
        spi_clk <= 0 # default state

    spi_cs_n <= 1 # disable spi


# ----------------------------------------------------------------------------

async def spi_driver_set_csn(csn):
    spi_cs_n <= csn # enable/disable spi

# ----------------------------------------------------------------------------

async def spi_driver_completion():
    await delay_ns(100)
    spi_cs_n <= 1 # disable spi

# ----------------------------------------------------------------------------

async def spi_tx_byte(cmd):
    """
        Send byte to spi, as 8 clock period, can be a command or a data
        using polarity= 0 (clk is 0 when idle)
        using phase= 0
        Clk : ____/----\
        Data: X=========
    """
    spi_cs_n._log.info(f"spi_tx_byte: cmd={cmd:02X}")
    if driver_control_clock:
        await delay_ns(spi_byte_pre_wait_ns)
    for i in range(8):
        data = (cmd>>(7-i) ) & 1
        await delay_ns(1)
        spi_mosi <= data

        # spi_cs_n._log.info(f"D[{i}]={data} /")

        if driver_control_clock:
            await delay_ns(spi_low_period_ns-1)
            spi_clk <= 1 # rising edge where the slave is to catch data
        else:
            await RisingEdge(spi_ext_ref_clk)

        # spi_cs_n._log.info(f"D[{i}]={data}\\")

        if driver_control_clock:
            await delay_ns(spi_low_period_ns-1)
            spi_clk <= 0 # falling edge, idle: get back to 0
            await delay_ns(spi_high_period_ns)
        else:
            #await delay_ns(1)
            await NextTimeStep() # wait rising/falling back to back is deadlocking !?
            await FallingEdge(spi_ext_ref_clk)

        # spi_cs_n._log.info(f"D[{i}]={data} Done")

        # stop driving data with correct data
        spi_mosi <= 0 # return data to 0 (now unused by slave)

    if driver_control_clock:
        await delay_ns(spi_byte_post_wait_ns)

# ----------------------------------------------------------------------------

async def spi_command(cmd=0xFF, chk=None, rif_chk=None, msg=""):
    """
        Run a sequence of BYTE on SPI Tx
        Typicaly this is a command + wdata (or nop to wait for rx data)
        chk allow to specify rx data expected (sequence of read data starting after first command)
        rif_chk allow to monitor and check the RIF CDC interface have the expected transaction
    """
    # help to convert int to single item list (user have format choice for parameters)
    if type(cmd) != list:
        cmd = [cmd]
    if chk:
        # Chk is defined, not None, still convert to list if not yet
        if type(chk) != list:
            chk = [chk]
    if rif_chk:
        # Rif check is requested, this is expected to be a dictionary, cycle to be checked is selectable
        # If no precise cycle requested, assume first cycle
        if not rif_chk.get('cycle'):
            rif_chk['cycle'] = 0

    spi_clk._log.info(f"### ------------- Starting new Spi_command : cmd = {':'.join([f'{c:02X}' for c in cmd])} -------------------")
    for cycle_num, c in enumerate(cmd):
        if rif_chk and rif_chk['cycle']==cycle_num:
            # this is the cycle where the gbus transaction should happen at the end of current cycle
            # fork the check to calidate the matching
            gbus_start_check_fork_task = cocotb.fork(gbus_start_check_task(rif_chk, msg))

        await spi_tx_byte(c)
        if cycle_num==0: # have to launch checker just after the first of cmd tx is complete
            if chk: # may be none, and nothing to be checked
                cocotb.fork(spi_rx_check_task(chk, msg))
                # forked checker includes it own timeout, and will raise error if something get wrong

# ----------------------------------------------------------------------------
