# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb.py
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from tb_monitor import spi_monitor_report
from tb_monitor import spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report
from drv_reset import reset_driver_connect, reset_dut

# ----------------------------------------------------------------------------

async def check_cfg_32(addr, value, msg):
    """
        Frontdoor check CFG content
        Actual read cycle on CFG, checking result
    """
    await spi_command(
        cmd=[
            SPI_CMD_CFG_RD_AT+addr,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT,
            SPI_CMD_CFG_RD_NEXT], # Rd(addr, next, next, next, next)
        # check as little endian
        chk=[(value>>0) & 0xFF, (value>>8) & 0xFF, (value>>16) & 0xFF, (value>>24) & 0xFF], # Read 4B of CFG register
        msg=msg)

# ----------------------------------------------------------------------------

@cocotb.test()
async def test_spi_cfg_ptr_inc(dut):
    """Try inc/dec CFG last/base register in spi IP."""

    # get info about limited bitwidth for hw ressources
    # register may be less 32 bits for low gate count if not needed
    BASE_PTR_BW = dut.BASE_PTR_BW.value.integer
    LAST_PTR_BW = dut.LAST_PTR_BW.value.integer
    BASE_PTR_MASK = (1<<BASE_PTR_BW)-1;
    LAST_PTR_MASK = (1<<LAST_PTR_BW)-1;

    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_monitor_prepare(
        clk=dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_driver_set_csn(0)
    dut._log.info("Running SPI CFG ptr inc/dec test!")

    # CFG base/last assume to init at 0, base@=8 Last@=0xC
    await check_cfg_32(addr=8, value=0, msg="Reading Base")
    await check_cfg_32(addr=12, value=0, msg="Reading Last")
    await spi_command(SPI_CMD_NOP) # NOP
    await spi_command(SPI_CMD_NOP) # NOP

    # Check several increment and step for Base
    expected_base = 0
    expected_last = 0
    for iteration in range(10):
        # BASE
        base_inc_step = random.randint(1, 256)
        base_count = random.randint(1, 4)
        dir = random.choice([-1,+1])
        if dir>0:
            base_command = SPI_CMD_CFG_INC_BASE + base_count - 1
        else:
            base_command = SPI_CMD_CFG_INC_BASE + 8 + base_count*dir
        base_offset = dir*base_inc_step*base_count
        dut._log.info(f"Checking increment of Base by {base_offset} :"
                      f" inc_step={base_inc_step} * count={dir}*{base_count}, using command 0x{base_command:02X}")
        await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+2, base_inc_step-1] ) # inc_step Base [2] is written
        await spi_command(cmd=base_command) # inc/dec Base
        expected_base += base_offset
        # get sure offset overflow are wrapped as in hw
        # clip due to limited bitwidth of hw
        expected_base = expected_base & BASE_PTR_MASK

        # keep as unsigned 32b
        if expected_base < 0:
            expected_base = expected_base + 0x1_0000_0000
        # read back to check the base ptr is correct
        await check_cfg_32(addr=8, value=expected_base, msg="Checking Base")
        # - - - - - - - - - - - -
        # LAST
        last_inc_step = random.randint(1, 256)
        last_count = random.randint(1, 4)
        dir = random.choice([-1,+1])
        if dir>0:
            last_command = SPI_CMD_CFG_INC_LAST + last_count - 1
        else:
            last_command = SPI_CMD_CFG_INC_LAST + 8 + last_count*dir
        last_offset = dir*last_inc_step*last_count
        dut._log.info(f"Checking increment of Last by {last_offset} :"
                      f" inc_step={last_inc_step} * count={dir}*{last_count}, using command 0x{last_command:02X}")
        await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+3, last_inc_step-1] ) # inc_step Last [3] is written
        await spi_command(cmd=last_command) # inc/dec Last
        expected_last += last_offset
        # keep as unsigned 32b
        if expected_last < 0:
            expected_last = expected_last + 0x1_0000_0000

        # get sure offset overflow are wrapped as in hw
        # clip due to limited bitwidth of hw
        expected_last = expected_last & LAST_PTR_MASK

        # read back to check the last ptr is correct
        await check_cfg_32(addr=12, value=expected_last, msg="Checking Last")


    await spi_command(SPI_CMD_NOP) # NOP


    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done SPI CFG ptr inc/dec test!")

