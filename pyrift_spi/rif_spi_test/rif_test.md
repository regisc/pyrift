# spi_test_regmap, rif_spi_test
## [0x00000000: rif_reg\[0\]](#rif_reg)  
## [0x00000002: rif_reg\[1\]](#rif_reg)  
## [0x00000004: rif_reg\[2\]](#rif_reg)  
## [0x00000006: rif_reg\[3\]](#rif_reg)  
## [0x00000008: rif_reg\[4\]](#rif_reg)  
## [0x0000000a: rif_reg\[5\]](#rif_reg)  
## [0x0000000c: rif_reg\[6\]](#rif_reg)  
## [0x0000000e: rif_reg\[7\]](#rif_reg)  
## [0x00000010: rif_reg\[8\]](#rif_reg)  
## [0x00000012: rif_reg\[9\]](#rif_reg)  
## [0x00000014: rif_reg\[10\]](#rif_reg)  
## [0x00000016: rif_reg\[11\]](#rif_reg)  
## [0x00000018: rif_reg\[12\]](#rif_reg)  
## [0x0000001a: rif_reg\[13\]](#rif_reg)  
## [0x0000001c: rif_reg\[14\]](#rif_reg)  
## [0x0000001e: rif_reg\[15\]](#rif_reg)  
## [0x00000020: rif_reg\[16\]](#rif_reg)  
## [0x00000022: rif_reg\[17\]](#rif_reg)  
## [0x00000024: rif_reg\[18\]](#rif_reg)  
## [0x00000026: rif_reg\[19\]](#rif_reg)  
## [0x00000028: rif_reg\[20\]](#rif_reg)  
## [0x0000002a: rif_reg\[21\]](#rif_reg)  
## [0x0000002c: rif_reg\[22\]](#rif_reg)  
## [0x0000002e: rif_reg\[23\]](#rif_reg)  
## [0x00000030: rif_reg\[24\]](#rif_reg)  
## [0x00000032: rif_reg\[25\]](#rif_reg)  
## [0x00000034: rif_reg\[26\]](#rif_reg)  
## [0x00000036: rif_reg\[27\]](#rif_reg)  
## [0x00000038: rif_reg\[28\]](#rif_reg)  
## [0x0000003a: rif_reg\[29\]](#rif_reg)  
## [0x0000003c: rif_reg\[30\]](#rif_reg)  
## [0x0000003e: rif_reg\[31\]](#rif_reg)  
## [0x00000040: rif_reg\[32\]](#rif_reg)  
## [0x00000042: rif_reg\[33\]](#rif_reg)  
## [0x00000044: rif_reg\[34\]](#rif_reg)  
## [0x00000046: rif_reg\[35\]](#rif_reg)  
## [0x00000048: rif_reg\[36\]](#rif_reg)  
## [0x0000004a: rif_reg\[37\]](#rif_reg)  
## [0x0000004c: rif_reg\[38\]](#rif_reg)  
## [0x0000004e: rif_reg\[39\]](#rif_reg)  
## [0x00000050: rif_reg\[40\]](#rif_reg)  
## [0x00000052: rif_reg\[41\]](#rif_reg)  
## [0x00000054: rif_reg\[42\]](#rif_reg)  
## [0x00000056: rif_reg\[43\]](#rif_reg)  
## [0x00000058: rif_reg\[44\]](#rif_reg)  
## [0x0000005a: rif_reg\[45\]](#rif_reg)  
## [0x0000005c: rif_reg\[46\]](#rif_reg)  
## [0x0000005e: rif_reg\[47\]](#rif_reg)  
## [0x00000060: rif_reg\[48\]](#rif_reg)  
## [0x00000062: rif_reg\[49\]](#rif_reg)  
## [0x00000064: rif_reg\[50\]](#rif_reg)  
## [0x00000066: rif_reg\[51\]](#rif_reg)  
## [0x00000068: rif_reg\[52\]](#rif_reg)  
## [0x0000006a: rif_reg\[53\]](#rif_reg)  
## [0x0000006c: rif_reg\[54\]](#rif_reg)  
## [0x0000006e: rif_reg\[55\]](#rif_reg)  
## [0x00000070: rif_reg\[56\]](#rif_reg)  
## [0x00000072: rif_reg\[57\]](#rif_reg)  
## [0x00000074: rif_reg\[58\]](#rif_reg)  
## [0x00000076: rif_reg\[59\]](#rif_reg)  
## [0x00000078: rif_reg\[60\]](#rif_reg)  
## [0x0000007a: rif_reg\[61\]](#rif_reg)  
## [0x0000007c: rif_reg\[62\]](#rif_reg)  
## [0x0000007e: rif_reg\[63\]](#rif_reg)  

---
# Registers details:
## rif_reg
Register reset value: 0x00000100  
Reset valid mask: 0x0000FFFF  
Register array of reset value is : 0x100, 0x302, 0x504, 0x706, 0x908, 0xb0a, 0xd0c, 0xf0e, 0x1110, 0x1312, 0x1514, 0x1716, 0x1918, 0x1b1a, 0x1d1c, 0x1f1e, 0x2120, 0x2322, 0x2524, 0x2726, 0x2928, 0x2b2a, 0x2d2c, 0x2f2e, 0x3130, 0x3332, 0x3534, 0x3736, 0x3938, 0x3b3a, 0x3d3c, 0x3f3e, 0x4140, 0x4342, 0x4544, 0x4746, 0x4948, 0x4b4a, 0x4d4c, 0x4f4e, 0x5150, 0x5352, 0x5554, 0x5756, 0x5958, 0x5b5a, 0x5d5c, 0x5f5e, 0x6160, 0x6362, 0x6564, 0x6766, 0x6968, 0x6b6a, 0x6d6c, 0x6f6e, 0x7170, 0x7372, 0x7574, 0x7776, 0x7978, 0x7b7a, 0x7d7c, 0x7f7e  
Register description:  
SPI testing  
  
| Bit |Name | SW  | HW  |Reset|Store| Cdc | If  |Desc |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|7:0|even_field|RW|R|0x0|F.Flop||IF_RIF_TEST|Even address on [7:0]<br>Array have reset value table :<br>Reset : 0x0, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x5e, 0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 0x76, 0x78, 0x7a, 0x7c, 0x7e<br>|
|15:8|odd_field|RW|R|0x1|F.Flop||IF_RIF_TEST|Odd address on [15:8]<br>Array have reset value table :<br>Reset : 0x1, 0x3, 0x5, 0x7, 0x9, 0xb, 0xd, 0xf, 0x11, 0x13, 0x15, 0x17, 0x19, 0x1b, 0x1d, 0x1f, 0x21, 0x23, 0x25, 0x27, 0x29, 0x2b, 0x2d, 0x2f, 0x31, 0x33, 0x35, 0x37, 0x39, 0x3b, 0x3d, 0x3f, 0x41, 0x43, 0x45, 0x47, 0x49, 0x4b, 0x4d, 0x4f, 0x51, 0x53, 0x55, 0x57, 0x59, 0x5b, 0x5d, 0x5f, 0x61, 0x63, 0x65, 0x67, 0x69, 0x6b, 0x6d, 0x6f, 0x71, 0x73, 0x75, 0x77, 0x79, 0x7b, 0x7d, 0x7f<br>|
  

---
## Timestamp and pyrift version info:  
----------------------------------------------------------  
Code generated automatically with Pyrift v0.7-work-in-progress  
Incremental version id: 63  
Pyrift can be found at https://gitlab.com/regisc/pyrift  
Python platform: 3.6.1 (v3.6.1:69c0db5, Mar 21 2017, 18:41:36) [MSC v.1900 64 bit (AMD64)]  
System: Windows-10-10.0.19041-SP0  
Generate Date: 2020-07-11 11:45:23.964527  

----------------------------------------------------------  
This code have been generated with pyrift tool (under GPL licence),  
but this generated file is not covered by pyrift licence.  
It is full property and/or responsability of the user of pyrift that generated this file.  

----------------------------------------------------------
