# user customized to find pyrift directory
import sys;sys.path.append("../..")

from pyrift_registermap import *

# Switch to 16 bit.
PyriftBase.dw=16
Field.default_support_hwbe = True # BE needed

RIF_SIZE = 128

# Acknowledge the risk and unlock the GENERIC feature
PyriftBase.experimental = True

# ----------------------------------------------------------------------------
# testing the rif, just provide a set of 128 x 16 bit register mapped on 256 Byte
# register is basic R/W with known reset value

# R1d register
rif_reg = Register(name="rif_reg", desc="SPI testing", array=RIF_SIZE//2)
rif_reg.add(Field(name="even_field", bit_range='7:0', desc="Even address on [7:0]",
    reset = tuple(2*i for i in range(RIF_SIZE//2)),
    sw=Access.RW, hw=Access.R))
rif_reg.add(Field(name="odd_field", bit_range='15:8', desc="Odd address on [15:8]",
    reset = tuple(2*i+1 for i in range(RIF_SIZE//2)),
    sw=Access.RW, hw=Access.R))

# ----------------------------------------------------------------------------
spi_test_regmap = RegisterMap("spi_test_regmap")
spi_test_regmap.interface_name = "IF_RIF_TEST"
spi_test_regmap.module_name = "rif_spi_test"
spi_test_regmap.uvm_regmodel_classname = "test_regmodel"
spi_test_regmap.rif_host_bus_msb = 6 # 128B allocated to TEST, 7 bit address decode are significant
spi_test_regmap.base_address = 0x00

spi_test_regmap.rif_host_bus = "GENERIC"

spi_test_regmap.add(rif_reg)

spi_test_regmap.validate()

#spi_test_regmap.write_html("rif_test.html")
spi_test_regmap.write_markdown("./rif_test.md")
#spi_test_regmap.write_map_table("rif_test.map_table.txt")
#spi_test_regmap.write_svd("rif_test.svd")
spi_test_regmap.write_verilog_interface("../tb/if_rif_test.sv")
spi_test_regmap.write_verilog_module("../tb/rif_test.sv")
#spi_test_regmap.write_verilog_unwrap("rif_test_unwrap.sv")
#spi_test_regmap.write_verilog_package("../tb/rif_test_pkg.sv") # pkg for enum definitions
#spi_test_regmap.write_chead("rif_test.h", with_define_constant=True)
#spi_test_regmap.write_regmodel_uvm("test_regmodel.svh")
