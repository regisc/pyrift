# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb_loader.py
# Por reset then let the loader proceed to read flash and init RIF
# Check read of rif register through SPI as slave at the end to verify loader init is OK
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from tb_monitor import spi_monitor_report
from tb_monitor import spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report
from drv_reset import reset_driver_connect, reset_dut
from tb_flash_model import flash_model_start

# ----------------------------------------------------------------------------

async def test_loader_init(dut, loader_drive_clock_after_load=1):
    """
        Try Read/Write accessing the RIF register.
        Restrict to Base[offset] addressing mode
    """

    # Add rif expected content as array of byte
    rif_content = [i for i in range(128)]

    # loader is reset by por, then will start init
    dut.loader_rst_ni <= 1

    # flash content will launch random write
    test_wr_addr = random.randint(0,31) # 5 bit addressing available on base[offset]
    test_wr_data = random.randint(0,255) # data byte is full random
    dut._log.info(f"test will write Rif[0x{test_wr_addr:02X}={test_wr_addr}]=0x{test_wr_data:02X} from flash loader")

    # connect flash model
    dut.flash_model_do_i <= 0
    cocotb.fork(flash_model_start(
        clk_i=dut.flash_model_clk_o,
        cs_ni=dut.flash_model_cs_n_o,
        di_i=dut.flash_model_di_o,
        do_o=dut.flash_model_do_i,
        do_oe_o=dut.flash_model_do_oe_i,
        flash_content=[
            # Micro code for rif initialization
            0xFF, #NOP

            # config setup for byte access from base
            SPI_CMD_CFG_WR_8_AT+1, #
            0x00,                  # [1][1:0] = access width = 0 = 8bit, [1][2]=burst mode =0

            # base + Offset addressing to byte byte oriented (instead of 16b=2B unit)
            # This also check to CFG write access
            SPI_CMD_CFG_WR_8_AT+4, # base_offset_unit (0x0 for 1 unit)
            0x00, #

            #
            SPI_CMD_RIF_WR_BASE_AT+test_wr_addr, # Rif[test_wr_addr] = test_wr_data
            test_wr_data, #

            # load complete reported with CFG write [5]=0x01 (GPO [0]=1)
            # After load complete decide how to drive clock with GPO[1]
            SPI_CMD_CFG_WR_8_AT+0x5,    # Cfg[5] = 0x01+2*loader_drive_clock_after_load
            1+2*loader_drive_clock_after_load, # set data bit to loader complete HERE !

            0xFF # extra NOP
        ]
    ))

    # fork ref clock generation for the loader
    # have clock ref and host spi clock with different freq, so switch clock misconfig can be observed
    # 150 ns for ref clock, 100 ns for spi host
    dut.loader_ref_clk_i <= 0
    cocotb.fork(Clock(dut.loader_ref_clk_i, 100, 'ns').start())

    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o,
        drive_clock=not loader_drive_clock_after_load, # clock to be driven either by loader or host-spi
        ext_ref_clk=dut.flash_model_clk_o) # used if SPI not driving clock
    await spi_monitor_prepare(
        clk=dut.flash_model_clk_o if loader_drive_clock_after_load else dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await gbus_probe(dut)
    dut._log.info("Running SPI RIF read test!")

    await Timer(10, 'ns') # let get to t>0, ready for reset
    await RisingEdge(dut.por_rst_ni) # Sync on end of por reset
    dut._log.info("Seen out of reset")

    # loader procedure should now run, check rising of flash CS that should flag loader completion
    timeout = Timer(20, 'us') # 10+us of loader expection, catch deadlock with 20 us timeout
    result = await First(RisingEdge(dut.flash_model_cs_n_o),timeout)
    if result==timeout:
        raise TestFailure(f"flash loader procedure timeout")

    dut._log.info("Loader completed")

    # 10 unused clock delay
    await delay_ns(1000)

    dut._log.info(f"test will read back Rif[0x{test_wr_addr:02X}={test_wr_addr}]=0x{test_wr_data:02X} from flash loader")
    # Now start to control as SPI host
    await spi_driver_set_csn(0)

    # issue read back of random data written by loader (base unit should remain as set by loader)
    await spi_command(
        cmd=[SPI_CMD_RIF_RD_BASE_AT+test_wr_addr, SPI_CMD_NOP],
        chk=[test_wr_data],
        #rif_chk={
        #        'address': test_wr_addr, #get sure offset overflow are wrapped as in hw
        #        'write':0, 'write_be':0,
        #        'rdata':test_wr_data,
        #        'cycle':0 # on read cycle, the req is issued with command
        #    }
    )

    await spi_command(SPI_CMD_NOP) # NOP
    # clean close spi usage removing CS
    await spi_driver_set_csn(1)

    await delay_ns(10000)

    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done SPI RIF read test!")

# ----------------------------------------------------------------------------