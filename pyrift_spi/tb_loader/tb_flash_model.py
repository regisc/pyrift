# ----------------------------------------------------------------------------

import cocotb

from cocotb.triggers import RisingEdge, FallingEdge, NextTimeStep, First, Event
from cocotb.result import TestFailure


# ----------------------------------------------------------------------------

async def flash_model_start(clk_i, cs_ni, di_i, do_o, do_oe_o, flash_content):
    # start flash model with provided io
    # flash_content is array of byte content of flash starting at @=0
    # do_oe_o provide tri-state control (verilator simulate without Z : TODO check conflict with loader)

    # init assuming start from pu reset
    do_oe_o <= 0 # tri-state
    do_o <= 0
    cmd_bit_count = 0
    cmd = 0
    read_address = 0
    read_in_progress = False
    read_bit_countdown = 7
    # in reset state is cs_ni dependant
    cs_ni._log.info(f"flash model : PU reset done")

    # event for detection loop
    clk_rising_evt = RisingEdge(clk_i)
    clk_falling_evt = FallingEdge(clk_i)
    cs_falling_evt = FallingEdge(cs_ni)

    while True:
        result = await First(cs_falling_evt, clk_rising_evt, clk_falling_evt)
        if(cs_ni.value != 0):
            if result == cs_falling_evt:
                cs_ni._log.info(f"flash model : CS reset detected")
            # CS_n==1 : flash in reset
            do_oe_o <= 0 # tri-state
            do_o <= 0
            cmd_bit_count = 0 # restart command decoding
            cmd = 0
            read_address = 0
            read_in_progress = False
            read_bit_countdown = 7
        elif(result == clk_rising_evt):
            # clock rising edge detected out of reset
            #clk_i._log.info(f"flash model : Clk rising edge detected")

            # detect command
            if cmd_bit_count < 8:
                cmd = (cmd<<1) | (di_i.value&1) # cmd get MSB first, will complete after 8 bit
                cmd_bit_count = cmd_bit_count+1 # one more bit detected
                if cmd_bit_count==8:
                    if cmd!= 3:
                        raise TestFailure(f"flash model : unsupported command detected: 0x{cmd:02X}")
            elif cmd_bit_count < 32:
                # detect start address
                read_address = (read_address<<1) | (di_i.value&1) # address get MSB first, will complete after 8 bit
                cmd_bit_count = cmd_bit_count+1 # one more bit detected
                if cmd_bit_count==32:
                    cs_ni._log.info(f"flash model : Read command detected : @=0x{read_address:06X}")
                    read_in_progress = True
                    read_bit_countdown = 7
        elif(result == clk_falling_evt and read_in_progress):
            if read_address >= len(flash_content):
                raise TestFailure(f"flash model : Read beyond flash content: len={len(flash_content):04X}, read @={read_address:06X}")
            read_data = flash_content[read_address]
            # cs_ni._log.info(f"flash model : Read data={read_data:02X}: @=0x{read_address:06X}, bit={read_bit_countdown}")
            if read_bit_countdown==0:
                cs_ni._log.info(f"flash model: @=0x{read_address:02X} Read data={read_data:02X}")
            read_bit = (read_data>>read_bit_countdown) & 1
            do_oe_o <= 1 # driving output
            do_o <= read_bit
            if read_bit_countdown > 0:
                read_bit_countdown = read_bit_countdown-1
            else:
                read_bit_countdown = 7
                read_address += 1

        await NextTimeStep()

# ----------------------------------------------------------------------------
