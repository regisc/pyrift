// ----------------------------------------------------------------------------
// pyrift SPI host interface testing test bench
// This is the top level TB in verilog
// Instanciate :
//  - the dut : pyrift_spi_bridge
//  - the test rif : ...
//  - CDC interface between bridge and RIF : TBD
//
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_spi_tb_loader (
   input  var logic por_rst_ni         , // Async active low reset
   input  var logic spi_clk_i          , // SPI reference clock from master
   input  var logic loader_ref_clk_i   , // clock for loader sequencer
   //
   input  var logic loader_rst_ni      , // Async active low reset for loader block
                                          // 0: reset the loader, let the host act as SPI controller to bypass loader
   // Flash model IO
   output var logic flash_model_clk_o  , //
   output var logic flash_model_cs_n_o , //
   output var logic flash_model_di_o   , //
   input  var logic flash_model_do_i   , //
   input  var logic flash_model_do_oe_i, // OE for handling Z limitation of verilator : TODO check conflict on loader_di
   //
   input  var logic spi_cs_ni          , // SPI selection from master (active low)
   input  var logic spi_mosi_i         , // SPI Data from master
   output var logic spi_miso_o         , // SPI Data to master
   output var logic spi_miso_oe_o        // SPI Data output enable control to master
);

// ----------------------------------------------------------------------------

localparam DW =16;
localparam RIF_ADDR_BW /*verilator public_flat_rd*/ = 8;
localparam BASE_PTR_BW /*verilator public_flat_rd*/= 8;
localparam LAST_PTR_BW /*verilator public_flat_rd*/= 8;

// ----------------------------------------------------------------------------

logic [3:0] gpo; // GPO from spi bridge, GPO[0] is used as loader_complete
logic [3:0] gpi; // GPI to spi bridge, unused
assign gpi = 4'h5; // dummy value if to be tested

logic loader_flash_clk;
logic loader_flash_clk_oe;
logic loader_do;
logic loader_do_oe;
logic loader_di;

logic flash_cs_n;
logic host_cs_n;

// Forward external SPI master to be host SPI controller
assign host_cs_n = spi_cs_ni;
assign spi_miso_o = loader_do;
assign spi_miso_oe_o = loader_do_oe;
// loader DI is mux from spi host or flash
// From Status of loader (host shall not drive IO if flash is selected)
assign loader_di = flash_cs_n ? spi_mosi_i : flash_model_do_i;

logic spi_clk;

logic spi_cs_n;
logic spi_mosi;
logic spi_miso;
logic spi_miso_oe;

pyrift_flash_loader i_pyrift_flash_loader (
   .loader_clk_i                (spi_clk_i          ),
   .flash_clk_o                 (loader_flash_clk   ),
   .flash_clk_oe_o              (loader_flash_clk_oe),

   .loader_do_o                 (loader_do          ),
   .loader_do_oe_o              (loader_do_oe       ),
   .loader_di_i                 (loader_di          ),
   //
   .flash_cs_no                 (flash_cs_n         ),
   .host_cs_ni                  (host_cs_n          ),
   //
   .rif_spi_clk_o               (spi_clk            ),
   .rif_spi_cs_no               (spi_cs_n           ),
   .rif_spi_mosi_o              (spi_mosi           ),
   .rif_spi_miso_i              (spi_miso           ),
   .rif_spi_miso_oe_i           (spi_miso_oe        ),
   //
   .load_completed_i            (gpo[0]             ),
   .drive_ref_clock_after_load_i(gpo[1]             ),
   .loader_rst_ni               (loader_rst_ni      ),
   // Global reset and clock input
   .por_rst_ni                  (por_rst_ni         ),
   .ref_clk_i                   (loader_ref_clk_i   )
);

// ----------------------------------------------------------------------------

// connect loader IO to drive flash model
assign flash_model_di_o = loader_do_oe ? loader_do : 0;
assign flash_model_clk_o = loader_flash_clk_oe ? loader_flash_clk : 0;
assign flash_model_cs_n_o = flash_cs_n;

// ----------------------------------------------------------------------------

IF_GENERIC_BUS  #(
    .DW(DW)
) if_generic(spi_clk);

// --------------------

pyrift_spi_host #(
   .RIF_ADDR_BW(RIF_ADDR_BW),
   .BASE_PTR_BW(BASE_PTR_BW),
   .LAST_PTR_BW(LAST_PTR_BW),
   .CDC        (0          ),
   .BE         (0          ),
   .DW         (DW         )
) i_pyrift_spi_host (
   .por_rst_ni   (por_rst_ni ),
   //
   .spi_clk_i    (spi_clk    ),
   //
   .spi_cs_ni    (spi_cs_n   ),
   .spi_mosi_i   (spi_mosi   ),
   .spi_miso_o   (spi_miso   ),
   .spi_miso_oe_o(spi_miso_oe),
   // GPIO
   .gpo_o        (gpo        ),
   .gpi_i        (gpi        ),
   // No CDC, re-used spi clock for rif
   .rif_clk_i    (spi_clk    ),
   .if_generic   (if_generic )
);


// ----------------------------------------------------------------------------

IF_RIF_TEST if_rif_test();

rif_spi_test i_rif_spi_test (
   .clk        (spi_clk    ),
   .rst_n      (por_rst_ni ),
   //
   .if_rif_test(if_rif_test),
   .if_generic (if_generic )
);


// ----------------------------------------------------------------------------

// if_rif_test to connect to hw user design

// ----------------------------------------------------------------------------

endmodule // pyrift_spi_tb_loader
