# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is the top level TB in python using cocotb framework
# Basic SPI test agent, allow to lauch SPI command and check spi read is as expected
# This is using async for coroutine def of cocotb, this require advance python version  : 3.5 or 3.6
# and cocotb v1.4+
# ----------------------------------------------------------------------------



import cocotb
from cocotb.regression import TestFactory

import sys
# get access to shared python verification libraries
sys.path.append('../tb')

# ----------------------------------------------------------------------------

# import all test to be run in regression

from test_spi_rif_rw_loader_in_reset import test_spi_rif_rw_loader_in_reset
from test_loader_init import test_loader_init

# ----------------------------------------------------------------------------

factory = TestFactory(test_loader_init)
factory.add_option("loader_drive_clock_after_load", [0, 1])
factory.generate_tests()

# ----------------------------------------------------------------------------

