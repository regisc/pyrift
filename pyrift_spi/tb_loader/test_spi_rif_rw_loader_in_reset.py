# ----------------------------------------------------------------------------
# pyrift SPI host interface testing
# This is one test of regression : import by spi_tb_loader.py
# Keep loader in reset, then SPI master is accessing RIF
# Check read and write of rif register through SPI
# - 8b access : any address
# - 16b access : even aligned address
# Checks are based on read value checked vs expected
# RIF implement a rw set of 128 bytes with known reset value
# all write are tracked so the modified values are known for next read check
# exotic addressing mode and auto-inc/dec are not used (only basic base[offset] is used)
# Offset unit is kept as 1 (so 8b or 16b access can have byte granulosity)
# Offset is range [0..31], Base is 0x10, covered address range is 0x10 to 0x2F
# Address range in rif_test is 128B in range 0 to 0x7F
# ----------------------------------------------------------------------------

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, First, NextTimeStep, Event
from cocotb.result import TestFailure
import random as random

from tb_shared import * # want all constant defined without prefix needed
from tb_spi_driver import spi_driver_setup, spi_driver_set_csn, spi_driver_completion, spi_command
from tb_monitor import spi_monitor_report
from tb_monitor import spi_monitor_prepare
from mon_gbus import gbus_probe, gbus_start_check_task, gbus_monitor_report
from drv_reset import reset_driver_connect, reset_dut

# ----------------------------------------------------------------------------

@cocotb.test()
async def test_spi_rif_rw_loader_in_reset(dut):
    """
        Try Read/Write accessing the RIF register.
        Restrict to Base[offset] addressing mode
    """

    # Add rif expected content as array of byte
    rif_content = [i for i in range(128)]

    # force loader in reset
    dut.loader_rst_ni <= 0
    dut.loader_ref_clk_i <= 0

    await reset_driver_connect(rst=dut.por_rst_ni)
    await spi_driver_setup(
        clk=dut.spi_clk_i,
        cs=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_monitor_prepare(
        clk=dut.spi_clk_i,
        cs_n=dut.spi_cs_ni,
        mosi=dut.spi_mosi_i,
        miso=dut.spi_miso_o,
        miso_oe=dut.spi_miso_oe_o)
    await spi_driver_set_csn(0)
    await gbus_probe(dut)
    dut._log.info("Running SPI RIF read test!")

    # set base address to non default : B=0x10
    base_address = 0x0000_0010
    await spi_command(cmd=[SPI_CMD_CFG_WR_32_AT+0x8, 0x10, 0x00, 0x00, 0x00]) # Wr32(8) = 0x0000_0010
    # [4][7:4]: Last Offset unit 3b,
    # [4][3:0]: Base Offset unit 3b
    await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+4, (0<<4) | 0] )

    dut._log.info("-------------------")
    dut._log.info("Running SPI RIF r/w")
    dut._log.info("-------------------")
    for iteration in range(100):
        # Switch to 8 or 16 bit data cycle on rif, no burst mode
        cycle_is_16b = random.randint(0,1)
        await spi_command(cmd=[SPI_CMD_CFG_WR_8_AT+1, cycle_is_16b] ) # [1][1:0] = access width = 0 = 8bit, [1][2]=burst mode =0
        if cycle_is_16b:
            cycle_offset = 2*random.randint(0,15) # 0, 2, ... , 28, 30
            cycle_be = 0b11
            dut._log.info(f"Prepare 16b cycle at @=0x{base_address+cycle_offset:x}, offset=0x{cycle_offset:x}")
        else:
            cycle_offset = random.randint(0,31) # 0, 1, ... , 30, 31
            cycle_be = 1<<(cycle_offset&1)
            dut._log.info(f"Prepare 8b cycle at @=0x{base_address+cycle_offset:x}, offset=0x{cycle_offset:x}")

        # either read or write
        cycle_is_write = random.randint(0,1)
        if cycle_is_write:
            command = SPI_CMD_RIF_WR_BASE_AT
            dut._log.info("Prepare write cycle")
        else:
            command = SPI_CMD_RIF_RD_BASE_AT
            dut._log.info("Prepare read cycle")
        # randomize address
        command += cycle_offset

        if cycle_is_write:
            # write cycle
            read_chk = []
            if cycle_is_16b:
                wdata_byte_list = [random.randint(0,255), random.randint(0,255)]
                rif_wdata_16b = (wdata_byte_list[1]<<8) + wdata_byte_list[0]
            else:
                wdata_byte_list = [random.randint(0,255)]
                rif_wdata_16b = wdata_byte_list[0]<<(8*(cycle_offset&1))
        else:
            # read cycle
            cycle_be = 0
            rif_wdata_16b = 0
            if cycle_is_16b:
                read_chk = [rif_content[base_address+cycle_offset], rif_content[base_address+cycle_offset+1]]
                wdata_byte_list = [SPI_CMD_NOP, SPI_CMD_NOP] # n pipe, fill rdata cycles with NOP
            else:
                read_chk = [rif_content[base_address+cycle_offset]]
                wdata_byte_list = [SPI_CMD_NOP] # n pipe, fill rdata cycles with NOP
            # read addressing is 16b aligned (either for 8b or 16b read)
            rif_addr_16b = (base_address+cycle_offset) & (~1) # keep even align address
            rif_rdata_16b = (rif_content[rif_addr_16b+1]<<8) + rif_content[rif_addr_16b]

        rif_chk={
                'address': base_address+cycle_offset,
                'write':cycle_is_write, 'write_be':cycle_be,
                'wdata':rif_wdata_16b,
                'cycle':cycle_is_write*(cycle_is_16b+1) # on read cycle, the req is issued with command
                                                        # on write, after 1 or 2 byte of wdata
            }
        if not cycle_is_write:
            rif_chk['rdata'] = rif_rdata_16b

        await spi_command(
            cmd=[command] + wdata_byte_list,
            chk=read_chk,
            rif_chk=rif_chk
        )

        # SPI rif access now done, keep track of wdata in expected rif_content
        if cycle_is_write:
            rif_content[base_address+cycle_offset]   = wdata_byte_list[0]
            if cycle_is_16b:
                rif_content[base_address+cycle_offset+1] = wdata_byte_list[1]

    await spi_command(SPI_CMD_NOP) # NOP

    await spi_driver_completion()
    spi_monitor_report(dut)
    gbus_monitor_report(dut)

    dut._log.info("Done SPI RIF read test!")

# ----------------------------------------------------------------------------
