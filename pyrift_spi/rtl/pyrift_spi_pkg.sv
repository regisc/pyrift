// ----------------------------------------------------------------------------
// Pyrift library to implement spi generic bridge to CDC and pyrift core
// This file: pyrift_spi_pkg.sv is provided as addon of pyrift project
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// --
// Pyrift package to declare constant that may be useful for user design:
// ----------------------------------------------------------------------------


package pyrift_spi_pkg;

// ----------------------------------------------------------------------------

// 16 CFG register in SPI bridge
localparam SPI_CFG_ADDR_TEST   = 'h0; //
localparam SPI_CFG_ADDR_CONFIG = 'h1; //
localparam SPI_CFG_ADDR_BASE_INC_STEP = 'h2; //
localparam SPI_CFG_ADDR_LAST_INC_STEP = 'h3; //
localparam SPI_CFG_ADDR_OFFSET_UNIT = 'h4; //
localparam SPI_CFG_ADDR_GPIO = 'h5; //
localparam SPI_CFG_ADDR_RESERVED_6 = 'h6; //
localparam SPI_CFG_ADDR_RESERVED_7 = 'h7; //
localparam SPI_CFG_ADDR_BASE0  = 'h8; //
localparam SPI_CFG_ADDR_BASE1  = 'h9; //
localparam SPI_CFG_ADDR_BASE2  = 'hA; //
localparam SPI_CFG_ADDR_BASE3  = 'hB; //
localparam SPI_CFG_ADDR_LAST0  = 'hC; //
localparam SPI_CFG_ADDR_LAST1  = 'hD; //
localparam SPI_CFG_ADDR_LAST2  = 'hE; //
localparam SPI_CFG_ADDR_LAST3  = 'hF; //


localparam SPI_CMD_CFG_RD_AT    = 'h00; //  address in 4 lsb
localparam SPI_CMD_CFG_RD_NEXT  = 'h10; //
localparam SPI_CMD_CFG_WR_8_AT  = 'h40; //   address in 4 lsb
localparam SPI_CMD_CFG_WR_16_AT = 'h50; //   address in 4 lsb
localparam SPI_CMD_CFG_WR_32_AT = 'h60; //   address in 4 lsb
localparam SPI_CMD_CFG_INC_BASE = 'h70; //   inc/dec in 3 lsb
localparam SPI_CMD_CFG_INC_LAST = 'h78; //   inc/dec in 3 lsb

localparam SPI_CMD_RIF_RD_BASE_AT      = 'h80; //   address in 5 lsb
localparam SPI_CMD_RIF_RD_LAST_AT      = 'hA0; //   address in 4 lsb
localparam SPI_CMD_RIF_RD_BASE_POSTINC = 'hB0; //
localparam SPI_CMD_RIF_RD_BASE_POSTDEC = 'hB1; //
localparam SPI_CMD_RIF_RD_LAST_POSTINC = 'hB4; //
localparam SPI_CMD_RIF_RD_LAST_POSTDEC = 'hB5; //
localparam SPI_CMD_RIF_RD_LAST_PREINC  = 'hB6; //
localparam SPI_CMD_RIF_RD_LAST_PREDEC  = 'hB7; //
localparam SPI_CMD_RIF_WR_BASE_AT      = 'hC0; //   address in 5 lsb
localparam SPI_CMD_RIF_WR_LAST_AT      = 'hE0; //   address in 4 lsb
localparam SPI_CMD_RIF_WR_BASE_POSTINC = 'hF0; //
localparam SPI_CMD_RIF_WR_BASE_POSTDEC = 'hF1; //
localparam SPI_CMD_RIF_WR_LAST_POSTINC = 'hF4; //
localparam SPI_CMD_RIF_WR_LAST_POSTDEC = 'hF5; //
localparam SPI_CMD_RIF_WR_LAST_PREINC  = 'hF6; //
localparam SPI_CMD_RIF_WR_LAST_PREDEC  = 'hF7; //

localparam SPI_CMD_NOP = 'hFF; //

// ----------------------------------------------------------------------------

endpackage // pyrift_spi_pkg
