// ----------------------------------------------------------------------------
// Pyrift library to implement spi generic bridge to CDC and pyrift core
// This file: pyrift_spi_bridge.sv is provided as addon of pyrift project
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// --
// Pyrift library to implement pyrift_spi_host wrapper of:
// - pyrift_spi_bridge
// - CDC layer to rif core
//
// CDC allow either a null-CDC for the rif to use direct spi clock as main clock
// or a true CDC allowing the SPI clock to be fully async to rif core
//
// Parameter DW is to match the DW used in pyrift generated core of RIF
// Parameter CDC allow to select the CDC scheme between SPI and rif core
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_spi_host #(
   parameter RIF_ADDR_BW = 32, // bitwidth used for rif addressing : gbus_address_o[RIF_ADDR_BW-1:...]
   parameter BASE_PTR_BW = 32, // bitwidth used for base address pointer (0 if unused, to minize generated hw)
   parameter LAST_PTR_BW = 32, // bitwidth used for last address pointer (0 if unused, to minize generated hw)
   parameter CDC         = 0 , // 0 for null CDC, 1 for true async CDC
   parameter BE          = 0 , // big endian option for address in CFG
   parameter DW          = 16  // TODO: have 32, and use cocotb bench to configure ...
) (
   input  var logic       por_rst_ni   , // Async active low reset
   input  var logic       spi_clk_i    , // SPI reference clock from master
   input  var logic       spi_cs_ni    , // SPI selection from master (active low)
   input  var logic       spi_mosi_i   , // SPI Data from master
   output var logic       spi_miso_o   , // SPI Data to master
   output var logic       spi_miso_oe_o, // SPI Data output enable control to master
   // GPIO
   output var logic [3:0] gpo_o        , // Output GPIO control from spi
   input  var logic [3:0] gpi_i        , // Input GPIO control to spi
   //
   // GENERIC interface used to control pyrift RIF core (synchronous to rif clk)
   input  var logic       rif_clk_i    , // Rif clock (same as spi_clk_i if CDC==0)
   IF_GENERIC_BUS.master  if_generic
);

`ifndef SYNTHESIS
   initial begin
      // Get sure DW unsupported size is detected early
      if(DW!=8 && DW!=16 && DW!=32) begin
         $error("Unsupported DW=", DW);
      end
   end
`endif

// ----------------------------------------------------------------------------

// generic bus from bridge to CDC to RIF
logic [31:0] gbus_address;
logic gbus_rd;
logic gbus_rd_early;
logic gbus_wr;
logic [DW/8-1:0] gbus_be;
logic [DW-1:0] gbus_wdata;
logic [DW-1:0] gbus_rdata;
logic gbus_req;
logic gbus_ack;

pyrift_spi_bridge #(
   .RIF_ADDR_BW(RIF_ADDR_BW),
   .BASE_PTR_BW(BASE_PTR_BW),
   .LAST_PTR_BW(LAST_PTR_BW),
   .BE         (BE         ),
   .DW         (DW         )
) i_pyrift_spi_bridge (
   .por_rst_ni     (por_rst_ni   ),
   //
   .spi_clk_i      (spi_clk_i    ),
   .spi_cs_ni      (spi_cs_ni    ),
   .spi_mosi_i     (spi_mosi_i   ),
   .spi_miso_o     (spi_miso_o   ),
   .spi_miso_oe_o  (spi_miso_oe_o),
   //
   .gpo_o          (gpo_o        ),
   .gpi_i          (gpi_i        ),
   //
   .gbus_address_o (gbus_address ),
   .gbus_rd_o      (gbus_rd      ),
   .gbus_rd_early_o(gbus_rd_early),
   .gbus_wr_o      (gbus_wr      ),
   .gbus_be_o      (gbus_be      ),
   .gbus_wdata_o   (gbus_wdata   ),
   //
   .gbus_rdata_i   (gbus_rdata   ),
   //
   .gbus_req_o     (gbus_req     ),
   .gbus_ack_i     (gbus_ack     )
);


if(CDC==0) begin
   // generate without CDC : spi_clk_i used also for RIF core
   // ----------------------------------------------------------------------------

   // Generic bus from pyrift_spi_bridge is resync and connected to atual RIF
   // Here basic NULL CDC connection is in place
   // Could be usable if SPI clk is continously provided, and re-used as clock in RIF

   // No CDC, req/ack is just loopback
   assign gbus_ack = gbus_req;

   // if_generic connection (TODO: have CDC converter module to encapsulate)
   assign if_generic.gaddr     = gbus_address;
   assign if_generic.grd       = gbus_rd;
   assign if_generic.grd_early = gbus_rd_early;
   assign if_generic.gwr       = gbus_wr;
   assign if_generic.gbe       = gbus_be;
   assign if_generic.gwdata    = gbus_wdata;

   assign gbus_rdata = if_generic.grdata;
   // if_generic.gready is not yet supported in no CDC mode (use rif without ready !!!)

   // ----------------------------------------------------------------------------

end else begin
   // generate with CDC conversion
   initial begin
      $error("SPI CDC not yet supported");
   end
end

// ----------------------------------------------------------------------------

endmodule // pyrift_spi_host
