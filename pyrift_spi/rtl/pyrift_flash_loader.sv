// ----------------------------------------------------------------------------
// Pyrift library to implement spi host support
// This file: pyrift_flash_loader.sv is provided as addon of pyrift project
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// --
// Pyrift library to implement pyrift_flash_loader:
// This allow rif to autoconfigure by fetching values from flash memory as a SPI master
// After this RIF initialization, the RIF is a SPI slave accesible by main host
//
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_flash_loader (
   // clock IO to flash and host
   input  var logic loader_clk_i                , // Loader clock IO : from host
   output var logic flash_clk_o                 , // Loader clock IO : to flash or host
   output var logic flash_clk_oe_o              , // Loader clock IO : to flash or host
   // data IO
   output var logic loader_do_o                 , // Loader data output : to flash DI or host DI
   output var logic loader_do_oe_o              , // Loader data output : to flash DI or host DI
   input  var logic loader_di_i                 , // Loader data input : from flash DO or host DO
   // Selection signals
   output var logic flash_cs_no                 , // Flash select output
   input  var logic host_cs_ni                  , // Host selection input
   // RIF SPI (loader as master, rif as slave)
   output var logic rif_spi_clk_o               , // SPI reference clock to RIF (possibly to user design if no CDC)
   output var logic rif_spi_cs_no               , // SPI selection to RIF (active low)
   output var logic rif_spi_mosi_o              , // SPI Data to RIF
   input  var logic rif_spi_miso_i              , // SPI Data from RIF
   input  var logic rif_spi_miso_oe_i           , // SPI Data output enable control from RIF
   // Mode control
   input  var logic load_completed_i            , // RIF report completion of configuration by loader
   input  var logic drive_ref_clock_after_load_i, // RIF report mode for clock out after loader complete
   input  var logic loader_rst_ni               , // input to force loader as inactive (0: host can use slave SPI to control RIF)
   input  var logic por_rst_ni                  , // reset input to initialize loader (SPI not usable if activate to 0)
   input  var logic ref_clk_i                     // reference clock for loader clocking (used to drive loader clock output)
   //
);

// ----------------------------------------------------------------------------

logic mux_gated_clock_to_rif;
logic host_rif_cs;
logic loader_reading_rif_cs;
logic loader_cs;
logic [7:0]  tx_buffer;
logic loader_complete;
logic drive_ref_clock_after_load_complete;

always_comb begin : proc_loader_reset
   if(loader_rst_ni == 0) begin
      // Loader is in reset, HOST SPI is driving directly the RIF
      rif_spi_clk_o = loader_clk_i;
      rif_spi_cs_no = host_cs_ni;
      rif_spi_mosi_o = loader_di_i;

      loader_do_o = rif_spi_miso_i; // bypass loader : data RIF --> data host
      loader_do_oe_o = rif_spi_miso_oe_i;

      flash_clk_o = 0;
      flash_clk_oe_o = 0; // Loader in reset, don't drive flash clock (Z ==> let host decide to drive flash clk line)

      flash_cs_no = 1; // leave flash inactive
   end else begin
      // SPI RIF is controlled by loader

      // TBD, no loader yet
      rif_spi_clk_o = mux_gated_clock_to_rif;
      rif_spi_cs_no = !(loader_reading_rif_cs || host_rif_cs);
      rif_spi_mosi_o = loader_di_i;

      // Loader send flash cmd to initiate read, drive till load complete, then let RIF control the data
      loader_do_o    = (loader_cs) ? tx_buffer[7] : rif_spi_miso_i;
      loader_do_oe_o = (loader_cs) ? 1            : rif_spi_miso_oe_i; // drive : either from LOADER or RIF

      // TODO: flash_clk is a gating of ref_clk, may be have a true module of gating instanciated
      //       rather than comb muxing of clock
      // clock enabling control is changed on falling edge of clock, so rising edge should be clean, falling would have jitter
      flash_clk_o = ref_clk_i;
      flash_clk_oe_o = loader_cs || // Loader in progress drive flash clock with ref until loader complete
                       (loader_complete && drive_ref_clock_after_load_complete); // drive depend on user config after loader complete

      flash_cs_no = !loader_cs; // flash active as loading process progress
   end
end

// ----------------------------------------------------------------------------

// clock control to RIF
// - either ref_clk         ( [1] if loader_cs )
//                          ( [2] if ! loader_cs && ! host_cs_ni==0 &&   drive_ref_clock_after_load_i)
// - or the HOST spi clock  (     if ! loader_cs && ! host_cs_ni==0 && ! drive_ref_clock_after_load_i)
// In any case, it is gated to minimize glitch issues, and be remap on clock gating cell of current techno
logic ref_clock_gating_rif_ce;
logic host_clock_gating_rif_ce;

assign ref_clock_gating_rif_ce = loader_cs || (! loader_cs && ! host_cs_ni &&   drive_ref_clock_after_load_complete);
assign host_clock_gating_rif_ce =              ! loader_cs && ! host_cs_ni && ! drive_ref_clock_after_load_complete;

logic gated_ref_clock;
logic gated_host_clock;

pyrift_ck_gate i_pyrift_ck_gate_ref (
   .clk_i    (ref_clk_i              ),
   .ce_i     (ref_clock_gating_rif_ce),
   .test_en_i(1'b0                   ),
   .clk_o    (gated_ref_clock        )
);
pyrift_ck_gate i_pyrift_ck_gate_host (
   .clk_i    (loader_clk_i            ),
   .ce_i     (host_clock_gating_rif_ce),
   .test_en_i(1'b0                    ),
   .clk_o    (gated_host_clock        )
);

assign mux_gated_clock_to_rif = gated_ref_clock || gated_host_clock;

// ----------------------------------------------------------------------------

// FSM to control loader after POR
// launch read data to flash RD command 03, should be generic enough to work with any SPI flash
// (DI = 03/@@/@@/@@/zz/zz/zz/...)
//  DO = zz/zz/zz/zz/D0/D1/D2/...
// Run on ref_clk_i falling edge (so the flash can sample data in middle of period on rising edge)

typedef enum logic [1:0] {
   ST_LOADER_STARTING,
   ST_LOADER_CMD,
   ST_LOADER_READER,
   ST_LOADER_COMPLETE
} loader_te;
loader_te loader_state, loader_state_next;
logic [2:0] bit_countdown, bit_countdown_next; // count down 7 to 0 Tx
logic [7:0]  tx_buffer_next;
logic [1:0]  cmd_count, cmd_count_next;
logic drive_ref_clock_after_load_complete_next;
logic clk_en;
assign clk_en = 1;


assign host_rif_cs = ! loader_cs && ! host_cs_ni;

always_ff @(negedge ref_clk_i or negedge por_rst_ni) begin : proc_fsm
   if(~por_rst_ni) begin
      loader_state  <= ST_LOADER_STARTING;
      bit_countdown <= 0;
      cmd_count <= 0;
      loader_cs <= 0;
      loader_complete <= 0;
      loader_reading_rif_cs <= 0;
      drive_ref_clock_after_load_complete <= 0;
   end else if(clk_en) begin
      loader_state  <= loader_state_next;
      bit_countdown <= bit_countdown_next;
      cmd_count <= cmd_count_next;
      loader_cs <= (loader_state_next==ST_LOADER_CMD) || (loader_state_next==ST_LOADER_READER);
      loader_complete <= (loader_state_next==ST_LOADER_COMPLETE);
      loader_reading_rif_cs <= (loader_state_next==ST_LOADER_READER);
      drive_ref_clock_after_load_complete <= drive_ref_clock_after_load_complete_next;
   end
end

always_comb begin : proc_state_next
   loader_state_next  = loader_state;
   bit_countdown_next = 0;
   cmd_count_next = cmd_count;
   drive_ref_clock_after_load_complete_next = drive_ref_clock_after_load_complete;
   case (loader_state)
      ST_LOADER_STARTING : begin
         if(1) begin // automatic start loader out of reset
            loader_state_next  = ST_LOADER_CMD;
            bit_countdown_next = 7;
            cmd_count_next = 0;
         end
      end
      ST_LOADER_CMD : begin
         if(bit_countdown < 1) begin
            // exhaust tx buffer in this clock, consider pipe another byte
            if(cmd_count!=3) begin // another byte of command
               // restart another byte
               loader_state_next  = ST_LOADER_CMD;
               bit_countdown_next = 7;
               cmd_count_next = cmd_count + 1;
            end else begin
               // finish current byte
               loader_state_next  = ST_LOADER_READER;
               bit_countdown_next = 7;
               cmd_count_next = 0;
            end
         end else begin
            // consume record_bit_per_clock , stay in RUN, more clock to finish
            loader_state_next  = ST_LOADER_CMD;
            bit_countdown_next = bit_countdown - 1;
         end
      end
      ST_LOADER_READER : begin
         if(bit_countdown < 1) begin
            // exhaust tx buffer in this clock, consider pipe another byte
            // finish current byte
            loader_state_next  = (load_completed_i) ? ST_LOADER_COMPLETE : ST_LOADER_READER;
            drive_ref_clock_after_load_complete_next = (load_completed_i) ? drive_ref_clock_after_load_i
                                                                      : drive_ref_clock_after_load_complete;
            bit_countdown_next = 7;
            cmd_count_next = 0;
         end else begin
            // consume record_bit_per_clock , stay in RUN, more clock to finish
            loader_state_next  = ST_LOADER_READER;
            bit_countdown_next = bit_countdown - 1;
         end
      end
      ST_LOADER_COMPLETE : begin
         loader_state_next  = ST_LOADER_COMPLETE; // after load complete, nothing to do
      end
      default : /* default */;
   endcase
end// proc_state_next

always_ff @(negedge ref_clk_i or negedge por_rst_ni) begin : proc_buffer
   if(~por_rst_ni) begin
      tx_buffer <= 0;
   end else if(clk_en) begin
      if(bit_countdown==0) begin // new byte of data for flash command
         tx_buffer <= tx_buffer_next;
      end else if (loader_state == ST_LOADER_CMD) begin
         // tx running, shift lsb to msb (msb are just tx done)
         tx_buffer <= {tx_buffer[7-1:0],1'b0};
      end // end else if (loader_state == ST_LOADER_CMD)
   end // end else if(clk_en)
end// proc_buffer

always_comb begin : proc_tx_buffer
   case (loader_state)
      ST_LOADER_STARTING : begin
         // Prepare for read command
         tx_buffer_next = 'h03;
      end
      ST_LOADER_CMD : begin
         // after read command, provide 3 byte address
         case (cmd_count)
            0: tx_buffer_next = 'h00;
            1: tx_buffer_next = 'h00;
            2: tx_buffer_next = 'h00;
            default: // finished to read data
               tx_buffer_next = 'hFF;
         endcase
      end
      ST_LOADER_READER : begin
         tx_buffer_next = 'hFF;
      end
      default : begin/* default : as ST_LOADER_STARTING */
         // Prepare for read command
         tx_buffer_next = 'h03;
      end
   endcase
end

// ----------------------------------------------------------------------------

endmodule // pyrift_flash_loader
