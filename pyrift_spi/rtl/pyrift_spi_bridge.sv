// ----------------------------------------------------------------------------
// Pyrift library to implement spi generic bridge to CDC and pyrift core
// This file: pyrift_spi_bridge.sv is provided as addon of pyrift project
// There is no copyright requirement, and can be re-used as see fit by user
// Please use it "AS IS"
// This file is not covered by GPL license of pyrift project
// --
// Pyrift library to implement SPI bridge between:
// - SPI user interface
// - generic pyrift interface (a CDC levelis need to get to pyrift core)
// This is using SPI clock to sequence the access. SPI clock may stop when access is done
// SPI read access is done writing command to trig for read command on RIF
// SPI dummy access to clock to transaction, then
// SPI read to get the read data for RIF.
//
// Parameter DW is to match the DW used in pyrift generated core of RIF
// ----------------------------------------------------------------------------

`default_nettype none

module pyrift_spi_bridge #(
   parameter RIF_ADDR_BW = 32, // bitwidth used for rif addressing : gbus_address_o[RIF_ADDR_BW-1:...]
   parameter BASE_PTR_BW = 32, // bitwidth used for base address pointer (0 if unused, to minize generated hw)
   parameter LAST_PTR_BW = 32, // bitwidth used for last address pointer (0 if unused, to minize generated hw)
   parameter BE          = 0 , // big endian option for address in CFG
   parameter DW          = 16  // TODO: have 32, and use cocotb bench to configure ...
) (
   input  var logic            por_rst_ni     , // Async active low reset
   input  var logic            spi_clk_i      , // SPI reference clock from master
   input  var logic            spi_cs_ni      , // SPI selection from master (active low)
   input  var logic            spi_mosi_i     , // SPI Data from master
   output var logic            spi_miso_o     , // SPI Data to master
   output var logic            spi_miso_oe_o  , // SPI Data output enable control to master
   // GPIO
   output var logic [     3:0] gpo_o          , // Output GPIO control from spi
   input  var logic [     3:0] gpi_i          , // Input GPIO control to spi
   // Generic bus, unclocked
   output var logic [    31:0] gbus_address_o , // Address for pyrift cycle
   output var logic            gbus_rd_o      , // read qualifier for pyrift cycle
   output var logic            gbus_rd_early_o, // early read for pyrift cycle without CDC
   output var logic            gbus_wr_o      , // write qualifier for pyrift cycle
   output var logic [DW/8-1:0] gbus_be_o      , // write Byte enable qualifier for pyrift cycle
   output var logic [  DW-1:0] gbus_wdata_o   , // Write data for pyrift cycle
   //
   input  var logic [  DW-1:0] gbus_rdata_i   , // CDC read data from RIF transfer
   // CDC sequence
   output var logic            gbus_req_o     , // CDC transient 0-> 1 to request transfer
   input  var logic            gbus_ack_i       // CDC transient following request as an ACK of transfer
);

`ifndef SYNTHESIS
   initial begin
      // Get sure DW unsupported size is detected early
      if(DW!=8 && DW!=16 && DW!=32) begin
         $error("Unsupported DW=", DW);
      end
   end
`endif

localparam logic [31:0] RIF_ADDRESS_MASK = (1<<RIF_ADDR_BW)-1;
localparam logic [31:0] BASE_PTR_MASK = (1<<BASE_PTR_BW)-1;
localparam logic [31:0] LAST_PTR_MASK = (1<<LAST_PTR_BW)-1;

// ----------------------------------------------------------------------------

logic spi_cs_rst_n; // Reset done between transaction on spi : CS==1 reset logic to restart from scratch
                    // Control reset of fsm around spi transaction, get sure to restart in clean way before next transfer
                    // (special care to reset bit counter so a new CS restart align correctly on byte boundaries)

                    // all other logic is just reset on POR (including CFG register state, Rif recorded @, data, ...)

assign spi_cs_rst_n = por_rst_ni && !spi_cs_ni;

// ----------------------------------------------------------------------------

// register storage for CFG
// [ 0]: RW test register, reset as 0x12, Any value can be written without side effect
logic [7:0] cfg_reg0_test;
// [ 1]: [1:0]: Rif access Width 2b : 0=8b, 1=16b, 2=24b, 3=32b : usually setup to default to actual pyrift width connected
//         [2]: Burst mode : set to one activate automatic burst mode without new cmd, burst end with cs_n^
//       [7:3]: reserved
logic [1:0] cfg_reg1_b10_rif_access_width;
logic       cfg_reg1_b2_burst_mode;
// [ 2]: Base Inc step (1..256)
logic [7:0] cfg_reg2_base_inc_step;
// [ 3]: Last Inc step (1..256)
logic [7:0] cfg_reg3_last_inc_step;
// [ 4]: [7:4]: Last Offset unit 3b,
//              4 bit value in 0=1By, 1=2By,  2=4By,  3=8By, ..., 7=128By, 8..15: reserved
logic [3:0] cfg_reg4_b74_last_offset_unit;
//       [3:0]: Base Offset unit 3b
//              4 bit value in 0=1By, 1=2By,  2=4By,  3=8By, ..., 7=128By, 8..15: reserved
logic [3:0] cfg_reg4_b30_base_offset_unit;
// [ 5]: GPIO
//       [3:0]: Up to 4 GPO : gpo[0] is reserved for flash loader completion signal
//       [7:4]: Up to 4 GPI
logic [3:0] cfg_reg5_b30_gpo;
// [ 6]: PHY config : clock phase, ... TBD
// [ 7]: RIF reset control, clock gating, ...
//
// Base address is "Main" pointer to RIF address : ref of structure for offset addressing
// [ 8]: Base[31:24] // Big endian
// [ 9]: Base[23:16] // Big endian
// [10]: Base[15:8] // Big endian
// [11]: Base[7:0] // Big endian
// [ 8]: Base[7:0] // Little endian
// [ 9]: Base[15:8] // Little endian
// [10]: Base[23:16] // Little endian
// [11]: Base[31:24] // Little endian
logic [31:0] cfg_reg8_base_addr;
//
// Last address accessed pointer to rif address.
// Can be used as tmp pointer and incremental mode to browse registers
// (Main access through base pointer also set last pointer)
// [12]: Last[31:24] // Big endian
// [13]: Last[23:16]
// [14]: Last[15:8]
// [15]: Last[7:0]
// [12]: Last[7:0] // Little endian
// [13]: Last[15:8]
// [14]: Last[23:16]
// [15]: Last[31:24] // Little endian
logic [31:0] cfg_reg12_last_addr;

// ----------------------------------------------------------------------------

logic [DW-1:0] rif_write_data; // The write data from spi to pyrift core (get to CDC)
logic [DW-1:0] rif_read_data; // The read data from pyrift core to SPI (get from CDC), already barrel shifted for unaligned access

// ----------------------------------------------------------------------------

logic [7:0] master_data; // deserialization of data from master
logic [7:0] master_data_next; // deserialization of data from master
logic [2:0] master_data_bitcount; // count bit already deserialized
logic master_data_valid; // final bit of byte available for master_data
logic spi_byte_completing; // just before final bit of byte available for master_data

assign master_data_next = {master_data[6:0],spi_mosi_i};

always_ff @(posedge spi_clk_i or negedge spi_cs_rst_n) begin : proc_master_data
   if(~spi_cs_rst_n) begin
      master_data <= 0;
      master_data_bitcount <= 0;
      master_data_valid <= 0;
      spi_byte_completing <= 0;
   end else begin
      // serialization cames with MSB first
      master_data <= master_data_next;
      master_data_bitcount <= master_data_bitcount+1;
      master_data_valid <= master_data_bitcount==7;
      spi_byte_completing <= master_data_bitcount==6;
   end
end

// ----------------------------------------------------------------------------

// command decoding : comb from master data in progress, to be qualified by spi_byte_completing

logic decoded_rif_wr; // cmd followed by write data to rif
always_comb begin : proc_cmd_decode_rif_wr
   casez (master_data_next)
      8'b0???????: decoded_rif_wr = 0; // CFG access, no rif write data expected
      8'b10??????: decoded_rif_wr = 0; // RIF read, no rif write data expected
      8'b110?????: decoded_rif_wr = 1; // Rif Write at base[@]
      8'b1110????: decoded_rif_wr = 1; // Rif Write at last[@]
      8'b1111000?: decoded_rif_wr = 1; // Rif Write [B++],[B--]
      8'b111101??: decoded_rif_wr = 1; // Rif Write [L++],[L--], [++L],[--L]
      8'b11111111: decoded_rif_wr = 0; // NOP
      default    : decoded_rif_wr = 0; // default
   endcase
end

logic decoded_rif_rd; // cmd followed by write data to rif
always_comb begin : proc_cmd_decode_rif_rd
   casez (master_data_next)
      8'b0???????: decoded_rif_rd = 0; // CFG access, no rif read data expected
      8'b100?????: decoded_rif_rd = 1; // Rif Read at base[@]
      8'b1010????: decoded_rif_rd = 1; // Rif Read at last[@]
      8'b1011000?: decoded_rif_rd = 1; // Rif Read [B++],[B--]
      8'b101101??: decoded_rif_rd = 1; // Rif Read [L++],[L--], [++L],[--L]
      8'b11??????: decoded_rif_rd = 0; // RIF write, no rif read data expected
      default    : decoded_rif_rd = 0; // default
   endcase
end

// decode flavour of rif addressing, all decoded_* are to be qualified by (expect_command && spi_byte_completing)
logic decoded_rif_addr_use_base; // main selector to detect used address (from which pointer in cfg)
logic decoded_rif_addr_use_last; // main selector to detect used address (from which pointer in cfg)
always_comb begin : proc_cmd_decode_addr_src
   casez (master_data_next)
      8'b1?0?????: begin decoded_rif_addr_use_base = 1; decoded_rif_addr_use_last=0; end // Base + offset
      8'b1?10????: begin decoded_rif_addr_use_base = 0; decoded_rif_addr_use_last=1; end // Last + offset
      8'b1?11000?: begin decoded_rif_addr_use_base = 1; decoded_rif_addr_use_last=0; end // B++/B--
      8'b1?1101??: begin decoded_rif_addr_use_base = 0; decoded_rif_addr_use_last=1; end // L++/L--/++L/--L
      default    : begin decoded_rif_addr_use_base = 0; decoded_rif_addr_use_last=0; end
   endcase
end


logic decoded_rif_addr_use_postinc; // detect post_inc/post_dec addressing ode on either base or last pointer
logic decoded_rif_addr_use_postdec; // detect post_inc/post_dec addressing ode on either base or last pointer
always_comb begin : proc_cmd_decode_post_addressing
   casez (master_data_next)
      8'b1?110000: begin decoded_rif_addr_use_postinc = 1; decoded_rif_addr_use_postdec=0; end // [B++]
      8'b1?110001: begin decoded_rif_addr_use_postinc = 0; decoded_rif_addr_use_postdec=1; end // [B--]
      8'b1?110100: begin decoded_rif_addr_use_postinc = 1; decoded_rif_addr_use_postdec=0; end // [L++]
      8'b1?110101: begin decoded_rif_addr_use_postinc = 0; decoded_rif_addr_use_postdec=1; end // [L--]
      default    : begin decoded_rif_addr_use_postinc = 0; decoded_rif_addr_use_postdec=0; end
   endcase
end

logic decoded_cfg_wr; // cmd followed by write data to cfg
logic [1:0] decoded_cfg_wr_countdown; // cmd followed by write data to cfg
always_comb begin : proc_cmd_decode_cfg_wr
   casez (master_data_next)
      8'b00??????: begin decoded_cfg_wr = 0; decoded_cfg_wr_countdown=0; end// CFG read, no cfg write data expected
      8'b0100????: begin decoded_cfg_wr = 1; decoded_cfg_wr_countdown=0; end// CFG Write 8b
      8'b0101????: begin decoded_cfg_wr = 1; decoded_cfg_wr_countdown=1; end// CFG Write 16b
      8'b0110????: begin decoded_cfg_wr = 1; decoded_cfg_wr_countdown=3; end// CFG Write 32b
      8'b0111????: begin decoded_cfg_wr = 0; decoded_cfg_wr_countdown=0; end// CFG reg change, no cfg write data expected
      8'b1???????: begin decoded_cfg_wr = 0; decoded_cfg_wr_countdown=0; end// RIF access, no cfg write data expected
      default    : begin decoded_cfg_wr = 0; decoded_cfg_wr_countdown=0; end// default
   endcase
end

// Pre inc/dec handling, only for last
logic decoded_rif_addr_use_preinc;
logic decoded_rif_addr_use_predec;
assign decoded_rif_addr_use_preinc = {master_data_next[7], master_data_next[5:0]} == 7'b1110110;
assign decoded_rif_addr_use_predec = {master_data_next[7], master_data_next[5:0]} == 7'b1110111;
logic decoded_rif_addr_use_last_pre;
assign decoded_rif_addr_use_last_pre = decoded_rif_addr_use_preinc || decoded_rif_addr_use_predec;

logic decoded_cfg_base_ptr_inc; // cmd trig inc/dec of base pointer
logic decoded_cfg_last_ptr_inc; // cmd trig inc/dec of last pointer
assign decoded_cfg_base_ptr_inc = master_data_next[7:3] == 5'b01110;
assign decoded_cfg_last_ptr_inc = master_data_next[7:3] == 5'b01111;
// ptr inc/dec use last 3 bit to eval inc_dec amount in -4 to +4 (0 excluded)
logic [11:0] inc_dec_amount; // range -4*256 .. 4*256 or -1024..+1024 : need 12 bits [-2048,+2047]
logic [8:0] current_inc_step; // base or last for current cmd (add +1, with extra bit provided)
assign current_inc_step = (decoded_cfg_last_ptr_inc || decoded_rif_addr_use_last_pre || decoded_rif_addr_use_last)
   ? cfg_reg3_last_inc_step+1
   : cfg_reg2_base_inc_step+1;

always_comb begin : proc_inc_dec_amount
   if(decoded_cfg_base_ptr_inc || decoded_cfg_last_ptr_inc) begin
      // command is a cfg ptr inc/dec
      case (master_data_next[2:0])
         0: inc_dec_amount = 12'(current_inc_step);
         1: inc_dec_amount = 12'(2*current_inc_step);
         2: inc_dec_amount = 12'(3*current_inc_step);
         3: inc_dec_amount = 12'(4*current_inc_step);
         4: inc_dec_amount = 12'(4096-4*current_inc_step);
         5: inc_dec_amount = 12'(4096-3*current_inc_step);
         6: inc_dec_amount = 12'(4096-2*current_inc_step);
         7: inc_dec_amount = 12'(4096-1*current_inc_step);
         default: inc_dec_amount = 12'(current_inc_step); // not used
      endcase
   end else if (decoded_rif_addr_use_postinc || decoded_rif_addr_use_postdec) begin
      // Apply either to base, last
      inc_dec_amount = (decoded_rif_addr_use_postinc) ? 12'(current_inc_step)
                                                      : 12'(4096-1*current_inc_step);
   end else if (decoded_rif_addr_use_last_pre) begin
      // Apply only to last
      inc_dec_amount = (decoded_rif_addr_use_preinc) ? 12'(current_inc_step)
                                                     : 12'(4096-1*current_inc_step);
   end else begin
      inc_dec_amount = 12'(current_inc_step); // not used
   end

end


logic decoded_cfg_rd; // cmd followed by read data from cfg
logic decoded_cfg_rd_autoaddr; // cmd followed by read data from cfg (auto address)
assign decoded_cfg_rd = master_data_next[7:4] == 4'b0000;
assign decoded_cfg_rd_autoaddr = master_data_next[7:0] == 8'b00010000;


// ----------------------------------------------------------------------------
//     ######   #######  ##     ## ##     ##    ###    ##    ## ########
//    ##    ## ##     ## ###   ### ###   ###   ## ##   ###   ## ##     ##
//    ##       ##     ## #### #### #### ####  ##   ##  ####  ## ##     ##
//    ##       ##     ## ## ### ## ## ### ## ##     ## ## ## ## ##     ##
//    ##       ##     ## ##     ## ##     ## ######### ##  #### ##     ##
//    ##    ## ##     ## ##     ## ##     ## ##     ## ##   ### ##     ##
//     ######   #######  ##     ## ##     ## ##     ## ##    ## ########
// command state
// at each full byte received, change expected next byte schedule
// - expect command : no history, just wait new command byte to decide
// - expect W single byte D0 + back to command
// - expect Ww dual byte D1,D0 + back to command
// - expect Wwww quad byte D3,D2,D1,D0 + back to command
// - expect Wwww... burst mode (8/16/32) (reset by cs_n^)

logic expect_command; // 1 if command is expected to be parsed
logic expect_rif_wr; // 1 if following byte is to be a write data to rif
logic [1:0] expect_rif_wr_counter; // 0,1,2,3 counting as data write progressing
logic expect_rif_rd; // 1 if following byte is to be a read data from rif
logic launch_rif_rd_pulse; // 1 pulse if starting RIF word serialization (initial or burst)
logic [1:0] expect_rif_rd_counter; // 0,1,2,3 counting as data read progressing
logic terminate_byte_rd_pulse; // 1 pulse if CFG/RIF read is terminating now

logic expect_cfg_wr; // 1 if following byte is to be a write data to CFG
logic [1:0] expect_cfg_wr_countdown; // 3,2,1,0 countdown as data write progressing
logic [3:0] cfg_wr_address;
logic expect_cfg_rd; // 1 if following byte is to be a read data from CFG
logic launch_cfg_rd_pulse; // 1 pulse if starting CFG byte serialization
logic [3:0] cfg_rd_address;

always_ff @(posedge spi_clk_i or negedge spi_cs_rst_n) begin : proc_write_data_sequence
   if(~spi_cs_rst_n) begin
      expect_command <= 1;
      expect_rif_wr <= 0;
      expect_rif_wr_counter <= 0;
      expect_rif_rd <= 0;
      expect_rif_rd_counter <= 0;
      expect_cfg_wr <= 0;
      expect_cfg_wr_countdown <= 0;
      cfg_wr_address <= 0;
      expect_cfg_rd <= 0;
      cfg_rd_address <= 0;
      launch_cfg_rd_pulse <= 0;
      launch_rif_rd_pulse <= 0;
      terminate_byte_rd_pulse <= 0;
   end else begin
      // Pulse return to 0
      launch_cfg_rd_pulse <= 0;
      launch_rif_rd_pulse <= 0;
      terminate_byte_rd_pulse <= 0;
      // read processing, read completion, can be overloaded by new command
      if (expect_cfg_rd && spi_byte_completing) begin
         // Byte read return to command automatically
         expect_cfg_rd <= 0; // Schedule CFG Read is done
         terminate_byte_rd_pulse <= 1;
         // address i auto inc at each byte read
         cfg_rd_address <= cfg_rd_address+1;
      end
      // Command processing
      if(expect_command && spi_byte_completing) begin
         // command should have been parsed now, update write data schedule accordingly
         if(decoded_cfg_rd|| decoded_cfg_rd_autoaddr) begin // from parse cmd
            expect_cfg_rd <= 1;
            launch_cfg_rd_pulse <= 1;
            // expect_command<=1; remain one as new command can run in parallel to read
            if(decoded_cfg_rd) begin
               //CFG address is extracted from command byte
               cfg_rd_address <= master_data_next[3:0];
            end
            // if(decoded_cfg_rd_autoaddr) : No change of addr, this re-use the previous on ealready incremented
         end else if(decoded_cfg_wr) begin // from parse cmd
            expect_cfg_wr <= 1;
            expect_cfg_wr_countdown <= decoded_cfg_wr_countdown; // wr size come from decoded cmd
            cfg_wr_address <= master_data_next[3:0]; //CFG address is extracted from command byte
            expect_command <= 0;
         end else if(decoded_rif_wr) begin // from parse cmd
            expect_rif_wr <= 1;
            expect_rif_wr_counter <= 0; // Rif write is always by cfg data width
            expect_command <= 0;
         end else if(decoded_rif_rd) begin // from parse cmd
            expect_rif_rd <= 1;
            launch_rif_rd_pulse <= 1;
            expect_rif_rd_counter <= 0; // Rif write is always by cfg data width
            expect_command <= 1; // reading uses miso, so new command should be possible : /!\ no protection vs invalid command !!!
         end
      end else if (expect_rif_wr && spi_byte_completing) begin
         // on-going write, sequence data as scheduled
         if (expect_rif_wr_counter<cfg_reg1_b10_rif_access_width) begin
            expect_rif_wr_counter <= expect_rif_wr_counter + 1;
         end else begin
            // last byte, unless burst is on-going
            expect_rif_wr <= cfg_reg1_b2_burst_mode; // continue only in burst mode
            expect_command <= !cfg_reg1_b2_burst_mode;
            expect_rif_wr_counter <= 0; // complete, in burst mode need to restart at 0
         end
      end else if (expect_rif_rd && spi_byte_completing) begin
         // on-going write, sequence data as scheduled
         if (expect_rif_rd_counter<cfg_reg1_b10_rif_access_width) begin
            expect_rif_rd_counter <= expect_rif_rd_counter + 1;
         end else begin
            // last byte, unless burst is on-going
            expect_rif_rd <= cfg_reg1_b2_burst_mode; // continue only in burst mode
            launch_rif_rd_pulse <= cfg_reg1_b2_burst_mode;
            expect_command <= !cfg_reg1_b2_burst_mode;
            terminate_byte_rd_pulse <= !cfg_reg1_b2_burst_mode; // No burst, so completing read sequence now
            expect_rif_rd_counter <= 0; // complete, in burst mode need to restart at 0
         end
      end else if (expect_cfg_wr && spi_byte_completing) begin
         // on-going write, sequence data as scheduled
         if (expect_cfg_wr_countdown>0) begin
            expect_cfg_wr_countdown <= expect_cfg_wr_countdown - 1;
            cfg_wr_address <= cfg_wr_address+1; //CFG address is auto inc on each write
         end else begin
            // last byte, no burst supported on CFG
            expect_cfg_wr <= 0; // last write
            expect_command <= 1; // back to command after last write
            expect_cfg_wr_countdown <= 0; // left at 0 (not used)
         end
      end
   end
end

// ----------------------------------------------------------------------------
//    ########  #### ########       ###    ########  ########  ########
//    ##     ##  ##  ##            ## ##   ##     ## ##     ## ##     ##
//    ##     ##  ##  ##           ##   ##  ##     ## ##     ## ##     ##
//    ########   ##  ######      ##     ## ##     ## ##     ## ########
//    ##   ##    ##  ##          ######### ##     ## ##     ## ##   ##
//    ##    ##   ##  ##          ##     ## ##     ## ##     ## ##    ##
//    ##     ## #### ##          ##     ## ########  ########  ##     ##
// Find out the address to be used for RIF transaction
// This is using either Base or Last pointer from CFG register
// It can be modified with an offset from command or pre inc/dec mode
// (post inc/post dec are handled directly in pointer logic)


logic [31:0] selected_rif_address;
assign selected_rif_address = RIF_ADDRESS_MASK & ((decoded_rif_addr_use_last) ? cfg_reg12_last_addr : cfg_reg8_base_addr);
logic decoded_rif_is_write;
assign decoded_rif_is_write = master_data_next[6];


// Offset option
logic decoded_rif_addr_use_offset; // offset from either base or last
logic decoded_rif_addr_use_offset_base;
logic decoded_rif_addr_use_offset_last;
assign decoded_rif_addr_use_offset_base = {master_data_next[7], master_data_next[5]} == 2'b10;
assign decoded_rif_addr_use_offset_last = {master_data_next[7], master_data_next[5:4]} == 3'b110;
assign decoded_rif_addr_use_offset = decoded_rif_addr_use_offset_base || decoded_rif_addr_use_offset_last;
// Offset is either 5b unsigned or 4b signed : ==> unify to 6b signed,
// then have scaling shift by 0 to 7 bit ==> 13 bit final
logic signed [12:0] decoded_rif_addr_offset_signed;
always_comb begin : proc_addr_offset
   decoded_rif_addr_offset_signed = 0; // default unused
   if(decoded_rif_addr_use_offset_base) begin
      decoded_rif_addr_offset_signed = 13'(master_data_next[4:0]); // Unsigned
      // scaling by 0 to 7 bit
      decoded_rif_addr_offset_signed = decoded_rif_addr_offset_signed << cfg_reg4_b30_base_offset_unit[2:0];
   end
   if(decoded_rif_addr_use_offset_last) begin
      decoded_rif_addr_offset_signed = 13'($signed(master_data_next[3:0])); // Signed extended to 6 bits
      // scaling by 0 to 7 bit
      decoded_rif_addr_offset_signed = decoded_rif_addr_offset_signed << cfg_reg4_b74_last_offset_unit[2:0];
   end
end

// Pre inc/dec handling, only for last, either Rd or Wr
logic signed [9:0] addr_preinc_step_signed; // last pre inc/dec for current cmd (One more bit : 10 for sign)
assign addr_preinc_step_signed =
   (master_data_next[0]) ? ~10'(cfg_reg3_last_inc_step) // Pre dec : +1 and change sign done by 1'complement
                         : 10'(cfg_reg3_last_inc_step)+10'd1; // Pre inc : positive, +1 and just zero extend

// Now select address offset from base/last offset or from pre inc/dec
logic signed [12:0] final_rif_addr_offset_signed;
always_comb begin : proc_addr_final_offset
   final_rif_addr_offset_signed = 0; // default unused
   if(decoded_rif_addr_use_offset) begin
      final_rif_addr_offset_signed = decoded_rif_addr_offset_signed;
   end
   if(decoded_rif_addr_use_preinc || decoded_rif_addr_use_predec) begin
      final_rif_addr_offset_signed = 13'(addr_preinc_step_signed); // Signed extended 10 to 13 bits
   end
end

logic [31:0] rif_addr;
logic [31:0] rif_addr_next;
logic [31:0] gbus_address_recorded;
assign rif_addr_next = RIF_ADDRESS_MASK & (selected_rif_address + 32'(final_rif_addr_offset_signed)); // sign extend offset to 32b, then unsigned add
logic rif_write;
logic [DW/8-1:0] rif_write_be;
always_ff @(posedge spi_clk_i or negedge por_rst_ni) begin : proc_rif_addr
   if(~por_rst_ni) begin
      rif_addr <= 0;
      rif_write <= 0;
      rif_write_be <= 0;
   end else begin
      if(expect_command && spi_byte_completing) begin
         if(decoded_rif_addr_use_base || decoded_rif_addr_use_last) begin
            rif_addr <= RIF_ADDRESS_MASK & rif_addr_next;
         end
         rif_write <= decoded_rif_is_write;
         // Here DW is sure to be checked as 8, 16 or 32 and nothing else
         // If access is wider than DW, then activate all BE (probably multi cycle ahead)
         // If access is smaller than DW, then use address lsb to have subset of be
         // Access size is defined from cfg_reg1_b10_rif_access_width
         // TODO: Get sure the BE are consistent with the rif_write_data (to be check for strange misaligned write)
         if(DW==8) begin
            rif_write_be <= decoded_rif_is_write; // 8 bit data, single bit BE
         end else if(DW==16) begin
            // cfg_reg1_b10_rif_access_width >= 1 => all enable 'b11
            // cfg_reg1_b10_rif_access_width == 0 => be='b01 or 'b10 from addr lsb
            if(cfg_reg1_b10_rif_access_width==0) begin
               // 1 byte cycle on 16 bit bus : BE is either 'b01 or 'b10 according to @ lsb
               rif_write_be <= (DW/8)'(decoded_rif_is_write<<rif_addr_next[0]);
            end else begin
               // 2 byte cycle on 16 bit bus : BE is 'b11 on write
               rif_write_be <= {DW/8{decoded_rif_is_write}};
               // 32bit or unsupported 24 then alias to 16 accessb
            end
         end else if(DW==32) begin
            if(cfg_reg1_b10_rif_access_width==0) begin
               // cfg_reg1_b10_rif_access_width == 0 => be='b0001 or 'b0010 or 'b0100 or 'b1000 from addr lsb
               rif_write_be <= ((DW/8)'(decoded_rif_is_write))<<rif_addr_next[1:0];
            end else if(cfg_reg1_b10_rif_access_width==1) begin
               // 16bit : 11 get shifted from @[1] to 0011 or 1100
               rif_write_be <= (DW/8)'({2{decoded_rif_is_write}}<<(2*rif_addr_next[1]));
            end else begin
               // cfg_reg1_b10_rif_access_width > 1 => all enable 'b1111
               rif_write_be <= {DW/8{decoded_rif_is_write}};
            end
         end
      end
   end
end

// ----------------------------------------------------------------------------
//    ########  #### ########    ########     ###    ########    ###
//    ##     ##  ##  ##          ##     ##   ## ##      ##      ## ##
//    ##     ##  ##  ##          ##     ##  ##   ##     ##     ##   ##
//    ########   ##  ######      ##     ## ##     ##    ##    ##     ##
//    ##   ##    ##  ##          ##     ## #########    ##    #########
//    ##    ##   ##  ##          ##     ## ##     ##    ##    ##     ##
//    ##     ## #### ##          ########  ##     ##    ##    ##     ##
// rif_write_data setting, generate according to DW
// expect_rif_wr_counter identify inside the sequence of byte written (range [0..cfg_reg1_b10_rif_access_width])
// BYTE ID written is in range [0..DW/8-1], starting with address LSB if write is more narrow than DW
// DW=8 : start = 0
// DW=16: start = @[0]
// DW=32: start = @[1:0]
// written byte is at start+expect_rif_wr_counter, any value beyond DW/8-1 lead to ignored byte
logic [2:0] written_byte_start; // range 0..7 to handle out of range written filtering
assign written_byte_start = (DW==32) ? {1'b0, rif_addr[1:0]} :
                            (DW==16) ? {2'b0, rif_addr[0]}   :
                            3'b000;
logic [2:0] written_byte_id; // range 0..7 to handle out of range written filtering
assign written_byte_id = written_byte_start + 3'(expect_rif_wr_counter);

genvar BYTE_ID;
generate
   for (BYTE_ID = 0; BYTE_ID < DW/8; BYTE_ID++) begin
      always_ff @(posedge spi_clk_i or negedge por_rst_ni) begin : proc_rif_write_data
         if(~por_rst_ni) begin
            rif_write_data[BYTE_ID*8+7:BYTE_ID*8+0] <= 0;
         end else begin
            if (expect_rif_wr && spi_byte_completing) begin
               if (BYTE_ID<written_byte_start) begin
                  rif_write_data[BYTE_ID*8+7:BYTE_ID*8+0] <= 0;
               end else  if (written_byte_id==BYTE_ID) begin
                  rif_write_data[BYTE_ID*8+7:BYTE_ID*8+0] <= master_data_next;
               end else if(BYTE_ID>0 && BYTE_ID > cfg_reg1_b10_rif_access_width) begin
                  // write detected, but this byte is not inside the current CFG width used for rif, just clear unused
                  rif_write_data[BYTE_ID*8+7:BYTE_ID*8+0] <= 0;
               end
            end
         end
      end

   end
   // read data comes from gbus_rdata_i
   // byte shuffling for un-aligned access, ??? mean padding at 0
   // DW=8
   //    [0]  <- [0]
   // DW=16   @[0]=0  @[0]=1
   //    [0]  <- [0]  <- [1]
   //    [1]  <- [1]  <- ???
   // DW=32   @[1:0]=0  @[1:0]=1  @[1:0]=2  @[1:0]=3
   //    [0]  <- [0]    <- [1]    <- [2]    <- [3]
   //    [1]  <- [1]    <- [2]    <- [3]    <- ???
   //    [2]  <- [2]    <- [3]    <- ???    <- ???
   //    [3]  <- [3]    <- ???    <- ???    <- ???
   if(DW==8) begin
      assign rif_read_data[7:0] = gbus_rdata_i[7:0];
   end
   if(DW==16) begin
      assign rif_read_data[7:0]  =
         (rif_addr[0] == 1'b0)    ? gbus_rdata_i[7:0]   :
         (rif_addr[0] == 1'b1)    ? gbus_rdata_i[15:8]  : 8'h00;
      assign rif_read_data[15:8] =
         (rif_addr[0] == 1'b0)    ? gbus_rdata_i[15:8]  : 8'h00;
   end
   if(DW==32) begin
      assign rif_read_data[7:0]  =
         (rif_addr[1:0] == 2'b00)    ? gbus_rdata_i[7:0]   :
         (rif_addr[1:0] == 2'b01)    ? gbus_rdata_i[15:8]  :
         (rif_addr[1:0] == 2'b10)    ? gbus_rdata_i[23:16] :
         (rif_addr[1:0] == 2'b11)    ? gbus_rdata_i[31:24] : 8'h00;
      assign rif_read_data[15:8] =
         (rif_addr[1:0] == 2'b00)    ? gbus_rdata_i[15:8]  :
         (rif_addr[1:0] == 2'b01)    ? gbus_rdata_i[23:16] :
         (rif_addr[1:0] == 2'b10)    ? gbus_rdata_i[31:24] : 8'h00;
      assign rif_read_data[23:16] =
         (rif_addr[1:0] == 2'b00)    ? gbus_rdata_i[23:16] :
         (rif_addr[1:0] == 2'b01)    ? gbus_rdata_i[31:24] : 8'h00;
      assign rif_read_data[31:24] =
         (rif_addr[1:0] == 2'b00)    ? gbus_rdata_i[31:24] : 8'h00;
   end
endgenerate



// ----------------------------------------------------------------------------
//     ######  ########  ######      ########     ###    ########    ###
//    ##    ## ##       ##    ##     ##     ##   ## ##      ##      ## ##
//    ##       ##       ##           ##     ##  ##   ##     ##     ##   ##
//    ##       ######   ##   ####    ##     ## ##     ##    ##    ##     ##
//    ##       ##       ##    ##     ##     ## #########    ##    #########
//    ##    ## ##       ##    ##     ##     ## ##     ##    ##    ##     ##
//     ######  ##        ######      ########  ##     ##    ##    ##     ##
// cfg write_data handling
always_ff @(posedge spi_clk_i or negedge por_rst_ni) begin : proc_cfg_write_data
   if(~por_rst_ni) begin
      cfg_reg0_test <= 8'h12;
      cfg_reg1_b10_rif_access_width <= 2'(DW/8-1); // reset match the pyrift DW
      cfg_reg1_b2_burst_mode <= 0; // reset match the pyrift DW
      cfg_reg2_base_inc_step <= 8'(DW/8-1); // reset match the pyrift DW, 0,1,3, higher value up 255 for user special
      cfg_reg3_last_inc_step <= 8'(DW/8-1); // reset match the pyrift DW, 0,1,3, higher value up 255 for user special
      cfg_reg4_b74_last_offset_unit <= 4'($clog2(DW/8)); // 16b:1, 32b:2, ...
      cfg_reg4_b30_base_offset_unit <= 4'($clog2(DW/8)); // 16b:1, 32b:2, ...
      cfg_reg5_b30_gpo <= 4'b0000;
      cfg_reg8_base_addr <= 0;
      cfg_reg12_last_addr <= 0;
   end else begin
      if(expect_cfg_wr && spi_byte_completing) begin
         // When writing multibyte in CFG, just write one byte at a time,
         // cfg_wr_address is incremented as progressing
         if(cfg_wr_address == 0) begin
            cfg_reg0_test[7:0] <= master_data_next;
         end
         if(cfg_wr_address == 1) begin
            cfg_reg1_b10_rif_access_width[1:0] <= master_data_next[1:0];
            cfg_reg1_b2_burst_mode <= master_data_next[2];
         end
         if(cfg_wr_address == 2) begin
            cfg_reg2_base_inc_step[7:0] <= master_data_next;
         end
         if(cfg_wr_address == 3) begin
            cfg_reg3_last_inc_step[7:0] <= master_data_next;
         end
         if(cfg_wr_address == 4) begin
            cfg_reg4_b74_last_offset_unit[3:0] <= master_data_next[7:4];
            cfg_reg4_b30_base_offset_unit[3:0] <= master_data_next[3:0];
         end
         if(cfg_wr_address == 5) begin
            cfg_reg5_b30_gpo[3:0] <= master_data_next[3:0];
         end
         if(cfg_wr_address == (BE?11:8)) begin
            cfg_reg8_base_addr[7:0] <= BASE_PTR_MASK[7:0] & master_data_next;
         end
         if(cfg_wr_address == (BE?10:9)) begin
            cfg_reg8_base_addr[15:8] <= BASE_PTR_MASK[15:8] & master_data_next;
         end
         if(cfg_wr_address == (BE?9:10)) begin
            cfg_reg8_base_addr[23:16] <= BASE_PTR_MASK[23:16] & master_data_next;
         end
         if(cfg_wr_address == (BE?8:11)) begin
            cfg_reg8_base_addr[31:24] <= BASE_PTR_MASK[31:24] & master_data_next;
         end
         if(cfg_wr_address == (BE?15:12)) begin
            cfg_reg12_last_addr[7:0] <= LAST_PTR_MASK[7:0] & master_data_next;
         end
         if(cfg_wr_address == (BE?14:13)) begin
            cfg_reg12_last_addr[15:8] <= LAST_PTR_MASK[15:8] & master_data_next;
         end
         if(cfg_wr_address == (BE?13:14)) begin
            cfg_reg12_last_addr[23:16] <= LAST_PTR_MASK[23:16] & master_data_next;
         end
         if(cfg_wr_address == (BE?12:15)) begin
            cfg_reg12_last_addr[31:24] <= LAST_PTR_MASK[31:24] & master_data_next;
         end
      end else begin
         // Other way to modify register content
         // CFG : auto inc modes from cfg command on expect_command)
         if(expect_command && decoded_cfg_base_ptr_inc && spi_byte_completing) begin
            cfg_reg8_base_addr <= BASE_PTR_MASK & (cfg_reg8_base_addr + 32'($signed(inc_dec_amount)));
         end
         if(expect_command && decoded_cfg_last_ptr_inc && spi_byte_completing) begin
            cfg_reg12_last_addr <= LAST_PTR_MASK & (cfg_reg12_last_addr + 32'($signed(inc_dec_amount)));
         end
         // RIF : auto inc modes from rif access command : inc_dec_amount is shared with CFG auto inc logic
         if(expect_command && decoded_rif_addr_use_base && decoded_rif_addr_use_offset && spi_byte_completing) begin
            // B[@]
            cfg_reg12_last_addr <= LAST_PTR_MASK & rif_addr_next;
         end
         if(expect_command && decoded_rif_addr_use_last && decoded_rif_addr_use_offset && spi_byte_completing) begin
            // L[@]
            cfg_reg12_last_addr <= LAST_PTR_MASK & rif_addr_next;
         end
         if(expect_command && decoded_rif_addr_use_base && decoded_rif_addr_use_postinc && spi_byte_completing) begin
            // [B++]
            cfg_reg8_base_addr <= BASE_PTR_MASK & (cfg_reg8_base_addr + 32'($signed(inc_dec_amount)));
            cfg_reg12_last_addr <= LAST_PTR_MASK & rif_addr_next;
         end
         if(expect_command && decoded_rif_addr_use_base && decoded_rif_addr_use_postdec && spi_byte_completing) begin
            // [B--]
            cfg_reg8_base_addr <= BASE_PTR_MASK & (cfg_reg8_base_addr + 32'($signed(inc_dec_amount)));
            cfg_reg12_last_addr <= LAST_PTR_MASK & rif_addr_next;
         end
         if(expect_command && decoded_rif_addr_use_last && decoded_rif_addr_use_postinc && spi_byte_completing) begin
            // [L++]
            cfg_reg12_last_addr <= LAST_PTR_MASK & (cfg_reg12_last_addr + 32'($signed(inc_dec_amount)));
         end
         if(expect_command && decoded_rif_addr_use_last && decoded_rif_addr_use_postdec && spi_byte_completing) begin
            // [L--]
            cfg_reg12_last_addr <= LAST_PTR_MASK & (cfg_reg12_last_addr + 32'($signed(inc_dec_amount)));
         end
         if(expect_command && decoded_rif_addr_use_preinc && spi_byte_completing) begin
            // [++L]
            cfg_reg12_last_addr <= LAST_PTR_MASK & (cfg_reg12_last_addr + 32'($signed(inc_dec_amount)));
         end
         if(expect_command && decoded_rif_addr_use_predec && spi_byte_completing) begin
            // [--L]
            cfg_reg12_last_addr <= LAST_PTR_MASK & (cfg_reg12_last_addr + 32'($signed(inc_dec_amount)));
         end
      end
   end
end

// CFG read data handling
logic [7:0] cfg_rd_data;
always_comb begin : proc_cfg_rd_data
   if (expect_cfg_rd) begin
      case (cfg_rd_address)
         0:  cfg_rd_data = cfg_reg0_test;
         1:  cfg_rd_data = {5'd0, cfg_reg1_b2_burst_mode, cfg_reg1_b10_rif_access_width[1:0]};
         2:  cfg_rd_data = cfg_reg2_base_inc_step;
         3:  cfg_rd_data = cfg_reg3_last_inc_step;
         4:  cfg_rd_data = {cfg_reg4_b74_last_offset_unit[3:0], cfg_reg4_b30_base_offset_unit[3:0]};
         5:  cfg_rd_data = {gpi_i[3:0], cfg_reg5_b30_gpo[3:0]};
         8:  cfg_rd_data = BE ? cfg_reg8_base_addr[31:24] : cfg_reg8_base_addr[7:0];
         9:  cfg_rd_data = BE ? cfg_reg8_base_addr[23:16] : cfg_reg8_base_addr[15:8];
         10: cfg_rd_data = BE ? cfg_reg8_base_addr[15:8]  : cfg_reg8_base_addr[23:16];
         11: cfg_rd_data = BE ? cfg_reg8_base_addr[7:0]   : cfg_reg8_base_addr[31:24];
         12: cfg_rd_data = BE ? cfg_reg12_last_addr[31:24] : cfg_reg12_last_addr[7:0];
         13: cfg_rd_data = BE ? cfg_reg12_last_addr[23:16] : cfg_reg12_last_addr[15:8];
         14: cfg_rd_data = BE ? cfg_reg12_last_addr[15:8]  : cfg_reg12_last_addr[23:16];
         15: cfg_rd_data = BE ? cfg_reg12_last_addr[7:0]   : cfg_reg12_last_addr[31:24];
         default : cfg_rd_data = 0;
      endcase
   end else begin
      cfg_rd_data = 0;
   end
end

assign gpo_o = cfg_reg5_b30_gpo;

// ----------------------------------------------------------------------------


// gbus output generation
always_ff @(posedge spi_clk_i or negedge por_rst_ni) begin : proc_rif_access_req
   if(~por_rst_ni) begin
      gbus_req_o <= 0;
      gbus_address_recorded <= 0;
      gbus_wr_o <= 0;
      gbus_be_o <= 0;
      gbus_rd_o <= 0;
   end else begin
      if(expect_rif_wr && expect_rif_wr_counter==cfg_reg1_b10_rif_access_width && spi_byte_completing) begin
         // Completing write cycle with last data byte on SPI : launch RIF cycle now
         gbus_req_o <= 1;
         gbus_address_recorded <= RIF_ADDRESS_MASK & rif_addr_next;
         gbus_wr_o <= 1;
         gbus_be_o <= rif_write_be;
      end else if(expect_command && decoded_rif_rd && spi_byte_completing) begin
         gbus_req_o <= 1;
         gbus_rd_o <= 1;
      end else if (gbus_req_o && gbus_ack_i) begin
         // track ack to complete access from CDC
         gbus_req_o <= 0; // TODO CDC : so far just short pulse ...
         gbus_address_recorded <= 0;
         gbus_wr_o <= 0;
         gbus_be_o <= 0;
         gbus_rd_o <= 0;
      end
   end
end

// Write data unknow at command event. It is ready just on last edge of spi clock for last write data
assign gbus_wdata_o = rif_write_data;
assign gbus_rd_early_o = expect_command && decoded_rif_rd && spi_byte_completing;

// have preview of rif address for early read setup, then mux the recorded stable for full cycle
assign gbus_address_o = RIF_ADDRESS_MASK & ((gbus_rd_early_o) ? rif_addr_next : rif_addr);

// ----------------------------------------------------------------------------
//    ##    ## ########  ######      ######## ########   ######   ########
//    ###   ## ##       ##    ##     ##       ##     ## ##    ##  ##
//    ####  ## ##       ##           ##       ##     ## ##        ##
//    ## ## ## ######   ##   ####    ######   ##     ## ##   #### ######
//    ##  #### ##       ##    ##     ##       ##     ## ##    ##  ##
//    ##   ### ##       ##    ##     ##       ##     ## ##    ##  ##
//    ##    ## ########  ######      ######## ########   ######   ########
//
// Below this point is NEGEDGE synchronous logic
// Used to toggle slave data toward MISO at falling edge of SPI_CLK
//
// ----------------------------------------------------------------------------

// Following signal from posedge domain trig action on negedge :
// - launch_cfg_rd_pulse
// - launch_rif_rd_pulse
// - terminate_byte_rd_pulse

logic [7:0] spi_miso_byte [DW/8]; // when reading RIF, need DW bit of storage for serialization
logic       spi_miso_valid;
logic       spi_miso_bit;

always_ff @(negedge spi_clk_i or negedge spi_cs_rst_n) begin : proc_spi_miso_byte
   if(~spi_cs_rst_n) begin
      spi_miso_valid<= 0;
   end else begin
      if(terminate_byte_rd_pulse) begin
         spi_miso_valid <= 0; // byte completing, possibly will re-activate
      end
      if (launch_cfg_rd_pulse || launch_rif_rd_pulse) begin
         spi_miso_valid <= 1;
      end
   end
end
logic spi_miso_byte_input_carry_next [DW/8]; // carry input when shifting
                                             //  [0] enter byte[0] lsb comes from byte[1] msb
                                             //  [...]
                                             //  [DW/8-2] enter byte[DW/8-2] lsb  comes from byte[DW/8] msb
                                             //  [DW/8-1] enter byte[DW/8-1] lsb  comes from byte[DW/8] msb (! non existing) <= 0
generate
   for (BYTE_ID = 0; BYTE_ID < DW/8; BYTE_ID++) begin : gen_serialize_byte
      if(BYTE_ID < DW/8-1) begin
         assign spi_miso_byte_input_carry_next[BYTE_ID] = spi_miso_byte[BYTE_ID+1][7];
      end else begin
         // last byte in serialization, no carry input available
         assign spi_miso_byte_input_carry_next[BYTE_ID] = 0;
      end
      // Serialization byte : [1],[2],[4] for 8,16,32 bit DW
      always_ff @(negedge spi_clk_i or negedge spi_cs_rst_n) begin : proc_spi_miso_byte
         if(~spi_cs_rst_n) begin
            spi_miso_byte[BYTE_ID] <= 0;
         end else begin
            if (launch_cfg_rd_pulse) begin
               if(BYTE_ID == 0) begin
                  // On CFG read, only 8 bit is read a time, use only spi_miso_byte[0] when CFG read serialized
                  spi_miso_byte[BYTE_ID] <= cfg_rd_data;
               end else begin
                  spi_miso_byte[BYTE_ID] <= 0; // only 8 bit used for CFG
               end
            end else if (launch_rif_rd_pulse) begin
               // On RIF read, full DW bit are read a time
               spi_miso_byte[BYTE_ID] <= rif_read_data[8*BYTE_ID+7:8*BYTE_ID+0];
            end else if (spi_miso_valid) begin
               // shift while read is active, 1 bit at a time
               spi_miso_byte[BYTE_ID] <= {spi_miso_byte[BYTE_ID][6:0], spi_miso_byte_input_carry_next[BYTE_ID]};
            end
         end
      end
   end
endgenerate
assign spi_miso_bit = spi_miso_byte[0][7]; // First byte, MSB is first to serial output

// drive pad:
assign spi_miso_o = (spi_miso_valid)? spi_miso_bit: 0;
assign spi_miso_oe_o = spi_miso_valid;

// ----------------------------------------------------------------------------

endmodule // pyrift_spi_bridge
